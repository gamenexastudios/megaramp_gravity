#if !PlayServices_Off
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames.BasicApi;
using GooglePlayGames;
using UnityEngine.UI;
public class PlayServicesHandler : MonoBehaviour
{
    //	public Text signInButtonText,authStatus;
    private bool mAuthenticating = false;
    public static PlayServicesHandler instance;
    public List<int> UnlockAchievementsInLvls;
    private string[] AchievementIds = new string[10];
    public bool IsCheckedSignIn;
    void Awake()
    {
        instance = this;
        IsCheckedSignIn = false;
    }
    // Use this for initialization
    void Start()
    {
        //  ADD THIS CODE BETWEEN THESE COMMENTS

        // Create client configuration
        PlayGamesClientConfiguration config = new
            PlayGamesClientConfiguration.Builder()
            .Build();

        // Enable debugging output (recommended)
        PlayGamesPlatform.DebugLogEnabled = true;

        // Initialize and activate the platform
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        ((PlayGamesPlatform)Social.Active).SetDefaultLeaderboardForUI(GPGSIds.leaderboard_best_score);
        //		PlayGamesPlatform.Instance.Authenticate(SignInCallback, true);
        Debug.Log("---------- play services authenticated=" + (PlayGamesPlatform.Instance.localUser.authenticated));

        //AchievementIds[0] = GPGSIds.achievement_1;
        //AchievementIds[1] = GPGSIds.achievement_2;
        //AchievementIds[2] = GPGSIds.achievement_3;
        //AchievementIds[3] = GPGSIds.achievement_4;
        //AchievementIds[4] = GPGSIds.achievement_5;
        //AchievementIds[5] = GPGSIds.achievement_6;
        //AchievementIds[6] = GPGSIds.achievement_7;
        //AchievementIds[7] = GPGSIds.achievement_8;
        //AchievementIds[8] = GPGSIds.achievement_9;
        //AchievementIds[9] = GPGSIds.achievement_10;

    }

    public void CheckAutoSignIn()
    {
        Debug.Log("-------- Check Auto signIn IsCheckedSignin="+IsCheckedSignIn);
        if (IsCheckedSignIn)
        {
            return;
        }
        Debug.Log("----------------CheckAutoSignIn 111111");
        if (PlayerPrefs.GetString("IsGoogleAuthenticate", "false") == "false")
        {
            Debug.Log("----------------CheckAutoSignIn 222222222");
            SignIn();
        }
        else
        {
            Debug.Log("----------------CheckAutoSignIn 333333");
            Debug.Log("------------ Google Authenticating in background");
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, true);
        }
        IsCheckedSignIn = true;
    }

    public void SignIn()
    {
        //		Btm_GPGManager.AutoGoogleSignIn ();
        //		return;
        Debug.Log("----------- playservices 111111111");
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            Debug.Log("----------- playservices 222222222");
            // Sign in with Play Game Services, showing the consent dialog
            // by setting the second parameter to isSilent=false.
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, false);
        }
        //		else {
        //			PlayGamesPlatform.Instance.SignOut();
        //			signInButtonText.text = "Sign In";
        //			authStatus.text = "";
        //		}
    }
    public void SignInCallback(bool success)
    {
        if (success)
        {
            Debug.Log(" Signed in!");
            PlayerPrefs.SetString("IsGoogleAuthenticate", "true");

        }
        else
        {
            Debug.Log("(Lollygagger) Sign-in failed...");

        }
    }
    public void ShowLeaderBoards()
    {
        Debug.Log("ShowLeaderBoards called");
        //		Btm_GPGManager.showLeaderBoard ();
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            Debug.Log("ShowLeaderBoards UI called instance=" + (PlayGamesPlatform.Instance));

            PlayGamesPlatform.Instance.ShowLeaderboardUI();
        }
        else
        {
            SignIn();
            Debug.Log("Cannot show leaderboard: not authenticated");
        }
    }
    public void ShowAchievements()
    {
        Debug.Log("ShowAchievements called");
        //		Btm_GPGManager.showAchievements ();
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            Debug.Log("ShowAchievements UI called=" + (PlayGamesPlatform.Instance));
            PlayGamesPlatform.Instance.ShowAchievementsUI();

        }
        else
        {
            SignIn();
            Debug.Log("Cannot show Achievements, not logged in");
        }
    }
    //public void Check_UnlockAchievement(int LvlNo)
    //{
    //    Debug.Log("--------------- CheckUnlockAchievement 1111 lvlno=" + LvlNo);
    //    if (UnlockAchievementsInLvls.Contains(LvlNo))
    //    {
    //        int AchIndex = UnlockAchievementsInLvls.IndexOf(LvlNo);
    //        Debug.Log("--------------- CheckUnlockAchievement 22222222 indx=" + AchIndex);
    //        if (Social.localUser.authenticated)
    //        {
    //            PlayGamesPlatform.Instance.ReportProgress(
    //                AchievementIds[AchIndex],
    //                100.0f, (bool success) =>
    //                {
    //                    Debug.Log("Achievement unlocked" +
    //                        success);
    //                });
    //        }
    //    }
    //    //		if (Social.localUser.authenticated) {
    //    //			PlayGamesPlatform.Instance.ReportProgress (
    //    //				GPGSIds.achievement_1,
    //    //				100.0f, (bool success) => {
    //    //				Debug.Log ("Achievement unlocked" +
    //    //				success);
    //    //			});
    //    //		}
    //}
    public void Check_UnlockAchievement(string AchievemntType)
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ReportProgress(
                AchievemntType,
                100.0f, (bool success) =>
                {
                    Debug.Log("Achievement unlocked" +
                        success);
                });
        }
    }
    public void SubmitScoreToLB(int score)
    {
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            // Note: make sure to add 'using GooglePlayGames'
            PlayGamesPlatform.Instance.ReportScore(score,
                GPGSIds.leaderboard_best_score,
                (bool success) =>
                {
                    Debug.Log("Score submitted to LB" + success);
                });
        }
    }

    public void CheckAchivements()
    {
        if (StaticVariables._iSelectedLevel >= 10)
        {
            Check_UnlockAchievement(GPGSIds.achievement_crazy_drive);
        }

        if (StaticVariables._iSelectedLevel >= 30)
        {
            Check_UnlockAchievement(GPGSIds.achievement_freak_drive);
        }

        if (StaticVariables._iSelectedLevel >= 50)
        {
            Check_UnlockAchievement(GPGSIds.achievement_top_class_drive);
        }

    }

    public void OncallAchievement(string AID)
    {
#if UNITY_EDITOR
        if (Social.localUser.authenticated)
        {
            Social.ReportProgress(AID, 100, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update Score Success");

                }
                else
                {
                    Debug.Log("Update Score Fail");
                }
            });
        }
#endif
    }

}
#endif
