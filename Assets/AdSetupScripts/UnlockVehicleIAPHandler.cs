﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UnlockVehicleIAPHandler : MonoBehaviour
{

    public GameObject UnlockVehiclePanel;

    public GameObject PopUp;
    public GameObject CloseBtn;

    public Text CarNameText;
    public List<string> CarNames;

    public Image VehicleToDisplay;

    public List<Sprite> AllCarSprites;

    private int CarIndexToUnlock = 0;
    public bool Active;
    public bool ReadyToDisplay;

    public static UnlockVehicleIAPHandler instance;

    public void Awake()
    {
        Active = false;
        ReadyToDisplay = true;
        instance = this;
        UnlockVehiclePanel.SetActive(false);
    }

    public void Open()
    {
        CheckIndexandDisplayCarIcon();

        if (CarIndexToUnlock!=0)
        {
            iTween.Stop(PopUp);
            PopUp.transform.localScale = Vector3.one;
            iTween.ScaleFrom(PopUp, iTween.Hash("x", 0, "y", 0, "time", 0.25f, "delay", 0, "islocal", true, "easetype", iTween.EaseType.linear));

            iTween.Stop(CloseBtn);
            CloseBtn.transform.localScale = Vector3.one;
            iTween.ScaleFrom(CloseBtn, iTween.Hash("x", 0, "y", 0, "time", 0.25f, "delay", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));

            UnlockVehiclePanel.SetActive(true);
        }
    }
    public void Close()
    {
        ReadyToDisplay = false;
        UnlockVehiclePanel.SetActive(false);
    }

    public void CheckIndexandDisplayCarIcon()
    {
        CarIndexToUnlock = 0;
        char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
        for (int i = 0; i < tempstring.Length-1; i++)
        {
            if (tempstring[i] != '1')
            {
                VehicleToDisplay.sprite = AllCarSprites[i];
                CarIndexToUnlock = i;
                CarNameText.text = CarNames[i].ToUpper();
                break;
            }
        }
    }

    public void BuyCarWithIAP()
    {
        UpgradeVechicle.Instance.CurrentCarIndex = CarIndexToUnlock;
        UpgradeVechicle.Instance.UnLockCar();
        Close();
    }
}
