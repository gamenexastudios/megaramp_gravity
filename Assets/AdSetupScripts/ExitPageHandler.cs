﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitPageHandler : MonoBehaviour
{
    //public RawImage ExitAdImg_LandScape = null, ExitAdImg_Portrait = null;
    //public GameObject YesBtn, NoBtn;
    public static ExitPageHandler Instance;
    public GameObject ExitPage_LandScape, ExitPage_Portrait, ExitPage_NoAd;
    public bool IsExitPageOpened;
    public RawImage[] ExitAdImg_Landscape, ExitAdImg_Portrait;
    private void Awake()
    {
        Instance = this;
        ExitPage_LandScape.SetActive(false);
        ExitPage_Portrait.SetActive(false);
        ExitPage_NoAd.SetActive(false);
        IsExitPageOpened = false;
    }
    public bool IsExitAdsAvailable()
    {
        bool IsAvailable = false;
        for (int i = 0; i < AdManager.Instance.ExitAdTextureList.Count; i++)
        {
            if (AdManager.Instance.ExitAdTextureList[i]!=null)
            {
                IsAvailable = true;
                break;
            }
        }
            return IsAvailable;
    }
    public void Open()
    {

        ExitPage_LandScape.SetActive(false);
        ExitPage_Portrait.SetActive(false);
        ExitPage_NoAd.SetActive(false);

        if (AdManager.Instance.isWifi_OR_Data_Availble() && IsExitAdsAvailable())
        {
            if (AdManager.Instance.IsLandScapeGame)
            {
                //ExitAdImg_LandScape.texture = AdManager.Instance.ExitAdTexture;
                for (int i = 0; i < ExitAdImg_Landscape.Length; i++)
                {
                    ExitAdImg_Landscape[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < AdManager.Instance.ExitAdTextureList.Count; i++)
                {
                    if (AdManager.Instance.ExitAdTextureList[i] != null)
                    {
                        ExitAdImg_Landscape[i].gameObject.SetActive(true);
                        ExitAdImg_Landscape[i].texture = AdManager.Instance.ExitAdTextureList[i];
                    }
                }
                ExitPage_LandScape.SetActive(true);
            }
            else if(ExitAdImg_Portrait!=null)
            {
                //ExitAdImg_Portrait.texture = AdManager.Instance.ExitAdTexture;
                for(int i=0;i<ExitAdImg_Portrait.Length;i++)
                {
                    ExitAdImg_Portrait[i].gameObject.SetActive(false);
                }
                for (int i=0;i<AdManager.Instance.ExitAdTextureList.Count;i++)
                {
                    Debug.LogError("Exit 1111111="+i);
                    if(AdManager.Instance.ExitAdTextureList[i]!=null)
                    {
                        Debug.LogError("Exit 22222="+i);

                        ExitAdImg_Portrait[i].gameObject.SetActive(true);
                        ExitAdImg_Portrait[i].texture = AdManager.Instance.ExitAdTextureList[i];
                    }
                }
                ExitPage_Portrait.SetActive(true);
            }
            else
            {
                ExitPage_NoAd.SetActive(true);
            }


        }
        else
        {
            ExitPage_NoAd.SetActive(true);
        }
        AdManager.Instance.IsForceHideBannerAd = true;
        AdController.Instance.HideIronSourceBannerAd();
        IsExitPageOpened = true;
        gameObject.SetActive(true);

        if(InGameUIHandler.Instance!=null && GameManager.Instance.IsInPlayMode)
        {
            InGameUIHandler.Instance.RccCanvas.gameObject.SetActive(false);
            Time.timeScale = 0;
        }

    }
    public void Close()
    {
        if (InGameUIHandler.Instance != null && GameManager.Instance.IsInPlayMode)
        {
            InGameUIHandler.Instance.RccCanvas.gameObject.SetActive(true);
        }
        Time.timeScale = 1;
        AdManager.Instance.IsForceHideBannerAd = false;
        AdManager.Instance.CheckNShowBannerAd();
        
        IsExitPageOpened = false;

        ExitPage_LandScape.SetActive(false);
        ExitPage_Portrait.SetActive(false);
        ExitPage_NoAd.SetActive(false);

    }
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!IsExitPageOpened)
            {
                Open();
            }
            else
            {
                Close();
            }
        }
    }
    public void NoClick()
    {
        Close();
        IsExitPageOpened = false;
        ExitPage_LandScape.SetActive(false);
        ExitPage_Portrait.SetActive(false);
        ExitPage_NoAd.SetActive(false);
    }
    public void YesClick()
    {
        //Application.Quit ();
        using (AndroidJavaClass javaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        using (AndroidJavaObject javaActivity = javaClass.GetStatic<AndroidJavaObject>("currentActivity"))
        {
            javaActivity.Call<bool>("moveTaskToBack", true);
            javaActivity.Call("finish");
        }
    }
    public void ExitAdClick()
    {
        //Debug.Log("ExitAdLink=" + AdManager.Instance.ExitAdLinkTo);
        //market://details?id=com.milliongames.megarampstunts.cardriving.rampjumping.games
        //Application.OpenURL(AdManager.Instance.ExitAdLinkTo);
        Close();
    }
    public void ExitAdClick(int Index)
    {
        Application.OpenURL("market://details?id=" + AdManager.Instance.ExitAdLinkToList[Index]);
        FirebaseController.Instance.LogFirebaseEvent("ExitAd_Click", "", "");

        // Close();
    }
}
