﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MenuSample : MonoBehaviour
{
    public static MenuSample Instance;
    public Text CoinsTxt;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        //Debug.LogError("------ MenuSample Start");

        Open();
    }
    public void Open()
    {
        //Debug.LogError("------ MenuSample Open");
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.PageType.Menu);
        }
        gameObject.SetActive(true);
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void RequestAdmobInterstitial()
    {
        Debug.Log("RequestAdmobInterstitial");
        //AdController.Instance.RequestAdmobInterstitial();
    }
    public void ShowAdmobInterstitial()
    {
        Debug.Log("ShowAdmobInterstitial");
        //AdController.Instance.ShowAdmobInterstitial();
    }
    public void RequestAdmobRewarded()
    {
        Debug.Log("RequestAdmobRewarded");
        //AdController.Instance.RequestAdmobRewardedAd();
    }
    public void ShowAdmobRewarded()
    {
        Debug.Log("ShowAdmobRewarded");
        //AdController.Instance.ShowAdmobRewardedAd();
    }
    public void ShowUnityInterstitialAd()
    {
//        AdController.Instance.ShowUnityInterstitialAd();
    }
    public void ShowUnityRewardedAd()
    {
  //      AdController.Instance.ShowUnityRewardedVideo();
    }
    public void ValidateIronsource()
    {
        //AdController.Instance.ValidateIronSource();
    }
    public void RequestIronsourceInterstitial()
    {
        //AdController.Instance.RequestIronSourceInterstitialAd();
    }
    public void ShowIronsourceInterstitial()
    {
        //AdController.Instance.ShowIronSourceInterstitialAd();
    }
    public void ShowIronsourceRewarded()
    {
        //AdController.Instance.ShowIronSourceRewardedVideo();
    }
    public void ThrowUncaughtException()
    {
        Debug.Log("Causing a platform crash.");
        //FirebaseController.Instance.ThrowUncaughtException();
    }

    // Log a caught exception.
    public void LogCaughtException()
    {
        //FirebaseController.Instance.LogCaughtException();
    }
    public void PlayClick()
    {
        Close();
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.Actiontype.Menu_Play);
        }
        LevelSelectionSample.Instance.Open();
    }
    public void GetReward()
    {
        AdManager.Instance.ShowRewardVideoWithCallback((result) =>
        {
            if (result)
            {
                int coins = PlayerPrefs.GetInt("MyAdCoins", 100);
                coins += 1000;
                PlayerPrefs.SetInt("MyAdCoins", coins);
                MenuSample.Instance.CoinsTxt.text = "Coins:" + coins;
                Debug.Log("------ Menu Watched video successfully");
                //CoinsClaimPopHandler.instance.Open(1000, AdManager.RewardDescType.WatchVideo);
            }
        });
    }
    public void ShowLB()
    {
        AdManager.Instance.ShowLeaderBoards();
    }
    public void ShowAch()
    {
        AdManager.Instance.ShowAchievements();
    }
}
