﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpgradeSample : MonoBehaviour
{
    public static UpgradeSample Instance;
    void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }
    public void Open()
    {
        gameObject.SetActive(true);
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.PageType.Upgrade);
        }
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void SelectUpgrade()
    {
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.Actiontype.Upgrade_Play);
        }
        SceneManager.LoadScene("InGameSample");
    }
    public void BackClick()
    {
        Close();
        LevelSelectionSample.Instance.Open();

    }
   
}
