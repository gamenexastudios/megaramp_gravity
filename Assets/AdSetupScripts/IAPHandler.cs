﻿//using Firebase.Sample.Analytics;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;


// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.
//namespace CompleteProject
//{
// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IAPHandler : MonoBehaviour, IStoreListener
{
    public IStoreController m_StoreController;          // The Unity Purchasing system.
    public static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    // Product identifiers for all products capable of being purchased: 
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values 
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.
    public static string kProductIDSubscription = "subscription";

    // Apple App Store-specific product identifier for the subscription product.
    private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

    // Google Play Store-specific product identifier subscription product.
    private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

    public string[] ConsumableProducts;
    public string[] NonConsumableProducts;
    public string[] SubscriptionProducts;

    //public int UnlockAllLevelsIndex, LevelsDiscountIndex, UnlockAllUpgradesIndex, UpgradesDiscountIndex, UnlockFullGameIndex=-1;

    //		public string[] InAppIds_All;
    //		public string[] InappIds_Buy_OneTimeOnly;

    //		public static IAPHandler instance;

    public delegate void InAppSuccessCheck();
    public static event InAppSuccessCheck InAppSuccessCallBack;

    public delegate void InAppFailCheck();
    public static event InAppFailCheck InAppSFailCallBack;
    //	public static IAPHandler instance;
    private static IAPHandler _Instance;

    public static IAPHandler instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = GameObject.FindObjectOfType<IAPHandler>();
            }
            return _Instance;
        }
    }


    void Awake()
    {
        //			instance = this;
    }
    void Start()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

#if !AdSetup_OFF
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        //			builder.AddProduct("100_gold_coins", ProductType.Consumable, new IDs
        //				{
        //					{"100_gold_coins_google", GooglePlay.Name},
        //					{"100_gold_coins_mac", MacAppStore.Name}
        //				});
        //		builder.AddProduct("100_gold_coins", ProductType.Consumable);
        for (int i = 0; i < ConsumableProducts.Length; i++)
        {
            builder.AddProduct(ConsumableProducts[i], ProductType.Consumable);
        }
        for (int i = 0; i < NonConsumableProducts.Length; i++)
        {
            builder.AddProduct(NonConsumableProducts[i], ProductType.NonConsumable);
        }
        for (int i = 0; i < SubscriptionProducts.Length; i++)
        {
            builder.AddProduct(SubscriptionProducts[i], ProductType.Subscription);
        }
        UnityPurchasing.Initialize(this, builder);
#endif
    }
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        Debug.Log("---------- IAP Handler OnInitialized");
        Debug.Log("products=" + m_StoreController.products);

        RestorePurchases();
        for (int i = 0; i < ConsumableProducts.Length; i++)
        {
            Product product = m_StoreController.products.WithID(ConsumableProducts[i]);
            PlayerPrefs.SetString("consumableProducts_" + i, product.metadata.localizedPriceString);
            Debug.Log("price of product=" + (product.metadata.ToString()) + "===" + (product.metadata.localizedPrice + "::::" + (product.metadata.localizedPriceString)));
        }
        for (int i = 0; i < NonConsumableProducts.Length; i++)
        {
            Product product = m_StoreController.products.WithID(NonConsumableProducts[i]);

            PlayerPrefs.SetString("nonConsumableProducts_" + i, product.metadata.localizedPriceString);
            Debug.Log("price of product=" + (product.ToString()) + "===" + (product.metadata.localizedPrice + "::::" + (product.metadata.localizedPriceString)));
        }
        for (int i = 0; i < SubscriptionProducts.Length; i++)
        {
            Product product = m_StoreController.products.WithID(SubscriptionProducts[i]);

            PlayerPrefs.SetString("subscriptionProducts_" + i, product.metadata.localizedPriceString);
        }
    }
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
#if !AdSetup_OFF
        m_StoreExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(result => {
            if (result)
            {
                Debug.Log("--- OnInitialized RestoreProducts");
                
                for (int i = 0; i < SubscriptionProducts.Length; i++)
                {
                    Product product = m_StoreController.products.WithID(SubscriptionProducts[i]);
                    //Debug.Log("-- RestorePurchases  subscription product at i="+i+":::product="+product);
                    if (product != null && product.hasReceipt && product.definition.type == ProductType.Subscription)
                    {
                        SubscriptionManager p = new SubscriptionManager(product, null);

                        SubscriptionInfo info = p.getSubscriptionInfo();
                        Debug.Log("product id is: " + info.getProductId());
                        Debug.Log("purchase date is: " + info.getPurchaseDate());
                        Debug.Log("subscription next billing date is: " + info.getExpireDate());
                        Debug.Log("is subscribed? " + info.isSubscribed().ToString());
                        Debug.Log("is expired? " + info.isExpired().ToString());
                        Debug.Log("is cancelled? " + info.isCancelled());
                        Debug.Log("product is in free trial peroid? " + info.isFreeTrial());
                        Debug.Log("product getfreetrial period= " + info.getFreeTrialPeriod());

                        Debug.Log("product is auto renewing? " + info.isAutoRenewing());
                        Debug.Log("subscription remaining valid time until next billing date is: " + info.getRemainingTime());
                        Debug.Log("is this product in introductory price period? " + info.isIntroductoryPricePeriod());
                        Debug.Log("the product introductory localized price is: " + info.getIntroductoryPrice());
                        Debug.Log("the product introductory price period is: " + info.getIntroductoryPricePeriod());
                        Debug.Log("the number of product introductory price period cycles is: " + info.getIntroductoryPricePeriodCycles());
                        if (info.isExpired().ToString().ToLower() == "false")
                        {
                            
                            IAPCallbacksHandler.instance.InAppCallBacks(product.definition.id, true);
                            checkUnlockAllPurchase(product.definition.id);
                            //break;
                        }
                        else
                        {
                            IAPCallbacksHandler.instance.InAppCallBacks(product.definition.id, false);

                        }
                    }
                    else
                    {
                        //Debug.Log("the product is not a subscription product");
                    }
                }
                for (int i = 0; i < NonConsumableProducts.Length; i++)
                {
                    Product product = m_StoreController.products.WithID(NonConsumableProducts[i]);
                    if (product != null && product.hasReceipt)
                    {
                        Debug.Log("Restored id is=" + NonConsumableProducts[i]);
                        if (!PlayerPrefs.HasKey("IsRestored"))
                        {
                            //AdManager.instance.ShowToast("Restored successfully");//AdSetup Check
                            PlayerPrefs.SetString("IsRestored", "true");
                            IAPCallbacksHandler.instance.InAppCallBacks(product.definition.id, true);
                        }
                    }
                }
            }
            else
            {
                // Restoration failed.
            }

        });
#endif
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    /*public void BuyConsumable()
    {
        // Buy the consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(kProductIDConsumable);
    }


    public void BuyNonConsumable()
    {
        // Buy the non-consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(kProductIDNonConsumable);
    }*/


    public void BuySubscription()
    {
        // Buy the subscription product using its the general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        // Notice how we use the general product identifier in spite of this ID being mapped to
        // custom store-specific identifiers above.
        BuyProductID(kProductIDSubscription);
    }
    public void BuyProductID(string InAppID)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(InAppID);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(InAppID);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    string productId;
    private string BuyClickRerencename,TypeOfProduct;
    public void BuyProductID(int InAppIndex, bool IsConsumable, bool IsNonConsumable, bool IsSubscription, AdManager.InAppButtonReference Buttonreferencename=AdManager.InAppButtonReference.None)
    //public void BuyProductID(int InAppIndex, bool IsConsumable, bool IsNonConsumable, bool IsSubscription)
    {
        Debug.Log("--------- BuyProductID");
        //if(BuyItem.InAppButtonReference.MenuCoinpack1)
        // {

        // }
        BuyClickRerencename = Buttonreferencename.ToString();////AdSetup Check
        Debug.Log("--- before pass event");
        if (!PlayerPrefs.HasKey(BuyClickRerencename + "_Click"))
        {
            //StartCoroutine(AdManager.instance.PassEvent(BuyClickRerencename+"_Click"));
            PlayerPrefs.SetInt(BuyClickRerencename + "_Click", 1);
        }
        Debug.Log("--- after pass event");
        Debug.Log("--------- BuyProductID IsSubsciption=" + IsSubscription + "InAPpIndex=" + InAppIndex);
        if (IsConsumable)
        {
            productId = ConsumableProducts[InAppIndex];
            TypeOfProduct = "Consumable";
        }
        else if (IsNonConsumable)
        {
            productId = NonConsumableProducts[InAppIndex];
            TypeOfProduct = "NonConsumable";
        }
        else
        {
            Debug.Log("------- Purchase subscription product");
            productId = SubscriptionProducts[InAppIndex];
            TypeOfProduct = "Subscription";
        }
        Debug.Log("---------- BuyProductID productID=" + productId);

        //if (!IsNonConsumable)
        //{
        //    productId = ConsumableProducts[InAppIndex];
        //    TypeOfProduct = "Consumable";
        //}
        //else
        //{
        //    productId = NonConsumableProducts[InAppIndex];
        //    TypeOfProduct = "NonConsumable";
        //}
#if UNITY_ANDROID && !UNITY_EDITOR
			// If Purchasing has been initialized ...
			if (IsInitialized())
			{
				// ... look up the Product reference with the general product identifier and the Purchasing 
				// system's products collection.
				Product product = m_StoreController.products.WithID(productId);

				// If the look up found a product for this device's store and that product is ready to be sold ... 
				if (product != null && product.availableToPurchase)
				{
					Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
					// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
					// asynchronously.
					m_StoreController.InitiatePurchase(product);
				}
				// Otherwise ...
				else
				{
					// ... report the product look-up failure situation  
					Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
			
			}
			// Otherwise ...
			else
			{
				// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
				// retrying initiailization.
				Debug.Log("BuyProductID FAIL. Not initialized.");
			}
#endif
#if UNITY_EDITOR
        if (InAppSuccessCallBack != null)
            InAppSuccessCallBack();
        IAPCallbacksHandler.instance.InAppCallBacks(productId,true);
        checkUnlockAllPurchase(productId);
        if (!PlayerPrefs.HasKey(BuyClickRerencename + "_Success"))
        {
            //StartCoroutine(AdManager.instance.PassEvent(BuyClickRerencename + "_Success"));

            PlayerPrefs.SetInt(BuyClickRerencename + "_Success", 1);
        }

#endif
    }

    //	public void IsCheckNonConsumable(string InAppID)
    //	{
    //		Debug.Log ("------- IsCheckNonConsumable appID="+InAppID);
    //		bool found = false;
    //		for (int i = 0; i < InappIds_Buy_OneTimeOnly.Length; i++) {
    //			if (String.Equals (InappIds_Buy_OneTimeOnly[i], InAppID, StringComparison.Ordinal)) {
    //				found = true;
    //				break;
    //			}
    //		}
    //		Debug.Log ("----- found="+found+":::BuyBtn="+(ClickedBuyBtn));
    //		if (found && ClickedBuyBtn!=null) {
    //			ClickedBuyBtn.GetComponent<Button> ().interactable = false;
    //		}
    //		
    //	}
    //		public void IsCheckNonConsumable(string InAppID)
    //		{
    //			Debug.Log ("------- IsCheckNonConsumable appID="+InAppID);			
    //			Debug.Log ("----------- BuyBtn="+(ClickedBuyBtn));
    //			if (ClickedBuyBtn!=null) {
    //				ClickedBuyBtn.GetComponent<Button> ().interactable = false;
    //			}
    //		}
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log("------------------ process purchase");
        bool validPurchase = true; // Presume valid for platforms with no R.V.

        // Unity IAP's validation logic is only included on these platforms.
        /*#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
        // Prepare the validator with the secrets we prepared in the Editor
        // obfuscation window.
    CrossPlatformValidator validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
            AppleTangle.Data(), Application.bundleIdentifier);

        try {
            // On Google Play, result has a single product ID.
            // On Apple stores, receipts contain multiple products.
            var result = validator.Validate(args.purchasedProduct.receipt);
            // For informational purposes, we list the receipt(s)
            Debug.Log("Receipt is valid. Contents:");
            foreach (IPurchaseReceipt productReceipt in result) {
//					Debug.Log(productReceipt.productID);
//					Debug.Log(productReceipt.purchaseDate);
//					Debug.Log(productReceipt.transactionID);
            }
        } catch (IAPSecurityException) {
            Debug.Log("Invalid receipt, not unlocking content");
            validPurchase = false;
        }
#endif
        */

        if (InAppSuccessCallBack != null)
        {
            InAppSuccessCallBack();
        }

        if (args.purchasedProduct.hasReceipt)
        {
            Debug.Log("Process purchase result=" + args.purchasedProduct.receipt);
        }
        else
        {
            Debug.Log("---- product dont have receipt");
        }


        IAPCallbacksHandler.instance.InAppCallBacks(args.purchasedProduct.definition.id, true);
        checkUnlockAllPurchase(args.purchasedProduct.definition.id);
        FirebaseController.Instance.LogFirebaseEvent("IAP_"+ args.purchasedProduct.definition.id, "Success", args.purchasedProduct.definition.id);

#if (UNITY_ANDROID)
        //GameAnalyticsController.Instance.SetGameAnalyticsInAppEvent(args.purchasedProduct, TypeOfProduct, BuyClickRerencename);
        //Firebasesetup.Instance.SetInapppurchaseevent(args.purchasedProduct.definition.id, TypeOfProduct, price, currency, BuyClickRerencename);
        if (!PlayerPrefs.HasKey(BuyClickRerencename + "_Success"))
        {
            //StartCoroutine(AdManager.instance.PassEvent(BuyClickRerencename + "_Success"));

            PlayerPrefs.SetInt(BuyClickRerencename + "_Success", 1);
        }

        //Firebasesetup.Instance.SetGameAnalyticsInAppEvent(args.purchasedProduct, TypeOfProduct, BuyClickRerencename);
        //FlurryHandler.Instance.SetGameAnalyticsInAppEvent(args.purchasedProduct, TypeOfProduct, BuyClickRerencename);

#endif
        //			if (String.Equals (args.purchasedProduct.definition.id,consumableProducts[0], StringComparison.Ordinal)) {
        //				Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //			}
        //			else if (String.Equals (args.purchasedProduct.definition.id, consumableProducts[1], StringComparison.Ordinal)) {
        //				Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //			}
        //			else if (String.Equals (args.purchasedProduct.definition.id, nonConsumableProducts[0], StringComparison.Ordinal)) {
        //				Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //				IsCheckNonConsumable (nonConsumableProducts [0]);
        //			}
        //			else if (String.Equals (args.purchasedProduct.definition.id, nonConsumableProducts[1], StringComparison.Ordinal)) {
        //				Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //				IsCheckNonConsumable (nonConsumableProducts [1]);
        //			}
        //			else 
        //			{
        //				InAppSFailCallBack ();
        //				Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        //			}

        //			// A consumable product has been purchased by this user.
        //			if (String.Equals (args.purchasedProduct.definition.id, consumableProducts[0], StringComparison.Ordinal)) {
        //				Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //			}
        //			else if (String.Equals (args.purchasedProduct.definition.id, consumableProducts[1], StringComparison.Ordinal)) {
        //				Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //			}
        //			else if (String.Equals (args.purchasedProduct.definition.id, nonConsumableProducts[0], StringComparison.Ordinal)) {
        //				Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //			}
        //			else if (String.Equals (args.purchasedProduct.definition.id, nonConsumableProducts[1], StringComparison.Ordinal)) {
        //				Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //			}
        //			
        //			// Or ... a subscription product has been purchased by this user.
        //			 else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
        //			{
        //				Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //				// TODO: The subscription item has been successfully purchased, grant this to the player.
        //			}


        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }

    //		public void OnPurchaseSucceeded(string storeSpecificId, string receipt, string transactionIdentifier)
    //		{
    //			Debug.Log ("------------ OnPurchaseSucceded");
    //			Debug.Log ("ids="+storeSpecificId+":::receipt="+receipt+":::transactionIdentifier="+transactionIdentifier);
    //		}
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        if (InAppSFailCallBack != null)
            InAppSFailCallBack();
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
    public bool IsRestorableProduct(int Index)
    {
        Product product = m_StoreController.products.WithID(NonConsumableProducts[Index]);
        if (product != null && product.hasReceipt)
        {
            return true;
        }
        return false;
    }
    public void checkUnlockAllPurchase(string productId)
    {
        //if (productId == NonConsumableProducts[UnlockAllUpgradesIndex] || productId == NonConsumableProducts[UpgradesDiscountIndex])
        //{
        //    AdManager.UpgradeUnlocked = 1;
        //}
        //else if (productId == NonConsumableProducts[UnlockAllLevelsIndex] || productId == NonConsumableProducts[LevelsDiscountIndex])
        //{
        //    AdManager.LevelsUnlocked = 1;
        //}
        
        //else if (UnlockFullGameIndex!=-1 && productId == NonConsumableProducts[UnlockFullGameIndex])
        //{
        //    AdManager.LevelsUnlocked = 1;
        //    AdManager.UpgradeUnlocked = 1;
        //}
        //else if (productId == SubscriptionProducts[UnlockAllLevelsIndex] || productId == SubscriptionProducts[LevelsDiscountIndex])
        //{
        //    AdManager.LevelsUnlocked = 1;
        //}
        //else if (productId == SubscriptionProducts[UnlockAllUpgradesIndex] || productId == SubscriptionProducts[UpgradesDiscountIndex])
        //{
        //    AdManager.LevelsUnlocked = 1;
        //}
        //else if (UnlockFullGameIndex != -1 && productId == SubscriptionProducts[UnlockFullGameIndex])
        //{
        //    AdManager.LevelsUnlocked = 1;
        //    AdManager.UpgradeUnlocked = 1;
        //}

    }
}
