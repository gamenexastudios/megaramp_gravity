﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Analytics;
using Firebase.Crashlytics;
using Firebase.Extensions;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;

public class FirebaseController : MonoBehaviour
{
    Firebase.FirebaseApp app;
    //protected bool IsFirebaseInitialized = false;
    private string topic = "TestTopic";
    RemotePropertyData RemoteDataInstance;
    public static FirebaseController Instance;
    public bool IsGotRemotData;
    public Firebase.InitResult isFirebaseInit;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Debug.Log("FirebaseController Start");
        try
        {
            Invoke("SetNLoadScene", 3f);

            IsGotRemotData = false;
            //IsFirebaseInitialized = false;
            RemoteDataInstance = new RemotePropertyData();

            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                var dependencyStatus = task.Result;
                Debug.Log("Firebase dependency status=" + dependencyStatus);
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    app = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here to indicate whether Firebase is ready to use by your app.

                    InitializeFirebase();
                }
                else
                {
                    Debug.Log("firebase dependency not available");

                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
        }
        catch(Exception e)
        {
            Debug.Log("Firebase Start Exception="+e);
            //LogFirebaseEvent("Firebase_Dependency_Error", "", "");

        }

    }

    void InitializeFirebase()
    {
        Debug.Log("------ InitializeFirebase");
        try
        {
            isFirebaseInit = Firebase.InitResult.Success;
            if (isFirebaseInit != Firebase.InitResult.Success)
            {
                SetNLoadScene();
                return;
            }
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);

            //Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
            //Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
            //Firebase.Messaging.FirebaseMessaging.SubscribeAsync(topic).ContinueWithOnMainThread(task =>
            //{
            //    LogTaskCompletion(task, "SubscribeAsync");
            //});

            //// This will display the prompt to request permission to receive
            //// notifications if the prompt has not already been displayed before. (If
            //// the user already responded to the prompt, thier decision is cached by
            //// the OS and can be changed in the OS settings).
            //Firebase.Messaging.FirebaseMessaging.RequestPermissionAsync().ContinueWithOnMainThread(
            //  task =>
            //  {
            //      LogTaskCompletion(task, "RequestPermissionAsync");
            //  }
            //);



            //System.Collections.Generic.Dictionary<string, object> defaults =
            //new System.Collections.Generic.Dictionary<string, object>();

            // These are the values that are used if we haven't fetched data from the
            // server
            // yet, or if we ask for values that the server doesn't have:
            //        defaults.Add("config_test_string", "default local string");
            //        defaults.Add("config_test_int", 1);
            //        defaults.Add("config_test_float", 1.0);
            //        defaults.Add("config_test_bool", false);
            //        string jsonData = @"{  
            //""LaunchAd"": ""false"",
            //""Menu_Play"": ""true"",
            //  ""LevelSelection_Play"": ""true"",
            //  ""Upgrade_Play"": ""true"",
            //  ""LevelComplete"": ""true"",
            //  ""LevelFail"": ""true"",
            //  ""Pause_Home"": ""false""
            //}";

            //string someJson = "{\"ErrorMessage\": \"\",\"ErrorDetails\": {\"ErrorID\": 111,\"Description\":{\"Short\": 0,\"Verbose\": 20},\"ErrorDate\": \"\"}}";
            //defaults.Add("InterstitialAds", jsonData);

            //Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);

            Crashlytics.IsCrashlyticsCollectionEnabled = true;
#if UNITY_EDITOR
            var settings = Firebase.RemoteConfig.FirebaseRemoteConfig.Settings;
            settings.IsDeveloperMode = true;
            Firebase.RemoteConfig.FirebaseRemoteConfig.Settings = settings;
#endif
            FetchDataAsync();
        }
        catch(Exception e)
        {
            Debug.Log("Firebase Init Exception=" + e);

            if (isFirebaseInit == Firebase.InitResult.Success)
            {
                LogFirebaseEvent("Firebase_Init_Error", "", "");
            }
        }

    }
    //public virtual void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    //{
    //    try
    //    {
    //        Debug.Log("----- OnMessageReceived");
    //        var notification = e.Message.Notification;
    //        Debug.Log("----- OnMessageReceived notification=" + notification);

    //        if (notification != null)
    //        {
    //            Debug.Log("title: " + notification.Title);
    //            Debug.Log("body: " + notification.Body);
    //            var android = notification.Android;
    //            if (android != null)
    //            {
    //                Debug.Log("android channel_id: " + android.ChannelId);
    //            }
    //        }
    //        if (e.Message.From.Length > 0)
    //            Debug.Log("from: " + e.Message.From);
    //        if (e.Message.Link != null)
    //        {
    //            Debug.Log("link: " + e.Message.Link.ToString());
    //        }
    //        if (e.Message.Data.Count > 0)
    //        {
    //            Debug.Log("data:");
    //            foreach (System.Collections.Generic.KeyValuePair<string, string> iter in
    //                     e.Message.Data)
    //            {
    //                Debug.Log("  " + iter.Key + ": " + iter.Value);
    //            }
    //        }
    //    }
    //    catch(Exception _exception)
    //    {
    //        Debug.Log("Firebase msg received Exception=" + _exception);

    //        if (isFirebaseInit == Firebase.InitResult.Success)
    //        {
    //            LogFirebaseEvent("Firebase_MsgReceive_Error", "", "");
    //        }
    //    }
    //}

    //public virtual void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    //{
    //    Debug.Log("------ OnTokenReceived " + token.Token);
    //}
    public void LogFirebaseEvent(string _log, string paramname, string value)
    {
        Debug.Log("------ LogFirebaseEvent IsFirebaseInitialized="+ isFirebaseInit);

        try
        {
            if (isFirebaseInit == Firebase.InitResult.Success)
            {
                Firebase.Analytics.FirebaseAnalytics.LogEvent(_log, new Firebase.Analytics.Parameter(paramname, value));
            }
        }
        catch (Exception e)
        {
            Debug.Log("----- FireBase log firebase event Exception___" + e);
            //FirebaseEvents.instance.SendCustomException(e);
        }
    }
    public void LogFirebaseEvent_LvlWin(int value)
    {
        Debug.Log("------ LogFirebaseEvent IsFirebaseInitialized=" + isFirebaseInit);

        try
        {
            if (isFirebaseInit == Firebase.InitResult.Success)
            {
                Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventLevelEnd, new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterLevel, "Level_" + value));

            }
        }
        catch (Exception e)
        {
            Debug.Log("----- FireBase log firebase event Exception___" + e);
            //FirebaseEvents.instance.SendCustomException(e);
        }
    }

    protected bool LogTaskCompletion(Task task, string operation)
    {
        Debug.Log("------ LogTaskCompletion");
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            //foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            //{
            //    string errorCode = "";
            //    Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
            //    if (firebaseEx != null)
            //    {
            //        errorCode = String.Format("Error.{0}: ",
            //          ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
            //    }
            //    Debug.Log(errorCode + exception.ToString());
            //}
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }

    // [START fetch_async]
    // Start a fetch request.
    // FetchAsync only fetches new data if the current data is older than the provided
    // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
    // By default the timespan is 12 hours, and for production apps, this is a good
    // number. For this example though, it's set to a timespan of zero, so that
    // changes in the console will always show up immediately.
    public Task FetchDataAsync()
    {
        Debug.Log("------ Fetching data...");
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(
            TimeSpan.Zero);
        return fetchTask.ContinueWithOnMainThread(FetchComplete);
    }
    //[END fetch_async]

    void FetchComplete(Task fetchTask)
    {
        Debug.Log("------ Fetch Complete");
        try
        {
            if (fetchTask.IsCanceled)
            {
                Debug.Log("------ Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                Debug.Log("------- Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                Debug.Log("------- Fetch completed successfully!");
            }

            var info = Firebase.RemoteConfig.FirebaseRemoteConfig.Info;
            switch (info.LastFetchStatus)
            {
                case Firebase.RemoteConfig.LastFetchStatus.Success:
                    Debug.Log("------ lastfetch success");
                    Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();

                    LoadRemoteValues();
                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Failure:
                    Debug.Log("------ lastfetch failure");

                    switch (info.LastFetchFailureReason)
                    {
                        case Firebase.RemoteConfig.FetchFailureReason.Error:
                            Debug.Log("Fetch failed for unknown reason");
                            break;
                        case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                    }
                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Pending:
                    Debug.Log("------ lastfetch pending");

                    Debug.Log("Latest Fetch call still pending.");
                    break;
            }
            if (info.LastFetchStatus != Firebase.RemoteConfig.LastFetchStatus.Success)
            {
                //AdManager.Instance.SetInitialAdIndex(false);
                Invoke("SetNLoadScene", 0.1f);

            }
        }
        catch(Exception e)
        {
            Debug.Log("Firebase Fetch complete Exception=" + e);

            if (isFirebaseInit == Firebase.InitResult.Success)
            {
                LogFirebaseEvent("Firebase_FetchComplete_Error", "", "");
            }
        }
    }
    public void LoadRemoteValues()
    {
        Debug.LogWarning("------------ LoadRemoteValues-------------");
        try
        {
            IsGotRemotData = true;
            AdManager.Instance.Menu_CrossPromotion_Ad = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Menu_CrossPromotion_Ad").BooleanValue;
            //AdController.Instance.IsEnableTestIds = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("EnableTestIds").BooleanValue;
            //AdController.Instance.CheckIds();
            string InterstitialAds_JsonString = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_Ads").StringValue;
            JSONNode json_InterstitalAds_Data = JSON.Parse(InterstitialAds_JsonString);

            AdManager.Instance.Launch_Ad = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_Launch_Ad").BooleanValue;
            AdManager.Instance.Menu_Play = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_Menu_Play").BooleanValue;
            AdManager.Instance.World_Play = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_World_Play").BooleanValue;
            AdManager.Instance.LvlSelection_Play = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_LvlSelection_Play").BooleanValue;
            AdManager.Instance.Upgrade_Play = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_Upgrade_Play").BooleanValue;
            AdManager.Instance.Lvl_Win = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_Lvl_Win").BooleanValue;
            AdManager.Instance.Lvl_Fail = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_Lvl_Fail").BooleanValue;
            AdManager.Instance.Pause_Home = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_Pause_Home").BooleanValue;

            Debug.LogError("--------- load remote data menuplay="+ AdManager.Instance.Menu_Play+ "--Lvl_Win="+ AdManager.Instance.Lvl_Win+":: fail="+ AdManager.Instance.Lvl_Fail);
            string BannerAds_JsonString = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_Ads").StringValue;
            JSONNode json_BannerAds_Data = JSON.Parse(BannerAds_JsonString);

            AdManager.Instance.BannerAd_Menu = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_Menu").BooleanValue;
            AdManager.Instance.BannerAd_LvlSelection = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_LvlSelection").BooleanValue;
            AdManager.Instance.BannerAd_WorldSelection = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_WorldSelection").BooleanValue;
            AdManager.Instance.BannerAd_Upgrade = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_Upgrade").BooleanValue;
            AdManager.Instance.BannerAd_InGame = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_InGame").BooleanValue;
            AdManager.Instance.BannerAd_LCPage = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_LCPage").BooleanValue;
            AdManager.Instance.BannerAd_LFPage = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_LFPage").BooleanValue;
            AdManager.Instance.BannerAd_Pause = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_Pause").BooleanValue;
            AdManager.Instance.BannerAd_Settings = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_Settings").BooleanValue;
            AdManager.Instance.BannerAd_Store = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Banner_Store").BooleanValue;

            AdManager.Instance.Ad_WinCounter = int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Ad_WinCounter").StringValue);
            AdManager.Instance.Ad_FailCounter = int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Ad_FailCounter").StringValue);
            AdManager.Instance.Ad_StartFromLvl = int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Ad_StartFromLvl").StringValue);


            //string Interstitial_AdTypes_JsonString = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Interstitial_AdTypes").StringValue;
            //JSONNode json_Interstitial_AdTypes_Data = JSON.Parse(Interstitial_AdTypes_JsonString);
            //string InterstitialAds_OrderString = json_Interstitial_AdTypes_Data["Ads_Order"];
            ////Debug.Log("----AdsOrderstring="+ InterstitialAds_OrderString);
            //string[] InterstitialAds_Order = InterstitialAds_OrderString.Split('_');

            //AdManager.Instance.RotationAdsList.Clear();
            //for (int i = 0; i < InterstitialAds_Order.Length; i++)
            //{
            //    //AdManager.Instance.RatingPopInLevels.Add(int.Parse(RateInLvls[i]));
            //    if (InterstitialAds_Order[i].Equals("Admob"))
            //    {
            //        AdManager.Instance.RotationAdsList.Add(AdManager.Ad_Type.Admob);
            //    }
            //    else if (InterstitialAds_Order[i].Equals("IronSource"))
            //    {
            //        AdManager.Instance.RotationAdsList.Add(AdManager.Ad_Type.IronSource);
            //    }
            //    else if (InterstitialAds_Order[i].Equals("Unity"))
            //    {
            //        AdManager.Instance.RotationAdsList.Add(AdManager.Ad_Type.Unity);
            //    }
            //    else if (InterstitialAds_Order[i].Equals("FB"))
            //    {
            //        AdManager.Instance.RotationAdsList.Add(AdManager.Ad_Type.FB);
            //    }
            //}
            //string Rewarded_AdTypes_JsonString = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Rewarded_AdTypes").StringValue;
            //JSONNode json_Rewarded_AdTypes_Data = JSON.Parse(Rewarded_AdTypes_JsonString);
            //string RewardedAds_OrderString = json_Rewarded_AdTypes_Data["Ads_Order"];
            //string[] RewardedAds_Order = RewardedAds_OrderString.Split('_');
            //AdManager.Instance.RotationVideoAdsList.Clear();
            //for (int i = 0; i < RewardedAds_Order.Length; i++)
            //{
            //    //AdManager.Instance.RatingPopInLevels.Add(int.Parse(RateInLvls[i]));
            //    if (RewardedAds_Order[i].Equals("Admob"))
            //    {
            //        AdManager.Instance.RotationVideoAdsList.Add(AdManager.Ad_Type.Admob);
            //    }
            //    else if (RewardedAds_Order[i].Equals("IronSource"))
            //    {
            //        AdManager.Instance.RotationVideoAdsList.Add(AdManager.Ad_Type.IronSource);
            //    }
            //    else if (RewardedAds_Order[i].Equals("Unity"))
            //    {
            //        AdManager.Instance.RotationVideoAdsList.Add(AdManager.Ad_Type.Unity);
            //    }
            //    else if (RewardedAds_Order[i].Equals("FB"))
            //    {
            //        AdManager.Instance.RotationVideoAdsList.Add(AdManager.Ad_Type.FB);
            //    }
            //}

            string RateLvlsString = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Rate_InLevels").StringValue;
            string[] RateInLvls = RateLvlsString.Split('_');
            AdManager.Instance.RatingPopInLevels.Clear();
            for (int i = 0; i < RateInLvls.Length; i++)
            {
                AdManager.Instance.RatingPopInLevels.Add(int.Parse(RateInLvls[i]));
            }

            //string CrossPromotionAds_JsonString = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotion_Ads").StringValue;
            //JSONNode json_CrossPromotionAds_Data = JSON.Parse(CrossPromotionAds_JsonString);
            Debug.LogError("------------ before spl offer check");
            string SpecialPack_JsonString = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("SplOffer").StringValue;
            JSONNode json_SpecialPack_Data = JSON.Parse(SpecialPack_JsonString);

            //AdManager.Instance._SplPackRemoteDataInstance = new SpecialPackRemoteData();
            //AdManager.Instance._SplPackRemoteDataInstance = JsonUtility.FromJson<SpecialPackRemoteData>(SpecialPack_JsonString);
            SpecialPackRemoteData.Instance.Active = json_SpecialPack_Data["Active"];
            SpecialPackRemoteData.Instance.Pack_type = json_SpecialPack_Data["Pack_type"];
            SpecialPackRemoteData.Instance.unique_index = json_SpecialPack_Data["unique_index"];
            SpecialPackRemoteData.Instance.ProductID_Price = json_SpecialPack_Data["ProductID_Price"];
            SpecialPackRemoteData.Instance.ProductID_Purchase = json_SpecialPack_Data["ProductID_Purchase"];
            SpecialPackRemoteData.Instance.Pack_Name = json_SpecialPack_Data["Pack_Name"];
            SpecialPackRemoteData.Instance.MaxCount = json_SpecialPack_Data["MaxCount"];
            SpecialPackRemoteData.Instance.Season = json_SpecialPack_Data["Season"];

            GiftBoxRewardHandler.instance.Active= Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Surprise_Gift").BooleanValue;
            GiftBoxRewardHandler.instance.SurpriseGiftMaxCount = int.Parse(Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Surprise_Gift_MaxCount").StringValue);

            UnlockVehicleIAPHandler.instance.Active = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("Upgrade_Single_IAP").BooleanValue;
            //SpecialPackRemoteData.Instance = JsonUtility.FromJson<SpecialPackRemoteData>(SpecialPack_JsonString);
            //Debug.LogError("_SplPackInstance=" + AdManager.Instance._SplPackRemoteDataInstance.Active + ":::"+ AdManager.Instance._SplPackRemoteDataInstance.ProductID_Price);

            string CrossPromotionNew_Ads_JsonString = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotion_Ads").StringValue;
            JSONNode json_CrossPromotionNew_Ads_Data = JSON.Parse(CrossPromotionNew_Ads_JsonString);
            AdManager.Instance.MenuAdsJsonLink = json_CrossPromotionNew_Ads_Data["Menu_Ad_JsonLink"];
            AdManager.Instance.ExitAdsJsonLink = json_CrossPromotionNew_Ads_Data["Exit_Ad_JsonLink"];

            if (AdManager.Instance.Menu_CrossPromotion_Ad)
            {
                if (!string.IsNullOrEmpty(AdManager.Instance.MenuAdsJsonLink))
                {
                    StartCoroutine(LoadMenuAdJson());
                }
            }
            if (!string.IsNullOrEmpty(AdManager.Instance.ExitAdsJsonLink))
            {
                StartCoroutine(LoadExitAdJson());
            }


            Invoke("SetNLoadScene", 1f);
        }
        catch(Exception e)
        {
            Debug.Log("Firebase load remote values=" + e);

            if (isFirebaseInit == Firebase.InitResult.Success)
            {
                LogFirebaseEvent("Firebase_LoadRemoteValues_Error", "", "");
            }
        }

    }

    private IEnumerator LoadMenuAdJson()
    {
        //Debug.LogError("---- LoadMenuAdJson");

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(AdManager.Instance.MenuAdsJsonLink);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError("LoadMenuAdJson not loaded=" + www.error);
        }
        else
        {
            //Debug.LogError("------------- LoadMenuAdJson Loaded=" + (www.downloadhandler.text));
            string CrossPromotionNew_Ads_JsonString = www.downloadHandler.text;
            if (string.IsNullOrEmpty(CrossPromotionNew_Ads_JsonString))
            {
                yield break;
            }
            JSONNode json_CrossPromotionNew_Ads_Data = JSON.Parse(CrossPromotionNew_Ads_JsonString);
            //Debug.LogError("---------- Before Apps1 is null so returning --------");

            if (json_CrossPromotionNew_Ads_Data["Apps"] == null)
            {
                //Debug.LogError("---------- Apps1 is null so returning --------");
                yield break;
            }
            if (json_CrossPromotionNew_Ads_Data["Apps"].Count == 0)
            {
                yield break;
            }
            
            AdManager.Instance.TotalAvailableMenuAdsCount = json_CrossPromotionNew_Ads_Data["Apps"].Count;

            GetMenuAdIndex(json_CrossPromotionNew_Ads_Data);// to check possibility of same game link

            Debug.LogError("CurrentMenuAdIndex=" + AdManager.Instance.CurrentMenuAdIndex);

            string packageName = json_CrossPromotionNew_Ads_Data["Apps"][AdManager.Instance.CurrentMenuAdIndex]["apppackage"];
            if (string.Equals(packageName, Application.identifier))
            {
                yield break;
            }
            AdManager.Instance.MenuAdImgLink = json_CrossPromotionNew_Ads_Data["Apps"][AdManager.Instance.CurrentMenuAdIndex]["appimage"];
            AdManager.Instance.MenuAdLinkTo = json_CrossPromotionNew_Ads_Data["Apps"][AdManager.Instance.CurrentMenuAdIndex]["apppackage"];


            StartCoroutine(LoadMenuAd());

        }
    }
    void GetMenuAdIndex(JSONNode json_CrossPromotionNew_Ads_Data)
    {
        AdManager.Instance.CurrentMenuAdIndex = PlayerPrefs.GetInt("CurrentMenuAdIndex", -1) + 1;
        if (AdManager.Instance.CurrentMenuAdIndex >= json_CrossPromotionNew_Ads_Data["Apps"].Count)
        {
            AdManager.Instance.CurrentMenuAdIndex = 0;
        }
        string packageName = json_CrossPromotionNew_Ads_Data["Apps"][AdManager.Instance.CurrentMenuAdIndex]["apppackage"];
        if (string.Equals(packageName, Application.identifier))  //packageName==Application.identifier)
        {
            AdManager.Instance.CurrentMenuAdIndex++;
            if (AdManager.Instance.CurrentMenuAdIndex >= json_CrossPromotionNew_Ads_Data["Apps"].Count)
            {
                AdManager.Instance.CurrentMenuAdIndex = 0;
            }
        }
    }
    private IEnumerator LoadExitAdJson()
    {
        //Debug.LogError("---- LoadExitJson");

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(AdManager.Instance.ExitAdsJsonLink);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError("LoadExitJson not loaded=" + www.error);
        }
        else
        {
            //Debug.LogError("------------- LoadExitJson Loaded=" + (www.downloadHandler.text));
            string CrossPromotionNew_Ads_JsonString = www.downloadHandler.text;
            if (string.IsNullOrEmpty(CrossPromotionNew_Ads_JsonString))
            {
                yield break;
            }
            JSONNode json_CrossPromotionNew_Ads_Data = JSON.Parse(CrossPromotionNew_Ads_JsonString);
            //Debug.LogError("---------- Before Apps1 is null so returning --------");

            AdManager.Instance.ExitAdImgLinkList.Clear();
            AdManager.Instance.ExitAdLinkToList.Clear();
            AdManager.Instance.ExitAdLinkToList_All.Clear();

            AdManager.Instance.ExitAdTextureList.Clear();

            if (json_CrossPromotionNew_Ads_Data["Apps"] == null)
            {
                //Debug.LogError("---------- Apps1 is null so returning --------");
                yield break;
            }
            if (json_CrossPromotionNew_Ads_Data["Apps"].Count == 0)
            {
                yield break;
            }
            AdManager.Instance.TotalExitAdsCount = json_CrossPromotionNew_Ads_Data["Apps"].Count;
            for (int i = 0; i < AdManager.Instance.TotalExitAdsCount; i++)
            {
                AdManager.Instance.ExitAdImgLinkList.Add(json_CrossPromotionNew_Ads_Data["Apps"][i]["appimage"]);
                AdManager.Instance.ExitAdLinkToList_All.Add(json_CrossPromotionNew_Ads_Data["Apps"][i]["apppackage"]);
            }

            StartCoroutine(LoadExitAd(0));

        }
    }
    private IEnumerator LoadMenuAd()
    {
        //Debug.Log("---- LoadMenuAd" );

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(AdManager.Instance.MenuAdImgLink);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("menuAd not loaded=" + www.error);
        }
        else
        {
            //Debug.Log("------------- MenuAdDownloaded ");
            Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            AdManager.Instance.MenuAdTexture = myTexture;
            MenuAdPageHandler.instance.MenuAdImgNew.texture = AdManager.Instance.MenuAdTexture;
        }
    }
    private IEnumerator LoadExitAd(int Index)
    {
        //Debug.LogError("---- LoadExitAd index="+Index);

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(AdManager.Instance.ExitAdImgLinkList[Index]);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("LoadExitAd not loaded=" + www.error);
        }
        else
        {
            Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            AdManager.Instance.ExitAdTextureList.Add(myTexture);
            AdManager.Instance.ExitAdLinkToList.Add(AdManager.Instance.ExitAdLinkToList_All[Index]);
        }
        Index++;
        if(Index<AdManager.Instance.TotalExitAdsCount)
        {
            StartCoroutine(LoadExitAd(Index));
        }
    }

    bool IsReadyToLoadScene;
    public void SetNLoadScene()
    {
        Debug.Log("------- SetNLoadScene 1111");
        CancelInvoke("SetNLoadScene");
        if (!IsReadyToLoadScene)
        {
            Debug.Log("------- SetNLoadScene 2222");
            IsReadyToLoadScene = true;
            AdManager.Instance.SetInitialAdIndex(IsGotRemotData);
        }
    }

    //// Causes an error that will crash the app at the platform level (Android or iOS)
    //public void ThrowUncaughtException()
    //{
    //    Debug.Log("Causing a platform crash.");
    //    throw new InvalidOperationException("Uncaught exception created from UI.");
    //}

    //// Log a caught exception.
    //public void LogCaughtException()
    //{
    //    Debug.Log("Catching an logging an exception.");
    //    try
    //    {
    //        throw new InvalidOperationException("This exception should be caught");
    //    }
    //    catch (Exception ex)
    //    {
    //        Crashlytics.LogException(ex);
    //    }
    //}

}

