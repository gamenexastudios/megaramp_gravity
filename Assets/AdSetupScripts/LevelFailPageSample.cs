﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelFailPageSample : MonoBehaviour
{
    public static LevelFailPageSample Instance;
    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }
    public void Open()
    {
        gameObject.SetActive(true);
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.PageType.LFPage);
        }
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void NextClick()
    {
        SceneManager.LoadScene("MenuSample");
    }
    public void RetryClick()
    {
        SceneManager.LoadScene("InGameSample");
    }
}
