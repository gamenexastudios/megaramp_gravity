﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
// Include Facebook namespace
using Facebook.Unity;
using UnityEngine.UI;
public class FacebookHandler : MonoBehaviour
{
    List<string> perms = new List<string>() { "public_profile", "email" };
    public static FacebookHandler instance;
    void Awake()
    {
        instance = this;
        if (!FB.IsInitialized)
        {
            //Debug.Log("------- FB not initialized call Init");
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            //Debug.Log("------- FB already initialized call activate app");
            FB.ActivateApp();
        }
    }
    private void InitCallback()
    {
        //Debug.Log("------ InitCallback");
        if (FB.IsInitialized)
        {
            //Debug.Log("------- FB initialized successfully");
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }
    public void OnApplicationPause(bool paused)
    {
        if (!paused)
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
        }
    }
    private void OnHideUnity(bool isGameShown)
    {
        Debug.Log("OnHideUnity isGameShown=" + isGameShown);
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            //			Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            //			Time.timeScale = 1;
        }
    }
    public void FBLogIn()
    {
        if (!FB.IsLoggedIn)
        {
            FB.LogInWithReadPermissions(perms, AuthCallback);
        }
        else
        {
            FBLogOut();
        }

    }
    public void FBLogOut()
    {
        FB.LogOut();
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("------ FB login successfull");
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }
    public void FBShare()
    {
        //FB.ShareLink(
        //    new Uri(AdManager.instance.ShareUrl),
        //    callback: ShareCallback
        //);
    }

    private void ShareCallback(IShareResult result)
    {
        if (result.Cancelled || !String.IsNullOrEmpty(result.Error))
        {
            Debug.Log("ShareLink Error: " + result.Error);
        }
        else if (!String.IsNullOrEmpty(result.PostId))
        {
            // Print post identifier of the shared content
            Debug.Log("ShareLink Error 222: " + result.PostId);
            Debug.Log(result.PostId);
        }
        else
        {
            // Share succeeded without postID
            Debug.Log("-------- ShareLink success!");
        }
    }
}
