﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameSample : MonoBehaviour
{
    public static InGameSample Instance;
    public Button LCBtn, LFBtn;
    void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        LCBtn.interactable = false;
        LFBtn.interactable = false;
        Invoke("EnableBtns", 3);
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.PageType.InGame);
        }

    }
    void EnableBtns()
    {
        LCBtn.interactable = true;
        LFBtn.interactable = true;
    }
    public void Open()
    {
        gameObject.SetActive(true);
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void ShowLevelComplete()
    {
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.Actiontype.Lvl_Win);
        }
        LevelCompletePageSample.Instance.Open();
    }
    public void ShowLevelFail()
    {
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.Actiontype.Lvl_Fail, StaticVariables._iSelectedLevel);
        }
        LevelFailPageSample.Instance.Open();
    }
}
