﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RatePageHandler : MonoBehaviour
{
	public static RatePageHandler Instance;
	public GameObject PopUp;
	public GameObject RateBtn, ThankYouBtn;
    public GameObject[] FillStars;
    public GameObject MainObj;
    //	public delegate void RateFailCheck ();
    //	public static event RateFailCheck RateFailCallBack;
    void Awake()
	{
        Instance = this;
        MainObj.SetActive(false);
	}
	public void Open()
	{

        MainObj.SetActive(true);
        //for (int i = 0; i < FillStars.Length; i++)
        //{
        //    FillStars[i].SetActive(false);
        //}

       // ThankYouBtn.SetActive(false);
	//	RateBtn.SetActive(false);

        iTween.Stop(PopUp);
		PopUp.transform.localPosition = Vector3.zero;
		iTween.MoveFrom(PopUp, iTween.Hash("y", 1000, "time", 0.4f, "islocal", true, "easetype", iTween.EaseType.spring));

        //iTween.Stop(ThankYouBtn);
        //ThankYouBtn.transform.localScale = Vector3.zero;
        //iTween.ScaleTo(ThankYouBtn, iTween.Hash("x",1,"y",1, "time", 0.4f, "islocal", true, "easetype", iTween.EaseType.spring));
    }
	public void Close()
	{
        MainObj.SetActive(false);
	}
    public void StarClick(int StarCount)
    {
        RateBtn.SetActive(false);
        ThankYouBtn.SetActive(false);
        for (int i = 0; i < FillStars.Length; i++)
        {
            if (i < StarCount)
            {
                FillStars[i].SetActive(true);
            }
        }
        if (StarCount > 3)
        {
            RateBtn.SetActive(true);
        }
        else
        {
            ThankYouBtn.SetActive(true);
        }
    }
    public void ThankYouClick()
    {
        PlayerPrefs.SetString("IsRated", "true");
        Close();
    }
    public void RateClick()
	{
		PlayerPrefs.SetString("IsRated", "true");
		Application.OpenURL("market://details?id=" + Application.identifier);
		
		Close();
	}
}
