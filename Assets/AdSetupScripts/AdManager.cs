﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdManager : MonoBehaviour
{
    public static AdManager Instance;
    static string _sUpgradeUnlocked = "UpgradeUnlocked";
    static string _sLevelsUnlocked = "LevelsUnlocked";
    //public List<int> RotationAdsList = new List<int> { 1, 2, 3 };
    //public List<int> RotationVideoAdsList = new List<int> { 1, 2, 3 };
    public List<Ad_Type> RotationAdsList = new List<Ad_Type> { Ad_Type.Admob, Ad_Type.IronSource, Ad_Type.Unity };
    public List<Ad_Type> RotationVideoAdsList = new List<Ad_Type> { Ad_Type.Admob, Ad_Type.IronSource, Ad_Type.Unity };
    public List<int> RatingPopInLevels = new List<int>();

    private int AdIndexAtRAdsList;
    private int AdIndexAtVideoRAdsList;

    public int AdDelay = 30;
    public float LastAdShownTime;

    [HideInInspector]
    public int SessionCount;

    public delegate void RewardVideoCallback(bool rewarded);
    public RewardVideoCallback RewardSuccessEvent;
    public GameObject LoadingPage;

    public string MenuAdsJsonLink = "http://cdn.appsupstudios.com/Gaming/gaming.json";
    public string ExitAdsJsonLink = "http://cdn.appsupstudios.com/Gaming/gaming_back_apps.json";

    public bool Menu_CrossPromotion_Ad,Launch_Ad, Menu_Play, World_Play, LvlSelection_Play, Upgrade_Play, Lvl_Win, Lvl_Fail, Pause_Home;
    public bool BannerAd_Menu, BannerAd_WorldSelection, BannerAd_LvlSelection, BannerAd_Upgrade, BannerAd_InGame, BannerAd_LCPage, BannerAd_LFPage, BannerAd_Pause, BannerAd_Settings, BannerAd_Store;
    public string MenuAdImgLink;//,ExitAdImgLink;
    public string MenuAdLinkTo;//,ExitAdLinkTo;
    public Texture MenuAdTexture;//, ExitAdTexture;

    public List<Texture> ExitAdTextureList=new List<Texture>();

    public List<string> ExitAdImgLinkList = new List<string>();
    public List<string> ExitAdLinkToList = new List<string>();
    public List<string> ExitAdLinkToList_All = new List<string>();

    public bool IsMenuAdShown;
    public bool IsLandScapeGame;
    public int TotalAvailableMenuAdsCount = 0;
    public int CurrentMenuAdIndex = 0;
    public int TotalExitAdsCount = 0;

    public int Ad_WinCounter, Ad_FailCounter,Ad_StartFromLvl;
    public SpecialPackRemoteData _SplPackRemoteDataInstance;

    public int WinCounter, FailCounter;
    public bool IsForceHideBannerAd;
    public enum Ad_Type
    {
        Admob,
        IronSource,
        Unity,
        FB
    }
    public enum PageType
    {
        Menu,
        LvlSelection,
        Upgrade,
        InGame,
        LCPage,
        LFPage,
        Pause,
        Settings,
        Store,
        Loading,
        WorldSelection
    }
    public enum Actiontype
    {
        Launch_Ad,
        Menu_Play,
        LvlSelection_Play,
        Upgrade_Play,
        Lvl_Win,
        Lvl_Fail,
        Pause_Home,
        LC_Next,
        World_Play
    }
    public enum InAppButtonReference
    {
        None,
        FullGame_Menu,

        Levels_PopUp,
        Levels_PopUp_Discount,
        Levels_LevelSelection,
        Levels_LockedLevel,

        Upgrades_PopUp,
        Upgrades_PopUp_Discount,
        Upgrades_UpgradePage,
        Upgrades_LockedUpgrade,
        Upgrades_Pricebutton,

        UnlockAll_LevelComplete,
        UnlockAll_LevelFail,
        UnlockAll_Pause,

        Resumes_PreLF,

        Store_Resumes,
        Store_Levels,
        Store_Upgrades,
        Store_FullGame,

        StoreCoinpack1,
        StoreCoinpack2,
        StoreCoinpack3,
        StoreCoinpack4,
        StorePremiumUpgrade,

        Upgrades_Premium,
        Unlockitemmenupage,
        MenuCoinpack1,
        MenuCoinpack2

    }
    public static int UpgradeUnlocked
    {
        get
        {
            return PlayerPrefs.GetInt(_sUpgradeUnlocked, 0);
        }
        set
        {
            PlayerPrefs.SetInt(_sUpgradeUnlocked, value);
        }
    }
    public static int LevelsUnlocked
    {
        get
        {
            return PlayerPrefs.GetInt(_sLevelsUnlocked, 0);
        }
        set
        {
            PlayerPrefs.SetInt(_sLevelsUnlocked, value);
        }
    }
    public static int SubscribeUnlocked
    {
        get
        {
            return PlayerPrefs.GetInt("Subscribe", 0);
        }
        set
        {
            PlayerPrefs.SetInt("Subscribe", value);
        }
    }
    void OnEnable()
    {
        //Debug.LogError("------ AdManager OnEnable 11");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    private void Awake()
    {
        Instance = this;
        LoadingPage.SetActive(true);
        if (Screen.width > Screen.height)
            IsLandScapeGame = true;
        else
            IsLandScapeGame = false;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
    private void Start()
    {
        //SubscribeUnlocked = 0;
        SetDefaultValues();
        WinCounter = 0;
        FailCounter = 0;
        Ad_StartFromLvl = 0;
        IsForceHideBannerAd = true;
        SessionCount = PlayerPrefs.GetInt("SessionCount", 0);
        PlayerPrefs.SetInt("SessionCount", (SessionCount + 1));
        //SetInitialAdIndex();

        //Invoke("LoadFirstScene",2);
        //LoadFirstScene();

    }

    private void SetDefaultValues()
    {
        IsMenuAdShown = false;

        AdController.Instance.possibleInterstitialRequestTypeCount = 0;
        AdController.Instance.possibleRewardedRequestTypeCount = 0;

        AdIndexAtRAdsList = -1;
        AdIndexAtVideoRAdsList = -1;

        LastAdShownTime = -AdDelay;

        //WinCounter = 0;
        //FailCounter = 0;

    }
    public void SetInitialAdIndex(bool IsGotRemoteData = false)
    {
        Debug.LogError("------- SetInitialAdIndex");
        SetDefaultValues();

        for (int i = 0; i < RotationAdsList.Count; i++)
        {
            switch(RotationAdsList[i])
            {
                case Ad_Type.IronSource:
                    if(!AdController.Instance.IsEnableIronSourceInterstitial)
                    AdController.Instance.possibleInterstitialRequestTypeCount++;//used this condition to restict multiple count incase added like 1_1_1
                    AdController.Instance.IsEnableIronSourceInterstitial = true;

                    break;
            }

        }
        for (int i = 0; i < RotationVideoAdsList.Count; i++)
        {
            switch (RotationVideoAdsList[i])
            {
                case Ad_Type.IronSource:
                    if (!AdController.Instance.IsEnableIronSourceReward)
                        AdController.Instance.possibleRewardedRequestTypeCount++;
                    AdController.Instance.IsEnableIronSourceReward = true;
                    break;
            }

        }
        setNextInterstitialAdIndex();
        setNextVideoAdIndex();

        RequestAd();
        AdController.Instance.RequestIronSourceBannerAd();
        //RequestRewardVideo();

        if (IsGotRemoteData)
        {
            Debug.LogError("----- Set Initial AdIndex Got Remote data");
            //AllAdsHandler.instance.InitAdTypes();
            //if (!WelcomePageHandler.instance.gameObject.activeInHierarchy && PlayerPrefs.GetString("IsGotWelcomeGift", "false") == "true")
            //{
            //    CheckAndLoadFirstScene();
            //}
            Debug.LogError("load first scene call 111111");
            Invoke("LoadFirstScene", 3);

        }
        else
        {
            Debug.LogError("load first scene call 22222222");

            Invoke("LoadFirstScene", 2);

        }


    }
    private void setNextInterstitialAdIndex()
    {
        AdIndexAtRAdsList++;
        if (AdIndexAtRAdsList >= RotationAdsList.Count)
        {
            AdIndexAtRAdsList = 0;
        }
    }
    private void setNextVideoAdIndex()
    {
        AdIndexAtVideoRAdsList++;
        if (AdIndexAtVideoRAdsList >= RotationVideoAdsList.Count)
        {
            AdIndexAtVideoRAdsList = 0;
        }
    }

    public bool IsShowBannerAd = false;

    public void RunActions(PageType CurrentPage, int LvlNo = 1, int score = 0)
    {
        IsShowBannerAd = false;
        //CurrentPageName = CurrentPage.ToString();
        Debug.LogError("------- RunActions pageType=" + CurrentPage + " Currentlevelnumber" + LvlNo);
        switch (CurrentPage)
        {
            case PageType.Menu:
                //AdShowInPages[0]
                //ShowAd();
                if(Menu_CrossPromotion_Ad && !IsMenuAdShown && MenuAdTexture!=null)
                {
                    MenuAdPageHandler.instance.Open();
                    IsMenuAdShown = true;
                }
                Invoke("CheckGoogleSignIn", 2);
                IsShowBannerAd = BannerAd_Menu;
                break;
            case PageType.LvlSelection:
                IsShowBannerAd = BannerAd_LvlSelection;
                break;
            case PageType.Upgrade:
                IsShowBannerAd = BannerAd_Upgrade;
                CheckNDisplayUpgrade_Single_IAP();
                break;
            case PageType.InGame:
                IsShowBannerAd = BannerAd_InGame;
                break;
            case PageType.LCPage:
                IsShowBannerAd = BannerAd_LCPage;
                CheckAndDisplayRatePopUp(LvlNo);
                break;
            case PageType.LFPage:
                IsShowBannerAd = BannerAd_LFPage;
                break;
            case PageType.Pause:
                IsShowBannerAd = BannerAd_Pause;
                break;
            case PageType.Settings:
                IsShowBannerAd = BannerAd_Settings;
                break;
            case PageType.Store:
                IsShowBannerAd = BannerAd_Store;
                break;
            case PageType.Loading:
                IsShowBannerAd = false;
                break;
            case PageType.WorldSelection:
                IsShowBannerAd = BannerAd_WorldSelection;
                break;
        }
#if !AdSetup_OFF
        if(IsShowBannerAd && !IsForceHideBannerAd)
        {
            AdController.Instance.ShowIronSourceBannerAd();
        }
        else
        {
            AdController.Instance.HideIronSourceBannerAd();
        }
#endif
    }
    private void CheckGoogleSignIn()
    {
#if !PlayServices_Off
        PlayServicesHandler.instance.CheckAutoSignIn();
#endif
    }
    public void CheckNShowBannerAd()
    {
        Debug.LogError("------- Check NShowBannerAd IsShowBannerAd="+IsShowBannerAd);
        if (IsShowBannerAd && !IsForceHideBannerAd)
        {
            AdController.Instance.ShowIronSourceBannerAd();
        }
        else
        {
            AdController.Instance.HideIronSourceBannerAd();
        }
    }

    public void RunActions(Actiontype CurrentAction, int LvlNo = 1, int score = 0)
    {
        //CurrentPageName = CurrentPage.ToString();
        Debug.LogError("------- RunActions ActionType=" + CurrentAction + " Currentlevelnumber" + LvlNo);
        switch (CurrentAction)
        {
            case Actiontype.Launch_Ad:
                if (Launch_Ad)
                {
                    ShowAd();
                    Launch_Ad = false;
                }
                break;
            case Actiontype.Menu_Play:
                Debug.Log("--- RunActions Menu_Play=" + Menu_Play);
                if (Menu_Play)
                ShowAd();
                break;
            case Actiontype.World_Play:
                Debug.Log("--- RunActions World_Play="+ World_Play);
                if (World_Play)
                    ShowAd();
                break;
            case Actiontype.LvlSelection_Play:
                if(LvlSelection_Play)
                ShowAd();
                break;
            case Actiontype.Upgrade_Play:
                if(Upgrade_Play)
                ShowAd();
                break;
            case Actiontype.Lvl_Win:
                Debug.Log("--- RunActions Lvl_Win=" + Lvl_Win+ "::VideoReward_2x="+ !InGameUIHandler.Instance.VideoReward_2x);
                WinCounter++;
                if (Lvl_Win && WinCounter >= Ad_WinCounter && LvlNo>=Ad_StartFromLvl && InGameUIHandler.Instance && !InGameUIHandler.Instance.VideoReward_2x)
                {
                    ShowAd();
                    WinCounter = 0;
                }
                if(LvlNo<=10)
                {
                    FirebaseController.Instance.LogFirebaseEvent("LevelWin_"+LvlNo, "Success", "");
                }
                else if (LvlNo > 10 && LvlNo <= 15)
                {
                    FirebaseController.Instance.LogFirebaseEvent("LevelWin_" + LvlNo, "Success", "");
                }
                else if (LvlNo > 15 && LvlNo <= 20)
                {
                    FirebaseController.Instance.LogFirebaseEvent("LevelWin_" + "15_20", "Success", "");
                }
                else if (LvlNo > 20 && LvlNo <= 25)
                {
                    FirebaseController.Instance.LogFirebaseEvent("LevelWin_" + LvlNo, "Success", "");
                }
                else if (LvlNo > 25 && LvlNo <= 30)
                {
                    FirebaseController.Instance.LogFirebaseEvent("LevelWin_" + "25_30", "Success", "");
                }
                else if (LvlNo > 30 && LvlNo < 40)
                {
                    FirebaseController.Instance.LogFirebaseEvent("LevelWin_" + "30_39", "Success", "");
                }
                else
                {
                    FirebaseController.Instance.LogFirebaseEvent("LevelWin_"+LvlNo, "Success", "");
                }
                FirebaseController.Instance.LogFirebaseEvent_LvlWin(LvlNo);
                break;
            case Actiontype.Lvl_Fail:
                FailCounter++;
                if (Lvl_Fail && FailCounter >= Ad_FailCounter && LvlNo >= Ad_StartFromLvl)
                {
                    ShowAd();
                    FailCounter = 0;
                }
                if (LvlNo <= 20)
                {
                    FirebaseController.Instance.LogFirebaseEvent("LevelFail_" + LvlNo, "Fail", "");
                }

                break;
            case Actiontype.Pause_Home:
                if(Pause_Home)
                ShowAd();
                break;
            case Actiontype.LC_Next:
                //CheckAndDisplayRatePopUp(LvlNo);
                break;
        }
    }
    private void CheckNDisplayUpgrade_Single_IAP()
    {
        if(UpgradeUnlocked!=1 && UnlockVehicleIAPHandler.instance!=null && UnlockVehicleIAPHandler.instance.Active && UnlockVehicleIAPHandler.instance.ReadyToDisplay)
        {
            UnlockVehicleIAPHandler.instance.Open();
        }
    }
    private bool CheckAndDisplayRatePopUp(int LvlNo)
    {
        Debug.LogError("---- Check And Display RatePopup Lvno=" + LvlNo);
        if (isWifi_OR_Data_Availble() && RatingPopInLevels.Contains(LvlNo) && PlayerPrefs.GetString("IsRated", "false") == "false")
        {
            Debug.LogError("---- Check And Display RatePopup before check ratepop");

            RatePageHandler.Instance.Open();
        }

        return false;
    }
    public void RequestAd()
    {
       
        Debug.Log("------- RequestAd AdIndexAtRAdsList=" + AdIndexAtRAdsList);
        if (isWifi_OR_Data_Availble())
        {
            try
            {
                AdController.Instance.RequestInterstitialLoopCheckCount = 0;

#if !AdSetup_OFF
                if (RotationAdsList.Count > 0 && RotationAdsList[AdIndexAtRAdsList] == Ad_Type.IronSource)
                {
                    AdController.Instance.RequestIronSourceInterstitial();//Admob
                }
#endif
            }
            catch (Exception e)
            {
                Debug.Log("Request Ad With Delay exception=" + e);
            }
        }




        //Debug.Log ("------- Request Ad IsEnableUnityInterstitial="+AllAdsHandler.instance.IsEnableUnityInterstital);
    }
    public void RequestRewardVideo()
    {
        return;
//        Debug.Log("Reward ad request 111");
//        try
//        {
//            AdController.Instance.RequestRewardedLoopCheckCount = 0;

//#if !AdSetup_OFF
//            if (RotationVideoAdsList.Count > 0 && RotationVideoAdsList[AdIndexAtVideoRAdsList] == Ad_Type.Admob)
//            {
//                AdController.Instance.RequestAdmobRewardedAd();//Admob
//            }
//#endif
        //}
        //catch (Exception e)
        //{
        //    Debug.Log("Request Ad With Delay exception=" + e);
        //}
    }
    public void ShowAd()
    {
        Debug.Log("------ Show Ad");
        if (PlayerPrefs.GetString("NoAds", "false") == "true")
        {
            Debug.Log("----NoAds purchased so returning..");
            return;
        }
        if (isWifi_OR_Data_Availble())
        {
            if (AdIndexAtRAdsList >= 0 && RotationAdsList.Count > 0)// && Time.time >= (LastAdShownTime + AdDelay))
            {
                if (RotationAdsList[AdIndexAtRAdsList] == Ad_Type.IronSource)
                {
                    Debug.Log("----- ironsource Admobe Interstitial Ad");
#if !AdSetup_OFF
                    AdController.Instance.ShowIronSourceInterstitial();
#endif
                }
                else
                {
                    Debug.LogError("----------- Ads Not Activated");
                }
                LastAdShownTime = Time.time;
                setNextInterstitialAdIndex();

            }
            else
            {
                Debug.LogError("------- Wait for addelay or Ads Not Activated");
            }
        }
    }
    public void ShowRewardVideoWithCallback(RewardVideoCallback SuccessCallback)
    {
        Debug.Log("------------ ShowRewardVideoWithCallback Ad ----------- AdIndexAtVideoRAdsList=" + AdIndexAtVideoRAdsList);

        RewardSuccessEvent = SuccessCallback;
#if UNITY_EDITOR
        VideoWatchedSuccessfully(true);
#elif UNITY_ANDROID
        if (isWifi_OR_Data_Availble())
        {
            if (AdIndexAtVideoRAdsList >= 0 && RotationVideoAdsList.Count > 0)
            {
                if (RotationVideoAdsList[AdIndexAtVideoRAdsList] == Ad_Type.IronSource)
                {
                    //Admob video
                    Debug.Log("-------- Reward callback Show Admob reward based video");
                    AdController.Instance.ShowIronSourceRewardedVideo();
                }
                setNextVideoAdIndex();
                //RequestRewardVideo();
            }
        }
        else
        {
            ShowToast("Please check your Internet connection");
        }
#endif

    }
    public void VideoWatchedSuccessfully(bool isRewarded = true)
    {
        if (RewardSuccessEvent != null)
        {
            RewardSuccessEvent(isRewarded);
            RewardSuccessEvent = null;
        }

    }
    public void ShowLeaderBoards()
    {
#if !PlayServices_Off
            PlayServicesHandler.instance.ShowLeaderBoards();
#endif
    }
    public void ShowAchievements()
    {
#if !PlayServices_Off
             PlayServicesHandler.instance.ShowAchievements();
#endif
    }

    public void SubmitScore(int score)
    {
#if !PlayServices_Off
             PlayServicesHandler.instance.SubmitScoreToLB(score);
#endif
    }
    public void UnlockAchievements(string achievement)
    {
#if !PlayServices_Off
             PlayServicesHandler.instance.Check_UnlockAchievement(achievement);
#endif
    }
    public void ShowMoreGames()
    {
        Application.OpenURL("https://play.google.com/store/apps/dev?id=6143935442918635295");
        //WebViewController.instance.ShowMoreGames();
    }
    public void BuyItem(int index, bool IsConumable = false, bool IsNonConsumable = false, bool IsSubscription = false, InAppButtonReference Buttonreferencename = InAppButtonReference.None)
    {
#if !AdSetup_OFF
        IAPHandler.instance.BuyProductID(index, IsConumable, IsNonConsumable, IsNonConsumable, Buttonreferencename);
#endif
    }
    public bool isWifi_OR_Data_Availble()
    {
        if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork
            || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void ShowToast(string msg)
    {
        Debug.Log("ShowToast msg="+msg);
        ToastHandler.Instance.ShowToast(msg);
    }
    public void LoadFirstScene()
    {
        Debug.LogError("------- LoadFirst scene");
        //StartCoroutine(OpenMenuScene(0));
        SceneManager.LoadScene(1);
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        UnlockVehicleIAPHandler.instance.ReadyToDisplay = true;
        //      Debug.LogError("OnSceneLoaded: " + scene.name);
        //Debug.Log(mode);
        //      if (scene.buildIndex != 1) {
        //          HideSocialBtns ();
        //      }
        switch (scene.buildIndex)
        {
            case 1:
                          Debug.LogError ("OnSceneLoaded first scene="+SceneManager.GetActiveScene().name);
                LoadingPage.SetActive(false);
                RunActions(Actiontype.Launch_Ad);
                //          SocialBtnsController.instance.Open ();
                //Debug.Log("Adinpages00=" + AdInPages[0] + ":::IsMenuAdOpened=" + IsMenuadloaded);
                //if (AdInPages [0] == 1 && MenuAdPageHandler.instance.MenuAdImgNew.texture != null && !IsMenuAdOpened) {
                //          MenuAdPageHandler.instance.Open ();
                //  IsMenuAdOpened = true;
                //}

                break;
        }
    }
}
