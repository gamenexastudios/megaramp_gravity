﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class GiftBoxRewardHandler : MonoBehaviour
{
    public GameObject GiftBoxPanel;

    public GameObject PopUp;
    public GameObject CloseBtn;

    public Animator GiftBoxAnimator;

    public List<int> GiftAmount;
    public List<Sprite> CoinPilesSprite;

    public Image CoinsImage;

    public GameObject displayGiftAmountParent;

    private bool doubleReward = false;

    public GameObject ClaimBtn;

    public GameObject doubleRewardBtn;

    public ParticleSystem[] GBConfettiParticles;

    int recivedAmount = 0;

    public static GiftBoxRewardHandler instance;
    public Text displayGiftAmountText;
    public bool Active;
    public int SurpriseGiftMaxCount;

    public void Awake()
    {
        Active = false;
        SurpriseGiftMaxCount = 1;
        instance = this;
        GiftBoxPanel.SetActive(false);
    }

    public void Open()
    {
        displayGiftAmountParent.transform.localScale = Vector3.zero;

        iTween.Stop(PopUp);
        PopUp.transform.localScale = Vector3.one;
        iTween.ScaleFrom(PopUp, iTween.Hash("x", 0, "y", 0, "time", 0.25f, "delay", 0, "islocal", true, "easetype", iTween.EaseType.linear));

        iTween.Stop(CloseBtn);
        CloseBtn.transform.localScale = Vector3.one;
        iTween.ScaleFrom(CloseBtn, iTween.Hash("x", 0, "y", 0, "time", 0.25f, "delay", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));

        ClaimBtn.SetActive(true);
        doubleRewardBtn.SetActive(false);
        GiftBoxPanel.SetActive(true);
        for (int i = 0; i < GBConfettiParticles.Length; i++)
        {
            GBConfettiParticles[i].gameObject.SetActive(false);

        }
    }
    public void Close()
    {
        if(GiftBoxCoroutine != null)
        StopCoroutine(GiftBoxCoroutine);
        displayGiftAmountParent.transform.localScale = Vector3.zero;

        for (int i = 0; i < GBConfettiParticles.Length; i++)
        {
            GBConfettiParticles[i].Stop();
            GBConfettiParticles[i].gameObject.SetActive(false);
        }
        GiftBoxPanel.SetActive(false);
        if (!doubleReward)
        {
            StaticVariables._iTotalCoins += recivedAmount;
        }
        else
        {
            StaticVariables._iTotalCoins += (recivedAmount * 2);
        }
        if (MenuUIManager.Instance != null)
        {
            MenuUIManager.Instance.UpdateText();
        }
        GiftBoxAnimator.SetBool("isGiftBoxClicked", false);
        int ClaimCount = PlayerPrefs.GetInt("Surprise_Gift_ClaimCount", 0);
        ClaimCount++;
        PlayerPrefs.SetInt("Surprise_Gift_ClaimCount", ClaimCount);

    }

    Coroutine GiftBoxCoroutine;

    public void OnClick_GiftBox()
    {

        if (GiftBoxAnimator != null && GiftBoxAnimator.GetBool("isGiftBoxClicked") == false)
        {
            GiftBoxCoroutine = StartCoroutine(DoubleRewardPopUp());
        }
    }

    IEnumerator DoubleRewardPopUp()
    {
        GiftBoxAnimator.SetBool("isGiftBoxClicked", true);
        int value = Random.Range(0, GiftAmount.Count);
        recivedAmount = GiftAmount[value];
        CoinsImage.sprite = CoinPilesSprite[value];

        iTween.Stop(displayGiftAmountParent);
        displayGiftAmountParent.transform.localScale = Vector3.one;
        iTween.ScaleFrom(displayGiftAmountParent, iTween.Hash("x", 0, "y", 0, "time", 0.5f, "delay", 0.5f, "islocal", true, "easetype", iTween.EaseType.linear));

        displayGiftAmountText.text = recivedAmount.ToString();
        for (int i = 0; i < GBConfettiParticles.Length; i++)
        {
            GBConfettiParticles[i].gameObject.SetActive(true);
            GBConfettiParticles[i].Play();
        }

        yield return new WaitForSeconds(4f);
        ClaimBtn.SetActive(false);
        doubleRewardBtn.SetActive(true);
        //iTween.Stop(doubleRewardBtn);
        //doubleRewardBtn.transform.localScale = Vector3.one;
        //iTween.ScaleFrom(doubleRewardBtn, iTween.Hash("x", 0, "y", 0, "time", 0.3f, "delay", 0f, "islocal", true, "easetype", iTween.EaseType.linear));
        //yield return new WaitForSeconds(0.3f);
        //doubleRewardBtn.GetComponent<PopTweenCustom>().CallFromOtherScript();
    }

    public void DoubleYourReward()
    {
        AdManager.Instance.ShowRewardVideoWithCallback((result) =>
        {
            if (result)
            {
                if (AdManager.Instance != null)
                    AdManager.Instance.ShowToast("CONGRATS! YOU HAVE DOUBLED YOUR REWARD");
                doubleReward = true;
                Close();
            }
        });

    }
}
