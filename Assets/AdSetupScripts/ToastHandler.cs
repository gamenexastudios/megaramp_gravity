﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToastHandler : MonoBehaviour
{
    public GameObject mainObj;
    public Text ToastText;
    public static ToastHandler Instance;
    private void Awake()
    {
        Instance = this;
        mainObj.transform.localScale = Vector3.zero;
        mainObj.SetActive(false);
    }
    public void ShowToast(string str)
    {
        ToastText.text = str;
        mainObj.SetActive(false);
        mainObj.SetActive(true);

    }
    //public void Close()
    //{
    //    iTween.Stop(mainObj);
    //    iTween.ScaleTo(mainObj, iTween.Hash("x", 0, "y",0, "time", 0.4f, "islocal", true, "easetype", iTween.EaseType.easeInBack));
    //}
}
