﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using System;
using TMPro;

public class BuyItem : MonoBehaviour
{
	public enum InAppCategory
	{
		Consumable,
		NonConsumable,
        Subscription
    }
    public enum InAppType
    {
        None,
        UnlockAllLevels,
        UnlockAllUpgrades,
        UnlockFullGame,
        NoAds
    }

    //public enum InAppButtonReference
    //{
    //    None,
    //    UnlockItemPageButton,
    //    UnlockItemLockedButton,
    //    UnlockLevelsPageButton,
    //    UnlockLevelsLockedButton,
    //    StorePageItemButton,
    //    StorePageLevelButton,
    //    Levelcompetepagebutton,
    //    Levelfailpagebutton,
    //    StoreCoinpack1,
    //    StoreCoinpack2,
    //    StoreCoinpack3,
    //    StoreCoinpack4,
    //    StoreUnlimitedresumes,
    //    IngameUnlimitedresumes,
    //    Premiumpopupbutton1,
    //    Premiumpopupbutton2,
    //    Premiumpopupbutton3,
    //    Unlockitemmenupage,
    //    MenuCoinpack1,
    //    MenuCoinpack2,
    //    StorePremiumUpgrade,
    //    UnlockFullGameMenu,
    //    UnlockFullGameStore,
    //    UnlockResumesStore,
    //    UnlockResumesPreLF,
    //    UnlockAllUpgradsType,
    //    UnlockAllUpgradesDiscountType,
    //    UnlockAllLevelsType,
    //    UnlockAllLevelsDiscountType,
    //    UnlockFullGameType
    //}

    public int Index;
    //public int NonConsumableIndex,SubscriptionIndex;

    public Text PriceTxt;
	public bool ShowPrice = true;
	private int DiscountIndex = -1;

	public InAppCategory inAppCategory;
    public InAppType inAppType;

    public AdManager.InAppButtonReference CurrentButtonReference;
    void OnEnable ()
	{

        //try
        //{
            //switch (TypeOfProduct)
            //{
            //    case InAppType.UnlockAllUpgradsType:
            //        Index = IAPHandler.instance.UnlockAllUpgradesIndex;
            //        inAppCategory = InAppCategory.NonConsumable;
            //        break;
            //    case InAppType.UnlockAllUpgradesDiscountType:
            //        Index = IAPHandler.instance.UpgradesDiscountIndex;
            //        inAppCategory = InAppCategory.NonConsumable;
            //        break;
            //    case InAppType.UnlockAllLevelsType:
            //        Index = IAPHandler.instance.UnlockAllLevelsIndex;
            //        inAppCategory = InAppCategory.NonConsumable;
            //        break;
            //    case InAppType.UnlockAllLevelsDiscountType:
            //        Index = IAPHandler.instance.LevelsDiscountIndex;
            //        inAppCategory = InAppCategory.NonConsumable;
            //        break;
            //    case InAppType.UnlockFullGameType:
            //        Index = IAPHandler.instance.UnlockFullGameIndex;
            //        inAppCategory = InAppCategory.NonConsumable;
            //        break;
            //}
            if(!AdManager.Instance || !IAPHandler.instance)
            {
                Debug.LogError("----- AdManager | IAPHandler references not found");
                return;
            }

            //if (inAppType == InAppType.UnlockAllLevels || inAppType == InAppType.UnlockAllUpgrades || inAppType == InAppType.UnlockFullGame)
            //{
            //    if(inAppCategory == InAppCategory.Subscription)
            //    {
            //        Index = SubscriptionIndex;
            //    }
            //    else if (inAppCategory == InAppCategory.NonConsumable)
            //    {
            //        Index = NonConsumableIndex;
            //    }
            //    if (AdManager.Instance.IAPType_External == AdManager.InAppCategory.NonConsumable && inAppCategory == InAppCategory.Subscription)
            //    {
            //        inAppCategory = InAppCategory.NonConsumable;
            //        Index = NonConsumableIndex;
            //    }
            //    else if (AdManager.Instance.IAPType_External == AdManager.InAppCategory.Subscription && inAppCategory == InAppCategory.NonConsumable)
            //    {
            //        inAppCategory = InAppCategory.Subscription;
            //        Index = SubscriptionIndex;
            //    }
            //}
            if (PriceTxt == null && ShowPrice && gameObject.transform.childCount > 0)
            {
                PriceTxt = GetComponentInChildren<Text>();
            }
            if (inAppCategory == InAppCategory.Consumable)
            {
                gameObject.GetComponent<Button>().interactable = true;
                if (PlayerPrefs.HasKey("consumableProducts_" + Index) && ShowPrice)
                {
                    PriceTxt.text = PlayerPrefs.GetString(("consumableProducts_" + Index), "BUY");
                }
            }
            else if (inAppCategory == InAppCategory.NonConsumable)
            {
            if (IAPHandler.instance.m_StoreController != null)
            {
                gameObject.GetComponent<Button>().interactable = true;
                if (IAPHandler.instance.IsRestorableProduct(Index))
                {
                    //It mean it is already puchased
                    Debug.Log("------------ It is purchased Already");
                    gameObject.GetComponent<Button>().interactable = false;
                }
                else
                {

                    if (inAppType == InAppType.UnlockAllLevels && AdManager.LevelsUnlocked == 1)
                    {
                        gameObject.GetComponent<Button>().interactable = false;

                    }
                    else if (inAppType == InAppType.UnlockAllUpgrades && AdManager.UpgradeUnlocked == 1)
                    {
                        gameObject.GetComponent<Button>().interactable = false;
                    }
                    else if (inAppType == InAppType.UnlockFullGame && AdManager.LevelsUnlocked == 1 && AdManager.UpgradeUnlocked == 1)
                    {
                        gameObject.GetComponent<Button>().interactable = false;
                    }
                    else if (inAppType == InAppType.NoAds && (AdManager.LevelsUnlocked == 1 || AdManager.UpgradeUnlocked == 1 || PlayerPrefs.GetString("NoAds", "false") == "true"))
                    {
                        gameObject.GetComponent<Button>().interactable = false;
                    }


                }
                }
                //if (PlayerPrefs.HasKey("nonConsumableProducts_" + Index) && ShowPrice)
                //{
                //    PriceTxt.text = PlayerPrefs.GetString(("nonConsumableProducts_" + Index), "BUY");
                //}
                SetPriceOfItem();
            }
            
            else if (inAppCategory == InAppCategory.Subscription)
            {
                gameObject.GetComponent<Button>().interactable = true;
                if (inAppType == InAppType.UnlockAllLevels && AdManager.LevelsUnlocked == 1)
                {
                    gameObject.GetComponent<Button>().interactable = false;
                }
                else if (inAppType == InAppType.UnlockAllUpgrades && AdManager.UpgradeUnlocked == 1)
                {
                    gameObject.GetComponent<Button>().interactable = false;
                }
                else if (inAppType == InAppType.UnlockFullGame && AdManager.LevelsUnlocked == 1 && AdManager.UpgradeUnlocked == 1)
                {
                    gameObject.GetComponent<Button>().interactable = false;
                }
                //if (PlayerPrefs.HasKey("subscriptionProducts_" + Index) && ShowPrice)
                //{
                //    PriceTxt.text = PlayerPrefs.GetString(("subscriptionProducts_" + Index), "BUY");
                //}
                SetPriceOfItem();
            }


            gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            gameObject.GetComponent<Button>().onClick.AddListener(delegate () {
                this.BuyClicked(Index);
            });
           
        //}
        //catch(Exception e)
        //{
        //    Debug.LogError("-------- BuyItem OnEnable exception ="+e);
        //}
	}
    public void SetPriceOfItem()
    {
        Debug.LogError("-------- setPriceOfItem");
        if (inAppCategory == InAppCategory.NonConsumable)
        {
            if (PlayerPrefs.HasKey("nonConsumableProducts_" + Index) && ShowPrice)
            {
                PriceTxt.text = PlayerPrefs.GetString(("nonConsumableProducts_" + Index), "BUY");
            }
            else
            {
                //switch (inAppType)
                //{
                //    case InAppType.UnlockAllLevels:
                //        PriceTxt.text = "Unlock All Levels";
                //        break;
                //    case InAppType.UnlockAllUpgrades:
                //        PriceTxt.text = "Unlock All Upgrades";
                //        break;
                //    case InAppType.UnlockFullGame:
                //        PriceTxt.text = "Unlock Full Game";
                //        break;
                //}
            }
        }
        else if (inAppCategory == InAppCategory.Subscription)
        {
            if (PlayerPrefs.HasKey("subscriptionProducts_" + Index) && ShowPrice)
            {
                PriceTxt.text = PlayerPrefs.GetString(("subscriptionProducts_" + Index), "BUY");
            }
            else
            {
                //switch (inAppType)
                //{
                //    case InAppType.UnlockAllLevels:
                //        PriceTxt.text = "Free - Unlock All Levels";
                //        break;
                //    case InAppType.UnlockAllUpgrades:
                //        PriceTxt.text = "Free - Unlock All Upgrades";
                //        break;
                //    case InAppType.UnlockFullGame:
                //        PriceTxt.text = "Free - Unlock Full Game";
                //        break;
                //}
            }
        }
    }
	void OnDisable ()
	{
		IAPHandler.InAppSuccessCallBack -= SuccessCallBack;
		IAPHandler.InAppSFailCallBack -= FailCallBack;
	}
	// Use this for initialization
	void Start ()
	{
       
	}

	public void BuyClicked (int InAppIndex)
	{
        try { 
		    Debug.Log ("----- BuyClicked Index="+InAppIndex);
		    if (inAppCategory == InAppCategory.NonConsumable) {
			    IAPHandler.InAppSuccessCallBack += SuccessCallBack;
			    IAPHandler.InAppSFailCallBack += FailCallBack;
		    }
            if (inAppCategory == InAppCategory.NonConsumable)
            {
                AdManager.Instance.BuyItem(InAppIndex, false, true, false,CurrentButtonReference);
            }
            else if (inAppCategory == InAppCategory.Consumable)
            {
                AdManager.Instance.BuyItem(InAppIndex, true, false, false, CurrentButtonReference);
            }
            else
            {
                AdManager.Instance.BuyItem(InAppIndex, false, false, true, CurrentButtonReference);
            }
        }
        catch(Exception e)
        {
            Debug.LogError("-------- BuyItem BuyClicked exception ="+e);
        }
	}

	public void SuccessCallBack ()
	{
		Debug.Log ("-------------- InAppSuccessCallBack");
        try
        {
            IAPHandler.InAppSuccessCallBack -= SuccessCallBack;
		    IAPHandler.InAppSFailCallBack -= FailCallBack;
		    if (inAppCategory == InAppCategory.NonConsumable) {
			    gameObject.GetComponent<Button> ().interactable = false;
		    } else {
			    gameObject.GetComponent<Button> ().interactable = true;
		    }
        }
        catch (Exception e)
        {
            Debug.LogError("Buy Item Success CallBack error=" + e);
        }
    }

	public void FailCallBack ()
	{
        try
        {
            Debug.Log("-------------- InAppFailedCallBack");
            gameObject.GetComponent<Button>().interactable = true;
        }
        catch(Exception e)
        {
            Debug.LogError("Buy Item Fail CallBack error="+e);
        }
	}

}
