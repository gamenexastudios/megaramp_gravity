﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuAdPageHandler : MonoBehaviour {
	//public Image MenuAdImg;
	public RawImage MenuAdImgNew=null;

	public GameObject CloseBtn;
    public GameObject MainObj;

    public static MenuAdPageHandler instance;
	void Awake()
	{
		instance = this;
        MainObj.SetActive (false);
	}
	public void Open()
	{
		//Invoke ("showMenuAd",AdManager.instance.menuAdDelay);
        showMenuAd();
        AdManager.Instance.IsMenuAdShown = true;
		//AdManager.instance.SetCurrentMenuAdIndex();
	}
	void showMenuAd()
	{
        Debug.Log("----- show Menu ad");
        MainObj.SetActive (true);
        PlayerPrefs.SetInt("CurrentMenuAdIndex", AdManager.Instance.CurrentMenuAdIndex);
        //MenuAdImg.CrossFadeAlpha (1, 1, true);
        CloseBtn.transform.localScale = Vector3.zero;

        Invoke("EnableCloseBtn",0.1f);
	}
    void EnableCloseBtn()
    {
        CloseBtn.transform.localScale = Vector3.one;
        iTween.ScaleFrom(CloseBtn, iTween.Hash("x", 0, "y", 0, "time", 0.4f, "delay", 2.5f, "easetype", iTween.EaseType.spring));
    }
    public void Close()
	{
        MainObj.SetActive (false);
        //AdManager.Instance.IsMenuAdShown = false;
    }
    public void MenuAdClick()
	{
		Debug.Log ("MenuAdLink="+AdManager.Instance.MenuAdLinkTo);
    //market://details?id=com.milliongames.megarampstunts.cardriving.rampjumping.games
        Application.OpenURL ("market://details?id="+AdManager.Instance.MenuAdLinkTo);
		Close ();
        if (FirebaseController.Instance != null)
            FirebaseController.Instance.LogFirebaseEvent("MenuAd_Click" , "", "");

    }
    //public void SetMenuAdTexture()
    //{
    //	MenuAdImg.GetComponent<Image> ().sprite = AdManager.instance.menuAdImg;
    //	//MenuAdImgNew.texture
    //}
}
