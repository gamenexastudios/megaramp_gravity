﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;
public class IAPCallbacksHandler : MonoBehaviour
{
    public static IAPCallbacksHandler instance;

    //public BtnAct_AdsRelated btnActInstance;
    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        //btnActInstance = GetComponent<BtnAct_AdsRelated>();
    }
    public void CheckSpecialPackIAP(string productID,bool IsConsumable=false)
    {
        if (SpecialPackRemoteData.Instance != null && String.Equals(productID, SpecialPackRemoteData.Instance.ProductID_Purchase))
        {
            PlayerPrefs.SetString(SpecialPackRemoteData.Instance.unique_index, "true");
            if (IsConsumable)
            {
                int maxCount = PlayerPrefs.GetInt(SpecialPackRemoteData.Instance.unique_index + "_MaxCount", 0);
                maxCount++;
                PlayerPrefs.SetInt(SpecialPackRemoteData.Instance.unique_index + "_MaxCount", maxCount);
            }
        }
    }
    public void InAppCallBacks(string productID, bool IsSuccessAndAvailable = true)
    {
        Debug.Log("Purchase Success product ID=" + (productID));

        PlayerPrefs.SetString("NoAds", "true");
        try
        {
            //Check products array size and delate if not required..
            #region ConsumableProducts
            for (int i = 0; i < IAPHandler.instance.ConsumableProducts.Length; i++)
            {
                if (String.Equals(productID, IAPHandler.instance.ConsumableProducts[i], StringComparison.Ordinal))
                {

                    switch (i)
                    {
                        case 0:
                            if (MenuUIManager.Instance)
                                MenuUIManager.Instance.PackPurchase_Success(1);
                            break;
                        case 1:
                            if (MenuUIManager.Instance)
                                MenuUIManager.Instance.PackPurchase_Success(2);
                            break;
                        case 2:
                            if (MenuUIManager.Instance)
                                MenuUIManager.Instance.PackPurchase_Success(3);
                            break;
                    }

                    CheckSpecialPackIAP(productID, true);
                    break;
                }
            }
            //if (IAPHandler.instance.ConsumableProducts.Length>=1 && String.Equals(productID, IAPHandler.instance.ConsumableProducts[0], StringComparison.Ordinal))
            //{
            //    MenuUIManager.Instance.PackPurchase_Success(1);
            //}
            //else if (IAPHandler.instance.ConsumableProducts.Length >=2 && String.Equals(productID, IAPHandler.instance.ConsumableProducts[1], StringComparison.Ordinal))
            //{
            //    MenuUIManager.Instance.PackPurchase_Success(2);
            //}
            //else if (IAPHandler.instance.ConsumableProducts.Length >= 3 && String.Equals(productID, IAPHandler.instance.ConsumableProducts[2], StringComparison.Ordinal))
            //{
            //    MenuUIManager.Instance.PackPurchase_Success(3);
            //}
            #endregion

            #region NonConsumableProducts

            for (int i = 0; i < IAPHandler.instance.NonConsumableProducts.Length; i++)
            {
                if (String.Equals(productID, IAPHandler.instance.NonConsumableProducts[i], StringComparison.Ordinal))
                {
                    Debug.LogError("Purchase Success INDEX Value = " + i);
                    switch (i)
                    {
                        case 0:
                            //add firebase logic here
                            BtnAct_CallIAPBuy(0); // Ads
                            break;
                        //case 1:
                        //    if (MenuUIManager.Instance)
                        //        BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(1); // Unlock All levels
                        //    break;
                        case 1:
                            BtnAct_CallIAPBuy(2); // Unlock All worlds
                            break;
                        case 2:
                            BtnAct_CallIAPBuy(3); // Unlock All Vehicles
                            break;
                        case 3:
                            BtnAct_CallIAPBuy(4); // Unlock All (Vehicles, worlds and No Ads)
                            break;
                        case 4:
                            BtnAct_CallIAPBuy(4); // Unlock All (Vehicles, worlds and No Ads)
                            break;
                        case 5:
                            BtnAct_CallIAPBuy(4); // Unlock All (Vehicles, worlds and No Ads)
                            break;
                        case 6:
                            BtnAct_CallIAPBuy(4); // Unlock All (Vehicles, worlds and No Ads)
                            break;
                        case 7:
                            BtnAct_CallIAPBuy(4); // Unlock All (Vehicles, worlds and No Ads)
                            break;
                        case 8:
                            if (UnlockVehicleIAPHandler.instance != null)
                            {
                                UnlockVehicleIAPHandler.instance.BuyCarWithIAP();
                            }
                            break;

                    }
                    CheckSpecialPackIAP(productID);
                    break;
                }
            }

            //if (IAPHandler.instance.NonConsumableProducts.Length >= 1 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[0], StringComparison.Ordinal))             
            //{
            //    BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(0); // Ads
            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 2 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[1], StringComparison.Ordinal))   
            //{
            //    BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(1); // Unlock All levels
            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 3 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[2], StringComparison.Ordinal))
            //{
            //    BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(2); // Unlock All worlds
            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 4 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[3], StringComparison.Ordinal))
            //{
            //    BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(3); // Unlock All Vehicles
            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 5 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[4], StringComparison.Ordinal))
            //{
            //    BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(4); // Unlock All (Vehicles, worlds and No Ads)
            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 6 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[5], StringComparison.Ordinal))
            //{

            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 6 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[6], StringComparison.Ordinal))
            //{
            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 6 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[7], StringComparison.Ordinal))
            //{
            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 6 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[8], StringComparison.Ordinal))
            //{
            //}
            //else if (IAPHandler.instance.NonConsumableProducts.Length >= 6 && String.Equals(productID, IAPHandler.instance.NonConsumableProducts[9], StringComparison.Ordinal))
            //{
            //}


            #endregion
            #region SubscriptionProducts
            if (IAPHandler.instance.SubscriptionProducts.Length >= 1 && String.Equals(productID, IAPHandler.instance.SubscriptionProducts[0], StringComparison.Ordinal))
            {
                if (IsSuccessAndAvailable)
                {
                    // Add Subscription Success logic
                    AdManager.SubscribeUnlocked = 1;

                }
                else
                {
                    AdManager.SubscribeUnlocked = 0;
                    // Add Subscription Expriry Logic
                }
            }
            else if (IAPHandler.instance.SubscriptionProducts.Length >= 2 && String.Equals(productID, IAPHandler.instance.SubscriptionProducts[1], StringComparison.Ordinal))
            {
                if (IsSuccessAndAvailable)
                {
                    // Add Subscription Success logic
                }
                else
                {
                    // Add Subscription Expriry Logic
                }
            }
            #endregion


        }
        catch (Exception e)
        {
            Debug.Log("InAppCallBacks exception =" + e);
        }
    }
    //public void AddCoins(int coins,AdManager.RewardDescType rewardDescType=AdManager.RewardDescType.WatchVideo,AdManager.RewardType rewardType=AdManager.RewardType.Coins)
    //{
    //	//Add coins to store

    //	if(rewardType!=AdManager.RewardType.WatchVideoAgain && rewardType!=AdManager.RewardType.WelcomeGift)
    //	{
    //		CoinsClaimPopHandler.instance.Open(coins,rewardDescType);
    //	}
    //}

    public void BtnAct_CallIAPBuy(int _index)
    {
        Debug.LogError("BtnAct_CallIAPBuy INDEX = " + _index);

#if !AdSetup_OFF

        //  Social.ReportProgress(GPGSIds.achievement_get_stuff, 100.0f, (bool success) => { });
        //Social.ReportProgress(GPGSIds.achievement_1st_bag, 100.0f, (bool success) => { });

#endif
        if (_index == 0) // NO ADS
        {
            Debug.Log("IAP CALL HERE : NOADS");
            PlayerPrefs.SetInt(MyGamePrefs.Is_NoAds, 1);
        }
        if (_index == 1) // UNLOCK ALL LEVELS IN SPECIFIC WORLD
        {
            Debug.Log("IAP CALL HERE : UNLOCK ALL LEVELS");
            //StaticVariables._iUnlockedLevels = StaticVariables._iTotalLevels;
        }
        if (_index == 2) // UNLOCK ALL WORLDS
        {
            Debug.LogError("IAP CALL HERE : UNLOCK ALL worlds");

            StaticVariables.UnlockedTheme = "Theme01";
            StaticVariables._iUnlockedLevels = 15;
            StaticVariables.UnlockedTheme = "Theme02";
            StaticVariables._iUnlockedLevels = 30;
            StaticVariables.UnlockedTheme = "Theme03";
            StaticVariables._iUnlockedLevels = 45;
            StaticVariables.UnlockedTheme = "Theme04";
            StaticVariables._iUnlockedLevels = 60;
            StaticVariables.UnlockedTheme = "Theme05";
            StaticVariables._iUnlockedLevels = 75;
            StaticVariables._iUnlockedWorldsCount = 4;
            if (AdManager.Instance != null)
            {
                AdManager.LevelsUnlocked = 1;
                Debug.LogError("UNLOCKED WORLDS ######  AdManager.LevelsUnlocked  " + AdManager.LevelsUnlocked);
            }
        }
        if (_index == 3) // UNLOCK ALL CARS
        {
            Debug.Log("IAP CALL HERE : UNLOCK CARS");

            PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, "1111111111111");
            if (AdManager.Instance != null)
            {
                AdManager.UpgradeUnlocked = 1;
            }
        }
        if (_index == 4) // UNLOCK EVERYTHING ( CARS + WORLDS + NOADS)
        {
            Debug.Log("IAP CALL HERE : UNLOCK ALL LEVELS AND CARS");
            StaticVariables.UnlockedTheme = "Theme01";
            StaticVariables._iUnlockedLevels = 15;
            StaticVariables.UnlockedTheme = "Theme02";
            StaticVariables._iUnlockedLevels = 30;
            StaticVariables.UnlockedTheme = "Theme03";
            StaticVariables._iUnlockedLevels = 45;
            StaticVariables.UnlockedTheme = "Theme04";
            StaticVariables._iUnlockedLevels = 60;
            StaticVariables.UnlockedTheme = "Theme05";
            StaticVariables._iUnlockedLevels = 75;
            StaticVariables._iUnlockedWorldsCount = 4;
            PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, "1111111111111");
            PlayerPrefs.SetInt(MyGamePrefs.Is_NoAds, 1);
            if (AdManager.Instance != null)
            {
                AdManager.LevelsUnlocked = 1;
                AdManager.UpgradeUnlocked = 1;
            }
            if(SplPackHandler.Instance!=null)
            {
                SplPackHandler.Instance.Close();
            }
        }

        //_gIndex = _index + 1;
        //Invoke("InactivatePurchaseButton", 1f);
    }

}
