﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdController : MonoBehaviour
{





    public static AdController Instance;
    public string IronSourceID;

    private bool IsIronSourceBannerAdLoaded;
    private bool IsReadyToRequestIronSourceBannerAd;

    //[HideInInspector]
    //public bool ShowIronSourceBannerAdAfterLoad;
    [HideInInspector]
    public bool IsEnableIronSourceInterstitial, IsEnableIronSourceReward;
   
    private bool IsVideoRewarded;

    private bool ReadyToRequestNextRewardVideoAd;
    private bool ReadyToRequestNextInterstitialAd;


    [HideInInspector]
    public int possibleInterstitialRequestTypeCount = 0;
    [HideInInspector]
    public int possibleRewardedRequestTypeCount = 0;
    [HideInInspector]
    public int RequestInterstitialLoopCheckCount = 0;
    [HideInInspector]
    public int RequestRewardedLoopCheckCount = 0;

    private AdType CurrentInterstitialAdType_Request;
    private AdType CurrentRewardAdType_Request;

    public enum AdType
    {
        Admob,
        IronSource,
        Facebook,
        Unity

    }
    private AdType CurrentInterstitialAdType_Show;
    private AdType CurrentRewardAdType_Show;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        IsIronSourceBannerAdLoaded = false;
        IsReadyToRequestIronSourceBannerAd = true;

        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

        IronSource.Agent.shouldTrackNetworkState(true);

        IronSourceEvents.onBannerAdLoadedEvent += BannerAdLoadedEvent;
        IronSourceEvents.onBannerAdLoadFailedEvent += BannerAdLoadFailedEvent;
        IronSourceEvents.onBannerAdClickedEvent += BannerAdClickedEvent;
        IronSourceEvents.onBannerAdScreenPresentedEvent += BannerAdScreenPresentedEvent;
        IronSourceEvents.onBannerAdScreenDismissedEvent += BannerAdScreenDismissedEvent;
        IronSourceEvents.onBannerAdLeftApplicationEvent += BannerAdLeftApplicationEvent;

        IronSource.Agent.validateIntegration();
        Debug.Log("IRON SOURCE ID : " + IronSourceID);
        IronSource.Agent.init(IronSourceID);

    }

    void OnEnable()
    {
        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
        IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
        IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
        IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
        IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
    }
    public void OnDestroy()
    {
        Debug.Log("---------- AdController OnDestroy");

        DestroyIronSourceBannerAd();
    }
    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }
    public void IronSourceValidateCheck()
    {
        IronSource.Agent.validateIntegration();
    }
    //Invoked when the initialization process has failed.
    //@param description - string - contains information about the failure.
    void InterstitialAdLoadFailedEvent(IronSourceError error)
    {
        Debug.Log("---- IronSource InterstitialAdLoadFailedEvent="+error);
    }
    //Invoked when the ad fails to show.
    //@param description - string - contains information about the failure.
    void InterstitialAdShowFailedEvent(IronSourceError error)
    {
        Debug.Log("---- IronSource InterstitialAdShowFailedEvent");
    }
    // Invoked when end user clicked on the interstitial ad
    void InterstitialAdClickedEvent()
    {
        Debug.Log("---- IronSource InterstitialAdClickedEvent");
    }
    //Invoked when the interstitial ad closed and the user goes back to the application screen.
    void InterstitialAdClosedEvent()
    {
        Debug.Log("---- IronSource InterstitialAdClosedEvent");
        AdManager.Instance.RequestAd();
    }
    //Invoked when the Interstitial is Ready to shown after load function is called
    void InterstitialAdReadyEvent()
    {
        Debug.Log("---- IronSource InterstitialAdReadyEvent");
    }
    //Invoked when the Interstitial Ad Unit has opened
    void InterstitialAdOpenedEvent()
    {
        Debug.Log("---- IronSource InterstitialAdOpenedEvent");
    }
    //Invoked right before the Interstitial screen is about to open. NOTE - This event is available only for some of the networks. 
    //You should treat this event as an interstitial impression, but rather use InterstitialAdOpenedEvent
    void InterstitialAdShowSucceededEvent()
    {
        Debug.Log("---- IronSource InterstitialAdShowSucceededEvent");
    }
    //used this for unity ads
    //private void Update()
    //{
    //    //Debug.Log("AdManager update");
    //    if (ReadyToRequestNextRewardVideoAd)
    //    {
    //        Debug.Log("---------- Requesting reward video again from update");
    //        AdManager.Instance.RequestRewardVideo();
    //        ReadyToRequestNextRewardVideoAd = false;
    //    }
    //    if (ReadyToRequestNextInterstitialAd)
    //    {
    //        Debug.Log("---------- Requesting Interstitial Ad again from update");
    //        AdManager.Instance.RequestAd();
    //        ReadyToRequestNextInterstitialAd = false;
    //    }
    //}
#if !AdSetup_OFF

    public void RequestIronSourceInterstitial()
    {
        Debug.Log("-------- Request IronSource Interstitial");
        if (IronSource.Agent.isInterstitialReady())
        {
            Debug.Log("-------- Request IronSource Interstitial already loaded so returning");
            return;
        }
        RequestInterstitialLoopCheckCount++;
        CurrentInterstitialAdType_Request = AdType.IronSource;

        IronSource.Agent.loadInterstitial();
    }


    public void ShowIronSourceInterstitial()
    {
        Debug.Log("------ Show IronSource Interstitial");
        if (IronSource.Agent.isInterstitialReady())
        {
            IronSource.Agent.showInterstitial();
            FirebaseController.Instance.LogFirebaseEvent("Interstitial_Show", "", "");

        }
        else
        {
            Debug.Log("------- IronSource Interstitial not ready try for other interstitial");
            CurrentInterstitialAdType_Show = AdType.IronSource;
            AdManager.Instance.RequestAd();
        }
    }

    /// <summary>
    /// IronSource Reward ad
    /// </summary>
    //Invoked when the RewardedVideo ad view has opened.
    //Your Activity will lose focus. Please avoid performing heavy 
    //tasks till the video ad will be closed.
    void RewardedVideoAdOpenedEvent()
    {
        Debug.Log("---- IronSource RewardedVideoAdOpenedEvent");
    }
    //Invoked when the RewardedVideo ad view is about to be closed.
    //Your activity will now regain its focus.
    void RewardedVideoAdClosedEvent()
    {
        Debug.Log("---- IronSource RewardedVideoAdClosedEvent");

        try
        {
            if (!IsVideoRewarded)
            {
                AdManager.Instance.VideoWatchedSuccessfully(false);
            }
            else
            {
                AdManager.Instance.VideoWatchedSuccessfully(true);
            }

            //AdManager.Instance.RequestRewardVideo();
        }
        catch (Exception e)
        {
            Debug.LogError("------- Admob Exception e=" + e.Message);
        }
    }
    //Invoked when there is a change in the ad availability status.
    //@param - available - value will change to true when rewarded videos are available. 
    //You can then show the video by calling showRewardedVideo().
    //Value will change to false when no videos are available.
    void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        Debug.Log("---- IronSource RewardedVideoAvailabilityChangedEvent");

        //Change the in-app 'Traffic Driver' state according to availability.
        bool rewardedVideoAvailability = available;
    }

    //Invoked when the user completed the video and should be rewarded. 
    //If using server-to-server callbacks you may ignore this events and wait for 
    // the callback from the  ironSource server.
    //@param - placement - placement object which contains the reward data
    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        Debug.Log("---- IronSource RewardedVideoAdRewardedEvent");
        IsVideoRewarded = true;
    }
    //Invoked when the Rewarded Video failed to show
    //@param description - string - contains information about the failure.
    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        Debug.Log("---- IronSource RewardedVideoAdShowFailedEvent");
    }

    // ----------------------------------------------------------------------------------------
    // Note: the events below are not available for all supported rewarded video ad networks. 
    // Check which events are available per ad network you choose to include in your build. 
    // We recommend only using events which register to ALL ad networks you include in your build. 
    // ----------------------------------------------------------------------------------------

    //Invoked when the video ad starts playing. 
    void RewardedVideoAdStartedEvent()
    {
        Debug.Log("---- IronSource RewardedVideoAdStartedEvent");
    }
    //Invoked when the video ad finishes playing. 
    void RewardedVideoAdEndedEvent()
    {
        Debug.Log("---- IronSource RewardedVideoAdEndedEvent");
    }
    void RewardedVideoAdClickedEvent(IronSourcePlacement placement)
    {
        Debug.Log("---- IronSource RewardedVideoAdClickedEvent");
    }

    public void ShowIronSourceRewardedVideo()
    {
        Debug.Log("-------- Show IronSource RewardedAd is Available="+(IronSource.Agent.isRewardedVideoAvailable()));
        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IsVideoRewarded = false;
            IronSource.Agent.showRewardedVideo();
        }
        else
        {
            Debug.Log("------- Admob Reward not ready try for other Reward");

            CurrentRewardAdType_Show = AdType.IronSource;

            AdManager.Instance.ShowToast("Video ad not available please check later");
        }
    }


    /// <summary>
    /// IronSource Reward Ad Close
    /// </summary>

    //////// Iron Source Banner
    //Invoked once the banner has loaded
    void BannerAdLoadedEvent()
    {
        IsIronSourceBannerAdLoaded = true;
        AdManager.Instance.CheckNShowBannerAd();
        Debug.Log("---- IronSource BannerAdLoadedEvent");
    }
    //Invoked when the banner loading process has failed.
    //@param description - string - contains information about the failure.
    void BannerAdLoadFailedEvent(IronSourceError error)
    {
        IsIronSourceBannerAdLoaded = false;
        IsReadyToRequestIronSourceBannerAd = true;
        Debug.Log("---- IronSource BannerAdLoadFailedEvent");
    }
    // Invoked when end user clicks on the banner ad
    void BannerAdClickedEvent()
    {
        Debug.Log("---- IronSource BannerAdClickedEvent");
    }
    //Notifies the presentation of a full screen content following user click
    void BannerAdScreenPresentedEvent()
    {
        Debug.Log("---- IronSource BannerAdScreenPresentedEvent");
    }
    //Notifies the presented screen has been dismissed
    void BannerAdScreenDismissedEvent()
    {
        Debug.Log("---- IronSource BannerAdScreenDismissedEvent");
    }
    //Invoked when the user leaves the app
    void BannerAdLeftApplicationEvent()
    {
        Debug.Log("---- IronSource BannerAdLeftApplicationEvent");
    }
    public void RequestIronSourceBannerAd()
    {
        Debug.Log("---- IronSource RequestIronSourceBannerAd="+ IsIronSourceBannerAdLoaded);
        if(!IsReadyToRequestIronSourceBannerAd)
        {
            return;
        }
        if (IsIronSourceBannerAdLoaded)
        {
            return;
        }
        IsReadyToRequestIronSourceBannerAd = false;
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
    }
    public void ShowIronSourceBannerAd()
    {
        Debug.Log("---- IronSource ShowIronSourceBannerAd="+ IsIronSourceBannerAdLoaded);
        if (IsIronSourceBannerAdLoaded)
        {
            IronSource.Agent.displayBanner();
        }
        else
        {
            RequestIronSourceBannerAd();
        }
    }
    public void HideIronSourceBannerAd()
    {
        Debug.Log("---- IronSource HideIronSourceBannerAd");
        if (IsIronSourceBannerAdLoaded)
        {
            IronSource.Agent.hideBanner();
        }
    }
    public void DestroyIronSourceBannerAd()
    {
        Debug.Log("---- IronSource DestroyIronSourceBannerAd");
        if (IsIronSourceBannerAdLoaded)
        {
            IronSource.Agent.destroyBanner();
        }
    }
    /// <summary>
    /// IronSource Banner Close
    /// </summary>

#endif
}
