﻿using Unity;
using System;
using UnityEngine;

[Serializable]
public class SpecialPackRemoteData : MonoBehaviour
{
    public static SpecialPackRemoteData Instance;
    public bool Active;
    public string Pack_type;
    public string unique_index;
    public string ProductID_Price;
    public string ProductID_Purchase;
    public string Pack_Name;
    public int MaxCount;
    //public string IapType;
    public string Season;


    private void Awake()
    {
        Instance = this;
    }

}


