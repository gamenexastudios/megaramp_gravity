﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectionSample : MonoBehaviour
{
    public static LevelSelectionSample Instance;
    void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }
    public void Open()
    {
        gameObject.SetActive(true);
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.PageType.LvlSelection);
        }
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void SelectLevel()
    {
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.Actiontype.LvlSelection_Play);
        }
        UpgradeSample.Instance.Open();
        Close();
    }
    public void BackClick()
    {
        Close();
        MenuSample.Instance.Open();

    }
}
