﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelCompletePageSample : MonoBehaviour
{
    public static LevelCompletePageSample Instance;
    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }
    public void Open()
    {
        gameObject.SetActive(true);
        if (AdManager.Instance)
        {
            AdManager.Instance.RunActions(AdManager.PageType.LCPage);
        }
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
    public void NextClick()
    {
        AdManager.Instance.RunActions(AdManager.Actiontype.LC_Next,3);
        SceneManager.LoadScene("MenuSample");
    }
    public void RetryClick()
    {
        SceneManager.LoadScene("InGameSample");
    }
}
