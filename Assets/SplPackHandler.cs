﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;
public class SplPackHandler : MonoBehaviour
{

    public static SplPackHandler Instance;
    public GameObject OfferPercentageObj;
    public Text OfferPercentageText;
    public Text OriginalPriceText, OfferPriceText;
    public Text TitleTxt;
    public InAppCategory _InAppCategory;
    public int OfferPrice, OriginalPrice,percentage;
    public int IAPIndex;
    public string ProductID_Purchase;
    public GameObject NonConsumableObj, ConsumableObj;
    public GameObject PopUp, CloseBtn;
    public enum InAppCategory
    {
        UnDefined,
        Consumable,
        NonConsumable,
        Subscription
    }
    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }
    public void CheckNOpenSplPack()
    {
        Debug.Log("----- Check N Open Spl Pack");
        if (AdManager.Instance != null)
        {
            AdManager.Instance.IsForceHideBannerAd = false;
        }
        if (AdManager.Instance != null)
        {
            AdManager.Instance.CheckNShowBannerAd();
        }
        if (GiftBoxRewardHandler.instance != null && GiftBoxRewardHandler.instance.Active && PlayerPrefs.GetInt("Surprise_Gift_ClaimCount",0)<GiftBoxRewardHandler.instance.SurpriseGiftMaxCount)
        {
            GiftBoxRewardHandler.instance.Open();
        }
        else
        {
            GetIAPTypeNIndex();
            if (SpecialPackRemoteData.Instance != null && SpecialPackRemoteData.Instance.Active)
            {
                if (_InAppCategory == InAppCategory.Consumable)
                {

                    int maxCount = PlayerPrefs.GetInt(SpecialPackRemoteData.Instance.unique_index + "_MaxCount", 0);
                    Debug.Log("max count=" + maxCount + " :: spl max count=" + SpecialPackRemoteData.Instance.MaxCount);
                    //if (!PlayerPrefs.HasKey(SpecialPackRemoteData.Instance.unique_index) && maxCount < SpecialPackRemoteData.Instance.MaxCount)
                    if (maxCount < SpecialPackRemoteData.Instance.MaxCount)
                    {
                        Open();
                        //maxCount++;
                        //PlayerPrefs.SetInt(SpecialPackRemoteData.Instance.unique_index + "_MaxCount", maxCount);
                    }
                }
                else if (_InAppCategory == InAppCategory.NonConsumable)
                {
                    if (!PlayerPrefs.HasKey(SpecialPackRemoteData.Instance.unique_index) && (AdManager.LevelsUnlocked != 1 || AdManager.UpgradeUnlocked != 1))
                    {
                        Open();
                    }
                }
            }
        }
    }
    public void Open()
    {
        Debug.Log("---- unlock all open");
        gameObject.SetActive(true);
        //Tween_SpecialPack._instance.Call_TweenIn();
        GetIAPTypeNIndex();
        //return;
        ConsumableObj.SetActive(false);
        NonConsumableObj.SetActive(false);

        if (_InAppCategory==InAppCategory.Consumable)
        {
            ConsumableObj.SetActive(true);
        }
        else if(_InAppCategory==InAppCategory.NonConsumable)
        {
            NonConsumableObj.SetActive(true);
        }

        iTween.Stop(PopUp);
        PopUp.transform.localScale = Vector3.one;
        iTween.ScaleFrom(PopUp, iTween.Hash("x", 0, "y", 0, "time", 0.25f, "delay", 0, "islocal", true, "easetype", iTween.EaseType.linear));

        iTween.Stop(CloseBtn);
        CloseBtn.transform.localScale = Vector3.one;
        iTween.ScaleFrom(CloseBtn, iTween.Hash("x", 0, "y", 0, "time", 0.25f, "delay", 0.25f, "islocal", true, "easetype", iTween.EaseType.linear));

    }
    void GetIAPTypeNIndex()
    {
        _InAppCategory = InAppCategory.UnDefined;
        percentage = 0;
        if (SpecialPackRemoteData.Instance!=null && !string.IsNullOrEmpty(SpecialPackRemoteData.Instance.ProductID_Purchase))
        {
            ProductID_Purchase = SpecialPackRemoteData.Instance.ProductID_Purchase;
            if(!string.IsNullOrEmpty(SpecialPackRemoteData.Instance.Pack_type))
            {
                TitleTxt.text = SpecialPackRemoteData.Instance.Pack_type;
            }
        }
        for (int i = 0; i < IAPHandler.instance.NonConsumableProducts.Length; i++)
        {
            if (ProductID_Purchase.Equals(IAPHandler.instance.NonConsumableProducts[i]))
            {
                _InAppCategory = InAppCategory.NonConsumable;

                IAPIndex = i;
                break;
            }
        }

        if (_InAppCategory == InAppCategory.UnDefined)
        {
            for (int i = 0; i < IAPHandler.instance.ConsumableProducts.Length; i++)
            {
                if (ProductID_Purchase.Equals(IAPHandler.instance.ConsumableProducts[i]))
                {
                    _InAppCategory = InAppCategory.Consumable;

                    IAPIndex = i;
                    break;
                }
            }
        }
        CheckNDisplayPercentage();

    }
    Product OriginalProduct = null;
    Product OfferProduct = null;
    void CheckNDisplayPercentage()
    {
        if (AdManager.Instance != null && AdManager.Instance.isWifi_OR_Data_Availble() && SpecialPackRemoteData.Instance != null && IAPHandler.instance !=null && IAPHandler.instance.m_StoreController!=null)
        {
            OriginalProduct = IAPHandler.instance.m_StoreController.products.WithID(SpecialPackRemoteData.Instance.ProductID_Price);
            OfferProduct = IAPHandler.instance.m_StoreController.products.WithID(SpecialPackRemoteData.Instance.ProductID_Purchase);
            if (OriginalProduct != null && OfferProduct != null)
            {
                OfferPrice = (int)(OfferProduct.metadata.localizedPrice);
                OriginalPrice = (int)(OriginalProduct.metadata.localizedPrice);
                if (OriginalPrice > 0)// in unity editor it will come zero
                {
                    percentage = 100 - (100 * OfferPrice / OriginalPrice);
                }
            }
        }

        if (percentage == 0)
        {
            HideOfferDetails();
        }
        else
        {
            OfferPercentageObj.SetActive(true);
            OfferPriceText.gameObject.SetActive(true);
            OriginalPriceText.gameObject.SetActive(true);

            OfferPercentageText.text = percentage + "% OFF";
            if (OriginalProduct != null && OfferProduct != null)
            {
                OfferPriceText.text = OfferProduct.metadata.localizedPriceString;
                OriginalPriceText.text = OriginalProduct.metadata.localizedPriceString;
            }

        }
    }
    void HideOfferDetails()
    {
        OfferPercentageObj.SetActive(false);
        OfferPriceText.gameObject.SetActive(false);
        OriginalPriceText.gameObject.SetActive(false);
    }
   
    public void Close()
    {
        Debug.Log("---- unlock all close");
        gameObject.SetActive(false);
    }
    public void BuyClick()
    {
        if(_InAppCategory==InAppCategory.Consumable)
        {
            AdManager.Instance.BuyItem(IAPIndex, true);
        }
        else if(_InAppCategory==InAppCategory.NonConsumable)
        {
            AdManager.Instance.BuyItem(IAPIndex, false,true);
        }

        FirebaseController.Instance.LogFirebaseEvent("UnlockEverything_Buy", "", "");

    }
}
