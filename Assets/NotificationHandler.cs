﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using Assets.SimpleAndroidNotifications;
using Assets.SimpleAndroidNotifications.Data;
using Assets.SimpleAndroidNotifications.Enums;

public class NotificationHandler : MonoBehaviour
{
    private string title = string.Empty;
    private string content = string.Empty;

    // Update is called once per frame

    public string[] Allnotificaiontexts;

    //int[] AllTimervalueinhrs = new int[] { 3, 24, 48, 72, 120, 168, 360, 528, 720 };
    //int[] AllTimervalueinhrs = new int[] { 10, 20, 30, 40, 50,60,70,80,90 };
    int[] AllTimervalueinhrs = new int[] { 24, 48, 72, 96, 120, 144, 168, 192, 216,240,264,288,312,336};

    private int Timervalueinhrs=10;
   
    private int Notificationindexvalue=0;
    public static NotificationHandler Instance;
    private void Awake()
    {
        //Debug.LogError("------ NotifyNow Awake 1111111");
        //Debug.Log("Notify Awake Start..");
        Instance = this;

        var callback = NotificationManager.GetNotificationCallback();

        //Debug.Log("------ NotifyNow Awake callback=" + callback);
        if (callback != null)
        {
            //Debug.Log("------11111 The app was started by clicking a notification, callback.Data = " + callback.Data);
            //Debug.Log("---- ID=" + callback.Id);
            if (callback.Data.Equals("Reward"))
            {
                Invoke("OpenRewardPage", 2);
            }
            else
            {
            }
        }
        // ****************** Uncomment this to check popup in Editor ****************
        //Invoke("OpenRewardPage", 2);

        //return;
        if (PlayerPrefs.GetInt("Stopnotification",0) == 1)
        {
            PlayerPrefs.SetInt("Notificationtimerindex", 0);

            //return;
        }
       
        NotificationManager.CancelAll();

        for (int i = 0; i < Allnotificaiontexts.Length; i++)
        {
            Notificationindexvalue = PlayerPrefs.GetInt("Notificationtimerindex", 0);
            if (Notificationindexvalue < Allnotificaiontexts.Length && Notificationindexvalue < AllTimervalueinhrs.Length)
            {
                Timervalueinhrs = AllTimervalueinhrs[Notificationindexvalue];
                content = Allnotificaiontexts[Notificationindexvalue];//PlayerPrefs.GetInt("Notificationtimerindex")
                //if(Notificationindexvalue<2)
                //{
                //    SetNotifications("Reward");
                //}
                //else
                //{
                    SetNotifications("General");
                //}
            }
        }

    }
    private void OpenRewardPage()
    {
       // NotificationRewardHandler.Instance.Open();
    }
    //void OnApplicationPause(bool Pause)
    //{
    //    //return;
    //    if (Pause)
    //    {
    //        if (PlayerPrefs.GetInt("Stopnotification",0) == 1)
    //        {
    //            return;
    //        }
    //        CheckNotificationsnow();
    //       // Debug.Log("Exists in Notification ..... ");
    //    }
    //}

    void SetNotifications(string NotificationType)
    {
        //Debug.LogError("----------- Timervalueinhrs="+Timervalueinhrs);
        var notificationParams = new NotificationParams
        {
            Id = UnityEngine.Random.Range(0, int.MaxValue),
            Delay = TimeSpan.FromHours(Timervalueinhrs),
            Title = Application.productName,
            Message = " " + content,
            Ticker = "Ticker",
            Sound = true,
            //CustomSound = "ding", // AAR\res\raw\ding.wav. Please refer to plugin manual to learn how to add custom sounds.
            Vibrate = true,
            Vibration = new[] { 500, 500, 500, 500, 500, 500 },
            Light = true,
            LightOnMs = 1000,
            LightOffMs = 1000,
            LightColor = Color.red,
            SmallIcon = NotificationIcon.Bell,
            SmallIconColor = new Color(0f, 0.5f, 0f),
            LargeIcon = "app_icon",
            ExecuteMode = NotificationExecuteMode.Inexact,
            Importance = NotificationImportance.Max,
            //CallbackData = "notification created at " + DateTime.Now
            CallbackData = NotificationType

        };
        //Debug.LogError("------- NotifyNow send custom timervalue="+Timervalueinhrs);
        NotificationManager.SendCustom(notificationParams);
    }
   
    
}
