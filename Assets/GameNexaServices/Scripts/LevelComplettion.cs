﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LevelComplettion : MonoBehaviour
{
   // public GameObject[] _completeStamp;
    public string levels = "90000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    public char[] _completedLevels;

    public int CompletedLevelsCount = 0;
    public TextMeshProUGUI LevelCompletedText;
    public static LevelComplettion _instance;
    private void Awake()
    {
        Debug.LogError("AWAKE LEVEL SELECTIONSSSSSSSSSS" + levels.Length);

        if (_instance == null)
            _instance = this;
        else
            Destroy(this);

        //DontDestroyOnLoad(this);
    }



    private void _OnEnable()
    {
        //enablesFunction();


        //Debug.LogError("Level number current  : " + StaticVariables._iSelectedLevel);
        //Debug.LogError("ON ENABLED LEVEL completion" + levels.Length+"levels="+levels);
        //Debug.LogError("ON ENABLED LEVEL completion _levelsString 555=" + StaticVariables._levelsString);
        if (PlayerPrefs.GetInt("FirstTimeOpening", 0) == 0)
        {

            //Debug.LogError("ON ENABLED LEVEL completion _levelsString 111=" + StaticVariables._levelsString);

            StaticVariables._levelsString = levels;
            //Debug.LogError("ON ENABLED LEVEL completion _levelsString 222=" + StaticVariables._levelsString+":: selectedLevel="+StaticVariables._iSelectedLevel);

            for (int i = 1; i < StaticVariables._iSelectedLevel; i++)
            {
                StaticVariables._levelsString = StaticVariables._levelsString.Remove(i, 1);
                StaticVariables._levelsString = StaticVariables._levelsString.Insert(i, "1");

            }
            //Debug.LogError("Level String  : " + StaticVariables._levelsString);
            //Debug.LogError("ON ENABLED LEVEL completion _levelsString 333=" + StaticVariables._levelsString);

            enablesFunction();
            //Debug.LogError("LEVEL STRING SETUP 111111 " + StaticVariables._levelsString.Length);
            PlayerPrefs.SetString("LevelsStamp", StaticVariables._levelsString);

            PlayerPrefs.SetInt("FirstTimeOpening", 1);
        }
        else
        {
            StaticVariables._levelsString = PlayerPrefs.GetString("LevelsStamp", "9000000000000000000000000000000000000000000000000000000000000000000000000000");

        }
        //Debug.LogError("ON ENABLED LEVEL completion _levelsString 4444=" + StaticVariables._levelsString);
        //Debug.LogError("static test 333=" + staticTest);

        // LevelCompletedText.text = "LEVEL " + StaticVariables._iCompletedLevels + " / 50";

    }

    private void Start()
    {
        _OnEnable();
        //Debug.Log("STATIC VARIABLE _iCompletedLevels is +++++++++++++++++++++++++++++++ " + StaticVariables._iCompletedLevels);
        LevelCompletedText.text = "LEVEL " + StaticVariables._iCompletedLevels + " / 75";
        LevelSelectionHandler.Instance.CompletedLevelCountTextInEachWorld[0].text = StaticVariables._iCompletedLevelsInTheme01 + "/15";
        LevelSelectionHandler.Instance.CompletedLevelCountTextInEachWorld[1].text = StaticVariables._iCompletedLevelsInTheme02 + "/15";
        LevelSelectionHandler.Instance.CompletedLevelCountTextInEachWorld[2].text = StaticVariables._iCompletedLevelsInTheme03 + "/15";
        LevelSelectionHandler.Instance.CompletedLevelCountTextInEachWorld[3].text = StaticVariables._iCompletedLevelsInTheme04 + "/15";
        LevelSelectionHandler.Instance.CompletedLevelCountTextInEachWorld[4].text = StaticVariables._iCompletedLevelsInTheme05 + "/15";
    }

    public void enablesFunction()
    {
        Debug.Log("LEVEL COMPLETETION enablesFunction #######");
        //_completedLevels = new char[101];
//        StaticVariables._levelsString = PlayerPrefs.GetString("LevelsStamp", "90000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");

        StaticVariables._levelsString = PlayerPrefs.GetString("LevelsStamp", "9000000000000000000000000000000000000000000000000000000000000000000000000000");
        Debug.LogError("_levelsString " + StaticVariables._levelsString.Length);
        // levels = levels.Remove(0,1);
        // levels = levels.Insert(0, "1");
        CompletedLevelsCount = 1;

        string _levelsString_Dummy = StaticVariables._levelsString;
        if (AdManager.SubscribeUnlocked == 1)
        {
            _levelsString_Dummy = string.Empty;
            for (int i = 0; i < StaticVariables._levelsString.Length; i++)
            {
                _levelsString_Dummy = _levelsString_Dummy+"1";
            }
        }

        for (int i = 1; i < StaticVariables._levelsString.Length; i++)
        {
            if (_levelsString_Dummy[i].Equals('1'))
            {
                //Debug.LogError("I VALUE IS :::::::::::: " + i);

              LevelSelectionHandler.Instance.LevelImages[i - 1].gameObject.GetComponent<Image>().sprite = LevelSelectionHandler.Instance.LevelCompletedSprite;
              LevelSelectionHandler.Instance.Rewards[i - 1].gameObject.GetComponent<TextMeshProUGUI>().color = LevelSelectionHandler.Instance.LevelCompletedTextColor;
                if(LevelSelectionHandler.Instance.ListOfLevels[i - 1].transform.childCount == 2)
                {
                    LevelSelectionHandler.Instance.ListOfLevels[i - 1].transform.GetChild(1).gameObject.SetActive(false);
                }
                CompletedLevelsCount++;
            }
        }
    }

    public void LevelCompleteIndication(int _levelNumber)
    {
        StaticVariables._levelsString = StaticVariables._levelsString.Remove(_levelNumber,1);
        StaticVariables._levelsString = StaticVariables._levelsString.Insert(_levelNumber, "1");
        //for (int i = 0; i < levels.Length; i++)
        //{
        //    _completedLevels[i] = StaticVariables._levelsString[i];
        //}
        Debug.Log("LevelCompleteIndication LEVELS COUNT " + levels.Length);
        Debug.LogError("LEVEL STRING SETUP 2222222222 " + StaticVariables._levelsString.Length);

        PlayerPrefs.SetString("LevelsStamp", levels);
    }

}
