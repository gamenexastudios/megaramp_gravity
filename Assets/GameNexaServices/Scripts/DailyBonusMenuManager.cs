﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyBonusMenuManager : MonoBehaviour
{
    [SerializeField] GameObject[] _animationCoin;
    [SerializeField] GameObject[] _claimButtons;
    [SerializeField] GameObject[] _tomorrowButton;


    [SerializeField] GameObject _dailyBonusButton;
    [SerializeField] GameObject _dailyBonusPannel;

    [SerializeField] GameObject _spriteChange;
    [SerializeField] Sprite _deactivate;
    [SerializeField] Sprite _activate;


    //Foe special car
    [SerializeField] GameObject _mainMenu;
    [SerializeField] GameObject _specialCarButton;
    //[SerializeField] GameObject _specialCarTImerText;
    [SerializeField] GameObject _backButton;
    

    //public Text _dailyBonusTime;

    public static DailyBonusMenuManager _instance;
    public bool IsBonusButtonClick;
    private void Awake()
    {
        IsBonusButtonClick = false;
        if (_instance == null)
            _instance = this;
        else
            Destroy(this);

        //PlayerPrefs.DeleteAll();
       // StaticVariables._iUnlockedLevels = 2;

    }

    private void Start()
    {

        if (PlayerPrefs.GetInt("firstvisit", 0) == 1)
        {
            _specialCarButton.SetActive(true);
            //_specialCarTImerText.SetActive(true);
        }
        else
        {
            _specialCarButton.SetActive(false);
            //_specialCarTImerText.SetActive(false);
        }
        Debug.LogError("----- Daily bonus menu manager start");

        for (int i = 0; i < DailyBonusManager.myScript.ParentPanel.Length; i++)
        {

            if (DailyBonusManager.myScript.ParentPanel[i].gameObject.GetComponent<Image>().sprite == DailyBonusManager.myScript.claimedBG)
            {
                Debug.LogError("----- Daily bonus menu manager start i="+i);

                DailyBonusManager.myScript.TickMark[i].gameObject.SetActive(true);
                DailyBonusManager.myScript.ClaimDummy[i].gameObject.SetActive(false);
            }
        }
        StartCoroutine(EnableClaimedBG());
    }

    public void ButtonActivation(bool _state)
    {
        //_dailyBonusButton.SetActive(_state);
        if(!_state)
        {
            //_spriteChange.GetComponent<Image>().sprite = _deactivate;
            //_spriteChange.GetComponent<Animator>().enabled = false;
            //_spriteChange.GetComponent<Button>().interactable = false;

            foreach (var item in _claimButtons)
            {
                item.SetActive(false);
            }
            foreach (var item in _tomorrowButton)
            {
                item.SetActive(false);
            }
            //////////////////_dailyBonusButton.SetActive(false);
            //_dailyBonusTime.enabled = true;
            //_dailyBonusTime.text = (24 - System.DateTime.Now.Hour).ToString("00") + ":" + (60 - System.DateTime.Now.Minute).ToString("00") + ":" + (60 - System.DateTime.Now.Second).ToString("00");

        }
        else
        {
            _dailyBonusButton.SetActive(true);
            _spriteChange.GetComponent<Image>().sprite = _activate;
            //_spriteChange.GetComponent<Animator>().enabled = true;
           // _spriteChange.GetComponent<Button>().interactable = true;
            //_dailyBonusTime.enabled = false;
            
        }
    }

    IEnumerator EnableClaimedBG()
    {
        Debug.LogError("----- Enable Claimed BG =");
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < DailyBonusManager.myScript.ParentPanel.Length; i++)
        {
            if (DailyBonusManager.myScript.ParentPanel[i].gameObject.GetComponent<Image>().sprite == DailyBonusManager.myScript.claimedBG)
            {
                Debug.Log("IN LOOOOOOOOOOOOOOP");
                DailyBonusManager.myScript.ClaimDummy[i].gameObject.SetActive(false);
                DailyBonusManager.myScript.TickMark[i].gameObject.SetActive(true);
            }
        }
        Debug.Log("DAYNUMBERG VALUE IS :::::::::::::::: " + PlayerPrefs.GetInt("DayNumberG"));
        
            for (int i = 0; i < PlayerPrefs.GetInt("DayNumberG"); i++)
        {
            Debug.Log("I VALUE IS :::::::::::::::: " +i);
            Debug.LogError("----- Enable Claimed BG i="+i);

            DailyBonusManager.myScript.ParentPanel[i].gameObject.GetComponent<Image>().sprite = DailyBonusManager.myScript.claimedBG;
            DailyBonusManager.myScript.TickMark[i].SetActive(true);
        }
        if (PlayerPrefs.GetInt("DayNumberG") > 7)
        {
            PlayerPrefs.SetInt("DayNumberG", 7);
        }
        DailyBonusManager.myScript.ClaimDummy[PlayerPrefs.GetInt("DayNumberG")].SetActive(false);
    }
    public void onBonusButtonClick()
    {
        if(AdManager.Instance!=null)
        {
            AdManager.Instance.IsForceHideBannerAd = true;
        }
        if (AdController.Instance != null)
        {
            AdController.Instance.HideIronSourceBannerAd();
        }
        IsBonusButtonClick = true;
        StartCoroutine(EnableClaimedBG());

        StaticVariables._dailyBonusDiplayed = false;
        _dailyBonusPannel.SetActive(true);
        
        if (DailyBonusManager.myScript.BonusAt_Day > DailyBonusManager.myScript.BtnCollect.Length)
        {
            DailyBonusManager.myScript.sevenPlusButton.SetActive(true);
            DailyBonusManager.myScript.Set_BonuseText(DailyBonusManager.myScript.BtnCollect.Length - 1);
        }
        else if(DailyBonusManager.myScript.BonusAt_Day != 0)
        {
            Debug.Log("11111111111111111 + DailyBonusManager.myScript.BonusAt_Day " + DailyBonusManager.myScript.BonusAt_Day);
            DailyBonusManager.myScript.BtnCollect[DailyBonusManager.myScript.BonusAt_Day - 1].gameObject.SetActive(true);
            DailyBonusManager.myScript.Set_BonuseText(DailyBonusManager.myScript.BonusAt_Day -1);
        }
        else  //Zero means use my playerpref
        {
            Debug.Log("Player Prefs value ::::::::::::::::::: " + PlayerPrefs.GetInt("DayNumberG"));
            DailyBonusManager.myScript.BtnCollect[PlayerPrefs.GetInt("DayNumberG")].gameObject.SetActive(true);
            DailyBonusManager.myScript.Set_BonuseText(PlayerPrefs.GetInt("DayNumberG"));
        }

        iTween.ScaleTo(_dailyBonusPannel.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
       
    }

    private void Update()
    {
        if (PlayerPrefs.GetInt("DailyBonusButton") == 0)
            ButtonActivation(false);
        else
            ButtonActivation(true);
          
    }

    public int _scoreToUpdate;

    public void AddCoinAddingAnimation()
    {
        //StartCoroutine(AddingCoins());
    }

    IEnumerator AddingCoins()
    {
        _animationCoin[0].SetActive(true);
        yield return new WaitForSeconds(0.2f);
        _animationCoin[1].SetActive(true);
        yield return new WaitForSeconds(0.2f);
        _animationCoin[2].SetActive(true);
        _animationCoin[0].SetActive(false);
        _animationCoin[1].SetActive(false);
        yield return new WaitForSeconds(0.2f);
        _animationCoin[3].SetActive(true);
        yield return new WaitForSeconds(0.2f);
        _animationCoin[4].SetActive(true);
        _animationCoin[3].SetActive(false);
        _animationCoin[4].SetActive(false);


        _animationCoin[0].SetActive(false);
        _animationCoin[1].SetActive(false);
        _animationCoin[2].SetActive(false);
        _animationCoin[3].SetActive(false);
        _animationCoin[4].SetActive(false);
        //yield return new WaitForSeconds(0.2f);
        //_animationCoin[5].SetActive(true);
        //yield return new WaitForSeconds(0.2f);
        //_animationCoin[6].SetActive(true);
        //yield return new WaitForSeconds(0.2f);
        //_animationCoin[7].SetActive(true);
        //yield return new WaitForSeconds(0.2f);

    }

    public void AddingScoreEnCall()
    {
        StartCoroutine(AddingScore());
    }

    IEnumerator AddingScore()
    {
      // Debug.LogError("TO UPDATE SCORE:" + _scoreToUpdate);

       int k = 0;
       while(k < _scoreToUpdate)
        {
          //  Debug.LogError(k);
            k += 1;
            yield return new WaitForSeconds(0.0001f);
            MenuUIManager.Instance.UpdateScore(1);
        }
    }



    public void PlayWithSpecialCar()

    {
        StaticVariables._iSelectedVechicle = 10;
        MenuUIManager.Instance.Menu_Action("Garage");

        foreach (var item in UpgradeVechicle.Instance.Cars)
        {
            item.SetActive(false);
        }

        UpgradeVechicle.Instance.CurrentCarIndex = 9;
        UpgradeVechicle.Instance.BtnAct_NextVehicle();
        _backButton.GetComponent<Image>().enabled = true;
        _mainMenu.SetActive(false);
        PlayerPrefs.SetInt("NewCarClicked", 1);
        PlayerPrefs.SetInt("DisableNewCar", 1);

    }



    //public void CloseClaimPannel()
    //{
    //    Debug.Log("Close claim panle Daily bonus menu manager");

    //    StaticVariables._onClaimButtonClick = false;
    //    DailyBonusMenuManager._instance.AddCoinAddingAnimation();
    //    if (!DailyBonusMenuManager._instance.IsBonusButtonClick && SplPackHandler.Instance != null)
    //    {
    //        SplPackHandler.Instance.CheckNOpenSplPack();
    //    }
    //    DailyBonusMenuManager._instance.IsBonusButtonClick = false;
    //}

    public void AchivementButtonPressed()
    {
# if !AdSetup_OFF
        AdManager.Instance.ShowAchievements();
#endif
    }

    public void LeaderBoardPressed()
    {
#if !AdSetup_OFF
        AdManager.Instance.ShowLeaderBoards();
#endif
    }
}
