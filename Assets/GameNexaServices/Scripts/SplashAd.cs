﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;


public class SplashAd : MonoBehaviour
{
    public GameObject _rateUsPannel;
    public bool _playLaunchAd = true;
    //public GameObject _launchScreen; 




    public string Is_Sound;

    public bool testMode = false;
    public static SplashAd instance;







    //Rate us Pannel Related
    public bool _rateUsLaunchDisplay = false;
    public int _rateUsDaysCounter = 5;

    //Unlock everything
    public GameObject _unlockEverythingPannel;
    public GameObject _specialIAPButton;
    public GameObject _specialIAPPannelBuyButton;
    public Text _packNameText;
    private void Awake()
    {
        //Sound Glitch
        Is_Sound = PlayerPrefs.GetString("MYSOUNDS");
        if (Is_Sound == "Off")
        {
            AudioListener.volume = 0;
        }
        else
        {
            AudioListener.volume = 1;
        }
        //End Of Sound Glitch


        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(this);

        //_launchScreen.SetActive(false);

        //if (!StaticVariables.launchAdPlayed)
        //{
        //    _launchScreen.SetActive(true);

         
        //    Invoke("playSplashScreenAd", 6.0f);
        //}
        //else
            //_launchScreen.SetActive(false);



        if (testMode)
        {
            StartCoroutine(TEstModeafterDelay());
        }


    }

    public IEnumerator TEstModeafterDelay()
    {
        yield return new WaitForSeconds(5);
        // MenuUIManager.Instance.UnlockAllLevels();
        StaticVariables.UnlockedTheme = "Theme01";
        StaticVariables._iUnlockedLevels = 15;
        StaticVariables.UnlockedTheme = "Theme02";
        StaticVariables._iUnlockedLevels = 30;
        StaticVariables.UnlockedTheme = "Theme03";
        StaticVariables._iUnlockedLevels = 45;
        StaticVariables.UnlockedTheme = "Theme04";
        StaticVariables._iUnlockedLevels = 60;
        StaticVariables.UnlockedTheme = "Theme05";
        StaticVariables._iUnlockedLevels = 75;
        StaticVariables._iUnlockedWorldsCount = 4;

        //LevelSelectionHandler.Instance.WorldSelection();

        PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, "1111111111111");
    }


//    void playSplashScreenAd()
//    {

//        if (_playLaunchAd)
//        {
//            Debug.Log("LAUNCH AD PLAYING");
//#if !AdSetup_OFF
//            AdManager.Instance.NormalIntertitialAd(); 
//#endif                    
//        }

//        _launchScreen.SetActive(false);

//#if !AdSetup_OFF
//        if (StaticVariables.specialIAPDisplay)
//        {
//            if (!StaticVariables.launchAdPlayed && !(StaticVariables._iUnlockedLevels == 80 && PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).Equals("1111111111111") && PlayerPrefs.GetInt(MyGamePrefs.Is_NoAds) == 1))
//                _unlockEverythingPannel.SetActive(true);
//            else
//                _specialIAPButton.SetActive(false);
//        }
//#endif


    //    _specialIAPButton.SetActive(StaticVariables.specialIAPDisplay);



    //    _specialIAPPannelBuyButton.GetComponent<IAPButton>().productId = StaticVariables.specialIAPString;
    //    _packNameText.text = StaticVariables.specialIAPPackName.ToUpper();



    //    StaticVariables.launchAdPlayed = true;

    //}

#if !AdSetup_OFF

    private void Update()
    {
        //if (_dailyReward)
        //{
        //    Debug.LogWarning("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX: " + StaticVariables._dailyBonusDiplayed);
        //    if (StaticVariables._dailyBonusDiplayed)
        //        _dailyReward.SetActive(true);
        //    else
        //        _dailyReward.SetActive(false);
        //}
    }


    public void CloseClaimPannel()
    {
        Debug.Log("Close claim panle splash ad");
        StaticVariables._onClaimButtonClick = false;
        DailyBonusMenuManager._instance.AddCoinAddingAnimation();
    }


//    public void Call_RateGame()
//    {
//        Debug.Log("RATE HERE");
////#if UNITY_ANDROID
////        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
////#elif UNITY_IOS
////        Application.OpenURL("");
////#endif
    //    _rateUsPannel.SetActive(false);
    //}

    //public void closeRateUs()
    //{
    //    _rateUsPannel.SetActive(false);
    //}

    //public void displayRateUs()
    //{
    //    //Rate us display logic
    //    if (_rateUsLaunchDisplay)
    //    {
    //       // Debug.LogError("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    //        //Debug.LogError(StaticVariables.daysPassed_Launch);
    //        //Debug.LogError(_rateUsDaysCounter);

    //        if (StaticVariables.daysPassed_Launch >= _rateUsDaysCounter)
    //        {
    //           // _rateUsPannel.SetActive(true);
    //            _rateUsPannel.SetActive(false);
    //            StaticVariables.daysPassed_Launch = 0;
    //            PlayerPrefs.SetInt("daysPassed_Launch", StaticVariables.daysPassed_Launch);
    //        }
    //    }
    //}
#endif
}
