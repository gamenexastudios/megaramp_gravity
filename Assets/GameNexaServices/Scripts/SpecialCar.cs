﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialCar : MonoBehaviour
{
    public GameObject _text;
    public Text _text2;
    public static SpecialCar _instace;
    public string _timeString;
    private void Awake()
    {
        if (_instace == null)
            _instace = this;
        else
            Destroy(this);
    }
    // Update is called once per frame
    void Update()
    {
        if (PlayerPrefs.GetInt("DisableNewCar", 0) == 0)
        {
            transform.gameObject.SetActive(true);
            _timeString = _text.GetComponent<Text>().text;
            _text2.text = _timeString;
        }
        else
        {

            transform.gameObject.SetActive(false);
            _text.SetActive(false);
            _text2.enabled = false;
        }


    }
}
