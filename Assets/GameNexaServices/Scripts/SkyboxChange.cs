﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxChange : MonoBehaviour
{
    [SerializeField] Material _sb1;
    [SerializeField] Material _sb2;
    [SerializeField] GameObject _directionLight;
    // Start is called before the first frame update
    void Start()
    {
        Debug.LogError(StaticVariables._iSelectedLevel);
        if (StaticVariables._iSelectedLevel <= 50)
        {
            _directionLight.SetActive(true);
        }

        if (StaticVariables._iSelectedLevel > 50)
        {
            RenderSettings.skybox = _sb2;
        }
    }
}
