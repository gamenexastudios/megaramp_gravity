﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
public class CarRotation : MonoBehaviour
{

    Vector3 _startTouchPoint, _endTouchPoint;
    float _xDiff;
    float _yDiff;
    public float _smoothness = 0.005f;



    [SerializeField] GameObject _unlockAllCarsButton;
    [SerializeField] float _xrot;
    [SerializeField] float _yrot;

    public float _xDiff_Rot;
    public float _yDiff_Rot;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        MobileTouchRotation();
       // MouseContrl();

        if (PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).Equals("1111111111111"))
            _unlockAllCarsButton.SetActive(false);

    }

    void MobileTouchRotation()
    {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
              

                if (touch.deltaPosition.x > touch.deltaPosition.y)
                {
                    if(touch.deltaPosition.x> 0)
                        _xDiff_Rot += Time.deltaTime * 30f;
                    else
                        _xDiff_Rot -= Time.deltaTime * 30f;
                }

                else
                {
                    if(touch.deltaPosition.y > 0)
                        _yDiff_Rot += Time.deltaTime * 15f;
                    else
                        _yDiff_Rot -= Time.deltaTime * 15f;
                }

                if (_yDiff_Rot < -7)
                    _yDiff_Rot = -7;
                else if (_yDiff_Rot > 40)
                    _yDiff_Rot = 40;
                touchrotation= (Quaternion.Euler(0, _xDiff_Rot, 0) * Quaternion.Euler(_yDiff_Rot, 0, 0));
                transform.rotation = touchrotation;
            }

            //if (Input.GetTouch(0).phase == TouchPhase.Ended)
            //{
            //    _xDiff_Rot = 0;
            //    _yDiff_Rot = 0;
            //    transform.rotation = Quaternion.Euler(0, 0, 0);
            //}
        }



    }

    Quaternion touchrotation;
    void MouseContrl()
    {
        _xDiff_Rot += Input.GetAxis("Mouse X");
        _yDiff_Rot += Input.GetAxis("Mouse Y");
        if (_yDiff_Rot < -7)
            _yDiff_Rot = -7;
        else if (_yDiff_Rot > 40)
            _yDiff_Rot = 40;

        transform.rotation = Quaternion.Euler(0, _xDiff_Rot, 0) * Quaternion.Euler(_yDiff_Rot, 0, 0);
    }

  /*  [HideInInspector]
    public bool on = true;





    public float Yposition;
    public float touchposition;



    public float speed = 2f;


    void LateUpdate()
    {
        if (Input.touchCount == 1)
        {

            Touch touch = Input.GetTouch(0);


            touchposition = touch.position.y;



            if (on == true)
            {
                if (touch.phase == TouchPhase.Moved)
                {

                        Vector2 touchdelta = touch.deltaPosition;
                        transform.rotation=Quaternion.Euler(100f-touchdelta.x * speed * Time.deltaTime, -touchdelta.y * speed * Time.deltaTime, 0);

                }

            }


        }







    }*/

}
