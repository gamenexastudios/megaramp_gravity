﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimeManager : MonoBehaviour
{
    //Test Variables

    [SerializeField] Text _timeText;
    [SerializeField] Text _timeText_garage;
    [SerializeField] Button _specialCarButton;
    [SerializeField] Button _specialCarButton_garage;


    public static TimeManager _instance;
    DateTime _added;


    DateTime _dailyBonus;
    public Text _dailyBonusTime;
    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(this);

        if (PlayerPrefs.GetInt("FirstTimerSet", 0) == 0)
        {
            _added = System.DateTime.Now.AddHours(24);
            PlayerPrefs.SetInt("FirstTimerSet", 1);
            PlayerPrefs.SetString("TimeCounter", _added.ToString());
        }
        else
        {
            PlayerPrefs.GetString("TimeCounter");
            _added = System.DateTime.Parse(PlayerPrefs.GetString("TimeCounter"));
          //  Debug.LogError(_added);
        }

        //_dailyBonus = System.DateTime.Now.AddHours(24).Subtract(System.DateTime.Now);
    }

    void Start()
    {
        
    }

   
    void Update()
    {

        if (!(_added.Subtract(System.DateTime.Now).Seconds < 0))
        {
          //  if(_timeText.enabled)
          //  _timeText.text = "UNLOCK TIME: " + _added.Subtract(System.DateTime.Now).Hours.ToString("00") + ":" + _added.Subtract(System.DateTime.Now).Minutes.ToString("00") + ":" + _added.Subtract(System.DateTime.Now).Seconds.ToString("00");

            /*
            if (_timeText_garage.enabled)
                _timeText_garage.text = "UNLOCK TIME: " + _added.Subtract(System.DateTime.Now).Hours.ToString("00") + ":" + _added.Subtract(System.DateTime.Now).Minutes.ToString("00") + ":" + _added.Subtract(System.DateTime.Now).Seconds.ToString("00");
            */
            
            //_specialCarButton.interactable = false;
            _specialCarButton_garage.interactable = false;

            UpgradeVechicle.Instance.specialCarUnlocked = false;
        }
        else
        {
           // _timeText.enabled = false;
           // _timeText_garage.enabled = false;

           // _specialCarButton.interactable = true;
            _specialCarButton_garage.interactable = true;

            UpgradeVechicle.Instance.specialCarUnlocked = true;
        }


        // Debug.LogError(System.DateTime.Now.AddHours(24).Subtract(System.DateTime.Now).Minutes - System.DateTime.Now.Minute);

        //_dailyBonusTime.text = (System.DateTime.Now.Hour - System.DateTime.Now.AddHours(24).Subtract(System.DateTime.Now).Hours) + ":" + (System.DateTime.Now.Minute - System.DateTime.Now.AddHours(24).Subtract(System.DateTime.Now).Minutes) + ":" + (System.DateTime.Now.Second - System.DateTime.Now.AddHours(24).Subtract(System.DateTime.Now).Seconds);
        //if (PlayerPrefs.GetInt("DailyBonusButton") == 0)
        //    _dailyBonusTime.text = (24 - System.DateTime.Now.Hour).ToString("00") + ":" + (60 - System.DateTime.Now.Minute).ToString("00") + ":" + (60 - System.DateTime.Now.Second).ToString("00");
        //else
        //    _dailyBonusTime.enabled = false;
    }
}
