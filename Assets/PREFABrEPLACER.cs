﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PREFABrEPLACER : MonoBehaviour
{

    public GameObject parent;
    public Transform[] wheretoplace;

    public GameObject prefab;

    GameObject current;
    public bool starts;
    private void OnDrawGizmos()
    {
        if (starts)
        {
            StartCoroutine(Placer());
            starts = false;
        }
    }

    int i;
    IEnumerator Placer()
    {
        while (i < 84)
        {

            yield return new WaitForSeconds(0.1f);
            current = Instantiate(prefab, wheretoplace[i].position, wheretoplace[i].rotation, parent.transform);
            current.transform.localScale = wheretoplace[i].localScale;
            i++;
            yield return null;
        }
        yield return null;
    }
}
