﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticVariables 
{
	public static string _Sounds = "Sound";
	public static string _Music = "Music";
	public static string _sSelectedLevel="SelectedLevel";
	public static string _sTotalCoins="TotalCoins";
	public static string _sSelectedVechicle="Vehcicle";
	public static string _sHomeOrGarage="HomeOrGarage";
	public static string _sTotalLevels="TotalLevels";
	public static string _sUnlockedLevels="UnlockedLevels";
	//public static string _sCarvisbleinpanel = "Carvisbleinpanel";
	public static string _sHelp="Help";

    public static bool _dailyBonusDiplayed = false;
    public static bool _onClaimButtonClick = false;

	public static string _sUnlockedWorldsCount = "WorldCount";
	public static int[] UnlockWorldAtLevelNo = { 6, 16, 26, 36 };

	public static int[] UnlockCarAtLevelNo = { 10, 30 };
	public static string _sUnlockCarBool = "UnlockCars";
	public static string[] splitUnlockCarBool;

	public static int[] RewardCoinsAtLevelNo = { 40, 50, 90 };
	public static string _sRewardBools = "0,0,0";
	public static string[] splittedRewardBool;

	public static int[] SpecificCar2XRewardAtLevelNo = { 20, 45, 70, 80 };
	public static string _sCar2XRewardBools = "0,0,0,0";
	public static string[] splitCar2XRewardBool;

	public static string _sUnlockedAllLevelsIAP = "UnlockAllLevelsIAP";
	public static string _sUnlockedAllCarsIAP = "UnlockAllCarsIAP";


	public static int _extra2x;

    public static bool playAdOnHomeButton = false;
	public static string UnlockedTheme = "Theme01";

	public static string _SCompletedLevels = "CompletedLevels";
	public static string _SCompletedLevelsInTheme01 = "CompletedLevelsInTheme01";
	public static string _SCompletedLevelsInTheme02 = "CompletedLevelsInTheme02";
	public static string _SCompletedLevelsInTheme03 = "CompletedLevelsInTheme03";
	public static string _SCompletedLevelsInTheme04 = "CompletedLevelsInTheme04";
	public static string _SCompletedLevelsInTheme05 = "CompletedLevelsInTheme05";

	public static int _iCompletedLevels
	{
		set
		{

			PlayerPrefs.SetInt(_SCompletedLevels, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_SCompletedLevels, 0);
		}
	}

	public static int _iCompletedLevelsInTheme01
	{
		set
		{

			PlayerPrefs.SetInt(_SCompletedLevelsInTheme01, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_SCompletedLevelsInTheme01, 0);
		}
	}

	public static int _iCompletedLevelsInTheme02
	{
		set
		{

			PlayerPrefs.SetInt(_SCompletedLevelsInTheme02, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_SCompletedLevelsInTheme02, 0);
		}
	}

	public static int _iCompletedLevelsInTheme03
	{
		set
		{

			PlayerPrefs.SetInt(_SCompletedLevelsInTheme03, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_SCompletedLevelsInTheme03, 0);
		}
	}

	public static int _iCompletedLevelsInTheme04
	{
		set
		{

			PlayerPrefs.SetInt(_SCompletedLevelsInTheme04, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_SCompletedLevelsInTheme04, 0);
		}
	}

	public static int _iCompletedLevelsInTheme05
	{
		set
		{

			PlayerPrefs.SetInt(_SCompletedLevelsInTheme05, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_SCompletedLevelsInTheme05, 0);
		}
	}


	public static int _iUnlockedWorldsCount
	{
		set
		{
			PlayerPrefs.SetInt(_sUnlockedWorldsCount, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_sUnlockedWorldsCount, 0);
		}
	}

	public static string _iUnlockCarBools
	{
		set
		{
			PlayerPrefs.SetString(_sUnlockCarBool, value);
		}
		get
		{
			return PlayerPrefs.GetString(_sUnlockCarBool, "0,0");
		}
	}


	public static string _iRewardBools
	{
		set
		{
			PlayerPrefs.SetString(_sRewardBools, value);
		}
		get
		{
			return PlayerPrefs.GetString(_sRewardBools, "0,0,0");
		}
	}


	public static string _iCar2XRewardBools
	{
		set
		{
			PlayerPrefs.SetString(_sCar2XRewardBools, value);
		}
		get
		{
			return PlayerPrefs.GetString(_sCar2XRewardBools, "0,0,0,0");
		}
	}

	public static int _iUnlockedAllLevelsIAP
	{
		set
		{
			PlayerPrefs.SetInt(_sUnlockedAllLevelsIAP, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_sUnlockedAllLevelsIAP, 0);
		}
	}

	public static int _iUnlockedAllCarsIAP
	{
		set
		{
			PlayerPrefs.SetInt(_sUnlockedAllCarsIAP, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_sUnlockedAllCarsIAP, 0);
		}
	}

	public static int _iHelp
	{
		set
		{
			PlayerPrefs.SetInt (_sHelp, value);	
		}
		get
		{
			return PlayerPrefs.GetInt (_sHelp, 0);	
		}
	}

	public static string _savepointHelp = "savepointHelp";
    public static int _isavepointHelp
    {
        set
        {
            PlayerPrefs.SetInt(_savepointHelp, value);
        }
        get
        {
            return PlayerPrefs.GetInt(_savepointHelp, 0);
        }
    }

    public static int _iUnlockedLevels
	{
		set
		{
			PlayerPrefs.SetInt (UnlockedTheme + _sUnlockedLevels, value);
		}
		get
		{
			 return PlayerPrefs.GetInt (UnlockedTheme + _sUnlockedLevels, 1);
			Debug.Log("UNLOCKED LEVELS IN WORLD ************************ "+ UnlockedTheme + _sUnlockedLevels);
		}
	}
	public static int _iTotalLevels
	{
		set
		{
			PlayerPrefs.SetInt (_sTotalLevels, value);
		}
		get
		{
			//return PlayerPrefs.GetInt (_sTotalLevels, 70);	
			return PlayerPrefs.GetInt (_sTotalLevels, 75);	  //change
		}
	}
	public static int[] EachLevelScore = 
	{
		100,300,500,600,700,800,900,1000,1100,1200,
        1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,
        2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,
        3300,3400,3500,3600,3700,3800,3900,4000,4100,4200,
        4300,4400,4500,4600,4700,4800,4900,5000,5100,5200,
        5300,5400,5500,5600,5700,5800,5900,6000,6100,6200,
        6300,6400,6500,6600,6700,6800,6900,7000,7100,7200,
        7300,7400,7500,7600,7700,7800,7900,8000,8100,8200,
        8300,8400,8500,8600,8700,8800,8900,9000,9100,9200,
        9300,9400,9500,9600,9700,9800,9900,10000,10100,10200

	};   //Change
	public static int _iSelectedLevel
	{
		set
		{
			PlayerPrefs.SetInt(_sSelectedLevel, value);
		}
		get 
		{
			return PlayerPrefs.GetInt(_sSelectedLevel,1);
		}
	}

	public static int _iTotalCoins
	{
		set
		{
			PlayerPrefs.SetInt (_sTotalCoins, value);
		}
		get
		{
			return PlayerPrefs.GetInt(_sTotalCoins,0);
		}
	}
	public static int _iSelectedVechicle
	{
		set
		{
			PlayerPrefs.SetInt (_sSelectedVechicle, value);
		}
		get
		{
			return PlayerPrefs.GetInt (_sSelectedVechicle, 1);
		}
	}
	public static string _iHomeOrGarage
	{
		set
		{
			PlayerPrefs.SetString (_sHomeOrGarage, value);
		}
		get
		{
			return PlayerPrefs.GetString (_sHomeOrGarage,"Home");
		}

	}

	public static string _Soundss
    {
		set
		{
			PlayerPrefs.SetString(_Sounds, value);
		}
		get
		{
			return PlayerPrefs.GetString(_Sounds, "SoundOn");
		}
	}


	public static string _Musicc
	{
		set
		{
			PlayerPrefs.SetString(_Music, value);
		}
		get
		{
			return PlayerPrefs.GetString(_Music, "MusicOn");
		}
	}



	//public static int _iCarAtScreen
	//{
	//	set
	//	{
	//		PlayerPrefs.SetInt(_sCarvisbleinpanel, value);
	//	}
	//	get
	//	{
	//		return PlayerPrefs.GetInt(_sCarvisbleinpanel, 0);
	//	}

	//}

	public static bool rewardCalled = false;
    public static bool launchAdPlayed = false;

    //Rate us 
    public static int rateus_levelCouter = 2;
    public static int daysPassed;
    public static int displayDate_Counter = 1;

    public static int daysPassed_Launch;
    public static bool _dailyBonusShown = false;


    public static bool _dontCallRewardAwake = false;
    public static string _levelsString;
    public static bool showBanner = true;
    public static bool showGameEndBanner = true;
    public static bool specialIAPDisplay = true;
    public static string specialIAPString = "com.carstunt.specialpack";
    public static string specialIAPPackName = "UNLOCK ALL";
}
