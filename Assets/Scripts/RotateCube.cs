﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCube : MonoBehaviour
{
    public float SpeedOfObject;
    public GameObject RotateObject;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RotateObject.transform.Rotate(new Vector3(0f, 1f, 0f) * Time.deltaTime * SpeedOfObject);
    }
}
