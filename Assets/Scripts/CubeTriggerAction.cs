﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeTriggerAction : MonoBehaviour
{
    public CubeMoveTo TargetScript;
    public ObjectRotateTo TargetRotScript;
    public GameObject SetActiveTarget;
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("HIT");
            if (TargetScript != null)
            {
                TargetScript.Move();
            }

            if (SetActiveTarget != null)
            {
                SetActiveTarget.gameObject.SetActive(true);
            }

            if (TargetRotScript != null)
            {
                TargetRotScript.Rotate();
            }
        }
    }
}
