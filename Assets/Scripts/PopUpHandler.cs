﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PopUpHandler : MonoBehaviour 
{
	public static PopUpHandler _myScript;
    public static PopUpHandler myScript
    {
        get
        {
            if (_myScript == null)
            {
                _myScript = FindObjectOfType<PopUpHandler>();
            }
            return _myScript;
        }
    }

    public GameObject PopUp_AlphaBG;

    public GameObject PopUp_WelComeReward;
    public GameObject PopUp_WelComeReward2;
    public GameObject Btn_WC2XClose;
    public Text Text_Welcome,Text_WelCome2x;

    public GameObject PopUp_UnlockAllUpgrades;
    public GameObject Btn_CloseUpgrades;
    public GameObject PopUp_UnlockAllLevels;
    public GameObject Btn_CloseLevels;

    public GameObject PopUp_50Disc,Btn50Close;
    public GameObject PopUp_75Disc, Btn75Close;
    public Text Text_50Disc, Text_75Disc;

    public static string Is_WelComeRewardTaken= "Is_WelComeRewardTakenA";

    public GameObject PopUp_2XLCReward;
    public GameObject Btn_2XLCRewardClose;

    public GameObject PopUp_2XDailyBonus;
    public GameObject Btn_2XDailyBonusClose;

    void Awake()
	{
		
	}

	void Start () 
	{
        Text_Welcome.text = "Thank You for Downloading\n"+Application.productName+"!\nPlease Collect your Welcome Bonus of 1000 !";
        Text_WelCome2x.text = "More Free Make it Double by Watching Video!!";
    }
	
    public void PopUpWelcome_In()
    {
        Debug.Log("[PH] PopUpWelcome_In");
        if (!PlayerPrefs.HasKey(Is_WelComeRewardTaken))
        {
            AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
            iTween.ScaleTo(PopUp_WelComeReward.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
        }
        else
        {
            if(DailyBonusManager.myScript)
            DailyBonusManager.myScript.CheckDailyBonus();
        }
    }

    public void PopUpWelcome_Out()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(PopUp_WelComeReward.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
    }

    public void BtnAct_WelComeRewardCollect()
    {
        PlayerPrefs.SetString(Is_WelComeRewardTaken, "true");
        PopUpWelcome_Out();
 
    }

    #region POPUP_WELCOME2XREWARD
    public void PopUpWelcome2_In()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
        iTween.ScaleTo(PopUp_WelComeReward2.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn_WC2XClose.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
    }

    public void PopUpWelcome2_Out()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(PopUp_WelComeReward2.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
        
    }

    public void BtnAct_2WelComeRewardCollect()
    {
        
    }

    public void DoubleRewardSuccess2XWC()
    {
        PopUpWelcome2_Out();
       
    }
    #endregion

    #region POPUP_UNLOCKALLUPGRADES
    public void PopUpUnlockUpgrades_In()
    {
        Debug.Log("[PH] PopUpUnlockUpgrades_In");
        AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
        iTween.ScaleTo(PopUp_UnlockAllUpgrades.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn_CloseUpgrades.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
    }

    public void PopUpUnlockUpgrades_Out()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(PopUp_UnlockAllUpgrades.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn_CloseUpgrades.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0f, "delay", 0, "easetype", iTween.EaseType.linear));
    }
    public void BtnAct_UnlockAllUpgrades(int _index)
    {
        //BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(_index);
    }
    #endregion

    #region POPUP_UNLOCKALLLEVELS
    public void PopUpUnlockLevels_In()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
        iTween.ScaleTo(PopUp_UnlockAllLevels.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn_CloseLevels.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
    }

    public void PopUpUnlockLevels_Out()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(PopUp_UnlockAllLevels.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn_CloseLevels.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0f, "delay", 0, "easetype", iTween.EaseType.linear));
    }
    public void BtnAct_UnlockAllLevels(int _index)
    {
        //BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(_index);
    }
    #endregion

    #region POPUP_DISCOUNT_50&75
    public void PopUp50Disc_In()
    {
        Debug.Log("[PH] PopUp50Disc_In");
        AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
        PopUp_50Disc.gameObject.SetActive(true);
        Text_50Disc.text = "TEXT HERE";
        iTween.ScaleTo(PopUp_50Disc.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn50Close.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
    }

    public void PopUp50Disc_Out()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(PopUp_50Disc.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn50Close.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0f, "delay", 0, "easetype", iTween.EaseType.linear));
    }
    public void BtnAct_50Disc(int _index)
    {
        //BtnAct_AdsRelated.myScript.BtnAct_CallIAPBuy(_index);
    }


    public void PopUp75Disc_In()
    {
        Debug.Log("[PH] PopUp75Disc_In");
        AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
        PopUp_75Disc.gameObject.SetActive(true);
        Text_75Disc.text = "TEXT HERE";
        iTween.ScaleTo(PopUp_75Disc.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn75Close.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 2, "easetype", iTween.EaseType.linear));
    }

    public void PopUp75Disc_Out()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(PopUp_75Disc.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn75Close.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0f, "delay", 0, "easetype", iTween.EaseType.linear));
    }
    public void BtnAct_75Disc(int _index)
    {
        BtnAct_AdsRelated.CallIAPBuy(_index);
    }

    public Text Text_50offText;
    public Text Text_75offText;

    public static int _tempcount;
    public static int _tempcountupgrade;
    public static int _tempcountls;

    public void Check_Discount()
    {
        
    }
    #endregion

    #region POPUP_2XLCREWARD
    public void PopUp2XLCReward_In()
    {
        Debug.Log("[PH] PopUp2XLCReward_In");
        AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
        iTween.ScaleTo(PopUp_2XLCReward.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn_2XLCRewardClose.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
    }

    public void PopUp2XLCReward_Out()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(PopUp_2XLCReward.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn_2XLCRewardClose.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0f, "delay", 0, "easetype", iTween.EaseType.linear));
    }

    public void BtnAct_2XLCRewardCollect()
    {
        Debug.Log("----------------------2XLCRewardCollect--------------");
    }

    public void VideoSuccess2XLCReward()
    {
		Debug.Log("----------------------Double Reward Called--------------");
        StaticVariables._iTotalCoins += StaticVariables.EachLevelScore[StaticVariables._iSelectedLevel - 1];
        if (InGameUIHandler.Instance)
        {
			InGameUIHandler.Instance.LevelDoubleReward();
        }
        //MyAdsManager.myScript.Set_Coins(1000);
        PopUp2XLCReward_Out();        
    }
    #endregion

    #region POPUP_2XDAILYBONUS
    public void PopUp2XDailyBonus_In()
    {
        Debug.Log("[PH] PopUp2XDailyBonus_In");
        AlphaScript.AlphFade(PopUp_AlphaBG, 0.95f, 0.2f, 0, iTween.EaseType.linear, true);
        iTween.ScaleTo(PopUp_2XDailyBonus.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
        iTween.ScaleTo(Btn_2XDailyBonusClose.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 3, "easetype", iTween.EaseType.linear));
    }

    public void PopUp2XDailyBonus_Out()
    {
        AlphaScript.AlphFade(PopUp_AlphaBG, 0f, 0.1f, 0, iTween.EaseType.linear, false);
        iTween.ScaleTo(PopUp_2XDailyBonus.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));
    }

    public void BtnAct_2XDailyBonusCollect()
    {
        Debug.Log("BtnAct_2XDailyBonusCollect");
    }
    #endregion

}
