﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlaneHandler : MonoBehaviour {


	// Use this for initialization

	public GameObject TargetObject1;
	public GameObject TargetObject2;
	public GameObject TargetObject3;
	public GameObject TargetObject4;
	public bool _IsBoolTargetObject1;
	public bool _IsBoolTargetObject2;
	public bool _IsBoolTargetObject3;
	public bool _isBoolTargetObject4;
	public GameObject AIWing;
	public float SpeedOfWing;
	void Start () 
	{

	}

	// Update is called once per frame
	void Update ()
	{
		AIWing.transform.Rotate (Vector3.left * Time.deltaTime * SpeedOfWing);
		if (_IsBoolTargetObject1) {
			if (transform.position == TargetObject1.transform.position) {
				_IsBoolTargetObject1 = false;
				_IsBoolTargetObject3 = false;
				_isBoolTargetObject4 = false;
				_IsBoolTargetObject2 = true;

			} else {
				transform.rotation = Quaternion.Euler (-90f, 0f, -90f);
				transform.position = Vector3.MoveTowards (transform.position, TargetObject1.transform.position, Time.deltaTime * 10f);
			}
		} 

		if (_IsBoolTargetObject2) {
			if (transform.position == TargetObject2.transform.position) {
				_IsBoolTargetObject2 = false;
				_IsBoolTargetObject1 = false;
				_isBoolTargetObject4 = false;
				_IsBoolTargetObject3 = true;
			} else {
				transform.rotation = Quaternion.Euler (-90f, 0f, 0f);
				transform.position = Vector3.MoveTowards (transform.position, TargetObject2.transform.position, Time.deltaTime * 10f);
			}
		}
		if (_IsBoolTargetObject3) {
			if (transform.position == TargetObject3.transform.position) {
				_IsBoolTargetObject1 = false;
				_IsBoolTargetObject2 = false;
				_IsBoolTargetObject3 = false;
				_isBoolTargetObject4 = true;
			} else {
				transform.rotation = Quaternion.Euler (-90f, -90f, -180f);
				transform.position = Vector3.MoveTowards (transform.position, TargetObject3.transform.position, Time.deltaTime * 10f);
			}
		}
		if (_isBoolTargetObject4) {
			if (transform.position == TargetObject4.transform.position) {

				_IsBoolTargetObject2 = false;
				_IsBoolTargetObject3 = false;
				_isBoolTargetObject4 = false;
				_IsBoolTargetObject1 = true;
			} else {
				transform.rotation = Quaternion.Euler (-90f, 0f, 180f);
				transform.position = Vector3.MoveTowards (transform.position, TargetObject4.transform.position, Time.deltaTime * 10f);
			}
		}
	}
	}