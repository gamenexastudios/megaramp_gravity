using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using System.Reflection;
using System;

public class GameManager : MonoBehaviour {
	public bool TestMode;
	public bool TestWithPrefab;
	public GameObject TestPrefab;
	public int TestLevel;

	public int startingLevelInCurrentScene;
	public GameObject[] ListOfLevels;

	[HideInInspector]
	public Material svaepointglass;
	[Space]

    public GameObject _adLoading;


	public static GameManager Instance;
    public GameObject PlayerPos;
    public GameObject CurrentLevelObj;

   
	//public List<GameObject> ListOfVehicle;
	//public Transform PlayerPosition;
	public GameObject CurrentVehile;
	public SavePointManager CurrentLevel;


    [HideInInspector]
    public Rampsrestart obstaclesRestart;

	[HideInInspector]
	public bool underObstacle;

	Material[] materialsFinal;

	public enum GameState
	{
		GamePlay,LevelComplete,LevelFaild,Pause,Revive
	}
	public GameState CurrentGameState;

	AudioSource LevelAudio;

	Texture[] finaltextures;
	public bool IsInPlayMode;
	public Material mat;


	void Awake()
	{
        if (Instance == null)
        {
			Instance = this;
        }
        else
        {
			Destroy(this);
        }
		IsInPlayMode = true;
		materialsFinal = new Material[5];
		finaltextures = new Texture[4];
		materialsFinal[0] = Resources.Load("Road_01Mat") as Material;
		materialsFinal[1] = Resources.Load("Base") as Material;
		materialsFinal[2] = Resources.Load("Entrance_Container_Material") as Material;
		materialsFinal[3] = Resources.Load("checkPoint") as Material;
		materialsFinal[4] = Resources.Load("Obstacles_Material") as Material;
	   // mat= Resources.Load("Levels/1") as Material;

		  LevelValue= StaticVariables._iSelectedLevel;


		/*
		RenderSettings.skybox = ListOfMaterials[Random.Range(0, ListOfMaterials.Count)];
		//Change fog color based on skybox
		if (RenderSettings.skybox.name == "Skybox Cubemap Extended Night")
		{
			RenderSettings.fogColor = new Color32(147, 35, 87, 255);
		}
		else if (RenderSettings.skybox.name == "SkyBox_Blue")
		{
			RenderSettings.fogColor = new Color32(4, 150, 178, 255);
		}
		else if (RenderSettings.skybox.name == "SkyBox_Green")
		{
			RenderSettings.fogColor = new Color32(150, 91, 0, 255);
		}
		*/

		/*
		if (PlayerPrefs.GetInt("introdone", 0) == 0)
        {
			Handheld.PlayFullScreenMovie("intro.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
			PlayerPrefs.SetInt("introdone", 1);
		}
		*/

		//AdManager.Instance.RequestRewardBasedVideo();

		//PlayerPrefs.DeleteAll();

	}

	void Start()
	{
		SetIntialProperties();
		obstaclesRestart = FindObjectOfType<Rampsrestart>();

		svaepointglass = Resources.Load("GlassTesting") as Material;
        svaepointglass.color = new Color(0.147f, 0.725f, 0.725f, 1);

        // if (AdManager.Instance)
        // AdManager.Instance.RequestBanner();

        if (LevelValue >= 1 && LevelValue <= 15)
		{
			Debug.Log("world1");
			finaltextures[0] = Resources.Load("Path_01") as Texture;
			materialsFinal[0].SetTexture("_MainTex", finaltextures[0]);
			materialsFinal[1].SetTexture("_MainTex", finaltextures[0]);


			finaltextures[1] = Resources.Load("Entrance_v01") as Texture;
			materialsFinal[2].SetTexture("_MainTex", finaltextures[1]);


			finaltextures[2] = Resources.Load("ChackPoint_v01") as Texture;
			materialsFinal[3].SetTexture("_MainTex", finaltextures[2]);

			finaltextures[3] = Resources.Load("Obstucles_v01") as Texture;
			materialsFinal[4].SetTexture("_MainTex", finaltextures[3]);

		}
		else if (LevelValue >= 16 && LevelValue <= 30)
		{
			Debug.Log("world2");

			finaltextures[0] = Resources.Load("Path_02") as Texture;
			materialsFinal[0].SetTexture("_MainTex", finaltextures[0]);
			materialsFinal[1].SetTexture("_MainTex", finaltextures[0]);


			finaltextures[1] = Resources.Load("Entrance_v02") as Texture;
			materialsFinal[2].SetTexture("_MainTex", finaltextures[1]);


			finaltextures[2] = Resources.Load("ChackPoint_v02") as Texture;
			materialsFinal[3].SetTexture("_MainTex", finaltextures[2]);

			finaltextures[3] = Resources.Load("Obstucles_v02") as Texture;
			materialsFinal[4].SetTexture("_MainTex", finaltextures[3]);
		}
		else if (LevelValue >= 31 && LevelValue <= 45)
		{
			Debug.Log("world3");

			finaltextures[0] = Resources.Load("Path_03") as Texture;
			materialsFinal[0].SetTexture("_MainTex", finaltextures[0]);
			materialsFinal[1].SetTexture("_MainTex", finaltextures[0]);


			finaltextures[1] = Resources.Load("Entrance_v03") as Texture;
			materialsFinal[2].SetTexture("_MainTex", finaltextures[1]);


			finaltextures[2] = Resources.Load("ChackPoint_v03") as Texture;
			materialsFinal[3].SetTexture("_MainTex", finaltextures[2]);

			finaltextures[3] = Resources.Load("Obstucles_v03") as Texture;
			materialsFinal[4].SetTexture("_MainTex", finaltextures[3]);
		}
		else if (LevelValue >= 46 && LevelValue <= 60)
		{
			Debug.Log("world4");

			finaltextures[0] = Resources.Load("Path_04") as Texture;
			materialsFinal[0].SetTexture("_MainTex", finaltextures[0]);
			materialsFinal[1].SetTexture("_MainTex", finaltextures[0]);


			finaltextures[1] = Resources.Load("Entrance_v04") as Texture;
			materialsFinal[2].SetTexture("_MainTex", finaltextures[1]);


			finaltextures[2] = Resources.Load("ChackPoint_v04") as Texture;
			materialsFinal[3].SetTexture("_MainTex", finaltextures[2]);

			finaltextures[3] = Resources.Load("Obstucles_v05") as Texture;
			materialsFinal[4].SetTexture("_MainTex", finaltextures[3]);
		}
		else if (LevelValue >= 61 && LevelValue <= 75)
		{
			Debug.Log("world5");

			finaltextures[0] = Resources.Load("Path_05") as Texture;
			materialsFinal[0].SetTexture("_MainTex", finaltextures[0]);
			materialsFinal[1].SetTexture("_MainTex", finaltextures[0]);


			finaltextures[1] = Resources.Load("Entrance_v05") as Texture;
			materialsFinal[2].SetTexture("_MainTex", finaltextures[1]);


			finaltextures[2] = Resources.Load("ChackPoint_v05") as Texture;
			materialsFinal[3].SetTexture("_MainTex", finaltextures[2]);

			finaltextures[3] = Resources.Load("Obstucles_v05") as Texture;
			materialsFinal[4].SetTexture("_MainTex", finaltextures[3]);
		}

		Invoke("CallRunactions",2);
		Resources.UnloadUnusedAssets();
		GC.Collect();
	}

	void CallRunactions()
    {
		if (AdManager.Instance != null)
		{
			AdManager.Instance.IsForceHideBannerAd = false;
		}
		if (AdManager.Instance != null)
		{
			AdManager.Instance.RunActions(AdManager.PageType.InGame);
		}
	}

	public void SetIntialProperties()
	{
		CurrentGameState = GameState.GamePlay;
        Debug.Log("GAME MANAGER: " + StaticVariables._iSelectedVechicle);

		if (!TestMode)
		{
			//CurrentLevel = ListOfLevels [StaticVariables._iSelectedLevel-1].gameObject.GetComponent<SavePointManager>();


			//CurrentLevelObj =Instantiate(Resources.Load("Levels/Level_" + StaticVariables._iSelectedLevel) as GameObject, Vector3.zero, Quaternion.identity,this.transform);

			if (startingLevelInCurrentScene > 0)
			{
				CurrentLevelObj = Instantiate(ListOfLevels[(StaticVariables._iSelectedLevel - startingLevelInCurrentScene)], Vector3.zero, Quaternion.identity, this.transform);
			}
			else
			{
				CurrentLevelObj = Instantiate(ListOfLevels[(StaticVariables._iSelectedLevel - 1)], Vector3.zero, Quaternion.identity, this.transform);
			}


			//PlayerPos = CurrentLevelObj.GetComponentInChildren<GameObject>();
		}
		else
		{
            //CurrentLevel = ListOfLevels [StaticVariables._iSelectedLevel-1].gameObject.GetComponent<SavePointManager>();
            if (TestWithPrefab)
            {
				CurrentLevelObj = Instantiate(TestPrefab, Vector3.zero, Quaternion.identity, this.transform);

            }
            else
            {
				//CurrentLevelObj = Instantiate(Resources.Load("Levels/Level_" + TestLevel) as GameObject, Vector3.zero, Quaternion.identity, this.transform);
				CurrentLevelObj = Instantiate(ListOfLevels[TestLevel - startingLevelInCurrentScene], Vector3.zero, Quaternion.identity, this.transform);


			}

		}
		CurrentLevelObj.gameObject.SetActive (true);

        CurrentLevel= CurrentLevelObj.gameObject.GetComponent<SavePointManager>();
		string value = (StaticVariables._iSelectedVechicle+1).ToString();
		Debug.Log(value);
		GameObject car= Resources.Load(value) as GameObject;
		//CurrentVehile = ListOfVehicle [StaticVariables._iSelectedVechicle].gameObject;

		//CurrentVehile.transform.position = PlayerPosition.position;
		//CurrentVehile.transform.position = CurrentLevelObj.GetComponent<SavePointManager>().LastSavePointPos.position;
		Transform currentVechile= GameObject.Find("PlayerPosition").transform;
		CurrentVehile = Instantiate(car, currentVechile.position, currentVechile.rotation, this.transform);
		CurrentVehile.SetActive(true);
		CurrentVehile.transform.position = currentVechile.position;
		CurrentVehile.transform.rotation = currentVechile.rotation;
        CurrentLevel.LastSavePointPos= currentVechile;
        RCC_Camera.Instance.playerCar =CurrentVehile.gameObject.transform;
	}


	public void Call_ResumeGameplay()
	{
		CurrentGameState = GameState.GamePlay;
		
		Rigidbody rb = CurrentVehile.GetComponent<Rigidbody>();
		rb.isKinematic = false;
		rb.velocity = Vector3.zero;
		rb.angularVelocity = Vector3.zero;
		CurrentVehile.transform.position = CurrentLevel.LastSavePointPos.position;
		CurrentVehile.transform.rotation = CurrentLevel.LastSavePointPos.rotation;
		InGameUIHandler.Instance.InGameEnable ();
		InGameUIHandler.Instance.Revive.SetActive (false);
		obstaclesRestart.Restart();

#if !AdSetup_OFF
        if (FirebaseController.Instance != null)
        {
            FirebaseController.Instance.LogFirebaseEvent("lastfailedAt", "checpoint", "Level_" + StaticVariables._iSelectedLevel + "_pos_" + CurrentLevel.LastSavePointPos.position);
            FirebaseController.Instance.LogFirebaseEvent("ReviveSuccess", "OnrewardSuccess", "Level_" + StaticVariables._iSelectedLevel);
        }
#endif
	}
    //public bool IsGrounded()
    //{
    //    bool isOnGround = false;
    //    if((CurrentVehile.GetComponent<RCC_CarControllerV3>().RearLeftWheelCollider.wheelCollider.isGrounded || CurrentVehile.GetComponent<RCC_CarControllerV3>().RearRightWheelCollider.wheelCollider.isGrounded)||
    //    (CurrentVehile.GetComponent<RCC_CarControllerV3>().FrontLeftWheelCollider.wheelCollider.isGrounded || CurrentVehile.GetComponent<RCC_CarControllerV3>().FrontRightWheelCollider.wheelCollider.isGrounded))
    //    {
    //        isOnGround = true;
    //    }
    //    return isOnGround;
    //}
    
	public bool _IsGrounded;

    public int LevelValue { get; private set; }

 //   public void ResetCar()
	//{
	//	if(IsGrounded())
	//	{


	//		CurrentVehile.transform.position = CurrentLevel.LastSavePointPos.position;
	//		CurrentVehile.transform.rotation = CurrentLevel.LastSavePointPos.rotation;
	//		CurrentVehile.transform.SetParent(null);
	//		Rigidbody rb = CurrentVehile.GetComponent<Rigidbody>();
	//		rb.velocity = Vector3.zero;
	//		rb.angularVelocity = Vector3.zero;
		
	//		obstaclesRestart.Restart();
	//	}
	//}

    public void close()
    {
        _adLoading.SetActive(false);
    }


	void OnDestroy()
	{
		
		foreach (FieldInfo field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
		{
			Type fieldType = field.FieldType;

			if (typeof(IList).IsAssignableFrom(fieldType))
			{
				IList list = field.GetValue(this) as IList;
				if (list != null)
				{
					list.Clear();
				}
			}

			if (typeof(IDictionary).IsAssignableFrom(fieldType))
			{
				IDictionary dictionary = field.GetValue(this) as IDictionary;
				if (dictionary != null)
				{
					dictionary.Clear();
				}
			}

			if (!fieldType.IsPrimitive)
			{
				field.SetValue(this, null);
			}
		}
	}
}
