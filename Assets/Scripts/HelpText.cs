﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpText : MonoBehaviour
{
    public Text[] HelpTexts;
    void Start()
    {
        for (int i = 0; i < HelpTexts.Length; i++)
        {
            HelpTexts[i].text = HelpTexts[i].text.ToUpper();
        }
    }
}
