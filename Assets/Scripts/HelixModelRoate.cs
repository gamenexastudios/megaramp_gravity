﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelixModelRoate : MonoBehaviour {

	// Use this for initialization
	public float HelixSpeed;
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate (Vector3.up * Time.deltaTime * HelixSpeed);
	}
}
