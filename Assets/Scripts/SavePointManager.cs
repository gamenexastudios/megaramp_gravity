﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePointManager : MonoBehaviour 
{
	public Transform LastSavePointPos;
	public static SavePointManager Instance;
	public List<ParticleSystem> ListOfStartParticles;
	public List<ParticleSystem> ListOfEndParticles;
	public bool _StartParticleIsActive;
	public GameObject[] CameraRotateObj;
    public GameObject LcTrigger;

	public void Awake()
	{
		Instance = this;
	}

	public void Start()
    {
 //     StartCoroutine (StartParticleCheck ());
        //StartParticleOn();
        for (int i = 0; i < ListOfEndParticles.Count; i++)
        {
            //if (ListOfStartParticles[i] != null)
            //    ListOfEndParticles[i].Stop();
        }
    }

	public void StartParticleOn()
	{
        if (_StartParticleIsActive)
        {
            for (int i = 0; i < ListOfStartParticles.Count; i++)
            {
                if (ListOfStartParticles[i] != null)
                    ListOfStartParticles[i].Play();
            }
            Invoke("StopParticleOff", 2f);
        }
    }

	public void StopParticleOff()
	{
        if (_StartParticleIsActive)
        {
            for (int i = 0; i < ListOfStartParticles.Count; i++)
            {
                if (ListOfStartParticles[i] != null)
                    ListOfStartParticles[i].Stop();
            }
            Invoke("StartParticleOn", 2f);
        }
    }



	private IEnumerator StartParticleCheck()
	{        
		//Wait for 14 secs.
		yield return new WaitForSeconds(3);

		//Turn My game object that is set to false(off) to True(on).
		StartParticleOn();

		//Turn the Game Oject back off after 1 sec.
		yield return new WaitForSeconds(4);

		//Game object will turn off
		StopParticleOff();
	}

	public void EndParticleActivate()
	{
        for (int i = 0; i < ListOfEndParticles.Count; i++)
        {
            ListOfEndParticles[i].Play();
        }
    }
	public void Update()
	{
		
	}

}
