﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    public float Speed;
    public float Length;
    public Vector3 direction;
    Vector3 startPos;
    
    void Start()
    {
        startPos = transform.localPosition;
    }
    private void FixedUpdate()
    {
        float _amt = Mathf.PingPong(Time.time * Speed, Length);
        //print(_amt);
        transform.localPosition = startPos + direction * _amt;

    }
}
