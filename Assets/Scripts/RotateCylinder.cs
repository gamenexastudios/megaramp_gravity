﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCylinder : MonoBehaviour
{
	public float SpeedOfObject;
	public GameObject RotateObject;
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		RotateObject.transform.Rotate (Vector3.left * Time.deltaTime * SpeedOfObject);
	}
}
