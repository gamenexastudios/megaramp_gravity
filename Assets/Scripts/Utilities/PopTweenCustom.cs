﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PopTweenCustom : MonoBehaviour 
{
    public enum AnimationsAt
    {
        AtAwake,
        AtStart,
        AtOnEnable
    }
    public AnimationsAt AnimationAt;

    public float ScaleTo;

	void Awake()
	{
        if (AnimationAt == AnimationsAt.AtAwake)
        {
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", ScaleTo, "y", ScaleTo, "time", 0.25f, "islocal", true, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
        }
    }

    void Start()
    {
        if (AnimationAt == AnimationsAt.AtStart)
        {
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", ScaleTo, "y", ScaleTo, "time", 0.25f, "islocal", true, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
        }
    }

    void OnEnable()
    {
        if (AnimationAt == AnimationsAt.AtOnEnable)
        {
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", ScaleTo, "y", ScaleTo, "time", 0.25f, "islocal", true, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
        }
    }

    public void CallFromOtherScript()
    {
        iTween.ScaleTo(this.gameObject, iTween.Hash("x", ScaleTo, "y", ScaleTo, "time", 0.25f, "islocal", true, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
    }
}
