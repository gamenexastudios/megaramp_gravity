﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundBtn : MonoBehaviour 
{
	public static SoundBtn myScript;

	public GameObject Btn_Sound;

	public Sprite BtnTex_On,BtnTex_Off;

	public Text Text_Sound;

	[HideInInspector]
	public string Is_Sound;

	public string SoundString;

	void Awake()
	{
		myScript=this;
        AwakeFunction();
    }
    public void AwakeFunction()
    {
        if (!PlayerPrefs.HasKey(SoundString))
        {
            PlayerPrefs.SetString(SoundString, "On");
        }
        Is_Sound = PlayerPrefs.GetString(SoundString);
        CheckSound();
    }
	public void SoundBtnAct()
	{
		if (Is_Sound == "On")
		{
			Is_Sound = "Off";
			PlayerPrefs.SetString(SoundString, Is_Sound);
			CheckSound();
		}
		else
		{
			Is_Sound = "On";
			PlayerPrefs.SetString(SoundString, Is_Sound);
			CheckSound();
		}
	}
	public Image ImgSound_Off, ImgSound_On;

	public void CheckSound()
	{
		if (Is_Sound=="Off")
		{
			Btn_Sound.GetComponent<Image>().sprite=BtnTex_Off;
			if (Text_Sound != null)
			{
				Text_Sound.text = "Sound Off";
				Text_Sound.color = new Color (Text_Sound.color.r,Text_Sound.color.g, Text_Sound.color.b, 0.7f);
				if (ImgSound_Off != null || ImgSound_On != null) {
					ImgSound_On.enabled = false;
					ImgSound_Off.enabled = true;
				}
			}
			AudioListener.volume = 0;
		}
		else
		{
			if (Text_Sound != null)
			{
				Text_Sound.text = "Sound On";

				Text_Sound.color = new Color (Text_Sound.color.r,Text_Sound.color.g, Text_Sound.color.b, 1f);
//				Text_Sound.color = new Color (1, 1, 1, 1);
				if (ImgSound_Off != null || ImgSound_On != null) 
				{
					ImgSound_On.enabled = true;
					ImgSound_Off.enabled = false;
				}
			}
			Btn_Sound.GetComponent<Image>().sprite=BtnTex_On;
			AudioListener.volume = 1;
		}
	}
}
