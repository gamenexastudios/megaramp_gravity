﻿using UnityEngine;

using System.Collections;
using UnityEngine.UI;

public class ColorChange : MonoBehaviour 
{
	public static ColorChange myScript;
	public bool Is_ColorChange;
	
	void Awake()
	{
		myScript=this;
	}
	
	float timeLeft;
	Color targetColor;

	void Update()
	{
		if (!Is_ColorChange || !this.gameObject.activeInHierarchy)
			return;
		
		if (timeLeft <= Time.deltaTime)
		{
			if (this.gameObject.GetComponent<Image> ()!=null)
			{
				this.gameObject.GetComponent<Image> ().color = targetColor;
			}
			if (this.gameObject.GetComponent<Text> ()!=null)
			{
				this.gameObject.GetComponent<Text> ().color = targetColor;
			}

			targetColor = new Color(Random.value, Random.value, Random.value);
			timeLeft = 0.2f;
		}
		else
		{
			if (this.gameObject.GetComponent<Image> ()!=null)
			{
				this.gameObject.GetComponent<Image> ().color = Color.Lerp (this.gameObject.GetComponent<Image> ().color, targetColor, Time.deltaTime / timeLeft);
			}
			if (this.gameObject.GetComponent<Text> ()!=null)
			{
				this.gameObject.GetComponent<Text> ().color = Color.Lerp (this.gameObject.GetComponent<Text> ().color, targetColor, Time.deltaTime / timeLeft);
			}

			timeLeft -= Time.deltaTime;
		}
	}
}