﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeCasting : MonoBehaviour 
{
	public static TypeCasting myScript;

	void Awake()
	{
		myScript=this;
	}

	public static int CharToInt(char c)
	{
		return (int)(c - '0');
	}

	public static char IntToChar(int _int,int _index)
	{
		string _tempS = _int.ToString ();
		return _tempS [_index];
	}

    public static int[] StringToIntArray(string _string)
    {
        char[] _tempchar = _string.ToCharArray();
        int[]  _arrayList = new int[_tempchar.Length];
        for (int i = 0; i < _tempchar.Length; i++)
        {
            _arrayList[i] = TypeCasting.CharToInt(_tempchar[i]);
        }
        return _arrayList;
    }

    public static int[] CharArrayToIntArray(char[] _charArray)
    {
        int[] _intarrayList = new int[_charArray.Length];
        for (int i = 0; i < _intarrayList.Length; i++)
        {
            _intarrayList[i] = (int)(_charArray[i] - '0');
        }
        return _intarrayList;
    }

    public static int[] StrinArrayToIntArray(string[] _stringarray)
    {
        int[] _intArray=new int[_stringarray.Length];
        for (int i = 0; i < _stringarray.Length; i++)
        {
            _intArray[i] = int.Parse(_stringarray[i]);
        }

        return _intArray;
    }
}
