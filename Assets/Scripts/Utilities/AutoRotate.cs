﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class AutoRotate : MonoBehaviour 
{
	public static AutoRotate myScript;
	public Vector3 RotateValues;

	void Awake()
	{
		myScript=this;
	}

	void Update () 
	{
		transform.Rotate(RotateValues*Time.deltaTime);
	}
}
