﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UtilityRS : MonoBehaviour 
{
	public static UtilityRS myScript;

	void Awake()
	{
		myScript=this;
		DontDestroyOnLoad (this.gameObject);
	}

	public void SetActiveWithDelay(GameObject TargetObj, bool IsActive,float Delay)
	{
		StartCoroutine (Call_SetActive(Delay,TargetObj,IsActive));
	}

	IEnumerator Call_SetActive(float Delay, GameObject TempObj,bool ActiveState)
	{
		yield return new WaitForSeconds (Delay);
		TempObj.gameObject.SetActive (ActiveState);
	}

	public static bool IsNeedImageFalse;
	public static void Value(GameObject TweenObj,float ValueNeed,float TweenTime,float DelayTime,iTween.EaseType EaseType, bool ObjActive)
	{
		GameObject TempObj = TweenObj;
		TempObj.gameObject.SetActive (true);
		IsNeedImageFalse = ObjActive;
		TempObj.AddComponent<AlphaScript>();

		if (TweenObj.GetComponent<Image>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from", TweenObj.gameObject.GetComponent<Image> ().fillAmount, "to", ValueNeed, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, "onupdate", "ValueNeedTo", "oncomplete", "OnValueComplete", "oncompletetarget", TweenObj.gameObject));
		} 
	}

	public void ValueNeedTo (float value)
	{
		this.gameObject.GetComponent<Image> ().fillAmount = value;
	}

	public void OnValueComplete ()
	{
		if(!IsNeedImageFalse)
		{
			this.gameObject.SetActive(false);
		}
		//		TempObj.gameObject.GetComponent<Image> ().color = new Color (TempObj.gameObject.GetComponent<Image> ().color.r, TempObj.gameObject.GetComponent<Image> ().color.g, TempObj.gameObject.GetComponent<Image> ().color.b, AlphaValue);
		if (this.GetComponent<AlphaScript> () != null)
		{
			Destroy (this.GetComponent<AlphaScript> ());
		}
	}

	#region RANDOMIZE AN ARRAY

	public static int[] RandomArray_Int(int[] _SourceArray)
	{
		for (int i = 0; i <_SourceArray.Length; i++)
		{
			int tmp = _SourceArray [i];
			int rand = Random.Range (i, _SourceArray.Length);
			_SourceArray [i] = _SourceArray [rand];
			_SourceArray [rand] = tmp;
		}
		return _SourceArray;
	}

	#endregion
}



