﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PopTween : MonoBehaviour 
{
	void Awake()
	{
		
	}

	void OnEnable () 
	{
        iTween.ScaleTo(this.gameObject, iTween.Hash("x", 1.1f, "y", 1.1f, "time", 0.25f, "islocal", true, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
    }
}
