﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using TMPro;
using System.Reflection;

public class DailyBonusManager : MonoBehaviour 
{
	public static DailyBonusManager myScript;

    public GameObject sevenPlusButton;

	public GameObject DailyBonusObj;
	public float DelayForShowBonus;
	public GameObject[] BtnCollect;

	[HideInInspector]
	public int PreviousBonus;

	DateTime oldDate;
	DateTime currentDate;
	TimeSpan Total_Diff_Time;

	int Diff_Days;
	long temp;

	[HideInInspector]
	public int BonusAt_PreviousDay;
	[HideInInspector]
	public int BonusAt_Day;
	public static string LastDayTime = "LastDayTimeA";
	public static string BonusTaken = "BonusTakenB";
	public static string PreviousBonusC="PreviousBonusCC";

	public int[] Rewards1;
	public GameObject[] TomorrowBanner;
	public GameObject[] TickMark;
    public GameObject[] ParentPanel;
    public GameObject[] ClaimDummy;


    public TextMeshProUGUI[] Text_Bonus;


    public RectTransform Btn_ClaimBtn;

    public Sprite claimedBG;
    public Sprite currentDayBG;
    public Sprite tomorrowBG;

	void Awake()
	{

            myScript = this;

            this.gameObject.SetActive(false);



            awakeFunction();     
        
    }
 

    public void awakeFunction()
    {


        if (PlayerPrefs.HasKey(LastDayTime) == false)
        {
            PlayerPrefs.SetString(LastDayTime, System.DateTime.Now.ToBinary().ToString());
        }

        if (!PlayerPrefs.HasKey(BonusTaken))
        {
            PlayerPrefs.SetString(BonusTaken, "false");
        }

        for (int i = 0; i < BtnCollect.Length; i++)
        {
            BtnCollect[i].gameObject.SetActive(false);
        }

        StaticVariables.daysPassed = PlayerPrefs.GetInt("daysPassed", 0);

        CheckDailyBonus();



    }

	public void Set_BonuseText(int _day)
	{

        //Debug.LogError("DAY VALUE :::::::::: " + _day);
		for (int i = 0; i < Text_Bonus.Length; i++) 
		{
			if ((_day+1) == i) 
			{
                TomorrowBanner[i].gameObject.SetActive(true);

            }
            else
            {
                TomorrowBanner[i].gameObject.SetActive(false);

            }

   //         if (i == (_day + 1) && (_day+1)<=Text_Bonus.Length) 
			//{
   //             //Debug.LogError("tomorrow true day=" + _day + "::i=" + i);

   //             TomorrowBanner[i].gameObject.SetActive (true);
			//	//Text_Bonus [i].alignment = TextAnchor.MiddleLeft;
			//}
			//else 
			//{
   //             //Debug.LogError("tomorrow false day=" + _day + "::i=" + i);

   //             TomorrowBanner[i].gameObject.SetActive (false);
			//}

			if (_day >= 1) 
			{
				if (i <= _day - 1) 
				{
                    //Debug.LogError("tick true day="+_day+"::i="+i+"day-1="+(_day-1));

                    ParentPanel[i].GetComponent<Image>().sprite = claimedBG;
                    TickMark[i].gameObject.SetActive(true);
                }
                else 
				{
                    //Debug.Log("TICK MARK ##############  FALSE " + i);
                    //Debug.LogWarning("tick false day=" + _day + "::i=" + i);

                    TickMark[i].gameObject.SetActive (false);
				}
			}
			else 
			{
                //Debug.Log("TICK MARK ##############  FALSE " + i);
                //Debug.LogWarning("tick false 2222 day=" + _day + "::i=" + i);

                TickMark[i].gameObject.SetActive (false);
			}
			Text_Bonus[i].text=""+Rewards1[i]+" COINS";
            //TickMark[i].gameObject.SetActive(true);


        }

        //print ("DAY : " + _day);
        if (PlayerPrefs.GetInt("DayNumberG") > 7)
        {
            PlayerPrefs.SetInt("DayNumberG", 7);
        }

    }

	public bool CheckDailyBonus()
	{
		currentDate = System.DateTime.Now;
		temp = Convert.ToInt64(PlayerPrefs.GetString(LastDayTime));
		oldDate = DateTime.FromBinary(temp);
		Total_Diff_Time = 	currentDate.Subtract(oldDate);
		Diff_Days=Total_Diff_Time.Days;
        print("Total Diff Days : " + Diff_Days);

        if (!PlayerPrefs.HasKey(PreviousBonusC))
		{
			
			PlayerPrefs.SetInt(PreviousBonusC,0);
			BonusAt_Day=1;
			PlayerPrefs.SetInt(PreviousBonusC,BonusAt_Day);
			Invoke("ShowBonus",DelayForShowBonus);
            print("Bonus AT FirstTime");

            return true;
        }
		else
		{
			BonusAt_PreviousDay=PlayerPrefs.GetInt(PreviousBonusC);
			if(Diff_Days>=1)
			{
				Diff_Days=1;
				BonusAt_Day=BonusAt_PreviousDay+1;
				PlayerPrefs.SetInt(PreviousBonusC,BonusAt_Day);
				PlayerPrefs.SetString(BonusTaken,"false");
				Invoke("ShowBonus",DelayForShowBonus);
                print("Bonus AT Normal Day");


                //Testing

                PlayerPrefs.SetString(LastDayTime, System.DateTime.Now.ToBinary().ToString());
                return true;      
            }
            else
			{
				if(PlayerPrefs.GetString(BonusTaken)=="false")
				{
					Diff_Days=1;
					BonusAt_Day=BonusAt_PreviousDay;
					PlayerPrefs.SetInt(PreviousBonusC,BonusAt_Day);
					PlayerPrefs.SetString(BonusTaken,"false");
					Invoke("ShowBonus",DelayForShowBonus);
                    print("Bonus At Missed Day");                  
                    return true;
                }
				else
				{
                    print("Bonus Already Taken");
                    StaticVariables._dailyBonusDiplayed = false;
                    if (!DailyBonusMenuManager._instance.IsBonusButtonClick && SplPackHandler.Instance != null)
                    {
                        SplPackHandler.Instance.CheckNOpenSplPack();
                    }
                    DailyBonusMenuManager._instance.IsBonusButtonClick = false;
                    return false;
                }
			}
		}

        print("Bonus Day : " + BonusAt_Day);
    }

    public GameObject PopUp_AlphaBG;
    public Text Text_2xText;
    public void ShowBonus()
	{
        PlayerPrefs.SetInt("DailyBonusButton", 1);
        if (!StaticVariables._dailyBonusShown)
       {
            StaticVariables._dailyBonusShown = true;
           // Debug.LogError("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            showBonusFunction();
        }
    }

    public void showBonusFunction()
    {
        Debug.LogError("Show Bonus Function BonusAt_Day="+ BonusAt_Day);
        if (AdManager.Instance != null)
        {
            AdManager.Instance.IsForceHideBannerAd = true;
        }
        if (AdController.Instance != null)
        {
            AdController.Instance.HideIronSourceBannerAd();
        }

        StaticVariables._dailyBonusDiplayed = true;
        DailyBonusObj.gameObject.SetActive(true); // Showing DailyBonus Page Here
        Tween_DailyBonus._instance.Call_TweenIn();

        //Rate us logic
        StaticVariables.daysPassed = PlayerPrefs.GetInt("daysPassed", 0);
        StaticVariables.daysPassed += 1;
        PlayerPrefs.SetInt("daysPassed", StaticVariables.daysPassed);

        if (SplashAd.instance._rateUsLaunchDisplay)
        {
            StaticVariables.daysPassed_Launch = PlayerPrefs.GetInt("daysPassed_Launch", 0);
            StaticVariables.daysPassed_Launch += 1;
            PlayerPrefs.SetInt("daysPassed_Launch", StaticVariables.daysPassed_Launch);
        }

        if (StaticVariables.daysPassed > StaticVariables.displayDate_Counter)
        {
            StaticVariables.daysPassed = 0;
            StaticVariables.daysPassed += 1;
            PlayerPrefs.SetInt("daysPassed", StaticVariables.daysPassed);
        }

        //End Rate Us Logic

        iTween.ScaleTo(DailyBonusObj.gameObject, iTween.Hash("x", 1, "y", 1, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));

        if (BonusAt_Day > BtnCollect.Length)
        {
            sevenPlusButton.SetActive(true);
            Set_BonuseText(BtnCollect.Length - 1);
        }
        else
        {
            ParentPanel[BonusAt_Day - 1].GetComponent<Image>().sprite = currentDayBG;
            BtnCollect[BonusAt_Day - 1].gameObject.SetActive(true);
            Set_BonuseText(BonusAt_Day - 1);
        }
    }
		
	public void CollecctBonus()
	{
        ParentPanel[PlayerPrefs.GetInt("DayNumberG")].GetComponent<Image>().sprite = claimedBG;
        //    ClaimDummy[PlayerPrefs.GetInt("DayNumberG")].SetActive(false);
        if (PlayerPrefs.GetInt("DayNumberG") > 7)
        {
            PlayerPrefs.SetInt("DayNumberG", 7);
        }

        Debug.Log("DAY G NUMBER ########### " + PlayerPrefs.GetInt("DayNumberG"));
        PlayerPrefs.SetInt("DayNumberG", PlayerPrefs.GetInt("DayNumberG", 0) + 1);
        

        StaticVariables._onClaimButtonClick = true;
        Btn_2XDailyBonus.gameObject.SetActive(true);

        Invoke("FalsingTheClaimClick", 8.0f);
		PlayerPrefs.SetString (BonusTaken, "true");

        PlayerPrefs.SetString (LastDayTime, System.DateTime.Now.ToBinary ().ToString ());

		if (BonusAt_Day >= Rewards1.Length) 
		{
            Debug.Log("COLLECT BONUS HERE : 7+");
            MenuUIManager.Instance.UpdateScore(Rewards1[Rewards1.Length - 1]);
           // DailyBonusMenuManager._instance._scoreToUpdate = Rewards1[Rewards1.Length - 1];
           // DailyBonusMenuManager._instance.AddingScoreEnCall();
        }
        else
		{
            Debug.Log("COLLECT BONUS HERE");
            MenuUIManager.Instance.UpdateScore(Rewards1[BonusAt_Day-1]);
           // DailyBonusMenuManager._instance._scoreToUpdate = Rewards1[BonusAt_Day - 1];
           // DailyBonusMenuManager._instance.AddingScoreEnCall();
        }

        // Debug.Log("Bonus Day:" + BonusAt_Day);

        //DailyBonusObj.gameObject.SetActive(false);
        ////////////////////////////COMMENTED///////////////////////////
         iTween.ScaleTo(DailyBonusObj.gameObject, iTween.Hash("x", 0, "y", 0, "time", 0.1f, "delay", 0, "easetype", iTween.EaseType.linear));



        //Text_2xText.text = "2" +" "+"x"+" "+ Rewards1 [BonusAt_Day- 1] +" "+"=" +" "+ 2 * Rewards1 [BonusAt_Day - 1];

        //PopUpHandler.myScript.PopUp2XDailyBonus_In();




        DailyBonusMenuManager._instance.ButtonActivation(false);
        PlayerPrefs.SetInt("DailyBonusButton", 0);
        //if (!DailyBonusMenuManager._instance.IsBonusButtonClick && SplPackHandler.Instance != null)
        //{
        //    SplPackHandler.Instance.CheckNOpenSplPack();
        //}
        //DailyBonusMenuManager._instance.IsBonusButtonClick = false;
        //Rate us
        //   SplashAd.instance.displayRateUs();
        // DailyBonusMenuManager._instance.AddCoinAddingAnimation();
    }
    public GameObject Btn_2XDailyBonus;
    public void VideoSuccess2XDailyBonus()
    {
        Debug.Log("VideoSuccess2XDailyBonus");
        // MenuUIManager.Instance.UpdateScore(Rewards1[BonusAt_Day - 1]*2);  //Going Triple
        MenuUIManager.Instance.UpdateScore(Rewards1[BonusAt_Day - 1]);
        StaticVariables._dailyBonusDiplayed = false;
        StaticVariables._onClaimButtonClick = false;
        Btn_2XDailyBonus.gameObject.SetActive(false);
        if (AdManager.Instance != null)
        {
            AdManager.Instance.IsForceHideBannerAd = false;
        }
        if (!DailyBonusMenuManager._instance.IsBonusButtonClick && SplPackHandler.Instance != null)
        {
            SplPackHandler.Instance.CheckNOpenSplPack();
        }
        

        if (AdManager.Instance != null)
        {
            AdManager.Instance.CheckNShowBannerAd();
        }
        
        DailyBonusMenuManager._instance.IsBonusButtonClick = false;
    }


    public void BtnAct_Share()
    {
        Debug.Log("SHARE");
    }

    public void FalsingTheClaimClick()
    {
        DailyBonusMenuManager._instance.AddCoinAddingAnimation();
        StaticVariables._onClaimButtonClick = false;
        DailyBonusMenuManager._instance.AddCoinAddingAnimation();
        Btn_2XDailyBonus.gameObject.SetActive(false);

        if (!DailyBonusMenuManager._instance.IsBonusButtonClick && SplPackHandler.Instance != null)
        {
            SplPackHandler.Instance.CheckNOpenSplPack();
        }
        DailyBonusMenuManager._instance.IsBonusButtonClick = false;
    }

    public void DailyBonusCloseButton()
    {

        DailyBonusObj.gameObject.SetActive(false); // Showing DailyBonus Page Here
        if (AdManager.Instance != null)
        {
            AdManager.Instance.IsForceHideBannerAd = false;
        }
        if (!DailyBonusMenuManager._instance.IsBonusButtonClick && SplPackHandler.Instance != null)
        {
            SplPackHandler.Instance.CheckNOpenSplPack();
        }
        DailyBonusMenuManager._instance.IsBonusButtonClick = false;
        if (AdManager.Instance != null)
            AdManager.Instance.CheckNShowBannerAd();
    }
    public void CloseDoubleClaimPannel()
    {
        Debug.Log("Close claim panle Daily bonus menu manager");
        CancelInvoke("FalsingTheClaimClick");

        StaticVariables._onClaimButtonClick = false;
        DailyBonusMenuManager._instance.AddCoinAddingAnimation();
        Btn_2XDailyBonus.gameObject.SetActive(false);
        if (AdManager.Instance != null)
        {
            AdManager.Instance.IsForceHideBannerAd = false;
        }
        if (!DailyBonusMenuManager._instance.IsBonusButtonClick && SplPackHandler.Instance != null)
        {
            SplPackHandler.Instance.CheckNOpenSplPack();
        }
        

        if (AdManager.Instance != null)
        {
            AdManager.Instance.CheckNShowBannerAd();
        }
        
        DailyBonusMenuManager._instance.IsBonusButtonClick = false;
    }


    void OnDestroy()
    {

        foreach (FieldInfo field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            Type fieldType = field.FieldType;

            if (typeof(IList).IsAssignableFrom(fieldType))
            {
                IList list = field.GetValue(this) as IList;
                if (list != null)
                {
                    list.Clear();
                }
            }

            if (typeof(IDictionary).IsAssignableFrom(fieldType))
            {
                IDictionary dictionary = field.GetValue(this) as IDictionary;
                if (dictionary != null)
                {
                    dictionary.Clear();
                }
            }

            if (!fieldType.IsPrimitive)
            {
                field.SetValue(this, null);
            }
        }
    }
}
