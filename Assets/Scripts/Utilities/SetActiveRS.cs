﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetActiveRS : MonoBehaviour 
{
	public static SetActiveRS myScript;
	public bool Is_ActiveState;

	void Awake()
	{
		myScript=this;
	}

	void Start () 
	{
		this.gameObject.SetActive (Is_ActiveState);
	}

}
