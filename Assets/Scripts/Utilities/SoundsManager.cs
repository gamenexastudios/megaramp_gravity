﻿using UnityEngine;
using System.Collections;

public class SoundsManager : MonoBehaviour 
{
	public static SoundsManager myScript;

	public GameObject Sound_MenuBG,Sound_BtnAct,Sound_UpgradeBG,Sound_PlayAreaBG;
	public GameObject Sound_LC,Sound_LF;

	public GameObject Sound_Punch1,Sound_Punch2,Sound_Kick1,Sound_Kick2;
	public GameObject Sound_SpecailPunch,Sound_PlayerDie;

	public GameObject Sound_PlayBG1,Sound_PlayBG2,Sound_PlayBG3,Sound_PlayBG4,Sound_PlayBG5,Sound_PlayBG6;

	public enum Sounds
	{
		None,MenuBG,UpgradeBG,BtnAct,LC,LF,PlayAreaBG,
		Punch1,Punch2,Kick1,Kick2,PlayerDie,
		SpecialPunch,
		PlayBG1,PlayBG2,PlayBG3,PlayBG4,PlayBG5,PlayBG6
	};

	[HideInInspector]
	public Sounds SoundClip;

	void Awake()
	{
		myScript = this;
		DontDestroyOnLoad (this.gameObject);
	}

	public void StopAllSounds()
	{
		StopSound(Sound_MenuBG);
		StopSound(Sound_UpgradeBG);
		StopSound(Sound_BtnAct);
		StopSound(Sound_LC);
		StopSound(Sound_LF);
		StopSound(Sound_PlayAreaBG);

		StopSound(Sound_PlayBG1);
		StopSound(Sound_PlayBG2);
		StopSound(Sound_PlayBG3);
		StopSound(Sound_PlayBG4);
		StopSound(Sound_PlayBG5);
		StopSound(Sound_PlayBG6);
	}

	void StopSound(GameObject _SoundSource)
	{
		if(_SoundSource!=null)
		ValueTo.SetValueTo (_SoundSource, 0f, 1f, 0f, iTween.EaseType.linear);
	}

	public void PlaySound(Sounds _sound,bool _isloop)
	{
		switch (_sound)
		{
		case Sounds.MenuBG:
			PlaySound (Sound_MenuBG,0.65f,1,_isloop);
			break;
		case Sounds.BtnAct:
			PlaySound (Sound_BtnAct, _isloop);
			break;
		case Sounds.LC:
			PlaySound (Sound_LC, _isloop);
			break;
		case Sounds.LF:
			PlaySound (Sound_LF, _isloop);
			break;
		case Sounds.UpgradeBG:
			PlaySound (Sound_UpgradeBG,0.65f,1, _isloop);
			break;
		case Sounds.PlayAreaBG:
			PlaySound (Sound_PlayAreaBG, _isloop);
			break;
		case Sounds.Punch1:
			PlaySound (Sound_Punch1,0.4f, _isloop);
			break;
		case Sounds.Punch2:
			PlaySound (Sound_Punch2, 0.4f,_isloop);
			break;
		case Sounds.Kick1:
			PlaySound (Sound_Kick1,0.4f, _isloop);
			break;
		case Sounds.Kick2:
			PlaySound (Sound_Kick2,0.4f, _isloop);
			break;
		case Sounds.SpecialPunch:
			PlaySound (Sound_SpecailPunch,0.4f, _isloop);
			break;
		case Sounds.PlayerDie:
			PlaySound (Sound_PlayerDie,0.4f, _isloop);
			break;

		case Sounds.PlayBG1:
			PlaySound (Sound_PlayBG1,0.05f, _isloop);
			break;
		case Sounds.PlayBG2:
			PlaySound (Sound_PlayBG2,0.05f, _isloop);
			break;
		case Sounds.PlayBG3:
			PlaySound (Sound_PlayBG3,0.05f, _isloop);
			break;
		case Sounds.PlayBG4:
			PlaySound (Sound_PlayBG4,0.05f, _isloop);
			break;
		case Sounds.PlayBG5:
			PlaySound (Sound_PlayBG5,0.05f, _isloop);
			break;
		case Sounds.PlayBG6:
			PlaySound (Sound_PlayBG6,0.05f, _isloop);
			break;
		}
	}

	void PlaySound(GameObject _SoundSource, bool _isloop)
	{
		if(_SoundSource != null)
		{
			ValueTo.SetValueTo (_SoundSource, 1f, 1f, 0f, iTween.EaseType.linear);
			_SoundSource.GetComponent<AudioSource> ().Play ();
			_SoundSource.GetComponent<AudioSource> ().loop = _isloop;
		}
	}

	void PlaySound(GameObject _SoundSource, float _vol,bool _isloop)
	{
		if(_SoundSource != null)
		{
			ValueTo.SetValueTo (_SoundSource, _vol, 0f, 0f, iTween.EaseType.linear);
			_SoundSource.GetComponent<AudioSource> ().Play ();
			_SoundSource.GetComponent<AudioSource> ().loop = _isloop;
		}
	}
	void PlaySound(GameObject _SoundSource, float _vol,float _delay,bool _isloop)
	{
		if(_SoundSource != null)
		{
			ValueTo.SetValueTo (_SoundSource, _vol, 0.5f, _delay, iTween.EaseType.linear);
			_SoundSource.GetComponent<AudioSource> ().Play ();
			_SoundSource.GetComponent<AudioSource> ().loop = _isloop;
		}
	}

//	public void PlaySoundOneShot(Sounds _sound,bool _isloop)
//	{
//		switch (_sound)
//		{
//		case Sounds.Punch1:
//			PlaySound (Sound_Punch1, _isloop);
//			break;
//		case Sounds.Punch2:
//			PlaySound (Sound_Punch2, _isloop);
//			break;
//		case Sounds.Kick1:
//			PlaySound (Sound_Kick1, _isloop);
//			break;
//		case Sounds.Kick2:
//			PlaySound (Sound_Kick2, _isloop);
//			break;
//		}
//	}
//
//	void PlaySoundOneShot(GameObject _SoundSource, bool _isloop)
//	{
//		if(_SoundSource != null)
//		{
//			_SoundSource.GetComponent<AudioSource> ().Play ();
//			_SoundSource.GetComponent<AudioSource> ().loop = _isloop;
//		}
//	}

}
