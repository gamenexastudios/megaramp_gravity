﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGamePrefs : MonoBehaviour 
{
    // PLAYERPREFS STRINGS
    public static string Is_NoAds = "Is_NoAdsS";

	public static string Is_Rated = "Is_RatedA";

	public static string Is_Shared = "Is_SharedB";

	public static string Is_RewardShared = "Is_RewardSharedC";

	public static string TotalCoins = "TotalCoinsB";

    public static string CurrentLevel = "CurrentlevelinGame";

    public static int CurrentAppVersion = 0;

    // ONLY STATIC VARIABLES
    public static string LoadingScene="Loading";

    public static string UnlockedCars = "100000000000000";
}
