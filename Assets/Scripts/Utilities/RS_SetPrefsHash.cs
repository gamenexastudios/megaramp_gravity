﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RS_SetPrefsHash : MonoBehaviour 
{
	public static RS_SetPrefsHash myScript;
	static bool Is_Created;
	void Awake()
	{
		if(!Is_Created)
		{
			DontDestroyOnLoad(gameObject);
			myScript = this;
			Is_Created = true;
		}
		else
		{
			Destroy(gameObject);
		}
	}

	public static void SetHashPrefs_Int(string PrefVar, int Value)
	{
		if (!PlayerPrefs.HasKey (PrefVar))
		{PlayerPrefs.SetInt (PrefVar,Value);}
	}

	public static void SetHashPrefs_String(string PrefVar, string Value)
	{
		if (!PlayerPrefs.HasKey (PrefVar))
		{PlayerPrefs.SetString (PrefVar,Value);}

	}

	public static void SetHashPrefs_Float(string PrefVar, float Value)
	{
		if (!PlayerPrefs.HasKey (PrefVar))
		{PlayerPrefs.SetFloat (PrefVar,Value);}
	}
}
