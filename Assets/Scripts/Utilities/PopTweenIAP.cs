﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PopTweenIAP : MonoBehaviour 
{
    public enum AnimationsAt
    {
        AtAwake,
        AtStart,
        AtOnEnable
    }
    public enum UnlockType
    {
        World,
        All,
    }
    public AnimationsAt AnimationAt;
    public UnlockType unlockType;


    public float ScaleTo;

	void Awake()
	{
        if (AnimationAt == AnimationsAt.AtAwake && unlockType == UnlockType.World && AdManager.Instance != null && AdManager.LevelsUnlocked != 1)
        {
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", ScaleTo, "y", ScaleTo, "time", 0.25f, "islocal", true, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
        }

        if (AnimationAt == AnimationsAt.AtAwake && unlockType == UnlockType.All && AdManager.Instance != null && (AdManager.LevelsUnlocked != 1 || AdManager.UpgradeUnlocked !=1))
        {
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", ScaleTo, "y", ScaleTo, "time", 0.25f, "islocal", true, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong));
        }
    }
}
