﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueTo : MonoBehaviour 
{
	public static ValueTo myScript;

	void Awake()
	{
		myScript=this;
	}

	public static void SetValueTo(GameObject TweenObj,float _startValue,float _targetValue,float TweenTime,float DelayTime,iTween.EaseType EaseType)
	{
		GameObject TempObj = TweenObj;
		TempObj.AddComponent<ValueTo>();
		if (TweenObj.GetComponent<Text>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",_startValue , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
				"onupdate", "ValueUpdateTextf", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		} 
		else
		if (TweenObj.GetComponent<Image>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",_startValue , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
				"onupdate", "ValueUpdateFillf", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		} 
		else
		if (TweenObj.GetComponent<AudioSource>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",_startValue , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
						"onupdate", "ValueUpdateVolumef", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		} 
	}

	public static void SetValueTo(GameObject TweenObj,int _startValue,int _targetValue,float TweenTime,float DelayTime,iTween.EaseType EaseType)
	{
		GameObject TempObj = TweenObj;
		TempObj.AddComponent<ValueTo>();
		if (TweenObj.GetComponent<Text>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",_startValue , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
				"onupdate", "ValueUpdateTextI", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		} 
		else
		if (TweenObj.GetComponent<Image>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",_startValue , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
				"onupdate", "ValueUpdateFillI", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		} 
		else
		if (TweenObj.GetComponent<AudioSource>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",_startValue , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
				"onupdate", "ValueUpdateVolumeI", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		} 
	}

	public static void SetValueTo(GameObject TweenObj,float _targetValue,float TweenTime,float DelayTime,iTween.EaseType EaseType)
	{
		GameObject TempObj = TweenObj;
		TempObj.AddComponent<ValueTo>();
		if (TweenObj.GetComponent<Text>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",int.Parse(TweenObj.gameObject.GetComponent<Text> ().text) , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
				"onupdate", "ValueUpdateTextf", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		} 
		else
		if (TweenObj.GetComponent<Image>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",TweenObj.GetComponent<Image>().fillAmount , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
					"onupdate", "ValueUpdateFillf", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		} 
		else
		if (TweenObj.GetComponent<AudioSource>()!=null) 
		{
			iTween.ValueTo (TweenObj.gameObject, iTween.Hash ("from",TweenObj.gameObject.GetComponent<AudioSource>().volume , "to", _targetValue, "time", TweenTime, "delay", DelayTime, "easetype", EaseType, 
						"onupdate", "ValueUpdateVolumef", "oncomplete", "OnValueToComplete", "oncompletetarget", TweenObj.gameObject));
		}
	}



	public void ValueUpdateTextI(int _updatevalue)
	{
		this.gameObject.GetComponent<Text> ().text = ""+_updatevalue;
	}
	public void ValueUpdateFillI(int _updatevalue)
	{
		this.gameObject.GetComponent<Image> ().fillAmount = +_updatevalue;
	}
	public void ValueUpdateVolumeI(int _updatevalue)
	{
		this.gameObject.GetComponent<AudioSource> ().volume =_updatevalue;
	}

	public void ValueUpdateTextf(float _updatevalue)
	{
		this.gameObject.GetComponent<Text> ().text = ""+_updatevalue;
	}
	public void ValueUpdateFillf(float _updatevalue)
	{
		this.gameObject.GetComponent<Image> ().fillAmount = +_updatevalue;
	}
	public void ValueUpdateVolumef(float _updatevalue)
	{
		this.gameObject.GetComponent<AudioSource> ().volume =_updatevalue;
	}
		
	public void OnValueToComplete ()
	{
		if (this.GetComponent<ValueTo> () != null)
		{
			Destroy (this.GetComponent<ValueTo> ());
		}
	}
}
