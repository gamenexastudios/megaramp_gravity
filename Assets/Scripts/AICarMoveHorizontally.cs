﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICarMoveHorizontally : MonoBehaviour
{

	public bool _IsMove;
	public GameObject DestinationObj;
	public GameObject SourceObj;
	public void Update()
	{

		if(_IsMove)
		{
			SourceObj.transform.position=Vector3.MoveTowards (SourceObj.transform.position, DestinationObj.transform.position,Time.deltaTime*10f);
		}
	}
	public void OnTriggerEnter(Collider col)
	{
		if(col.tag.Contains("Player"))
		{
			_IsMove = true;
		}	
	}

}

