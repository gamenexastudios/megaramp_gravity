﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tween_Menu : MonoBehaviour
{
    public static Tween_Menu _instance;

    public GameObject[] ScaleObj;
    public GameObject[] MoveObj;
    public GameObject[] MoveObj_FromDown;
    public GameObject[] MoveObj_FromRight;
    public GameObject[] MoveObj_FromLeft;
    public GameObject[] AlphaObjs;


    void Awake()
    {
        _instance = this;
        for (int i = 0; i < AlphaObjs.Length; i++)
        {
            if (AlphaObjs[i] != null)
            {
                AlphaObjs[i].GetComponent<Image>().color = new Color(AlphaObjs[i].GetComponent<Image>().color.r, AlphaObjs[i].GetComponent<Image>().color.g, AlphaObjs[i].gameObject.GetComponent<Image>().color.b, 0);
            }
        }
    }

    public void Call_TweenIn()
    {
        gameObject.SetActive(true);

        float _delay = 0f;

        for (int i = 0; i < AlphaObjs.Length; i++)
        {
            if (AlphaObjs[i] != null)
            {
                AlphaScript.AlphFade(AlphaObjs[i].gameObject, 0.95f, 0.25f, _delay, iTween.EaseType.linear, true);
                _delay += 0.1f;
            }
        }

        for (int i = 0; i < ScaleObj.Length; i++)
        {
            ScaleObj[i].transform.localScale = Vector3.zero;
        }       

        for (int i = 0; i < MoveObj.Length; i++)
        {
            iTween.MoveFrom(MoveObj[i].gameObject, iTween.Hash("Y", 1000, "time", 0.25f, "delay", _delay, "easetype", iTween.EaseType.easeOutCirc));
            _delay += 0.1f;
        }

        for (int i = 0; i < ScaleObj.Length; i++)
        {
            iTween.ScaleTo(ScaleObj[i].gameObject, iTween.Hash("Scale", Vector3.one, "time", 0.25f, "delay", _delay, "easetype", iTween.EaseType.linear));

            _delay += 0.1f;
        }
        for (int i = 0; i < MoveObj_FromDown.Length; i++)
        {
            iTween.MoveFrom(MoveObj_FromDown[i].gameObject, iTween.Hash("Y", -1000, "time", 0.25f, "delay", _delay, "easetype", iTween.EaseType.easeOutCirc));
            _delay += 0.1f;
        }
        for (int i = 0; i < MoveObj_FromLeft.Length; i++)
        {
            iTween.MoveFrom(MoveObj_FromLeft[i].gameObject, iTween.Hash("X", -2000, "time", 0.25f, "delay", _delay, "easetype", iTween.EaseType.easeOutCirc));
            _delay += 0.1f;
        }
        for (int i = 0; i < MoveObj_FromRight.Length; i++)
        {
            iTween.MoveFrom(MoveObj_FromRight[i].gameObject, iTween.Hash("X", 2000, "time", 0.25f, "delay", _delay, "easetype", iTween.EaseType.easeOutCirc));
            _delay += 0.05f;
        }
        
    }

    public void Call_TweenOut()
    {
        for (int i = 0; i < AlphaObjs.Length; i++)
        {
            if (AlphaObjs[i] != null)
            {
                AlphaObjs[i].GetComponent<Image>().color = new Color(AlphaObjs[i].GetComponent<Image>().color.r, AlphaObjs[i].GetComponent<Image>().color.g, AlphaObjs[i].gameObject.GetComponent<Image>().color.b, 0);
            }
        }
        gameObject.SetActive(false);
    }
}
