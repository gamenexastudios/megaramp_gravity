﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxDynamic : MonoBehaviour
{
   public Material sb1;
    public Material sb2;


    void Start()
    {

        Debug.Log("SELECTED LEVEL IS:     " + StaticVariables._iSelectedLevel);
        if (StaticVariables._iSelectedLevel <= 50)
            RenderSettings.skybox = sb1;
        else
            RenderSettings.skybox = sb2;
    }
    // Update is called once per frame
    void Update()
    {
        if (StaticVariables._iSelectedLevel <= 50)
            RenderSettings.skybox = sb1;
        else
            RenderSettings.skybox = sb2;
    }

   
}
