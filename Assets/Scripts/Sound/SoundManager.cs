﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour 
{
	public static SoundManager myScript;
	static bool Is_Created;

	public AudioSource Sound_Menu,Sound_Button,Sound_LC,Sound_LF,Sound_InGameBG,Sound_LS,Sound_Garage;

	void Awake()
	{
		if(!Is_Created)
		{
			DontDestroyOnLoad(gameObject);
			myScript = this;
			Is_Created = true;
		}
		else
		{
			Destroy(gameObject);
		}
	}



	

    public void PlaySound(AudioSource _AudioSource)
	{
		if (!_AudioSource.isPlaying) 
		{
			_AudioSource.Play ();
		}
	}

	public void StopSound(AudioSource _AudioSource)
	{
		_AudioSource.Stop ();
	}

	public void StopAll()
	{
		Sound_Menu.Stop ();
		Sound_Button.Stop ();
		Sound_LC.Stop ();
		Sound_LF.Stop ();
		Sound_InGameBG.Stop ();
		Sound_LS.Stop ();
		Sound_Garage.Stop ();
	}
}
