﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateUsManager : MonoBehaviour
{
    public GameObject _rateUsPannel;

    public static RateUsManager _instance;



    //Counters
    public int _levelCompleteCounter = 0;
    public int _levelCompleteDisplayCounter;



    //Testing
    public int daysPassed;
    public int displayDate_counter;
    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(this);

        _rateUsPannel.SetActive(false);
       
    }

    private void Start()
    {
        _levelCompleteCounter = PlayerPrefs.GetInt("Rateus_levelsCompleted", 0);
        _levelCompleteDisplayCounter = StaticVariables.rateus_levelCouter;



        //Testing 
        daysPassed = StaticVariables.daysPassed;
        displayDate_counter = StaticVariables.displayDate_Counter;
    }

    public bool DisplayRateUsPannel()
    {
        if (StaticVariables.daysPassed >= StaticVariables.displayDate_Counter)
        {
            _levelCompleteCounter += 1;
            PlayerPrefs.SetInt("Rateus_levelsCompleted", _levelCompleteCounter);
            if (_levelCompleteCounter >= _levelCompleteDisplayCounter)
            {
                Debug.LogError("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                _rateUsPannel.SetActive(true);
                _levelCompleteCounter = 0;
                PlayerPrefs.SetInt("Rateus_levelsCompleted", 0);
                //PlayerPrefs.SetInt("daysPassed", 0);
                return true;
            }
            else
            {
                _rateUsPannel.SetActive(false);
                return false;
            }
        }
        return false;
    }

    public void CloseButton_RateUs()
    {
        _rateUsPannel.SetActive(false);
    }


}
