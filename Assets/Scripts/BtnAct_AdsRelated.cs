using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BtnAct_AdsRelated : MonoBehaviour
{
    public GameObject _insufficientCoins;

    public GameObject _extra2xButton;
    public int _extra2xbool;

    public GameObject _rateUsPannel;

    public static BtnAct_AdsRelated myScript;
    public GameObject noInternetPannel;
    //static bool Is_Created;
    public void Awake()
    {
        myScript = this;

    }

    private void Start()
    {
        _extra2xbool = StaticVariables._extra2x;

        if (_extra2xButton != null)
        {
            if (StaticVariables._extra2x == 1)
                _extra2xButton.SetActive(true);
            else
                _extra2xButton.SetActive(false);
        }
    }

    public void BtnAct_CallRewardVideo(int _videoType)
    {
        Debug.Log("CALLL REWARD VIDEO 111111111111");
        StaticVariables.rewardCalled = true;
        Debug.LogWarning(StaticVariables.rewardCalled);
#if !AdSetup_OFF
        if (AdManager.Instance != null)
        {

            //AdManager.Instance.ShowRewardedVideo(_videoType);

            AdManager.Instance.ShowRewardVideoWithCallback((result) =>
            {
                if (result)
                {
                    onrewardthis(_videoType);
                    //stop timer
                    if (InGameUIHandler.Instance.timerCoroutine != null)
                    {
                        InGameUIHandler.Instance.StopReviveCoroutine();
                    }
                }
                else
                {
                    //Start timer
                    InGameUIHandler.Instance.StartReviveCoroutine();
                }
            });
        }
#endif
    }

    public void Update()
    {

        //#if !AdSetup_OFF
        //        if (AdManager.Instance != null&& AdManager.Instance.doreward)
        //        {
        //            onrewardthis(AdManager.Instance.rewardtype);
        //            AdManager.Instance.doreward = false;
        //        }
        //#endif

        _extra2xbool = StaticVariables._extra2x;
    }
    public void onrewardthis(int _videoType)
    {

        Debug.Log("BUTTON ACT : VIDEO AD");
        if (_videoType == 0)
        {
            Debug.Log("BUTTON ACT : VIDEO AD : 500 COINS");
            if (MenuUIManager.Instance)
            {
                MenuUIManager.Instance.UpdateScore(500);
                PlayerPrefs.SetString("BonusTakenB", "true");
                PlayerPrefs.SetString("LastDayTimeA", System.DateTime.Now.ToBinary().ToString());
#if !AdSetup_OFF
                if (FirebaseController.Instance != null)
                    FirebaseController.Instance.LogFirebaseEvent("Added500C", "OnrewardSuccess", "Level_" + StaticVariables._iSelectedLevel);
#endif

            }
        }
        if (_videoType == 2)
        {
            if (GameManager.Instance)
            {
                GameManager.Instance.Call_ResumeGameplay();
            }
        }

        if (_videoType == 5)
        {
            Debug.Log("BUTTON ACT : VIDEO AD : LC DOUBLE");
        }

        if (_videoType == 6)
        {
            Debug.Log("BUTTON ACT : VIDEO AD : LC 2x DOUBLE");

            if (InGameUIHandler.Instance)
            {
                InGameUIHandler.Instance.Success_2XLCCoins();
            }
#if !AdSetup_OFF
            if (FirebaseController.Instance != null)
                FirebaseController.Instance.LogFirebaseEvent("Success_2XLCCoins", "OnrewardSuccess", "Level_" + StaticVariables._iSelectedLevel);
#endif

        }

        if (_videoType == 7)
        {
            Debug.Log("BUTTON ACT : VIDEO AD : 2C DAILY BONUS");
            if (DailyBonusManager.myScript)
            {
                DailyBonusManager.myScript.VideoSuccess2XDailyBonus();
            }
        }

        if (_videoType == 8)
        {

            MenuUIManager.Instance.UpdateScore(500);



        }
    }

    public void BtnAct_CallUnityRewardVideo()
    {

    }
    int _gIndex = 0;
    public void BtnAct_CallIAPBuy1(int _index)
    {
        Debug.LogError("BtnAct_CallIAPBuy INDEX = " + _index);

#if !AdSetup_OFF

        //  Social.ReportProgress(GPGSIds.achievement_get_stuff, 100.0f, (bool success) => { });
        //Social.ReportProgress(GPGSIds.achievement_1st_bag, 100.0f, (bool success) => { });

#endif
        if (_index == 0) // NO ADS
        {
            Debug.Log("IAP CALL HERE : NOADS");
            PlayerPrefs.SetInt(MyGamePrefs.Is_NoAds, 1);
        }
        if (_index == 1) // UNLOCK ALL LEVELS IN SPECIFIC WORLD
        {
            Debug.Log("IAP CALL HERE : UNLOCK ALL LEVELS");
            //StaticVariables._iUnlockedLevels = StaticVariables._iTotalLevels;
        }
        if (_index == 2) // UNLOCK ALL WORLDS
        {
            Debug.LogError("IAP CALL HERE : UNLOCK ALL worlds");

            StaticVariables.UnlockedTheme = "Theme01";
            StaticVariables._iUnlockedLevels = 15;
            StaticVariables.UnlockedTheme = "Theme02";
            StaticVariables._iUnlockedLevels = 30;
            StaticVariables.UnlockedTheme = "Theme03";
            StaticVariables._iUnlockedLevels = 45;
            StaticVariables.UnlockedTheme = "Theme04";
            StaticVariables._iUnlockedLevels = 60;
            StaticVariables.UnlockedTheme = "Theme05";
            StaticVariables._iUnlockedLevels = 75;
            StaticVariables._iUnlockedWorldsCount = 4;
            if (AdManager.Instance != null)
            {
                AdManager.LevelsUnlocked = 1;
                Debug.LogError("UNLOCKED WORLDS ######  AdManager.LevelsUnlocked  " + AdManager.LevelsUnlocked);
            }
        }
        if (_index == 3) // UNLOCK ALL CARS
        {
            Debug.Log("IAP CALL HERE : UNLOCK CARS");

            PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, "1111111111111");
            if (AdManager.Instance != null)
            {
                AdManager.UpgradeUnlocked = 1;
            }


        }
        if (_index == 4) // UNLOCK EVERYTHING ( CARS + WORLDS + NOADS)
        {
            Debug.LogError("IAP CALL HERE : UNLOCK ALL LEVELS AND CARS");
            StaticVariables.UnlockedTheme = "Theme01";
            StaticVariables._iUnlockedLevels = 15;
            StaticVariables.UnlockedTheme = "Theme02";
            StaticVariables._iUnlockedLevels = 30;
            StaticVariables.UnlockedTheme = "Theme03";
            StaticVariables._iUnlockedLevels = 45;
            StaticVariables.UnlockedTheme = "Theme04";
            StaticVariables._iUnlockedLevels = 60;
            StaticVariables.UnlockedTheme = "Theme05";
            StaticVariables._iUnlockedLevels = 75;
            StaticVariables._iUnlockedWorldsCount = 4;
            PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, "1111111111111");
            PlayerPrefs.SetInt(MyGamePrefs.Is_NoAds, 1);
            if (AdManager.Instance != null)
            {
                AdManager.LevelsUnlocked = 1;
                AdManager.UpgradeUnlocked = 1;
            }

        }

        //_gIndex = _index + 1;
        //Invoke("InactivatePurchaseButton", 1f);
    }

    void InactivatePurchaseButton()
    {
        MenuUIManager.Instance.StorePurchase_Success(_gIndex);
    }

    public void IAPCall_UnlockCars()
    {
        PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, "1111111111111");
        MenuUIManager.Instance.StorePurchase_Success(2);
    }

    public void IAPCall_UnlockLevels()
    {
        StaticVariables._iUnlockedLevels = StaticVariables._iTotalLevels;
        MenuUIManager.Instance.StorePurchase_Success(1);
    }

    public void IAPCall_EveryThing()
    {
        StaticVariables._iUnlockedLevels = StaticVariables._iTotalLevels;
        PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, "1111111111111");
        MenuUIManager.Instance.StorePurchase_Success(3);
    }

    public void IAPCall_Ads()
    {
        PlayerPrefs.SetInt(MyGamePrefs.Is_NoAds, 1);
        //MenuUIManager.Instance.StorePurchase_Success(1);
    }

    public static void CallIAPBuy(int _index)
    {

    }



    //public static bool Is_BannerShowing;
    //    public void CallBannerView()
    //    {
    //#if !AdSetup_OFF
    //        if (AdManager.Instance != null)
    //        {
    //            AdManager.Instance.ActivateBanner();
    //            Is_BannerShowing = true;
    //        }
    //#endif
    //}


    //https://www.instagram.com/gamenexastudio/?hl=en
    //https://twitter.com/gamenexastudio?lang=en
    //https://www.facebook.com/GameNexa/
    public void BtnAct_MoreGames()
    {
        Debug.Log("MORE GAMES HERE");
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/dev?id=6143935442918635295&hl=en");
#elif UNITY_IOS
        Application.OpenURL("");
#endif
    }

    public void Call_FbLike()
    {
        Application.OpenURL("https://www.facebook.com/GameNexa/");
    }

    public void Call_InstaFollow()
    {
        Application.OpenURL("https://www.instagram.com/gamenexastudio/?hl=en");
    }

    public void Call_TwitterFollow()
    {
        Application.OpenURL("https://twitter.com/gamenexastudio?lang=en");
    }

    public void Call_YoutubeSubscribe()
    {
        Application.OpenURL("https://www.youtube.com/channel/UCJjihk7h29HXbG5S3pxPZow?sub_confirmation=1");
    }

    public void Call_Policy()
    {
        Application.OpenURL("http://gamenexastudio.blogspot.com/2017/10/privacy-policy.html");
    }

    public void Call_terms()
    {
        Application.OpenURL("http://gamenexastudio.blogspot.com/2017/10/term-of-use.html");
    }

    public void Call_RateGame()
    {
        Debug.Log("RATE HERE");
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
#elif UNITY_IOS
        Application.OpenURL("");
#endif

        //  _rateUsPannel.SetActive(false);

    }

    private string appUrl = "";

    public void NativeShareLink()
    {
        string subjectString = "All New Exciting Car Stunts Game";
#if UNITY_ANDROID
        appUrl = "https://play.google.com/store/apps/details?id=" + Application.identifier;
#elif UNITY_IOS
                        appUrl = "itms-apps://itunes.apple.com/app/id" + "123456789" + "?mt=8";
#endif
        //new NativeShare().SetTitle(Application.productName).SetText(subjectString + "\n" + appUrl).Share();
#if !AdSetup_OFF
        if (FirebaseController.Instance != null)
            FirebaseController.Instance.LogFirebaseEvent("NativeShare", "Success", "");
#endif
    }

    public void Call_FBShareGame()
    {
        NativeShareLink();
    }

    public void Call_NativeShare()
    {
        //NativeShareLink();
        Debug.LogError("NTATIVE SHARE*********");
        NativeShareHandler.Instance.NativeShareLink();
    }

    public void Call_NativeShareGameScore()
    {
        NativeShareLink();
    }

    public void BtnAct_SignIn()
    {
#if !PlayServices_Off
        PlayServicesHandler.instance.CheckAutoSignIn();
#endif
    }

    public void BtnAct_LB()
    {
        //GooglePlayGamesmanager._instance.();
    }

    public void BtnAct_Ach()
    {
#if !AdSetup_OFF
        AdManager.Instance.ShowAchievements();
#endif
    }

    public void ReviveWith500Coins()
    {

        if (StaticVariables._iTotalCoins >= 500)
        {
            StaticVariables._iTotalCoins -= 500;
            //GameManager.Instance.Call_ResumeGameplay();
            CoinsManger.manager.TotalCoins -= 500;
            //int totalCount =  int.Parse(CoinsManger.manager.ToatalcoinsText.text) - 500;
            InGameUIHandler.Instance.ToatalcoinsText.text = CoinsManger.manager.TotalCoins.ToString();

            //GameManager.Instance.obstaclesRestart.Restart();
            if (InGameUIHandler.Instance.timerCoroutine != null)
            {
                //StopCoroutine(InGameUIHandler.Instance.timerCoroutine);
                InGameUIHandler.Instance.StopReviveCoroutine();
            }

            GameManager.Instance.Call_ResumeGameplay();
        }
        else
        {
            _insufficientCoins.SetActive(true);
            InGameUIHandler.Instance.StopReviveCoroutine();
        }


    }

    public void OK()
    {
        InGameUIHandler.Instance.StartReviveCoroutine();
        _insufficientCoins.SetActive(false);
    }


    public void IAPCall_BuyCoins(int _amount)
    {
        StaticVariables._iTotalCoins += _amount;
    }
}


//FirebaseEvents.instance.LogFirebaseEvent("Funnel_LevelBtn", "Scene" + SceneManager.GetActiveScene().buildIndex, "LevelBtn");
//Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventLevelEnd, new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterLevel, "Level_" + lvl.value));
//FirebaseEvents.instance.SetUserProperty("star_count", "" + DataManager.instance.GetCollectedStarCount(0, obstacles.Length - 1));