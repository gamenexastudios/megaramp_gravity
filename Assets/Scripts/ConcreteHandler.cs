﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConcreteHandler : MonoBehaviour
{

	public void OnTriggerEnter(Collider col)
	{
		this.transform.parent.gameObject.GetComponent<Rigidbody> ().isKinematic = false;
		this.transform.parent.gameObject.GetComponent<Rigidbody> ().useGravity = true;
	}

}
