﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenAnimation : MonoBehaviour {

	// Use this for initialization
	public GameObject LeftDoor;
	public GameObject RightDoor;

	void Start ()
	{
		
	}
	

	public void OnTriggerEnter(Collider col)
	{
		iTween.RotateTo(LeftDoor.gameObject,iTween.Hash("rotation",new Vector3(0,140,0),"easytype",iTween.EaseType.easeInOutSine,"time",1f,"islocal",true));
		iTween.RotateTo(RightDoor.gameObject,iTween.Hash("rotation",new Vector3(0,-140,0),"easetype",iTween.EaseType.easeInOutSine,"time",1f,"islocal",true));
	}
}
