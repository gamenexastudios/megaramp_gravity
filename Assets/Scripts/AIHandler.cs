﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AIHandler : MonoBehaviour
{

	public GameObject AIObject;

	public void Start()

	{
		AIObject.SetActive (false);
	}

	public void OnTriggerEnter(Collider col)
	{

		AIObject.SetActive (true);
	}

}
