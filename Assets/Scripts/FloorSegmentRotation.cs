﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorSegmentRotation : MonoBehaviour {

	public GameObject[] FloorSegment;
	public float SpeedOfSegment;
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		for(int i=0;i<FloorSegment.Length;i++)
		{
			FloorSegment [i].transform.Rotate (Vector3.up * Time.deltaTime * SpeedOfSegment);
		}
	}
}
