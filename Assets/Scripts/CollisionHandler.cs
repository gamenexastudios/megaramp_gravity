﻿using System.Collections;
using UnityEngine;
public class CollisionHandler : MonoBehaviour
{
	public Transform[] raycastPosition;

	public bool _IsSavePointTouched;
	public static CollisionHandler Instance;



    Collider _colGlobal;

	public float raycastLenght=1f;



	public void Awake()
	{
		Instance = this;
	}

	public void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.name=="LC_Trigger")
		{
			//Invoke("PlayerMakeFalse", 0.3f);
			//StartCoroutine(PlayerMakeFalse(col.transform.position));
			if (GameManager.Instance._IsGrounded == true)
			{
				GameManager.Instance.CurrentVehile.GetComponent<Rigidbody>().isKinematic = true;
				GameManager.Instance.CurrentVehile.GetComponent<RCC_CarControllerV3>().enabled = false;
			}


		}

		if (col.gameObject.name=="LF_Trigger")
		{
			if (GameManager.Instance.CurrentGameState == GameManager.GameState.GamePlay) 
			{
				if (_IsSavePointTouched) 
				{
                    //if (MyAdsManager.myScript)
                    {
                        //if (MyAdsManager.myScript.IsVideoAdsReady())
                        {
							InGameUIHandler.Instance.Invoke("ReviveEnable", 1f);
							GameManager.Instance.CurrentVehile.GetComponent<Rigidbody>().isKinematic = true;
							GameManager.Instance.CurrentGameState = GameManager.GameState.Revive;
                      
                        }
                        //else
                        //{
                        //    InGameUIHandler.Instance.Invoke("LF", 0.2f);
                        //    GameManager.Instance.CurrentGameState = GameManager.GameState.LevelFaild;
                        //    GameManager.Instance.CurrentVehile.GetComponent<Rigidbody>().isKinematic = true;
                        //}
                    }
			
				}
				else
				{
					InGameUIHandler.Instance.Invoke ("LF", 0.2f);
					GameManager.Instance.CurrentGameState = GameManager.GameState.LevelFaild;
					GameManager.Instance.CurrentVehile.GetComponent<Rigidbody> ().isKinematic = true;
				}
			}

		}

		if(col.tag.Contains("SavePoint"))
		{
			_IsSavePointTouched = true;
			Debug.Log("----------Save Point Position"+col.transform.position);
			//GameManager.Instance.ListOfLevels [StaticVariables._iSelectedLevel].GetComponent<SavePointManager> ().LastSavePointPos = col.transform;
			GameManager.Instance.CurrentLevel.LastSavePointPos = col.transform;
            //Debug.Log("-------Selected Level"+GameManager.Instance.ListOfLevels [StaticVariables._iSelectedLevel]);
            GameManager.Instance.svaepointglass.color = new Color(1f, .2f, 0, 1);
            _colGlobal = col;
            Invoke("ColorChangeBackInvoke", 5);
            
		}

		 if(col.tag.Contains("StartParticle"))
		{
			GameManager.Instance.CurrentLevel.GetComponent<SavePointManager> ()._StartParticleIsActive = true;
			GameManager.Instance.CurrentLevel.GetComponent<SavePointManager> ().StartParticleOn();
		}
		 if(col.tag.Contains("EndParticle"))
		{
			GameManager.Instance.CurrentLevel.GetComponent<SavePointManager> ().EndParticleActivate ();
		}
		 if(col.tag.Contains("CameraTrigger"))
		{
				Debug.LogError("LC 00000000000000 Current game state " + GameManager.Instance.CurrentGameState);

			col.gameObject.SetActive (false);
			//LevelCompleteCameraRotation.Instance._IsRccCameraRotate = true;
			CinematicCameraMovement.instance.On = true;
			//GameManager.Instance.CurrentVehile.GetComponent<RCC_CarControllerV3> ().enabled = false;
			//RCC_Camera.Instance.playerCar = null;
			InGameUIHandler.Instance.InGameDisable ();

			if (GameManager.Instance.CurrentGameState == GameManager.GameState.GamePlay)
			{
				GameManager.Instance.CurrentGameState = GameManager.GameState.LevelComplete;
				Debug.LogError("LC 111111111");
				InGameUIHandler.Instance.Invoke("LC", 3.5f);
			}
		}

        if (col.tag.Contains("SavePointHelp"))
        {
            InGameUIHandler.Instance.Show_SavepointHelp();
        }
    }

	public void OnTriggerExit(Collider col)
	{
		
	  if(col.tag.Contains("StartParticle"))
		{
			GameManager.Instance.CurrentLevel.GetComponent<SavePointManager> ().StopParticleOff ();
			GameManager.Instance.CurrentLevel.GetComponent<SavePointManager> ()._StartParticleIsActive = false;


		}
	}
	IEnumerator PlayerMakeFalse(Vector3 newpos)
	{
		yield return new WaitForSeconds(0.3f);
		GameManager.Instance.CurrentVehile.GetComponent<Rigidbody>().isKinematic = true;
		yield return new WaitForSeconds(3f);
        GameManager.Instance.CurrentVehile.gameObject.SetActive (false);
	}


	RaycastHit hit;

	public void FixedUpdate()
	{
	
		
	/*	if(Physics.BoxCast(RayCastTarget.transform.position,halfboxsize, RayCastTarget.transform.TransformDirection(Vector3.down), out hit,transform.rotation, raycastLenght) && !hit.transform.IsChildOf(this.transform) && !hit.collider.isTrigger)
        {
			Debug.DrawRay(hit.point, hit.normal,Color.red);
			GameManager.Instance._IsGrounded = true;
        }
        else
        {		
			GameManager.Instance._IsGrounded = false;
		}*/



			if(Physics.Raycast(raycastPosition[0].position, raycastPosition[0].TransformDirection(Vector3.down), out hit, raycastLenght) && !hit.transform.IsChildOf(this.transform) && !hit.collider.isTrigger||
				Physics.Raycast(raycastPosition[1].position, raycastPosition[1].TransformDirection(Vector3.down), out hit, raycastLenght) && !hit.transform.IsChildOf(this.transform) && !hit.collider.isTrigger||
				Physics.Raycast(raycastPosition[2].position, raycastPosition[2].TransformDirection(Vector3.down), out hit, raycastLenght) && !hit.transform.IsChildOf(this.transform) && !hit.collider.isTrigger||
				Physics.Raycast(raycastPosition[3].position, raycastPosition[3].TransformDirection(Vector3.down), out hit, raycastLenght) && !hit.transform.IsChildOf(this.transform) && !hit.collider.isTrigger)
		    {

				GameManager.Instance._IsGrounded = true;

            }
            else
            {
				GameManager.Instance._IsGrounded = false;
			
			}
        


	}

	void ColorChangeBackInvoke()
    {
		GameManager.Instance.svaepointglass.color = new Color(0.1473389f, 0.7264151f, 0.7264151f, 1);
    }


}
