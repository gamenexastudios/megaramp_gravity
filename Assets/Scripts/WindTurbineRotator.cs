﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindTurbineRotator : MonoBehaviour
{
	public GameObject[] NumberOfTurbines;
	public float TurbineSpeed;
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		for(int i=0;i<NumberOfTurbines.Length;i++)
		{
			if (i == 0) 
			{
				NumberOfTurbines [i].transform.Rotate (Vector3.left * Time.deltaTime * TurbineSpeed );
			}
			else
			{
				NumberOfTurbines [i].transform.Rotate (Vector3.left * Time.deltaTime * TurbineSpeed*2);
			}
		}
	}
}
