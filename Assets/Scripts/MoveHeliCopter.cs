﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveHeliCopter : MonoBehaviour {

	// Use this for initialization
	public static MoveHeliCopter Instance;
	public Vector3 HeliCopterStartPos;
	public Vector3 HeliCopterEndPos;
	public bool _IsMoveHeli;

	void Start () 
	{
		Instance = this;
		Debug.Log ("Start Pos" + HeliCopterStartPos);
		Debug.Log ("End Pos" + HeliCopterEndPos);
	}
	
	// Update is called once per frame
	void Update () 
	{
//		if(_IsMoveHeli)
//		{
			transform.position = Vector3.Lerp(HeliCopterStartPos, HeliCopterEndPos, 10f*Time.fixedDeltaTime);
		//}
	}
}
