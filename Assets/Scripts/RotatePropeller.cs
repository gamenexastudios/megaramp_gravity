﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePropeller : MonoBehaviour {

	// Use this for initialization
	public float Speed;
	public List<GameObject> ListOfRotatePropeller;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		for(int i=0;i<ListOfRotatePropeller.Count;i++)
		{
			ListOfRotatePropeller[i].transform.Rotate (Vector3.forward * Time.deltaTime * Speed);
		}
	}
}
