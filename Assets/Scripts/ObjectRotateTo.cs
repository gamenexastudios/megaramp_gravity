﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotateTo : MonoBehaviour
{
    public Vector3 TargetRot;
    public iTween.EaseType AnimationType;

    public void Rotate()
    {
        iTween.RotateTo(this.gameObject, iTween.Hash("X", TargetRot.x, "Y", TargetRot.y, "Z", TargetRot.z, "islocal", true, "time", 0.3f, "delay", 0, "easetype", AnimationType));
    }
}
