﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompleteCameraRotation : MonoBehaviour
{

	public RCC_CarControllerV3 carController;

	public bool _IsRccCameraRotate;
	public static LevelCompleteCameraRotation Instance;

	public static int MyPos;
	void Start () 
	{
		Instance = this;

		MyPos = 0;

		//MyPos = Random.Range (0, 2);
		carController = GetComponent<RCC_CarControllerV3>();
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		if(_IsRccCameraRotate)
		{
			//MyDebug.Log ("Camera Value------------" + MyPos);
			this.transform.position=Vector3.Lerp (this.transform.position,GameManager.Instance.CurrentLevel.GetComponent<SavePointManager>().CameraRotateObj[MyPos].transform.position, Time.deltaTime * 2f);
			this.transform.rotation=Quaternion.Slerp (this.transform.rotation,GameManager.Instance.CurrentLevel.GetComponent<SavePointManager>().CameraRotateObj[MyPos].transform.rotation, Time.deltaTime *2f);
			//Invoke("LoookAtCar",2.5f);
			//Invoke ("MoveCar", 1f);
		}
	}
	void LoookAtCar()
	{
		this.transform.LookAt (GameManager.Instance.CurrentVehile.transform);
	}

	public void MoveCar()
	{
		// GameManager.Instance.CurrentVehile.transform.Translate (Vector3.forward * Time.deltaTime * 5f);
		//GameManager.Instance.CurrentVehile.transform.position = Vector3.MoveTowards(GameManager.Instance.CurrentVehile.transform.position, SavePointManager.Instance.LcTrigger.transform.position, Time.deltaTime * 5f);

		//carController.brakeTorque = 100000f;
		//carController.handbrakeInput = 1f;

		GameManager.Instance.CurrentVehile.transform.forward = Vector3.RotateTowards(GameManager.Instance.CurrentVehile.transform.forward, SavePointManager.Instance.LcTrigger.transform.position - GameManager.Instance.CurrentVehile.transform.position, 4 * Time.deltaTime, 0.0f);
		GameManager.Instance.CurrentVehile.transform.position = Vector3.MoveTowards(GameManager.Instance.CurrentVehile.transform.position, SavePointManager.Instance.LcTrigger.transform.position, 4 * Time.deltaTime);



	}
}
