﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnimationHandler : MonoBehaviour 
{

	public List<PageAnimHandler> ListOfObjects;

	public List<GameObject> mlist_UiTabObj;
	public static UIAnimationHandler Instance;
	void Awake()
	{
		Instance = this;
	}
	void Start ()
	{
		//StartCoroutine(Enable_UIAnimation(0));
	}
	float _delay;
	public IEnumerator Enable_UIAnimation(int ID)
	{
		yield return new WaitForSeconds (0.1f);

		foreach(PageAnimHandler page in ListOfObjects)
		{
//			Debug.Log (page.name);
			page.Call_TweenOut ();
		
		}
		ListOfObjects [ID].Call_TweenIn ();
//		if(ID==0)
		{
			foreach (GameObject Go in mlist_UiTabObj)
			{
				Go.transform.localScale = Vector3.zero;
				iTween.ScaleTo (Go.gameObject, iTween.Hash ("Scale", Vector3.one, "time", 0.25, "Delay", _delay, "easetype", iTween.EaseType.spring));
			}
		}
	}

}