﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Reflection;
using System;

public class MenuUIManager : MonoBehaviour
{
	public static MenuUIManager Instance;
	public int PackBackVal;
	public List<TextMeshProUGUI> TotalScoreTxts;
	public GameObject BackGroundImage;


    //public GameObject _dailyRewardPannel;

    public int playAdOnBackButton = 0;

    public static bool inGarage = false;

    public enum GameState
	{
		Menu,Settings,Store,Pack,Garage,LevelSelection,Gift
	}
    public GameState CurrentGameState;
    public GameState previousGameState;

    public GameObject Btn_NoAds;

    //Changing player name
    public TMP_InputField inputField;
    public string userName;

    public Button SettingsBtn;
    public Button PackBtn;


    public Button SoundsOn;
    public Button SoundsOff;

    public Button MusicOn;
    public Button MusicOff;

    public Button GraphicsHigh;
    public Button GraphicsMedium;
    public Button GraphicsLow;

    public Sprite enableSprite;
    public Sprite disableSprite;
    public Sprite offSprite;

    private static string soundsStatus;
    private static string musicStatus;
    private static string graphicsStatus;

    [Header("UNLOCK NEXT LEVEL IMAGES")]
    public GameObject[] nextWorldImages;

    [Header("UNLOCK NEXT LEVEL IMAGES")]
    public GameObject[] unlockCarImages;

    [Header("UNLOCK ALL & VIP SUBSCRIPTION")]

    public GameObject vipSubscriptionPanel;
    public Button SpecialPackBtn;

    //public GameObject unlockAllPanel;
    //public TextMeshProUGUI unlockAllOfferText;

    public GameObject LoadingCanvas;

    void Awake()
	{
        LoadingCanvas.SetActive(false);
		Instance = this;
		UpdateText ();

        userName = PlayerPrefs.GetString("username");
        inputField.text = userName;
    }

    void Start () 
	{
        vipSubscriptionPanel.SetActive(false);
        //unlockAllPanel.SetActive(false);

        if (StaticVariables._iHomeOrGarage == "Home")
		{
			Menu_Action ("Menu");
		}
		else
		{
            BackGroundImage.gameObject.SetActive(false);
            Tween_Menu._instance.Call_TweenOut();
            StaticVariables._iHomeOrGarage = "Home";
            Menu_Action("Garage");

        }        
        Check_InAppButtons();
        SettingsCurrentStatus();

        if (AdManager.LevelsUnlocked == 1 && AdManager.UpgradeUnlocked == 1)
        {
            SpecialPackBtn.interactable = false;
        }
    }

    public void Update()
    {
	   // Debug.Log("UNLOCKED THEME ***** " + StaticVariables.UnlockedTheme + " I UNLOCKED LEVEL ***** " + StaticVariables._iUnlockedLevels + " S UNLOCKED LEVEL ***** " + StaticVariables._sUnlockedLevels);

    }

    void SettingsCurrentStatus()
    {
        soundsStatus = PlayerPrefs.GetString("Sounds");
        musicStatus = PlayerPrefs.GetString("Music");
        graphicsStatus = PlayerPrefs.GetString("Graphics");

        GraphicsQuality(graphicsStatus);
        ChangeQuality(graphicsStatus);
    }



    public void GraphicsQuality(string value)
    {
        if (value == "High")
        {
            MenuUIManager.Instance.GraphicsHigh.GetComponent<Image>().sprite = enableSprite;
            MenuUIManager.Instance.GraphicsMedium.GetComponent<Image>().sprite = disableSprite;
            MenuUIManager.Instance.GraphicsLow.GetComponent<Image>().sprite = disableSprite;
        }
        else if (value == "Medium")
        {
            MenuUIManager.Instance.GraphicsHigh.GetComponent<Image>().sprite = disableSprite;
            MenuUIManager.Instance.GraphicsMedium.GetComponent<Image>().sprite = enableSprite;
            MenuUIManager.Instance.GraphicsLow.GetComponent<Image>().sprite = disableSprite;
        }
        else if (value == "Low")
        {
            MenuUIManager.Instance.GraphicsHigh.GetComponent<Image>().sprite = disableSprite;
            MenuUIManager.Instance.GraphicsMedium.GetComponent<Image>().sprite = disableSprite;
            MenuUIManager.Instance.GraphicsLow.GetComponent<Image>().sprite = enableSprite;
        }
        graphicsStatus = value;
        PlayerPrefs.SetString("Graphics", graphicsStatus);
        ChangeQuality(value);
    }

    void ChangeQuality(string name)
    {
        int indexValue = 2; 
        if (name == "High")
        {
            indexValue = 3;

        }
        else if (name == "Medium")
        {
            indexValue = 2;

        }
        else if (name == "Low")
        {
            indexValue = 1;

        }

        QualitySettings.SetQualityLevel(indexValue, true);
        QualitySettingss(indexValue);
    }
    void QualitySettingss(int indexvalue)
    {
        if (indexvalue == 1)
        {
            QualitySettings.shadows = ShadowQuality.Disable;

        }
        else if (indexvalue == 2)
        {
            QualitySettings.shadows = ShadowQuality.All;

        }
        else if (indexvalue == 3)
        {
            QualitySettings.shadows = ShadowQuality.All;
          

        }
        QualitySettings.pixelLightCount = 1;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
        QualitySettings.antiAliasing=0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = false;
        QualitySettings.billboardsFaceCameraPosition = false;
        QualitySettings.resolutionScalingFixedDPIFactor = 1;

        QualitySettings.shadowmaskMode = ShadowmaskMode.Shadowmask;
        QualitySettings.shadowResolution = ShadowResolution.Medium;
        QualitySettings.shadowProjection = ShadowProjection.StableFit;
        QualitySettings.shadowDistance = 30f;
        QualitySettings.shadowNearPlaneOffset = 3f;
        QualitySettings.shadowCascades = 0;
        QualitySettings.skinWeights = SkinWeights.OneBone;
        QualitySettings.vSyncCount = 0;
        QualitySettings.asyncUploadPersistentBuffer = false;
    }

    //Player name editing
    public void SetUserName()
    {
        PlayerPrefs.SetString("username", inputField.text);
        userName = PlayerPrefs.GetString("username");
    }

    public void Playbtn()
    {

        if (PlayerPrefs.GetInt("firstvisit", 0) == 0)
        {
            PlayerPrefs.SetInt("firstvisit", 1);
            StaticVariables._iSelectedVechicle = 0;
            FindObjectOfType<LevelSelectionHandler>().LevelLoad(1);
        }
        else
        {
            Menu_Action("Garage");
        }

        if (AdManager.Instance != null)
        {
            //AdManager.Instance.IncreaseLoseCounter();
            AdManager.Instance.RunActions(AdManager.Actiontype.Menu_Play);

        }

    }
	public void Menu_Action(string actionname)
	{
        previousGameState = CurrentGameState;

		switch(actionname)
		{
		    case "Menu":
                inGarage = false;
                CurrentGameState = GameState.Menu;
			    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
                Tween_Menu._instance.Call_TweenIn();
                UpgradeVechicle.Instance.CheckBtnVisibility();
                PackBackVal = 0;
                print("AM HERE @ MENU");

                
                if (AdManager.Instance != null)
                {
                    AdManager.Instance.IsForceHideBannerAd = false;
                    AdManager.Instance.RunActions(AdManager.PageType.Menu);
                }
                break;

		    case "Settings":
                SettingsBtn.interactable = false;
                PackBtn.interactable = true;
			    CurrentGameState = GameState.Settings;
			    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
                Tween_Store._instance.Call_TweenOut();

                Tween_Settings._instance.Call_TweenIn();
                Debug.Log("SETTINGS!!!!!!!!!!!!");
                PackBackVal = 1;
                if (AdManager.Instance != null)
                {
                    AdManager.Instance.RunActions(AdManager.PageType.Settings);
                }
                break;

		    case "Store":
			    CurrentGameState = GameState.Store;
			    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
                Tween_Store._instance.Call_TweenIn();
			    PackBackVal = 2;
                if (AdManager.Instance != null)
                {
                    AdManager.Instance.RunActions(AdManager.PageType.Store);
                }
                break;

		    case "Pack":
                SettingsBtn.interactable = true;
                PackBtn.interactable = false;
                CurrentGameState = GameState.Pack;
			    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
                Tween_Store._instance.Call_TweenOut();

                Tween_Packs._instance.Call_TweenIn();
                if (AdManager.Instance != null)
                {
                    AdManager.Instance.RunActions(AdManager.PageType.Store);
                }
                break;

		    case "Garage":
                inGarage = true;
			    CurrentGameState = GameState.Garage;
			    Utility.myScript.SetActiveWithDelay (BackGroundImage, false, 0.1f);
                Tween_Garage._instance.Call_TweenIn();
                UpgradeVechicle.Instance.CheckBtnVisibility();
                PackBackVal = 4;
                print("AM HERE @ Garage");
                if (AdManager.Instance != null)
                {
                    AdManager.Instance.RunActions(AdManager.PageType.Upgrade);
                }
                break;

		    case "LevelSelection":
			    CurrentGameState = GameState.LevelSelection;
			    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
                Tween_LS._instance.Call_TweenIn();
                PackBackVal = 5;
                Debug.Log("LevelSelection");
                UpgradeVechicle.Instance.CheckBtnVisibility();

                // LevelSelectionHandler.Instance.checkLocks();
                // LevelSelectionHandler.Instance.SetPosRecentLevel();
                LevelSelectionHandler.Instance.WorldSelection();
               
                break;

		    case "Gift":
			    CurrentGameState = GameState.Gift;
			    Utility.myScript.SetActiveWithDelay (BackGroundImage, true,0.1f);
			    StartCoroutine (UIAnimationHandler.Instance.Enable_UIAnimation (6));
			    PackBackVal = 6;
			    break;

            case "BackButton":
                SettingsBtn.interactable = true;
                PackBtn.interactable = true;
                Utility.myScript.SetActiveWithDelay(BackGroundImage, true, 0.1f);
                if (inGarage == true)
                {
                    Menu_Action("Garage");
                }
                else
                {
                    Menu_Action("Menu");
                }

                break;

		case "PackBack":

			    if(PackBackVal==0)
			    {
				    CurrentGameState = GameState.Menu;
				    Utility.myScript.SetActiveWithDelay (BackGroundImage, true,0.1f);
                    Tween_Menu._instance.Call_TweenIn();
                }
			    else if(PackBackVal==1)
			    {
				    CurrentGameState = GameState.Settings;
				    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
                    Tween_Settings._instance.Call_TweenIn();
                }
			    else if(PackBackVal==2)
			    {
				    CurrentGameState = GameState.Store;
				    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
                    Tween_Store._instance.Call_TweenIn();
                }
			    else if(PackBackVal==3)
			    {
				    CurrentGameState = GameState.Pack;
				    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
				    //StartCoroutine (UIAnimationHandler.Instance.Enable_UIAnimation (PackBackVal));
			    }
			    else if(PackBackVal==4)
			    {
				    CurrentGameState = GameState.Pack;
				    Utility.myScript.SetActiveWithDelay (BackGroundImage, false, 0.1f);
                    Tween_Garage._instance.Call_TweenIn();
			    }
			    else if(PackBackVal==5)
			    {
				    CurrentGameState = GameState.LevelSelection;
				    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
                    //StartCoroutine (UIAnimationHandler.Instance.Enable_UIAnimation (PackBackVal));
                    Tween_LS._instance.Call_TweenIn();

                }
			    else if(PackBackVal==6)
			    {
				    CurrentGameState = GameState.Gift;
				    Utility.myScript.SetActiveWithDelay (BackGroundImage, true, 0.1f);
				    //StartCoroutine (UIAnimationHandler.Instance.Enable_UIAnimation (PackBackVal));
			    }   
				
			break;
        }
#if !AdSetup_OFF
        if (FirebaseController.Instance != null)
            FirebaseController.Instance.LogFirebaseEvent("Funnel_"+actionname, "Scene" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex, actionname);
#endif
    }

    public void UpdateText()
	{		
		for(int i=0;i<TotalScoreTxts.Count;i++)
		{
            if(TotalScoreTxts[i]!=null)
			TotalScoreTxts[i].text=StaticVariables._iTotalCoins+"";
		}
	}

	public void UpdateScore(int Score)
	{
		Debug.Log ("-----------Update Score Calling" + Score);
		StaticVariables._iTotalCoins=StaticVariables._iTotalCoins + Score;
        UpdateText();
    }


    public void UnlockAllCars()
    {
        PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, "1111111111111");
        
        StaticVariables._iUnlockedAllCarsIAP = 1;

        for (int i = 0; i < unlockCarImages.Length; i++)
        {
            unlockCarImages[i].SetActive(false);
        }

        if (UpgradeVechicle.Instance)
        {
            UpgradeVechicle.Instance.UnLockCar();
        }
    }

    public void UnlockAllLevels()
    {
        Debug.Log("********* UNLOCK ALL LEVELS********");
        StaticVariables._iUnlockedAllLevelsIAP = 1;

        StaticVariables._iUnlockedWorldsCount = 4;

        LevelSelectionHandler.Instance.WorldSelection();

        for (int i = 0; i < nextWorldImages.Length; i++)
        {
            nextWorldImages[i].SetActive(false);
        }

        for (int i = 0; i < 5; i++)
        {
            if (i == 0)
            {
                StaticVariables.UnlockedTheme = "Theme01";
                StaticVariables._iUnlockedLevels = 15;
            }
            else if (i == 1)
            {
                StaticVariables.UnlockedTheme = "Theme02";
                StaticVariables._iUnlockedLevels = 30;
            }
            else if (i == 2)
            {
                StaticVariables.UnlockedTheme = "Theme03";
                StaticVariables._iUnlockedLevels = 45;
            }
            else if (i == 3)
            {
                StaticVariables.UnlockedTheme = "Theme04";
                StaticVariables._iUnlockedLevels = 60;
            }
            else if (i == 4)
            {
                StaticVariables.UnlockedTheme = "Theme05";
                StaticVariables._iUnlockedLevels = 75;
            }
            else
            {
                
            }
            if (LevelSelectionHandler.Instance)
            {
                LevelSelectionHandler.Instance.checkLocks();
            }

            Debug.Log("#######UNLOCKED ALL BUTTON CLICKED ############" + StaticVariables.UnlockedTheme +"    " + StaticVariables._iUnlockedLevels);
        }

/*
        StaticVariables._iUnlockedLevels = StaticVariables._iTotalLevels;
        if (LevelSelectionHandler.Instance)
        {
            LevelSelectionHandler.Instance.checkLocks();
        }
*/
    }

    public void PackPurchase_Success(int _ipack)
    {
#if !AdSetup_OFF

        //Social.ReportProgress(GPGSIds.achievement_get_stuff, 100.0f, (bool success) => { });
#endif
        switch (_ipack)
        {
            case 1:
                UpdateScore(50000);
                Debug.Log("PACK 1 SUCCESS");
                break;
            case 2:
                UpdateScore(100000);
                Debug.Log("PACK 2 SUCCESS");
                break;
            case 3:
                UpdateScore(250000);
                Debug.Log("PACK 3 SUCCESS");
                break;
        }
#if !AdSetup_OFF
        if (FirebaseController.Instance != null)
            FirebaseController.Instance.LogFirebaseEvent("Coinpack_purchase", "Scene" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex, _ipack.ToString());
#endif
    }

    public void StorePurchase_Success(int _istore)
    {
        switch (_istore)
        {
            case 0:
                PlayerPrefs.SetInt(MyGamePrefs.Is_NoAds, 1);
                //Invoke("Check_InAppButtons", 2.0f);
                Debug.Log("InAPP 1 SUCCESS : REMOVE ADS");
                break;
            case 1:
                Debug.Log("InAPP 2 SUCCESS  : UNLOCK ALL LEVELS");
                UnlockAllLevels();
                break;
            case 2:
                Debug.Log("InAPP 3 SUCCESS : UNLOCK ALL VEHICLES");
                UnlockAllCars();
                //PlayerPrefs.SetInt(MyGamePrefs.Is_NoAds, 1);
                //Check_InAppButtons();
                //GooglePlayGamesmanager._instance.OncallAchievement(GPGSIds.achievement_garage_star);
                break;

            case 3:
                Debug.Log("InAPP 4 SUCCESS : UNLOCK ALL");
                UnlockAllCars();
                UnlockAllLevels();
                break;
        }
        //GooglePlayGamesmanager._instance.OncallAchievement(GPGSIds.achievement_top_player);
#if !AdSetup_OFF
        if (FirebaseController.Instance != null)
            FirebaseController.Instance.LogFirebaseEvent("Store", "Scene" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex, _istore.ToString());
#endif
    }

    public void Check_InAppButtons()
    {
        if (PlayerPrefs.GetInt(MyGamePrefs.Is_NoAds) == 1)
        {
            if (Btn_NoAds != null)
            {
                Btn_NoAds.gameObject.SetActive(false);
            }
        }
    }

    //public void DailyRewardCloseButton()
    //{
    //    _dailyRewardPannel.SetActive(false);
    //    if (!DailyBonusMenuManager._instance.IsBonusButtonClick && SplPackHandler.Instance != null)
    //    {
    //        SplPackHandler.Instance.CheckNOpenSplPack();
    //    }
    //    DailyBonusMenuManager._instance.IsBonusButtonClick = false;

    //}


    public void EnableVIPSubscriptionPanel()
    {
        vipSubscriptionPanel.SetActive(true);
    }
    public void EnableUnlockAllPanel()
    {
        //unlockAllPanel.SetActive(true);
    }

    void OnDestroy()
    {

        foreach (FieldInfo field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            Type fieldType = field.FieldType;

            if (typeof(IList).IsAssignableFrom(fieldType))
            {
                IList list = field.GetValue(this) as IList;
                if (list != null)
                {
                    list.Clear();
                }
            }

            if (typeof(IDictionary).IsAssignableFrom(fieldType))
            {
                IDictionary dictionary = field.GetValue(this) as IDictionary;
                if (dictionary != null)
                {
                    dictionary.Clear();
                }
            }

            if (!fieldType.IsPrimitive)
            {
                field.SetValue(this, null);
            }
        }
    }
    public void SpecialPackPanelCall()
    {
        if (SplPackHandler.Instance != null)
        {
            SplPackHandler.Instance.Open();
        }
    }
}