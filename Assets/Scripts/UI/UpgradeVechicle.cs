﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Reflection;
using System;

public class UpgradeVechicle : MonoBehaviour
{
    public CarScroller carUpgrade;
    public GameObject[] Cars;
    public GameObject Btn_BuyCar, Btn_BuyCarVideo;
    public GameObject Btn_Play;
    public Image Btn_NextCar;
    public Image Btn_PrevCar;
    public TextMeshProUGUI Text_CarName;
    public string[] CarNames;
    public int[] CarPrices;
    public int[] CarSpeeedValues;
    public int[] CarAccelerationValues;
    //public Text Text_CarSpeeds;
    //public Text Text_CarAcceleration;
    public GameObject mg_BtnUnlockAllCarsInCarSelection;
    public GameObject mg_BtnStoreUnlockCars;
    public GameObject mg_BtnUnlockAllLvlsInStore;
    public GameObject mg_BtnUnlockALlLvlsInLevelSel;
    public GameObject mg_BtnUnlockAll;
    public GameObject AlphaImage;
    public GameObject BtnBuyParent;
    public string CarVideosCount;
    public static string CarVideosCountPref = "CarVideosCountPrefA";
    int CarValue;

    public GameObject freeCarUnlockBG;
    public TextMeshProUGUI freeCarUnlockDescriptionText;

    public static UpgradeVechicle Instance;
    public bool _IsLock;
    public GameObject[] LeftLights;
    public GameObject[] RightLights;
    public List<GameObject> ListCarOfPositions;

    public GameObject UpgradeCamera;
    public Transform Pos_Camera_Car;
    public Transform Pos_Camera_Monster;



    //GameNexa Code
    public GameObject _selectedImage;
    public Transform _carsArray;
    public GameObject _playText;
    public GameObject _selectText;
    //public Text _specialCarTimerText;
    public GameObject _lock;



    public bool carSelectPlayAd = false;
    public bool specialCarUnlocked;
    bool nxtPressed = false;
    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {

        RS_SetPrefsHash.SetHashPrefs_String(MyGamePrefs.UnlockedCars, "1000000000000");
        //RS_SetPrefsHash.SetHashPrefs_String(MyGamePrefs.UnlockedCars, "1111111111");
        RS_SetPrefsHash.SetHashPrefs_String(CarVideosCountPref, CarVideosCount);

        if (StaticVariables._iSelectedVechicle != 0)
        {
            CurrentCarIndex = StaticVariables._iSelectedVechicle - 1;
        }
        else
        {
            CurrentCarIndex = 0;
        }


        CheckBtnVisibility();
        Upgrade_In();

        //CheckCurrent_Vehicle();

        if (StaticVariables._iUnlockedLevels >= 6)
        {
            //  UnlockCarSpecialCars(2);
        }
        if (StaticVariables._iUnlockedLevels >= 13)
        {
            // UnlockCarSpecialCars(4);
        }


        StartCoroutine(Show());




    }

    IEnumerator Show()
    {
        yield return new WaitForSeconds(1);
        ShowSelected();
    }

    IEnumerator StartUpgradeCar()
    {
        Debug.Log("CarIndexm" + CurrentCarIndex);
        yield return new WaitForSeconds(1f);
        carUpgrade.currentcar += (CurrentCarIndex - 1);
        carUpgrade.MoveCar(CurrentCarIndex * 14);

    }

    public int value;
    public bool _IsMoveCamera;

    public void Update()
    {

        //  Debug.Log(StaticVariables._iSelectedVechicle);

        ShowSelected();

    }

    public void RayCastImplementation()
    {
        //if(MenuHandler.)

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100))
        {
            //for (int i = 1; i <= 11; i++) {

            if (hit.collider.tag.Contains("Player"))
            {

                char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
                CheckCurrentVehicle();
                //Debug.Log ("hi");
                if (tempstring[CurrentCarIndex] == '1')
                {
                    //Debug.Log ("we are in if");
                    BtnAct_Play();

                }
            }
            //}
        }
        else
        {
            //Debug.Log ("Your in unlock levels page");
        }
    }

    public void BtnBuy()
    {
        char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
        int _tempcoins = StaticVariables._iTotalCoins;
        if (tempstring[CurrentCarIndex] != '1')
        {
            if (_tempcoins >= CarPrices[CurrentCarIndex])
            {
                MenuUIManager.Instance.UpdateScore(-CarPrices[CurrentCarIndex]);
                MenuUIManager.Instance.UpdateText();
                UnLockCar();
                Debug.Log("Here We Are In Button Buy");
            }
            else
            {
                MenuUIManager.Instance.Menu_Action("Pack");
            }
        }
    }

    public void BtnAct_Play()
    {
        StaticVariables._iSelectedVechicle = CurrentCarIndex;
        MenuUIManager.Instance.Menu_Action("LevelSelection");
        LevelSelectionHandler.Instance.LevelBtnColorChanger();
        //MenuUIManager.inGarage = false;

        if (AdManager.Instance != null)
        {
            //AdManager.Instance.IncreaseLoseCounter();
            AdManager.Instance.RunActions(AdManager.Actiontype.Upgrade_Play);

        }

    }

    public GameObject mg_Envi;
    public void Upgrade_In()
    {
        CurrentCarIndex = StaticVariables._iSelectedVechicle;
        Debug.Log("cURRENT Vehicle INDEX vALUE" + CurrentCarIndex);
        CheckCurrent_Vehicle();
        //Invoke ("UpGradeSound", 0.5f);
    }

    public void UpGradeSound()
    {

    }

    public void Upgrade_Out()
    {
        Btn_NextCar.gameObject.SetActive(false);
        Btn_PrevCar.gameObject.SetActive(false);
    }

    public float CheckCurrentCarTime;

    public void CheckCurrent_Vehicle()
    {

        char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
        char[] tempVideostring = PlayerPrefs.GetString(CarVideosCountPref).ToCharArray();

        Debug.Log("UNLCOKED CARS 11111111111111111 " + PlayerPrefs.GetString(MyGamePrefs.UnlockedCars));

        //int VideoCount = TypeCasting.CharToInt(tempVideostring[CurrentCarIndex]);

        //  Btn_BuyCarVideo.transform.GetChild(0).GetComponent<Text>().text = "WATCH " + VideoCount + " VIDEO(S) TO UNLOCK THE CAR";

        CheckCurrentVehicle();

        //Text_CarSpeeds.text = CarSpeeedValues[CurrentCarIndex] + "";
        //Text_CarAcceleration.text = CarAccelerationValues[CurrentCarIndex] + "";
        Debug.Log("tempstring........." + tempstring[CurrentCarIndex]);

        if (AdManager.Instance != null && AdManager.SubscribeUnlocked == 1)
        {
            _IsLock = false;
            //Btn_BuyCar.gameObject.transform.parent.gameObject.SetActive(false);
            //Btn_Play.gameObject.transform.parent.gameObject.SetActive (true);
            Btn_BuyCar.gameObject.SetActive(false);
            freeCarUnlockBG.SetActive(false);
            Btn_Play.gameObject.SetActive(true);
            _lock.SetActive(false);
            return;
        }
        if (tempstring[CurrentCarIndex] == '1')
        {
            _IsLock = false;
            //Btn_BuyCar.gameObject.transform.parent.gameObject.SetActive(false);
            //Btn_Play.gameObject.transform.parent.gameObject.SetActive (true);
            Btn_BuyCar.gameObject.SetActive(false);
            freeCarUnlockBG.SetActive(false);
            Btn_Play.gameObject.SetActive(true);
            _lock.SetActive(false);
        }
        else
        {
            _IsLock = true;
            //Btn_BuyCar.gameObject.transform.parent.gameObject.SetActive(true);
            Btn_Play.gameObject.SetActive(false);
            if (CurrentCarIndex == 2 || CurrentCarIndex == 4)
            {
                Btn_BuyCar.gameObject.SetActive(false);
                freeCarUnlockBG.SetActive(true);
            }
            else
            {
                freeCarUnlockBG.SetActive(false);
                Btn_BuyCar.gameObject.SetActive(true);
                Btn_BuyCar.gameObject.transform.localScale = new Vector3(1, 1, 1);
            }
            //Btn_Play.gameObject.transform.parent.gameObject.SetActive (false);	
            _lock.SetActive(true);
        }

        /*  if (CurrentCarIndex == 4||CurrentCarIndex>=6)
          {
              //UpgradeCamera.transform.position = Pos_Camera_Monster.position;
              UpgradeCamera.transform.localPosition = new Vector3(0.03999996f, -0.3f, -6.05f);
          }
          else
          {
              // UpgradeCamera.transform.position = Pos_Camera_Car.position;            
              UpgradeCamera.transform.localPosition = new Vector3(0.03999996f, -0.3f, -6.05f);
          }*/



    }

    public int CurrentCarIndex;
    public void BtnAct_NextVehicle()
    {
        nxtPressed = true;


        if (CurrentCarIndex <= Cars.Length - 2)
        {
            Debug.Log("CarIndexmnext" + CurrentCarIndex);

            CurrentCarIndex++;
            value = CurrentCarIndex;
            Debug.Log("CarIndexmnextafter" + CurrentCarIndex);
            CheckCurrent_Vehicle();
            //StaticVariables._iSelectedVechicle = CurrentCarIndex;
            //_specialCarTimerText.enabled = false;
        }
        else if (CurrentCarIndex == 9)
        {
            CurrentCarIndex += 1;
            // Cars[9].SetActive(false);
            // Cars[10].SetActive(true);
            if (specialCarUnlocked)
            {
                Btn_BuyCar.gameObject.SetActive(false);
                Btn_Play.gameObject.SetActive(true);
            }
            else
            {
                Btn_BuyCar.gameObject.SetActive(false);
                Btn_Play.gameObject.SetActive(false);
            }
            StaticVariables._iSelectedVechicle = 10;
            Text_CarName.text = "SPECIAL CAR";
            //_specialCarTimerText.text = SpecialCar._instace._text.GetComponent<Text>().text.ToString();
            //_specialCarTimerText.enabled = true;
            //Removing lock button to black car
            _lock.SetActive(false);
        }

        //Custom Function 
        ShowSelected();
    }
    public void BtnAct_PrevVehicle()
    {
        nxtPressed = true;

        //UpgradeManager.myScript.CheckCurrentCarTime = 0.5f;
        if (CurrentCarIndex > 0)
        {
            Debug.Log("CarIndexminprevious" + CurrentCarIndex);

            value = CurrentCarIndex - 1;
            CurrentCarIndex--;

            Debug.Log("CarIndexminpreviousafter" + CurrentCarIndex);

            CheckCurrent_Vehicle();
            //StaticVariables._iSelectedVechicle = CurrentCarIndex;
            //Cars [CurrentCarIndex].transform.position = ListCarOfPositions [CurrentCarIndex].transform.position;

        }


        //Temp Code
        if (CurrentCarIndex < Cars.Length - 1)
        {
            //_specialCarTimerText.enabled = false;
        }
        else if (CurrentCarIndex == 9)
        {
           // _specialCarTimerText.enabled = true;
        }

        ShowSelected();
    }

    public void CheckCurrentVehicle()
    {
        //Debug.Log ("here current car index"+CurrentCarIndex);
        if (CurrentCarIndex == 0)
            Btn_PrevCar.gameObject.SetActive(false);
        else
            Btn_PrevCar.gameObject.SetActive(true);

        if (CurrentCarIndex == Cars.Length - 1)
            Btn_NextCar.gameObject.SetActive(false);
        else
            Btn_NextCar.gameObject.SetActive(true);
        for (int i = 0; i < Cars.Length; i++)
        {
            Cars[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < Cars.Length; i++)
        {
            if (i == CurrentCarIndex)
            {
                Cars[i].gameObject.SetActive(true);
                Cars[i].gameObject.transform.localPosition = new Vector3(Cars[i].gameObject.transform.localPosition.x, 0.5f, Cars[i].gameObject.transform.localPosition.z);
                //Debug.Log(CameraMovingPositions[i]);
                //Debug.Log ("Current Car Index"+CurrentCarIndex);
                CurrentCarIndex = i;


                Text_CarName.text = "" + CarNames[i].ToUpper();
                if (i == 2)
                {
                    freeCarUnlockBG.SetActive(true);
                    freeCarUnlockDescriptionText.text = "COMPLETE 10 LEVELS IN FOREST\nWORLD TO UNLOCK THIS CAR";
                    //Btn_BuyCar.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "COMPLETE 10 LEVELS IN 1ST\nWORLD TO UNLOCK THIS CAR";
                    //Btn_BuyCar.transform.GetChild(0).GetComponent<TextMeshProUGUI>().fontSize = 16;
                }
                else
                if (i == 4)
                {
                    freeCarUnlockBG.SetActive(true);
                    freeCarUnlockDescriptionText.text = "COMPLETE 10 LEVELS IN ICE AGE\nWORLD TO UNLOCK THIS CAR";
                    //Btn_BuyCar.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "COMPLETE 10 LEVELS IN 3RD\nWORLD TO UNLOCK THIS CAR";
                    //Btn_BuyCar.transform.GetChild(0).GetComponent<TextMeshProUGUI>().fontSize = 16;

                }
                else
                {
                    freeCarUnlockBG.SetActive(false);
                    Btn_BuyCar.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "" + CarPrices[i];
                    Btn_BuyCar.transform.GetChild(0).GetComponent<TextMeshProUGUI>().fontSize = 30;

                }
                value = CurrentCarIndex;
            }
            //else
            //{
            //	Cars[i].gameObject.SetActive(false);
            //}

        }
    }

    public void BtnAct_Buy()
    {
        Debug.Log("Button Buy is Working");

        char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
        int _tempcoins = StaticVariables._iTotalCoins;
        {
            if (CurrentCarIndex == 2)
            {

            }
            else
            if (CurrentCarIndex == 4)
            {

            }
            else
            if (_tempcoins >= CarPrices[CurrentCarIndex])
            {
                UnLockCar();
            }
        }
    }

    public void UnLockCar()
    {
        char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
        tempstring[CurrentCarIndex] = '1';
        PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, new string(tempstring));
        CheckCurrent_Vehicle();
        StaticVariables._iSelectedVechicle = CurrentCarIndex;
        CheckBtnVisibility();
    }

    public void CheckBtnVisibility()
    {

        //if (PlayerPrefs.GetString(MyGamePrefs.UnlockedCars) == "1111111111111")
        if (AdManager.Instance != null && AdManager.UpgradeUnlocked == 1)
        {
            //mg_BtnStoreUnlockCars.gameObject.SetActive (false);
            mg_BtnStoreUnlockCars.gameObject.GetComponent<Button>().interactable = false;
            mg_BtnUnlockAllCarsInCarSelection.SetActive(false);
        }
        //if (StaticVariables._iUnlockedLevels == StaticVariables._iTotalLevels)
        //if (StaticVariables._iUnlockedWorldsCount >= 4)
        if (AdManager.Instance != null && AdManager.LevelsUnlocked == 1)
        {
            Debug.LogError("UNLOCKED WORLD COUNT ::::::::::: " + StaticVariables._iUnlockedWorldsCount);
            mg_BtnUnlockALlLvlsInLevelSel.SetActive(false);
            //mg_BtnUnlockAllLvlsInStore.SetActive (false);
            mg_BtnUnlockAllLvlsInStore.gameObject.GetComponent<Button>().interactable = false;
        }

        //if (PlayerPrefs.GetString(MyGamePrefs.UnlockedCars) == "1111111111111" && StaticVariables._iUnlockedLevels == StaticVariables._iTotalLevels)
        //if (PlayerPrefs.GetString(MyGamePrefs.UnlockedCars) == "1111111111111" && StaticVariables._iUnlockedWorldsCount >= 4)
        if (AdManager.Instance != null && AdManager.LevelsUnlocked == 1 && AdManager.UpgradeUnlocked == 1)
        {
            Debug.LogError("UNLOCKED CARS ::::::::::: " + PlayerPrefs.GetString(MyGamePrefs.UnlockedCars));
            Debug.LogError("UNLOCKED LEVELS ::::::::::: " + StaticVariables._iUnlockedLevels);

            mg_BtnUnlockALlLvlsInLevelSel.SetActive(false);

            //mg_BtnUnlockAllLvlsInStore.SetActive (false);
            mg_BtnUnlockAllLvlsInStore.gameObject.GetComponent<Button>().interactable = false;
            //mg_BtnStoreUnlockCars.gameObject.SetActive (false);
            mg_BtnStoreUnlockCars.gameObject.GetComponent<Button>().interactable = false;
            mg_BtnUnlockAllCarsInCarSelection.SetActive(false);
           // mg_BtnUnlockAll.SetActive(false);
            mg_BtnUnlockAll.gameObject.GetComponent<Button>().interactable = false;

        }
    }

    public void VideoSuccess_CarUnlock()
    {
        char[] tempstring = PlayerPrefs.GetString(CarVideosCountPref).ToCharArray();
        int VideoCount = TypeCasting.CharToInt(tempstring[CurrentCarIndex]);
        if (VideoCount > 0)
        {
            VideoCount--;
            tempstring[CurrentCarIndex] = TypeCasting.IntToChar(VideoCount, 0);
            if (VideoCount <= 0)
            {
                tempstring[CurrentCarIndex] = '0';
                UnLockCar();
            }
            PlayerPrefs.SetString(CarVideosCountPref, new string(tempstring));
        }

        Btn_BuyCarVideo.transform.GetChild(0).GetComponent<Text>().text = "WATCH " + VideoCount + " VIDEO(S) TO UNLOCK THE CAR";

        if (VideoCount > 0)
        {

            Debug.Log("Reward Video Succesfully Completed" + "\nWatch " + VideoCount + " Video(s) to Unlock the Car");
        }
        else
        {
            Debug.Log("Reward Video Succesfully Completed!" + "\nYou Have Unlocked " + CarNames[CurrentCarIndex] + "!!");
        }
    }

    public void UnlockCarSpecialCars(int _i)
    {
        char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
        tempstring[_i] = '1';
        PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, new string(tempstring));
        CheckCurrent_Vehicle();
        StaticVariables._iSelectedVechicle = CurrentCarIndex;
        CheckBtnVisibility();
    }




    //My code!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    void ShowSelected()
    {
        if (_carsArray.GetChild(StaticVariables._iSelectedVechicle).gameObject.activeSelf)
        {
            _selectedImage.SetActive(true);
            _playText.SetActive(true);
            _selectText.SetActive(false);
        }
        else
        {
            _selectedImage.SetActive(false);
            _playText.SetActive(false);
            _selectText.SetActive(true);
        }

        CarGlitchFunc();


    }

    void CarGlitchFunc()
    {
        if (StaticVariables._iSelectedVechicle == 10)
        {
            if (!nxtPressed)
            {
                if (UpgradeVechicle.Instance.specialCarUnlocked)
                    Btn_Play.SetActive(true);
                else
                    Btn_Play.SetActive(false);
            }

        }
    }

    void OnDestroy()
    {

        foreach (FieldInfo field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            Type fieldType = field.FieldType;

            if (typeof(IList).IsAssignableFrom(fieldType))
            {
                IList list = field.GetValue(this) as IList;
                if (list != null)
                {
                    list.Clear();
                }
            }

            if (typeof(IDictionary).IsAssignableFrom(fieldType))
            {
                IDictionary dictionary = field.GetValue(this) as IDictionary;
                if (dictionary != null)
                {
                    dictionary.Clear();
                }
            }

            if (!fieldType.IsPrimitive)
            {
                field.SetValue(this, null);
            }
        }
    }
}
