﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Reflection;
using System;

public class LevelSelectionHandler : MonoBehaviour
{

    // Use this for initialization
    public Scrollbar ScrollBarValue;
    public GameObject LevelSelContent;
    public GameObject[] Level_Sets;
    public List<GameObject> ListOfLevels;
    public List<GameObject> ListOfLevelNoText;
    public List<GameObject> LockIcons;
    public List<GameObject> UnlockIcons;
    public List<GameObject> Locks;
    //public List<GameObject> ListOfLevelImages;

    public List<GameObject> Rewards;

    public List<GameObject> LevelImages;
    public List<GameObject> RewardBG;

    public Sprite LockedLevelSprite;
    public Sprite CurrentLevelSprite;
    public Sprite LevelCompletedSprite;

    public Sprite RewardLockedBG;
    public Sprite RewardUnLockedBG;

    public Image BluePath;
    public Image GreenPath;
    public Scrollbar horizontalScroll;

    //public List<Texture> World01_Textures;
    //public List<Texture> World02_Textures;
    //public List<Texture> World03_Textures;
    //public List<Texture> World04_Textures;
    //public List<Texture> World05_Textures;

    //public List<Material> MaterialsToChange;

    public Image CurrentWorldImage;
    public Sprite[] worldSpritesToAssign;

    private Texture[] currentWorld = new Texture[5];



    public static LevelSelectionHandler Instance;
    public Color LevelCompletedTextColor;
    public Color LevelUnlockedTextColor;
    public Color LevellockedTextColor;





    public bool LevelSelectionAd = false;

    public TextMeshProUGUI worldName;
    public GameObject worldImage;

    public GameObject worldSelectionPanel;
    public Button[] worldButtons;
    public TextMeshProUGUI[] CompletedLevelCountTextInEachWorld;
    public GameObject[] CompletedLevelsParent;

    public GameObject[] UnlockNextLevelsICONS;
    public GameObject[] UnlockNextCarsICONS;

    public GameObject levelSelectionPanel;

    public void Awake()
    {
        Debug.LogError("LEVEL SELECION HANDLER AWAKE ###### ");
        Instance = this;
    }
    private void OnEnable()
    {
        Debug.LogError("LEVEL SELECION HANDLER ON ENABLE ###### ");

    }

    void Start()
    {
        StaticVariables.splittedRewardBool = StaticVariables._iRewardBools.Split(',');
        StaticVariables.splitUnlockCarBool = StaticVariables._iUnlockCarBools.Split(',');
        StaticVariables.splitCar2XRewardBool = StaticVariables._iCar2XRewardBools.Split(',');

        Debug.Log(" _iRewardBools VALUE%%%%%%%%%%%%%%%%%  : " + StaticVariables._iRewardBools);
        Debug.Log(" _iUnlockCarBools VALUE%%%%%%%%%%%%%%%%%  : " + StaticVariables._iUnlockCarBools);
        Debug.Log(" _iCar2XRewardBools VALUE%%%%%%%%%%%%%%%%%  : " + StaticVariables._iCar2XRewardBools);


        InitialProperties();
        // checkLocks();
        // SetLevelsPos();
        //for (int i = 0; i < currentWorld.Length; i++)
        //{
        //    currentWorld[i] = World01_Textures[i];
        //}

        //  SetPosRecentLevel();
    }
    // Update is called once per frame
    void Update()
    {
        LevelBtnColorChanger();

        //if (Input.GetKeyDown(KeyCode.N))
        //    customFunction();
    }

    int divideNo = 0;
    public void InitialProperties()
    {

        for (int i = 0; i < ListOfLevels.Count; i++)
        {
            ListOfLevelNoText.Add(ListOfLevels[i].transform.GetChild(0).GetChild(0).gameObject);
            LevelImages.Add(ListOfLevels[i].transform.GetChild(0).gameObject);
            RewardBG.Add(ListOfLevels[i].transform.GetChild(0).GetChild(1).gameObject);
            Rewards.Add(ListOfLevels[i].transform.GetChild(0).GetChild(1).GetChild(0).gameObject);

            //ListOfLevelImages.Add (ListOfLevels [i].transform.GetChild (1).GetChild(0).gameObject);
            //ListOfLevelBoarder.Add(ListOfLevels[i].transform.GetChild(0).gameObject);
            //UnlockIcons.Add(ListOfLevels[i].transform.GetChild(4).gameObject);
            //LockIcons.Add (ListOfLevels [i].transform.GetChild (5).gameObject);
            //Locks.Add (ListOfLevels [i].transform.GetChild (4).gameObject);
            //ListOfLevelImages.Add (ListOfLevels [i].transform.GetChild (5).gameObject);
        }

        for (int i = 0; i < ListOfLevelNoText.Count; i++)
        {
            if (i >= 0 && i <= 14)
            {
                divideNo = 00;
            }
            else if (i >= 15 && i <= 29)
            {
                divideNo = 15;
            }
            else if (i >= 30 && i <= 44)
            {
                divideNo = 30;
            }
            else if (i >= 45 && i <= 59)
            {
                divideNo = 45;
            }
            else if (i >= 60 && i <= 74)
            {
                divideNo = 60;
            }
            ListOfLevelNoText[i].GetComponent<TextMeshProUGUI>().text = "" + ((i + 1) - (divideNo)).ToString("00");

        }

        for (int i = 0; i < RewardBG.Count; i++)
        {
            RewardBG[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < Rewards.Count; i++)
        {
            Rewards[i].GetComponent<TextMeshProUGUI>().text = StaticVariables.EachLevelScore[i].ToString();
        }


        foreach (GameObject L_Lock in LockIcons)
            L_Lock.gameObject.SetActive(true);

        foreach (GameObject L_Unlock in UnlockIcons)
            L_Unlock.gameObject.SetActive(false);

        //		foreach(GameObject L_LevelImages in ListOfLevelImages)
        //			L_LevelImages.gameObject.SetActive (false);

        

    }

    public void PathUpdate(int currentUnlockedLevel)
    {
        switch (StaticVariables.UnlockedTheme)
        {
            case "Theme01":
                break;
            case "Theme02":
                currentUnlockedLevel = currentUnlockedLevel - 15;
                break;
            case "Theme03":
                currentUnlockedLevel = currentUnlockedLevel - 30;
                break;
            case "Theme04":
                currentUnlockedLevel = currentUnlockedLevel - 45;
                break;
            case "Theme05":
                currentUnlockedLevel = currentUnlockedLevel - 60;
                break;
        }

        switch (currentUnlockedLevel)
        {
            case 1:
                GreenPath.fillAmount = 0;
                BluePath.fillAmount = 1;
                horizontalScroll.value = 0;
                break;
            case 2:
                GreenPath.fillAmount = 0.05f;
                BluePath.fillAmount = 1 - 0.05f;
                horizontalScroll.value = 0;
                break;
            case 3:
                GreenPath.fillAmount = 0.125f;
                BluePath.fillAmount = 1 - 0.125f;
                horizontalScroll.value = 0;
                break;
            case 4:
                GreenPath.fillAmount = 0.175f;
                BluePath.fillAmount = 1 - 0.175f;
                horizontalScroll.value = 0;
                break;
            case 5:
                GreenPath.fillAmount = 0.26f;
                BluePath.fillAmount = 1 - 0.26f;
                horizontalScroll.value = 0.325f;
                break;
            case 6:
                GreenPath.fillAmount = 0.275f;
                BluePath.fillAmount = 1 - 0.275f;
                horizontalScroll.value = 0.325f;
                break;
            case 7:
                GreenPath.fillAmount = 0.35f;
                BluePath.fillAmount = 1 - 0.35f;
                horizontalScroll.value = 0.325f;
                break;
            case 8:
                GreenPath.fillAmount = 0.45f;
                BluePath.fillAmount = 1 - 0.45f;
                horizontalScroll.value = 1;
                break;
            case 9:
                GreenPath.fillAmount = 0.47f;
                BluePath.fillAmount = 1 - 0.47f;
                horizontalScroll.value = 1;
                break;
            case 10:
                GreenPath.fillAmount = 0.59f;
                BluePath.fillAmount = 1 - 0.59f;
                horizontalScroll.value = 1;
                break;
            case 11:
                GreenPath.fillAmount = 0.66f;
                BluePath.fillAmount = 1 - 0.66f;
                horizontalScroll.value = 1;
                break;
            case 12:
                GreenPath.fillAmount = 0.74f;
                BluePath.fillAmount = 1 - 0.74f;
                horizontalScroll.value = 1;
                break;
            case 13:
                GreenPath.fillAmount = 0.81f;
                BluePath.fillAmount = 1 - 0.81f;
                horizontalScroll.value = 1;
                break;
            case 14:
                GreenPath.fillAmount = 0.9f;
                BluePath.fillAmount = 1 - 0.9f;
                horizontalScroll.value = 1;
                break;
            case 15:
                GreenPath.fillAmount = 0.975f;
                BluePath.fillAmount = 1 - 0.975f;
                horizontalScroll.value = 1;
                break;

            default:
                break;
        }
    }

    public void LevelImageUpdate(int loopNo)
    {
        LevelComplettion._instance.enablesFunction();

        LevelImages[loopNo].gameObject.GetComponent<Button>().interactable = true;
        RewardBG[loopNo].gameObject.SetActive(true);
        RewardBG[loopNo].gameObject.GetComponent<Image>().sprite = RewardUnLockedBG;
        if (LevelImages[loopNo].gameObject.GetComponent<Image>().sprite == CurrentLevelSprite)
        {
            Rewards[loopNo].gameObject.GetComponent<TextMeshProUGUI>().color = LevelUnlockedTextColor;
        }
    }
    public void checkLocks()
    {
        switch (StaticVariables.UnlockedTheme)
        {
            case "Theme01":
                int UnlockedLevels = StaticVariables._iUnlockedLevels - 1;
                RewardBG[StaticVariables._iUnlockedLevels].SetActive(true);

                if (AdManager.SubscribeUnlocked == 1)
                {
                    UnlockedLevels = 14;
                }
                PathUpdate(UnlockedLevels + 1);

                if (UnlockedLevels <= 15)
                {
                    for (int i = 0; i <= UnlockedLevels; i++)
                    {
                        LevelImageUpdate(i);
                    }
                }
                break;

            case "Theme02":
                UnlockedLevels = StaticVariables._iUnlockedLevels - 1;
                RewardBG[StaticVariables._iUnlockedLevels].SetActive(true);

                if (AdManager.SubscribeUnlocked == 1)
                {
                    UnlockedLevels = 29;
                }
                PathUpdate(UnlockedLevels + 1);

                if (UnlockedLevels <= 30)
                {
                    for (int i = 15; i <= UnlockedLevels; i++)
                    {
                        LevelImageUpdate(i);
                    }
                }
                break;

            case "Theme03":
                UnlockedLevels = StaticVariables._iUnlockedLevels - 1;
                RewardBG[StaticVariables._iUnlockedLevels].SetActive(true);

                if (AdManager.SubscribeUnlocked == 1)
                {
                    UnlockedLevels = 44;
                }
                PathUpdate(UnlockedLevels + 1);

                if (UnlockedLevels <= 45)
                {
                    for (int i = 30; i <= UnlockedLevels; i++)
                    {
                        LevelImageUpdate(i);
                    }
                }
                break;

            case "Theme04":
                UnlockedLevels = StaticVariables._iUnlockedLevels - 1;
                RewardBG[StaticVariables._iUnlockedLevels].SetActive(true);

                if (AdManager.SubscribeUnlocked == 1)
                {
                    UnlockedLevels = 59;
                }
                PathUpdate(UnlockedLevels + 1);

                if (UnlockedLevels <= 60)
                {
                    for (int i = 45; i <= UnlockedLevels; i++)
                    {
                        LevelImageUpdate(i);
                    }
                }
                break;

            case "Theme05":
                UnlockedLevels = StaticVariables._iUnlockedLevels - 1;
                RewardBG[StaticVariables._iUnlockedLevels - 1].SetActive(true);

                if (AdManager.SubscribeUnlocked == 1)
                {
                    UnlockedLevels = 74;
                }
                PathUpdate(UnlockedLevels + 1);

                if (UnlockedLevels <= 75)
                {
                    for (int i = 60; i <= UnlockedLevels; i++)
                    {
                        LevelImageUpdate(i);
                    }
                }
                break;
            default:
                break;
        }

        Debug.LogWarning("Unlocked Lves: " + StaticVariables._iUnlockedLevels);
        Debug.LogWarning("Total levels: " + StaticVariables._iTotalLevels);
        UpgradeVechicle.Instance.CheckBtnVisibility();

        /*
        if (StaticVariables.UnlockedTheme == "Theme01")
        {
            int UnlockedLevels = StaticVariables._iUnlockedLevels - 1;

            if (UnlockedLevels <= 10)
            {
                for (int i = 0; i <= UnlockedLevels; i++)
                {
                    //UnlockIcons [i].gameObject.SetActive (true);
                    //LockIcons [i].gameObject.SetActive (false);
                    //Commenting 
                    Locks[i].gameObject.SetActive(false);
                    ListOfLevelImages[i].gameObject.SetActive(true);
                    ListOfLevelImages[i].GetComponent<Image>().color = UnlockedColor;
                    ListOfLevelBoarder[i].gameObject.SetActive(true);
                }
            }
        }

        if (StaticVariables.UnlockedTheme == "Theme02")
        {
            int UnlockedLevels = StaticVariables._iUnlockedLevels - 1;

            if (UnlockedLevels <= 20)
            {
                for (int i = 10; i <= UnlockedLevels; i++)
                {
                    Debug.Log("**************");
                    //UnlockIcons [i].gameObject.SetActive (true);
                    //LockIcons [i].gameObject.SetActive (false);
                    //Commenting 
                    Locks[i].gameObject.SetActive(false);
                    ListOfLevelImages[i].gameObject.SetActive(true);
                    ListOfLevelImages[i].GetComponent<Image>().color = UnlockedColor;
                    ListOfLevelBoarder[i].gameObject.SetActive(true);
                }
            }

        }
        */

    }

    public void UpdateWorldInfo(int worldNo)
    {
        switch (worldNo)
        {
            case 0:
                worldName.text = "Forest";
                CurrentWorldImage.sprite = worldSpritesToAssign[0];
                break;
            case 1:
                worldName.text = "Desert";
                CurrentWorldImage.sprite = worldSpritesToAssign[1];
                break;
            case 2:
                worldName.text = "Snow";
                CurrentWorldImage.sprite = worldSpritesToAssign[2];
                break;
            case 3:
                worldName.text = "Sci-Fi";
                CurrentWorldImage.sprite = worldSpritesToAssign[3];
                break;
            case 4:
                worldName.text = "Coming Soon";
                CurrentWorldImage.sprite = worldSpritesToAssign[4];
                break;
        }
    }

    //public void ChangeMaterialTexture(int themeNo)
    //{
    //    switch (themeNo)
    //    {
    //        case 0:
    //            for (int i = 0; i < currentWorld.Length; i++)
    //            {
    //                currentWorld[i] = World01_Textures[i];
    //            }
    //            break;
    //        case 1:
    //            for (int i = 0; i < currentWorld.Length; i++)
    //            {
    //                currentWorld[i] = World02_Textures[i];
    //            }
    //            break;
    //        case 2:
    //            for (int i = 0; i < currentWorld.Length; i++)
    //            {
    //                currentWorld[i] = World03_Textures[i];
    //            }
    //            break;
    //        case 3:
    //            for (int i = 0; i < currentWorld.Length; i++)
    //            {
    //                currentWorld[i] = World04_Textures[i];
    //            }
    //            break;
    //        case 4:
    //            for (int i = 0; i < currentWorld.Length; i++)
    //            {
    //                currentWorld[i] = World05_Textures[i];
    //            }
    //            break;
    //        default:
    //            for (int i = 0; i < currentWorld.Length; i++)
    //            {
    //                currentWorld[i] = World01_Textures[i];
    //            }
    //            break;

    //    }

    //    for (int i = 0; i < MaterialsToChange.Count; i++)
    //    {
    //        MaterialsToChange[i].SetTexture("_MainTex", currentWorld[i]);
    //    }
    //}

    public void LevelLoad(int LevelValue)
    {
        if (AdManager.Instance != null)
        {
            AdManager.Instance.IsForceHideBannerAd = true;
        }
        if (AdController.Instance != null)
        {
            AdController.Instance.HideIronSourceBannerAd();
        }
        MenuUIManager.Instance.LoadingCanvas.SetActive(true);
        //if(LevelValue<=StaticVariables._iUnlockedLevels)
        //{
        // DontDestroyOnLoad(LevelLoader.instance.Levels[LevelValue-1].gameObject);
        StaticVariables._iSelectedLevel = LevelValue;
        Debug.LogWarning(StaticVariables._iSelectedLevel);

        // UnloadSceneOptions(scee);

        //Updated code for scene saparation
        if (LevelValue >= 1 && LevelValue <= 15)
        {
            //ChangeMaterialTexture(0);
            LoadingManager.SceneName = "World01_meshCombine";
        }
        else if (LevelValue >= 16 && LevelValue <= 30)
        {
            //ChangeMaterialTexture(1);
            LoadingManager.SceneName = "World02_meshCombine";
            //  worldName.text = "Desert";
        }
        else if (LevelValue >= 31 && LevelValue <= 45)
        {
            //ChangeMaterialTexture(2);
            LoadingManager.SceneName = "World03";
            //   worldName.text = "Snowland";
        }
        else if (LevelValue >= 46 && LevelValue <= 60)
        {
            //ChangeMaterialTexture(3);
            LoadingManager.SceneName = "World04";
            //   worldName.text = "Sci-Fi";
        }
        else if (LevelValue >= 61 && LevelValue <= 75)
        {
            //ChangeMaterialTexture(4);
            LoadingManager.SceneName = "World05";
            //    worldName.text = "Not Yet Decided";
        }
        else
        {
            LoadingManager.SceneName = "TestingEnvi_02";
        }
        Debug.Log("UNLOCKED LEVELS IN WORLD ************************ " + StaticVariables.UnlockedTheme + StaticVariables._iUnlockedLevels);

        SceneManager.LoadScene("LoadingScene");

        //      }
        //      else
        //{
        //          Debug.LogWarning("Somethign is wrong");
        //}

        if (AdManager.Instance != null)
        {
            //AdManager.Instance.IncreaseLoseCounter();
            AdManager.Instance.RunActions(AdManager.Actiontype.LvlSelection_Play);

        }
    }

    private void UnloadSceneOptions(Scene scee)
    {
        throw new NotImplementedException();
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(1);
    }

    public void SetPosRecentLevel()
    {
        if (StaticVariables._iUnlockedLevels >= 1 && StaticVariables._iUnlockedLevels <= 15)
        {
            SetOfLevelsInd = 0;
        }
        else
        if (StaticVariables._iUnlockedLevels >= 16 && StaticVariables._iUnlockedLevels <= 30)
        {
            SetOfLevelsInd = 1;
        }
        else
        if (StaticVariables._iUnlockedLevels >= 31 && StaticVariables._iUnlockedLevels <= 45)
        {
            SetOfLevelsInd = 2;
        }
        else
        if (StaticVariables._iUnlockedLevels >= 46 && StaticVariables._iUnlockedLevels <= 60)
        {
            SetOfLevelsInd = 3;
        }
        else
        if (StaticVariables._iUnlockedLevels >= 61 && StaticVariables._iUnlockedLevels <= 75)
        {
            SetOfLevelsInd = 4;
        }

        SetLevelsPos();
    }

    public void LevelBtnColorChanger()
    {
        //if(ScrollBarValue.value<0.2f)
        //{

        //	Group2.gameObject.SetActive (false);
        //	Group4.SetActive (false);
        //	Group3.SetActive (false);
        //	Group1.SetActive (true);
        //}
        //else if(ScrollBarValue.value>0.2f&&ScrollBarValue.value<0.5f)
        //{

        //	Group4.SetActive (false);
        //	Group2.SetActive (true);
        //	Group3.SetActive (false);
        //	Group1.SetActive (false);
        //}
        //else if(ScrollBarValue.value>0.5f&&ScrollBarValue.value<=0.8f)
        //{

        //	Group4.SetActive (false);
        //	Group2.SetActive (false);
        //	Group3.SetActive (true);
        //	Group1.SetActive (false);
        //}
        //else if(ScrollBarValue.value>0.8f)
        //{

        //	Group4.SetActive (true);
        //	Group2.SetActive (false);
        //	Group3.SetActive (false);
        //	Group1.SetActive (false);
        //}
    }


    public Image Btn_Prev, Btn_Next;

    int SetOfLevelsInd = 0;

    public void BtnAct_NextSet(int _Val)
    {
        if (SetOfLevelsInd < 4)      // b4 9
        {
            SetOfLevelsInd += _Val;
            SetLevelsPos();
        }
    }

    public void BtnAct_PrevSet(int _Val)
    {
        if (SetOfLevelsInd > 0)
        {
            SetOfLevelsInd -= _Val;
            SetLevelsPos();
        }
    }

    public void SetLevelsPos()
    {
        /*
        for (int j = 0; j < Level_Sets.Length; j++)
        {
            if (j == SetOfLevelsInd)
            {
                for (int i = 0; i < Level_Sets[SetOfLevelsInd].transform.childCount; i++)
                {
                    iTween.ScaleTo(Level_Sets[SetOfLevelsInd].transform.GetChild(i).gameObject, iTween.Hash("Scale", Vector3.one, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
                }
            }
            else
            {
                for (int i = 0; i < Level_Sets[j].transform.childCount; i++)
                {
                    iTween.ScaleTo(Level_Sets[j].transform.GetChild(i).gameObject, iTween.Hash("Scale", Vector3.zero, "time", 0.25f, "delay", 0, "easetype", iTween.EaseType.linear));
                }
            }
        }

        if (SetOfLevelsInd <= 0)
        {Btn_Prev.color = new Color(Btn_Prev.color.r, Btn_Prev.color.g, Btn_Prev.color.b, 0.5f);}
        else
        {Btn_Prev.color = new Color(Btn_Prev.color.r, Btn_Prev.color.g, Btn_Prev.color.b, 1f);}

        if (SetOfLevelsInd >= 4)  // before 14
        {Btn_Next.color = new Color(Btn_Next.color.r, Btn_Next.color.g, Btn_Next.color.b, 0.5f);}
        else
        {Btn_Next.color = new Color(Btn_Next.color.r, Btn_Next.color.g, Btn_Next.color.b, 1f);}

        */
        UpdateWorldInfo(SetOfLevelsInd);

    }

    public void WorldSelection()
    {
        Debug.LogError("THEMEs UNLOCKED STATIC VARIABLE : ++++++ " + StaticVariables._iUnlockedWorldsCount);

        worldSelectionPanel.SetActive(true);
        levelSelectionPanel.SetActive(false);
        if (AdManager.SubscribeUnlocked == 1)
        {
            for (int i = 0; i < worldButtons.Length; i++)
            {
                worldButtons[i].interactable = true;
            }
        }
        else
        {

            for (int i = 0; i <= StaticVariables._iUnlockedWorldsCount; i++)
            {
                worldButtons[i].interactable = true;
                CompletedLevelsParent[i].SetActive(true);
            }
        }
        if (AdManager.Instance != null)
        {
            AdManager.Instance.RunActions(AdManager.PageType.WorldSelection);
        }
    }

    public void SelectLevelsInWorld(int worldNo)
    {

        for (int i = 0; i < Level_Sets.Length; i++)
        {
            Level_Sets[i].SetActive(false);
        }

        switch (worldNo)
        {
            case 1:
                StaticVariables.UnlockedTheme = "Theme01";
                Level_Sets[0].SetActive(true);

                break;
            case 2:
                StaticVariables.UnlockedTheme = "Theme02";
                Level_Sets[1].SetActive(true);

                break;
            case 3:
                StaticVariables.UnlockedTheme = "Theme03";
                Level_Sets[2].SetActive(true);
                break;
            case 4:
                StaticVariables.UnlockedTheme = "Theme04";
                Level_Sets[3].SetActive(true);
                break;
            case 5:
                StaticVariables.UnlockedTheme = "Theme05";
                Level_Sets[4].SetActive(true);
                break;
            default:
                Level_Sets[0].SetActive(true);
                break;
        }
        Debug.Log("UNLOCKED WORLD *********** " + StaticVariables.UnlockedTheme + "   Unlock Levels ************* " + StaticVariables._iUnlockedLevels);

        worldSelectionPanel.SetActive(false);
        levelSelectionPanel.SetActive(true);

        // customFunction2();
        checkLocks();
        //SetPosRecentLevel();
        if (AdManager.Instance != null)
        {
            AdManager.Instance.RunActions(AdManager.PageType.LvlSelection);
        }

        if (AdManager.Instance != null)
        {
            AdManager.Instance.RunActions(AdManager.Actiontype.World_Play);
        }

        if (AdManager.LevelsUnlocked == 1)
        {
            for (int i = 0; i < UnlockNextLevelsICONS.Length; i++)
            {
                UnlockNextLevelsICONS[i].SetActive(false);
            }

        }
        if (AdManager.UpgradeUnlocked == 1)
        {
            for (int i = 0; i < UnlockNextCarsICONS.Length; i++)
            {
                UnlockNextCarsICONS[i].SetActive(false);
            }
        }
        else 
        {
            char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();

            if (tempstring[2] == '1')
            {
                UnlockNextCarsICONS[0].SetActive(false);
            }

            if (tempstring[4] == '1')
            {
                UnlockNextCarsICONS[2].SetActive(false);
            }
        }

       
    }




    public void customFunction()
    {
        Invoke("customFunction2", 0.1f);
    }

    public void customFunction2()
    {
        StaticVariables._iSelectedLevel += 1;
        if (StaticVariables._iSelectedLevel >= 1 && StaticVariables._iSelectedLevel <= 10)
        {
            SetOfLevelsInd = 0;
        }
        else
        if (StaticVariables._iSelectedLevel >= 11 && StaticVariables._iSelectedLevel <= 20)
        {
            SetOfLevelsInd = 1;
        }
        else
        if (StaticVariables._iSelectedLevel >= 21 && StaticVariables._iSelectedLevel <= 30)
        {
            SetOfLevelsInd = 2;
        }
        else
        if (StaticVariables._iSelectedLevel >= 31 && StaticVariables._iSelectedLevel <= 40)
        {
            SetOfLevelsInd = 3;
        }
        else
        if (StaticVariables._iSelectedLevel >= 41 && StaticVariables._iSelectedLevel <= 50)
        {
            SetOfLevelsInd = 4;
        }
        SetLevelsPos();
    }

    void OnDestroy()
    {

        foreach (FieldInfo field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            Type fieldType = field.FieldType;

            if (typeof(IList).IsAssignableFrom(fieldType))
            {
                IList list = field.GetValue(this) as IList;
                if (list != null)
                {
                    list.Clear();
                }
            }

            if (typeof(IDictionary).IsAssignableFrom(fieldType))
            {
                IDictionary dictionary = field.GetValue(this) as IDictionary;
                if (dictionary != null)
                {
                    dictionary.Clear();
                }
            }

            if (!fieldType.IsPrimitive)
            {
                field.SetValue(this, null);
            }
        }
    }
}
