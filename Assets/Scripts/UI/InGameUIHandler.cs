﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Reflection;
using System;

public class InGameUIHandler : MonoBehaviour
{

    public GameObject RccCanvas;
    public GameObject OurCanvas;
    public GameObject LevelComplete;
    public GameObject LevelFaild;
    public GameObject Pause;
    public TextMeshProUGUI TxtTotalScoreLC, TxtTotalScoreLC2, collectedCoins, eachLevelBonus;
    public TextMeshProUGUI TxtTotalScoreLF;
    public Button BTnDoubleReward;
    //public Text TxtLevelScore;
    public GameObject Revive;
    public List<Text> TotalScoreTxts;
    public TextMeshProUGUI LevelNo;
    public GameObject Help, SavepointHelp;
    public static InGameUIHandler Instance;
    public GameObject Btn_LCNext, Btn_LFRetry;

    public TextMeshProUGUI revivePanelTimerText;
    private int reviveTimeLeft = 5;

    public GameObject LoadingCanvas;

    public bool VideoReward_2x = false;
    public TextMeshProUGUI ToatalcoinsText;
    void Awake()
    {
        Instance = this;
        LoadingCanvas.SetActive(false);
    }
    void Start()
    {
        InGameEnable();
        if (StaticVariables._iHelp == 0)
        {
            Help.SetActive(true);
        }
        else
        {
            Help.SetActive(false);
        }

        //Invoke("VehicleSoundOn", 1f);

#if !AdSetup_OFF
        Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventLevelStart, new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterLevel, "Level_" + StaticVariables._iSelectedLevel));
#endif
        Debug.Log("INGAME HERE");
        //AdManager.Instance.RequestRewardBasedVideo();
    }

    public static int LevelCompleteCount;
    public int DoubleReward = 0;

    public void LC()
    {
        Debug.LogError("LC 222222222222");

        //Stamping
        // LevelComplettion._instance.LevelCompleteIndication(StaticVariables._iSelectedLevel);
        if (StaticVariables._levelsString[StaticVariables._iSelectedLevel] == '0')
        {
            Debug.LogError("LC 333333333333333");

            Debug.Log("######### LEVEL STRING of " + StaticVariables._iSelectedLevel + " is 0#########");
            StaticVariables._iCompletedLevels++;

            if (StaticVariables._iSelectedLevel >= 1 && StaticVariables._iSelectedLevel <= 15)
            {
                StaticVariables._iCompletedLevelsInTheme01++;
            }
            else if (StaticVariables._iSelectedLevel >= 16 && StaticVariables._iSelectedLevel <= 30)
            {
                StaticVariables._iCompletedLevelsInTheme02++;
            }
            else if (StaticVariables._iSelectedLevel >= 31 && StaticVariables._iSelectedLevel <= 45)
            {
                StaticVariables._iCompletedLevelsInTheme03++;
            }
            else if (StaticVariables._iSelectedLevel >= 46 && StaticVariables._iSelectedLevel <= 60)
            {
                StaticVariables._iCompletedLevelsInTheme04++;
            }
            else if (StaticVariables._iSelectedLevel >= 61 && StaticVariables._iSelectedLevel <= 75)
            {
                StaticVariables._iCompletedLevelsInTheme05++;
            }
        }

        StaticVariables._levelsString = StaticVariables._levelsString.Remove(StaticVariables._iSelectedLevel, 1);
        StaticVariables._levelsString = StaticVariables._levelsString.Insert(StaticVariables._iSelectedLevel, "1");
        //for (int i = 0; i < levels.Length; i++)
        //{
        //    _completedLevels[i] = levels[i];
        //}
        Debug.LogError("LEVEL STRING SETUP 3333333333333 " + StaticVariables._levelsString.Length);

        //PlayerPrefs.SetString("LevelsStamp", StaticVariables._levelsString);
        //Stamping End


        LevelCompleteCount++;

        LCEnable();

        TxtTotalScoreLC2.text = "" + StaticVariables.EachLevelScore[StaticVariables._iSelectedLevel - 1] + " COINS";

        //if (!RateUsManager._instance.DisplayRateUsPannel())
        //{
        //    AdManager.Instance.IncreaseWinCounter();
        //}


        if (StaticVariables._iSelectedLevel != 5 && StaticVariables._iSelectedLevel != 12)
        {
            VideoReward_2x = false;
            Tween_2XLC._instance.Call_TweenIn();
        }
        else
        {
            LCPage();
        }


#if !PlayServices_Off

        //FirebaseController.Instance.SetUserProperty("Level", StaticVariables._iSelectedLevel.ToString());
        //Add to loader board!!
        if (PlayServicesHandler.instance)
        {
            PlayServicesHandler.instance.SubmitScoreToLB(StaticVariables._iTotalCoins);

            //Check Achivements
            PlayServicesHandler.instance.CheckAchivements();
        }
#endif
    }

    public int LC2Xcoins = 1;

    [Header("Unlockcars_Sprites")]
    public Image _carUnlock;
    public Sprite Car02Sprite, Car03Sprite, Car04Sprite;

    [Header("Unlock_New_Worlds")]
    public Image _worldUnlock;
    public Sprite world02Sprite, world03Sprite, world04Sprite, world05Sprite;

    [Header("Specific Car")]
    public string[] SpecificCar;
    public Image _SpecificCarUnlockICon;
    public Sprite SpecificCar01Sprite, SpecificCar02Sprite;

    [Header("Reward Coins Amount")]
    public TextMeshProUGUI coinsRewardTextTMP;
    public int[] RewardAmount;

    private int currentRewardCoins;
    private int Current2xValue;


    public void LCPage()
    {
        ;
        Tween_LC._instance.Call_TweenIn();


        if (AdManager.Instance != null)
        {
            //AdManager.Instance.IncreaseLoseCounter();
            AdManager.Instance.RunActions(AdManager.Actiontype.Lvl_Win, StaticVariables._iSelectedLevel);
            AdManager.Instance.RunActions(AdManager.PageType.LCPage, StaticVariables._iSelectedLevel);

        }
        //        if (!RateUsManager._instance.DisplayRateUsPannel())
        //        {
        //#if !AdSetup_OFF
        //            AdManager.Instance.IncreaseWinCounter();
        //#endif
        //}

        #region COINS_REWARDS
        if (AdManager.SubscribeUnlocked != 1)
        {
            if (StaticVariables._iSelectedLevel == StaticVariables.RewardCoinsAtLevelNo[0] && StaticVariables.splittedRewardBool[0] == "0")
            {
                StaticVariables.splittedRewardBool = StaticVariables._iRewardBools.Split(',');
                StaticVariables.splittedRewardBool[0] = "1";
                string str = string.Join(",", StaticVariables.splittedRewardBool);
                StaticVariables._iRewardBools = str;

                Tween_CoinsReward._instance.Invoke("Call_TweenIn", 1);
                currentRewardCoins = RewardAmount[0];
                coinsRewardTextTMP.text = "YOU HAVE EARNED " + RewardAmount[0] + " AS BONUS";
            }
            else if (StaticVariables._iSelectedLevel == StaticVariables.RewardCoinsAtLevelNo[1] && StaticVariables.splittedRewardBool[1] == "0")
            {
                StaticVariables.splittedRewardBool = StaticVariables._iRewardBools.Split(',');
                StaticVariables.splittedRewardBool[1] = "1";
                string str = string.Join(",", StaticVariables.splittedRewardBool);
                StaticVariables._iRewardBools = str;

                Tween_CoinsReward._instance.Invoke("Call_TweenIn", 1);
                currentRewardCoins = RewardAmount[1];
                coinsRewardTextTMP.text = "YOU HAVE EARNED " + RewardAmount[1] + " AS BONUS";
            }
            else if (StaticVariables._iSelectedLevel == StaticVariables.RewardCoinsAtLevelNo[2] && StaticVariables.splittedRewardBool[2] == "0")
            {
                StaticVariables.splittedRewardBool = StaticVariables._iRewardBools.Split(',');
                StaticVariables.splittedRewardBool[2] = "1";
                string str = string.Join(",", StaticVariables.splittedRewardBool);
                StaticVariables._iRewardBools = str;

                Tween_CoinsReward._instance.Invoke("Call_TweenIn", 1);
                currentRewardCoins = RewardAmount[2];
                coinsRewardTextTMP.text = "YOU HAVE EARNED " + RewardAmount[2] + " AS BONUS";
            }
            else
            {
                currentRewardCoins = 0;
            }
        }


        #endregion

        #region PLAY_With_SPECIFIC_CAR_GET_2X_REWARD
        if (AdManager.SubscribeUnlocked != 1)
        {
            if (StaticVariables._iSelectedLevel == StaticVariables.SpecificCar2XRewardAtLevelNo[0] && GameManager.Instance.CurrentVehile.name == SpecificCar[0] && StaticVariables.splitCar2XRewardBool[0] == "0")
            {
                _SpecificCarUnlockICon.sprite = SpecificCar01Sprite;

                StaticVariables.splitCar2XRewardBool = StaticVariables._iCar2XRewardBools.Split(',');
                StaticVariables.splitCar2XRewardBool[0] = "1";
                string a = string.Join(",", StaticVariables.splitCar2XRewardBool);
                StaticVariables._iCar2XRewardBools = a;

                Tween_PlayWithSpecificCar._instance.Invoke("Call_TweenIn", 1);
                Current2xValue = 2;
            }
            else if (StaticVariables._iSelectedLevel == StaticVariables.SpecificCar2XRewardAtLevelNo[1] && GameManager.Instance.CurrentVehile.name == SpecificCar[1] && StaticVariables.splitCar2XRewardBool[1] == "0")
            {
                _SpecificCarUnlockICon.sprite = SpecificCar02Sprite;

                StaticVariables.splitCar2XRewardBool = StaticVariables._iCar2XRewardBools.Split(',');
                StaticVariables.splitCar2XRewardBool[1] = "1";
                string a = string.Join(",", StaticVariables.splitCar2XRewardBool);
                StaticVariables._iCar2XRewardBools = a;

                Tween_PlayWithSpecificCar._instance.Invoke("Call_TweenIn", 1);
                Current2xValue = 2;
            }
            else
            {
                Current2xValue = 1;
            }
        }
        #endregion


        StaticVariables._iTotalCoins = ((StaticVariables._iTotalCoins) + currentRewardCoins + ((StaticVariables.EachLevelScore[StaticVariables._iSelectedLevel - 1] + CoinsManger.manager.collectedCoins) * LC2Xcoins)) * Current2xValue;

        if (AdManager.SubscribeUnlocked != 1)
        {
            if (StaticVariables._iUnlockedLevels <= StaticVariables._iSelectedLevel && StaticVariables._iUnlockedLevels < StaticVariables._iTotalLevels)
            {
                StaticVariables._iUnlockedLevels = StaticVariables._iUnlockedLevels + 1;
                PlayerPrefs.SetString("LevelsStamp", StaticVariables._levelsString);
            }
        }

        #region UNLOCK_NEXT_WORLD
        if (AdManager.SubscribeUnlocked != 1)
        {


            if (StaticVariables.UnlockedTheme == "Theme01")
            {
                if (StaticVariables._iUnlockedLevels == StaticVariables.UnlockWorldAtLevelNo[0] && StaticVariables._iUnlockedWorldsCount == 0 && StaticVariables._iUnlockedAllLevelsIAP == 0)
                {
                    StaticVariables.UnlockedTheme = "Theme02";
                    StaticVariables._iUnlockedWorldsCount = 1;
                    StaticVariables._iUnlockedLevels = 16;

                    Tween_UnlockNewWorld._instance.Invoke("Call_TweenIn", 1);
                    _worldUnlock.sprite = world02Sprite;
                }
            }
            else if (StaticVariables.UnlockedTheme == "Theme02")
            {

                if (StaticVariables._iUnlockedLevels == StaticVariables.UnlockWorldAtLevelNo[1] && StaticVariables._iUnlockedWorldsCount == 1 && StaticVariables._iUnlockedAllLevelsIAP == 0)
                {
                    Debug.Log("****************THEME 02**************");
                    StaticVariables.UnlockedTheme = "Theme03";
                    StaticVariables._iUnlockedWorldsCount = 2;
                    StaticVariables._iUnlockedLevels = 31;
                    Tween_UnlockNewWorld._instance.Invoke("Call_TweenIn", 1);
                    _worldUnlock.sprite = world03Sprite;
                }
            }
            else if (StaticVariables.UnlockedTheme == "Theme03")
            {
                if (StaticVariables._iUnlockedLevels == StaticVariables.UnlockWorldAtLevelNo[2] && StaticVariables._iUnlockedWorldsCount == 2 && StaticVariables._iUnlockedAllLevelsIAP == 0)
                {
                    StaticVariables.UnlockedTheme = "Theme04";
                    StaticVariables._iUnlockedWorldsCount = 3;
                    StaticVariables._iUnlockedLevels = 46;
                    Tween_UnlockNewWorld._instance.Invoke("Call_TweenIn", 1);
                    _worldUnlock.sprite = world04Sprite;
                }
            }
            else if (StaticVariables.UnlockedTheme == "Theme04")
            {
                if (StaticVariables._iUnlockedLevels == StaticVariables.UnlockWorldAtLevelNo[3] && StaticVariables._iUnlockedWorldsCount == 3 && StaticVariables._iUnlockedAllLevelsIAP == 0)
                {
                    StaticVariables.UnlockedTheme = "Theme05";
                    StaticVariables._iUnlockedWorldsCount = 4;
                    StaticVariables._iUnlockedLevels = 61;
                    Tween_UnlockNewWorld._instance.Invoke("Call_TweenIn", 1);
                    _worldUnlock.sprite = world05Sprite;
                }
            }
        }
        Debug.Log("UNLOCKED WORLD ************ " + StaticVariables.UnlockedTheme + "UNLOCKED LEVEL ************ " + StaticVariables._iUnlockedLevels);

        #endregion

#if !PlayServices_Off

        if (StaticVariables._iUnlockedLevels >= 30)
        {
            PlayServicesHandler.instance.OncallAchievement(GPGSIds.achievement_crazy_drive);
        }
        else if (StaticVariables._iUnlockedLevels >= 20)
        {
            PlayServicesHandler.instance.OncallAchievement(GPGSIds.achievement_garage_star);
        }
        else if (StaticVariables._iUnlockedLevels >= 15)
        {
            PlayServicesHandler.instance.OncallAchievement(GPGSIds.achievement_top_class_drive);
        }
        else if (StaticVariables._iUnlockedLevels >= 10)
        {
            PlayServicesHandler.instance.OncallAchievement(GPGSIds.achievement_get_stuff);
        }
        else if (StaticVariables._iUnlockedLevels >= 5)
        {
            PlayServicesHandler.instance.OncallAchievement(GPGSIds.achievement_freak_drive);
        }
#endif
        Invoke("LevelSucessDetails", 0.5f);


        Debug.Log("LEVEL COMPLETE HERE");
        //if (AdManager.Instance != null)
        //{
        //          Debug.LogWarning(StaticVariables.rewardCalled);
        //          if (StaticVariables.rewardCalled == false)
        //          {
        //              Debug.LogWarning("WRONGGGGGGGGGGGGGGGGGGGGGG ");
        //              AdManager.Instance.IncreaseCounter();
        //          }
        //          else
        //          {
        //              Debug.LogWarning("RIGHTTTTTTTTTTTTTTTTTTTTTTT");
        //              StaticVariables.rewardCalled = false;
        //          }

        //}


        #region UNLOCK_NEXT_CAR / RATE US
        if (AdManager.SubscribeUnlocked != 1)
        {
            if (StaticVariables._iSelectedLevel == StaticVariables.UnlockCarAtLevelNo[0] && StaticVariables.splitUnlockCarBool[0] == "0" && StaticVariables._iUnlockedAllCarsIAP == 0)
            {

                char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
                if (tempstring[2] != '1')
                {
                    Tween_UnlockSpclCar._instance.Invoke("Call_TweenIn", 1);
                    StaticVariables._iSelectedVechicle = 2;
                    _carUnlock.sprite = Car02Sprite;
                    tempstring[2] = '1';
                    Debug.Log("11111111111111111111111111111 + UNLOCKED CARS  :::::: " + PlayerPrefs.GetString(MyGamePrefs.UnlockedCars));
                    PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, new string(tempstring));
                    Debug.Log("22222222222222222222222222222 + UNLOCKED CARS  :::::: " + PlayerPrefs.GetString(MyGamePrefs.UnlockedCars));

                    StaticVariables.splitUnlockCarBool = StaticVariables._iUnlockCarBools.Split(',');
                    StaticVariables.splitUnlockCarBool[0] = "1";
                    string str = string.Join(",", StaticVariables.splitUnlockCarBool);
                    StaticVariables._iUnlockCarBools = str;
                }

            }
            else if (StaticVariables._iSelectedLevel == StaticVariables.UnlockCarAtLevelNo[1] && StaticVariables.splitUnlockCarBool[1] == "0" && StaticVariables._iUnlockedAllCarsIAP == 0)
            {

                char[] tempstring = PlayerPrefs.GetString(MyGamePrefs.UnlockedCars).ToCharArray();
                if (tempstring[4] != '1')
                {
                    Tween_UnlockSpclCar._instance.Invoke("Call_TweenIn", 1);
                    StaticVariables._iSelectedVechicle = 4;
                    _carUnlock.sprite = Car04Sprite;
                    tempstring[4] = '1';
                    Debug.Log("11111111111111111111111111111 + UNLOCKED CARS  :::::: " + PlayerPrefs.GetString(MyGamePrefs.UnlockedCars));
                    PlayerPrefs.SetString(MyGamePrefs.UnlockedCars, new string(tempstring));
                    Debug.Log("22222222222222222222222222222 + UNLOCKED CARS  :::::: " + PlayerPrefs.GetString(MyGamePrefs.UnlockedCars));

                    StaticVariables.splitUnlockCarBool = StaticVariables._iUnlockCarBools.Split(',');
                    StaticVariables.splitUnlockCarBool[1] = "1";
                    string str = string.Join(",", StaticVariables.splitUnlockCarBool);
                    StaticVariables._iUnlockCarBools = str;

                }

            }
        }
        //        else if (LevelCompleteCount >= 5)
        //        {
        //            LevelCompleteCount = 0;
        //            Tween_Rate._instance.Call_TweenIn();
        //        }
        //        else
        //        {
        //#if !AdSetup_OFF
        //            if (StaticVariables.showGameEndBanner && !RateUsManager._instance.DisplayRateUsPannel())
        //                AdManager.Instance.RequestBannerInGame();
        //#endif
        //}
#if !AdSetup_OFF
        Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventLevelEnd, new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterLevel, "Level_" + StaticVariables._iSelectedLevel));
#endif

        #endregion
    }

    public static int LevelFailCount;
    public void LF()
    {
        LevelFailCount++;

        if (GameManager.Instance.CurrentGameState == GameManager.GameState.Revive || GameManager.Instance.CurrentGameState == GameManager.GameState.GamePlay || GameManager.Instance.CurrentGameState == GameManager.GameState.LevelFaild)
        {
            //Debug.Log ("we are In Level Faild");
            GameManager.Instance.CurrentGameState = GameManager.GameState.LevelFaild;
            LFEnable();
            Tween_LF._instance.Call_TweenIn();
            Invoke("LevelFaildDetails", 0.5f);
            //TxtLevelScore.text = StaticVariables.EachLevelScore [StaticVariables._iSelectedLevel]+"";

            //            if (LevelFailCount >= 3)
            //            {
            //                Tween_UnlockAllLevel._instance.Call_TweenIn();
            //                LevelFailCount = 0;
            //            }
            //            else
            //            {
            //#if !AdSetup_OFF
            //                if (StaticVariables.showGameEndBanner && !RateUsManager._instance.DisplayRateUsPannel())
            //                    AdManager.Instance.RequestBannerInGame();
            //#endif
            //}
        }

        Debug.Log("LEVEL FAIL HERE");
    }

    public void InGameUIAction(string name)
    {
        switch (name)
        {
            case "Retry":
                TimeScaleOne();
                if (AdController.Instance != null)
                {
                    AdController.Instance.HideIronSourceBannerAd();
                }
                LoadingCanvas.SetActive(true);
                Debug.Log(SceneManager.GetActiveScene().name);
                LoadingManager.SceneName = SceneManager.GetActiveScene().name;
                //LoadingManager.SceneName = "Scn_Levels_New";
                SceneManager.LoadScene("LoadingScene");
                Debug.Log("RETRY HERE");
                break;

            case "NextLevel":
                TimeScaleOne();
                if (AdController.Instance != null)
                {
                    AdController.Instance.HideIronSourceBannerAd();
                }
                LoadingCanvas.SetActive(true);
                Debug.Log("CURERNT LEVEL IS ::::::::: " + StaticVariables._iSelectedLevel);

                if (StaticVariables._iSelectedLevel % 5 == 0)
                {
                    goto case "Garage";
                }
                else
                {
                    StaticVariables._iSelectedLevel++;
                    Debug.Log("NEXT LEVEL IS ::::::::: " + StaticVariables._iSelectedLevel);
                    Debug.Log(SceneManager.GetActiveScene().name);
                    LoadingManager.SceneName = SceneManager.GetActiveScene().name;
                    //LoadingManager.SceneName = "Scn_Levels_New";
                    SceneManager.LoadScene("LoadingScene");
                    Debug.Log("NEXT LEVEL HERE");
                }

                break;

            case "Garage":
                TimeScaleOne();
                if (AdController.Instance != null)
                {
                    AdController.Instance.HideIronSourceBannerAd();
                }
                LoadingCanvas.SetActive(true);
                StaticVariables._iHomeOrGarage = "Garage";
                // LoadingManager.SceneName = "Scn_Menu";
                LoadingManager.SceneName = "Scn_Menu_Updated";
                SceneManager.LoadScene("LoadingScene");
                Debug.Log("UPGRADE HERE");
                break;

            case "Home":
                TimeScaleOne();
                if (AdController.Instance != null)
                {
                    AdController.Instance.HideIronSourceBannerAd();
                }
                LoadingCanvas.SetActive(true);

#if !AdSetup_OFF
                if (AdManager.Instance != null)
                {
                    //AdManager.Instance.IncreaseLoseCounter();
                    AdManager.Instance.RunActions(AdManager.Actiontype.Pause_Home);

                }
#endif

                Debug.Log("HOME HERE");
                StaticVariables._iHomeOrGarage = "Home";
                //LoadingManager.SceneName = "Scn_Menu";
                LoadingManager.SceneName = "Scn_Menu_Updated";
                SceneManager.LoadScene("LoadingScene");
                break;
            case "Pause":
                GameManager.Instance.CurrentGameState = GameManager.GameState.Pause;
                PauseEnable();
                Debug.Log("PAUSE HERE");
                TimeScaleZero();

                AudioManager.instance.GameSounds.Stop();
                //AdManager.Instance.ShowInterstitial();
                break;
            case "Resume":
                GameManager.Instance.CurrentGameState = GameManager.GameState.GamePlay;
                TimeScaleOne();
                ResumeEnable();


                AudioManager.instance.GameSounds.Play();

                Debug.Log("RESUME HERE");

                break;

        }
#if !AdSetup_OFF
        if (FirebaseController.Instance != null)
            FirebaseController.Instance.LogFirebaseEvent("Funnel_" + name, "Scene" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex, name.ToString());
#endif
    }

    public void PauseEnable()
    {
        GameManager.Instance.IsInPlayMode = false;
        RccCanvas.gameObject.SetActive(false);
        LevelComplete.SetActive(false);
        LevelFaild.SetActive(false);
        Pause.SetActive(true);

    }
    public void ResumeEnable()
    {
        GameManager.Instance.IsInPlayMode = true;

        Pause.SetActive(false);
        LevelComplete.SetActive(false);
        LevelFaild.SetActive(false);
        RccCanvas.gameObject.SetActive(true);

    }
    public void LCEnable()
    {

        StartCoroutine(LCDelay());
    }

    IEnumerator LCDelay()
    {
        InGameAudioManager.instance.CarSounds();
        yield return new WaitForSeconds(2f);
        AudioManager.instance.LevelFInishedSucess();

        OurCanvas.gameObject.SetActive(true);
        RccCanvas.gameObject.SetActive(false);
        LevelFaild.gameObject.SetActive(false);
        Pause.gameObject.SetActive(false);
        GameManager.Instance.IsInPlayMode = false;
        //LevelComplete.gameObject.SetActive (true);
    }

    public void LFEnable()
    {


        GameManager.Instance.IsInPlayMode = false;
        OurCanvas.gameObject.SetActive(true);
        RccCanvas.gameObject.SetActive(false);
        LevelComplete.gameObject.SetActive(false);
        Revive.SetActive(false);
        Pause.gameObject.SetActive(false);
        LevelFaild.gameObject.SetActive(true);
        AudioManager.instance.LevelFailed();

#if !AdSetup_OFF
        if (AdManager.Instance != null)
        {
            //AdManager.Instance.IncreaseLoseCounter();
            AdManager.Instance.RunActions(AdManager.Actiontype.Lvl_Fail, StaticVariables._iSelectedLevel);
            AdManager.Instance.RunActions(AdManager.PageType.LFPage, StaticVariables._iSelectedLevel);

        }
        Firebase.Analytics.FirebaseAnalytics.LogEvent("Level_failed", new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterLevel, "Level_" + StaticVariables._iSelectedLevel));
#endif
    }
    public void ReviveEnable()
    {
        GameManager.Instance.IsInPlayMode = false;
        reviveTimeLeft = 5;
        OurCanvas.gameObject.SetActive(true);
        RccCanvas.SetActive(false);
        LevelComplete.gameObject.SetActive(false);
        LevelFaild.SetActive(false);
        Pause.SetActive(false);
        Revive.SetActive(true);
        StartReviveCoroutine();
    }

    public Coroutine timerCoroutine;
    public IEnumerator ReviveTimer()
    {
        while (reviveTimeLeft >= 0)
        {
            revivePanelTimerText.text = reviveTimeLeft.ToString();
            yield return new WaitForSeconds(1);
            reviveTimeLeft--;
            yield return null;
        }
        ReviveClose();
        yield return null;
    }

    public void StopReviveCoroutine()
    {
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
        }
    }

    public void StartReviveCoroutine()
    {
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
        }
        timerCoroutine = StartCoroutine(ReviveTimer());
    }

    public void ReviveClose()
    {
        StopReviveCoroutine();
        LF();
    }

    public void TimeScaleZero()
    {
        Time.timeScale = 0;
    }
    public void TimeScaleOne()
    {
        Time.timeScale = 1;
    }
    public void InGameEnable()
    {
        Debug.LogError("---- InGameEnable");
        GameManager.Instance.IsInPlayMode = true;
        OurCanvas.gameObject.SetActive(true);
        //RccCanvas.gameObject.SetActive(true);
        LevelComplete.SetActive(false);
        LevelFaild.SetActive(false);
        Pause.SetActive(false);
        Revive.SetActive(false);
        if(ExitPageHandler.Instance!=null && ExitPageHandler.Instance.IsExitPageOpened)
        {
            RccCanvas.gameObject.SetActive(false);
        }
        else
        {
            RccCanvas.gameObject.SetActive(true);
        }
    }
    public void InGameDisable()
    {
        GameManager.Instance.IsInPlayMode = false;
        OurCanvas.gameObject.SetActive(false);
        RccCanvas.gameObject.SetActive(false);
        LevelComplete.SetActive(false);
        LevelFaild.SetActive(false);
        Pause.SetActive(false);
        Revive.SetActive(false);
    }
    int DefaultValue = 0;
    public void ChangeControllsDown()
    {
        switch (DefaultValue)
        {
            case 0:
                RCC_MobileButtons.Instance.ChangeController(0);
                break;
            case 2:
                RCC_MobileButtons.Instance.ChangeController(2);
                break;
        }
    }
    public void ChangeControllsUp()
    {
        if (DefaultValue == 0)
        {
            DefaultValue = 2;
        }
        else
        {
            DefaultValue = 0;
        }
    }

    public void UpdateText()
    {
        for (int i = 0; i < TotalScoreTxts.Count; i++)
        {
            TotalScoreTxts[i].text = StaticVariables._iTotalCoins + "";
        }
    }

    public void UpdateScore(int Score)
    {
        StaticVariables._iTotalCoins = StaticVariables._iTotalCoins + Score;
    }

    public void LevelSucessDetails()
    {
        Debug.Log(StaticVariables._iTotalCoins + "total coins");
        iTween.ValueTo(TxtTotalScoreLC.gameObject, iTween.Hash("from", 0, "to", ((CoinsManger.manager.collectedCoins + StaticVariables.EachLevelScore[StaticVariables._iSelectedLevel - 1] + currentRewardCoins)) * Current2xValue, "onupdate", "UpdateLevelSuccessScore", "time", 0.5f, "delay", 0.15f, "easetype", iTween.EaseType.linear, "onupdatetarget", this.gameObject));
        iTween.ValueTo(collectedCoins.gameObject, iTween.Hash("from", 0, "to", (CoinsManger.manager.collectedCoins), "onupdate", "UpdatecollectedCoins", "time", 0.5f, "delay", 0.15f, "easetype", iTween.EaseType.linear, "onupdatetarget", this.gameObject));
        iTween.ValueTo(eachLevelBonus.gameObject, iTween.Hash("from", 0, "to", (StaticVariables.EachLevelScore[StaticVariables._iSelectedLevel - 1]), "onupdate", "UpdateeachLevelBonus", "time", 0.5f, "delay", 0.15f, "easetype", iTween.EaseType.linear, "onupdatetarget", this.gameObject));

        //if (Btn_LCNext.GetComponent<Animator>())
        //    Btn_LCNext.GetComponent<Animator>().enabled = true;
        // RateUsManager._instance.ShowRateUs();
    }

    public void UpdateLevelSuccessScore(int _value)
    {
        TxtTotalScoreLC.text = "TOTAL REWARDS : " + (_value * LC2Xcoins);
    }
    public void UpdatecollectedCoins(int _value)
    {
        collectedCoins.text = "" + (_value);
    }
    public void UpdateeachLevelBonus(int _value)
    {
        eachLevelBonus.text = "" + (_value);
    }

    public void LevelFaildDetails()
    {
        //iTween.ValueTo (TxtTotalScoreLF.gameObject, iTween.Hash ("from", 0,"to",StaticVariables._iTotalCoins ,"onupdate","UpdateLevelFaildScore", "time", 0.5f, "delay", 0.15f, "easetype", iTween.EaseType.linear,"onupdatetarget",this.gameObject));	
        // Btn_LFRetry.GetComponent<Animator>().enabled = true;
    }
    public void UpdateLevelFaildScore(int _value)
    {
        TxtTotalScoreLF.text = "" + _value;
        Debug.Log("iam" + _value);

    }

    public void Help_Continue()
    {
        StaticVariables._iHelp = 1;
        Help.SetActive(false);
    }


    //	public int DoubleReward=0;
    public void LevelDoubleReward()
    {
        if (DoubleReward == 0)
        {
            DoubleReward = 1;
            TxtTotalScoreLC.text = (((StaticVariables.EachLevelScore[StaticVariables._iSelectedLevel - 1] + CoinsManger.manager.collectedCoins) * 2f)).ToString();
            //BTnDoubleReward.gameObject.SetActive (false);
            BTnDoubleReward.transform.parent.gameObject.SetActive(false);
        }

    }

    public void Show_SavepointHelp()
    {
        if (StaticVariables._isavepointHelp == 0)
        {
            SavepointHelp.gameObject.SetActive(true);
            StaticVariables._isavepointHelp = 1;
            Time.timeScale = 0;
        }
        else
        {

        }
    }

    public void Success_2XLCCoins()
    {
        VideoReward_2x = true;
        LC2Xcoins = 2;
        Tween_2XLC._instance.Call_TweenOut();
        int LC2Coins = StaticVariables.EachLevelScore[StaticVariables._iSelectedLevel - 1];
        StaticVariables._iTotalCoins = StaticVariables._iTotalCoins + StaticVariables.EachLevelScore[StaticVariables._iSelectedLevel - 1];
        LCPage();
    }

    public void Hide_SavepointHelp()
    {
        SavepointHelp.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    void OnDestroy()
    {

        foreach (FieldInfo field in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            Type fieldType = field.FieldType;

            if (typeof(IList).IsAssignableFrom(fieldType))
            {
                IList list = field.GetValue(this) as IList;
                if (list != null)
                {
                    list.Clear();
                }
            }

            if (typeof(IDictionary).IsAssignableFrom(fieldType))
            {
                IDictionary dictionary = field.GetValue(this) as IDictionary;
                if (dictionary != null)
                {
                    dictionary.Clear();
                }
            }

            if (!fieldType.IsPrimitive)
            {
                field.SetValue(this, null);
            }
        }
    }

    public void ResetCar()
    {
        if (GameManager.Instance._IsGrounded)
        {
            GameManager.Instance.CurrentVehile.transform.position = GameManager.Instance.CurrentLevel.LastSavePointPos.position;
            GameManager.Instance.CurrentVehile.transform.rotation = GameManager.Instance.CurrentLevel.LastSavePointPos.rotation;
            GameManager.Instance.CurrentVehile.transform.SetParent(null);
            Rigidbody rb = GameManager.Instance.CurrentVehile.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            GameManager.Instance.obstaclesRestart.Restart();
        }

    }
    public void UnlockEverything_Clicked()
    {
        FirebaseController.Instance.LogFirebaseEvent("LC_UnlockEverything", "", "");
    }

    public void UnlockWorlds_Clicked()
    {
        FirebaseController.Instance.LogFirebaseEvent("LF_UnlockWorlds", "", "");
    }

}

