﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class PageAnimHandler : MonoBehaviour
{
	// HARISH //
	public TweenHandler[] Tween_Objs;

	bool mb_FstTime;

	void OnEnable ()
	{
		if (mb_FstTime == false) 
		{
			mb_FstTime = true;
			return;
		}
//		Tween_Objs = this.gameObject.GetComponentsInChildren<TweenHandler> ();
	}

	void Awake ()
	{
		Tween_Objs = this.gameObject.GetComponentsInChildren<TweenHandler> ();
	}

	public void Call_TweenIn ()
	{
		for (int i = 0; i < Tween_Objs.Length; i++) 
		{
			if (Tween_Objs [i] != null)
			{
				Tween_Objs [i].Call_Anim ();
			}
				
		}
	}

	public void Call_TweenOut ()
	{
		for (int i = 0; i < Tween_Objs.Length; i++) 
		{
			if (Tween_Objs [i] != null)
				Tween_Objs [i].Reverse_Anim ();
		}
	}
}
