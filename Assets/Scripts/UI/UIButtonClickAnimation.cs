﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIButtonClickAnimation : MonoBehaviour,IPointerUpHandler,IPointerDownHandler 
{
//	public float mf_EffectVal;
	void Start () 
	{

	}

	public void OnPointerUp (PointerEventData eventData)
	{

	}
	public void OnPointerDown (PointerEventData eventData)
	{
		iTween.ScaleTo(this.gameObject,iTween.Hash("scale",new Vector3(0.9f,0.9f,0.9f),"time",0.1f,"delay",0f,"easetype",iTween.EaseType.linear));
		iTween.ScaleTo(this.gameObject,iTween.Hash("scale",Vector3.one,"time",0.25f,"delay",0.1f,"easetype",iTween.EaseType.easeOutBounce));
//		iTween.RotateTo(this.gameObject,iTween.Hash("z",180f,"time",0.15)); 
	}
}
