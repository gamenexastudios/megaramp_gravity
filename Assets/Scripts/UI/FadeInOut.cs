﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour 
{
	public static FadeInOut _instance;
	public GameObject FadeImage;

	void Awake()
	{
		_instance = this;
	}

	public IEnumerator Call_Fade(float _delay)
	{
		yield return new WaitForSeconds (_delay);
		iTween.ValueTo (FadeImage.gameObject, iTween.Hash ("from", FadeImage.gameObject.GetComponent<Image> ().color.a, "to", 1, "Time", 0.5f,
			"easetype", iTween.EaseType.linear,"OnUpdate", "SpriteFade","OnComplete","OnFadeComplete","OnCompleteTarget",this.gameObject,"onupdatetarget", this.gameObject));//
	}

	void SpriteFade(float _val)
	{
//		Debug.Log ("---- Fade IN");
		FadeImage.gameObject.GetComponent<Image> ().color = new Color (FadeImage.gameObject.GetComponent<Image> ().color.r, FadeImage.gameObject.GetComponent<Image> ().color.g, FadeImage.gameObject.GetComponent<Image> ().color.b, _val);	
	}

	void OnFadeComplete()
	{
//		Debug.Log ("---- Fade Out");
		iTween.ValueTo (FadeImage.gameObject, iTween.Hash ("from", FadeImage.gameObject.GetComponent<Image> ().color.a, "to", 0, "Time", 0.5f, "delay",0.15f,
			"easetype", iTween.EaseType.linear,"OnUpdate", "SpriteFade","onupdatetarget", this.gameObject));
	}
}
