﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonClickEffect : MonoBehaviour,IPointerUpHandler,IPointerDownHandler {

	Image _img;

	// Use this for initialization
	GameObject _obj;
	void Start () 
	{
//		_img	= GetComponent<Image>();
		_obj = this.gameObject;//.parent.gameObject;
	}

	public void OnPointerDown (PointerEventData eventData)
	{

//		Debug.Log("---- Click");
//		iTween.PunchScale (gameObject, iTween.Hash ("y", 0.5f, "time", 0.2f));
	iTween.ScaleTo(_obj.gameObject,iTween.Hash("scale",new Vector3(0.9f,0.9f,0.9f),"time",0.1f,"delay",0f,"easetype",iTween.EaseType.linear));
  iTween.ScaleTo(_obj.gameObject,iTween.Hash("scale",Vector3.one,"time",0.25f,"delay",0.1f,"easetype",iTween.EaseType.easeOutBounce));
	
	}
	public void OnPointerUp (PointerEventData eventData)
	{
//		transform.localScale	= Vector3.one;
	
	}
}
