﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TweenHandler:MonoBehaviour
{
	// HARISH //
	public enum e_EffectType
	{
		Slide,Scale,AlphaFade,ImageFill,Scale_X,Scale_Y
	}
	[Header("EffectType :")]
	public e_EffectType m_EffectType;
	public bool mb_WithAlphaFade,mb_isBG;
	public bool mb_NeedRotation;
    public bool IsNeed_ScaleRevValue;
    public Vector2 ScaleRevValue;

    public enum e_SlideFrom
	{
		None,Left,Right,Up,Down
	}
	[Header("Slide From :")]
	public e_SlideFrom m_SlideFrom;

	[Header("Effect Details :")]
	public iTween.EaseType effectType;
	public iTween.LoopType loopType;


	public float mf_AlphaVal,mf_Time, mf_Delay, mf_RevDelay,mf_XVal,mf_YVal,mf_FillVal;

	public bool mb_IsText;
	public Color mc_Originalcolor = Color.white;


	public RectTransform m_rect;
	public Vector2 mv_PosVec;

	void OnEnable ()
	{
		m_rect=this.GetComponent<RectTransform>();
		Set_InitialPos ();
	}
	void Start()
	{
//		m_rect=this.GetComponent<RectTransform>();
//		Set_InitialPos ();
//		Debug.Log ("----" + m_rec);
	}
	void Set_InitialPos()
	{
		if(m_EffectType==e_EffectType.Slide)
		{
			mv_PosVec = m_rect.anchoredPosition;

			if(m_SlideFrom==e_SlideFrom.Left)
			{
				m_rect.anchoredPosition = new Vector2 (-5000, m_rect.anchoredPosition.y);
    		}
			else if(m_SlideFrom==e_SlideFrom.Right)
			{
				m_rect.anchoredPosition = new Vector2 (1500, m_rect.anchoredPosition.y);
			}
			else if(m_SlideFrom==e_SlideFrom.Up)
			{
				m_rect.anchoredPosition = new Vector2 (m_rect.anchoredPosition.x,1500);
			}
			else if(m_SlideFrom==e_SlideFrom.Down)
			{
				m_rect.anchoredPosition = new Vector2 (m_rect.anchoredPosition.x,-1500);
			}
		}

		else if(m_EffectType==e_EffectType.Scale)
		{
//            if(IsNeed_ScaleRevValue)
//                this.transform.localScale = new Vector3(ScaleRevValue.x,ScaleRevValue.y,0);
//            else
			this.transform.localScale=Vector3.zero;
//			Debug.Log("----- X");
//			this.transform.localScale= new Vector3(1,0,0);
		}

		else if(m_EffectType==e_EffectType.AlphaFade)
		{
			if (mb_IsText)
			{
				mc_Originalcolor	= gameObject.GetComponent<Text> ().color;
				this.GetComponent<Text> ().color	= new Color (mc_Originalcolor.r, mc_Originalcolor.g, mc_Originalcolor.b, 0);
			}
			else
			{
				mc_Originalcolor	= gameObject.GetComponent<Image> ().color;
				this.GetComponent<Image>().color = new Color(mc_Originalcolor.r,mc_Originalcolor.g,mc_Originalcolor.b,0);
			}
		}
		else if (m_EffectType==e_EffectType.ImageFill) 
		{
			this.gameObject.GetComponent<Image> ().fillAmount = 0;
		}
		else if(m_EffectType==e_EffectType.Scale_X)
		{
			this.transform.localScale=new Vector3(0,1,1);
		}
		else if(m_EffectType==e_EffectType.Scale_Y)
		{
			this.transform.localScale=new Vector3(1,0,1);
		}
		if(mb_WithAlphaFade)
		{
			mc_Originalcolor	= gameObject.GetComponent<Image> ().color;
			this.GetComponent<Image>().color = new Color(mc_Originalcolor.r,mc_Originalcolor.g,mc_Originalcolor.b,0);
		}

		if(mb_isBG)
			this.GetComponent<Image> ().enabled = false;
//		else
//			this.GetComponent<Image> ().enabled = true;
	}

#region Call_Anim
	public void Call_Anim ()
	{
		if(m_EffectType==e_EffectType.Slide)
		{
			switch(m_SlideFrom)
			{
			case e_SlideFrom.Left:
			case e_SlideFrom.Right:
			case e_SlideFrom.Up:
			case e_SlideFrom.Down:
				iTween.ValueTo (this.gameObject, iTween.Hash ("from",m_rect.anchoredPosition,"to",mv_PosVec,"islocal",true, "looptype", loopType,
					"onupdate","MoveToObj", "Time", mf_Time, "easeType", effectType,"delay", mf_Delay,"onupdatetarget",this.gameObject));
					break;
			}
		}

		else if(m_EffectType==e_EffectType.Scale)
		{
			iTween.ScaleTo (this.gameObject, iTween.Hash ("x", 1f, "y", 1f, "time", mf_Time, "delay", mf_Delay, "looptype", loopType,"islocal",true, "easetype", effectType));
		}
		else if(m_EffectType==e_EffectType.Scale_X)
		{
			iTween.ScaleTo (this.gameObject, iTween.Hash ("x", 1f, "Time", mf_Time, "delay", mf_Delay, "looptype", loopType,"islocal",true, "easetype", effectType));
		}
		else if(m_EffectType==e_EffectType.Scale_Y)
		{
			iTween.ScaleTo (this.gameObject, iTween.Hash ("y", 1f, "Time", mf_Time, "delay", mf_Delay, "looptype", loopType,"islocal",true, "easetype", effectType));
		}
		else if(m_EffectType==e_EffectType.AlphaFade)
		{
			if (mb_IsText) 
			{
				mc_Originalcolor	= mc_Originalcolor;
				iTween.ValueTo (this.gameObject, iTween.Hash ("from", this.gameObject.GetComponent<Text> ().color.a, "to", mf_AlphaVal, "Time", (mf_Time - 0.1f), "delay", mf_Delay, "easetype", effectType, "onupdate", "TextFade", "oncomplete", "OnTextFadeComplete", "oncompletetarget", this.gameObject));
			}
			else 
			{
				iTween.ValueTo (this.gameObject, iTween.Hash ("from", this.gameObject.GetComponent<Image> ().color.a, "to", mf_AlphaVal, "Time", (mf_Time- 0.1f), "delay", mf_Delay, "easetype", effectType, "onupdate", "SpriteFade", "oncomplete", "OnSpriteFadeComplete", "oncompletetarget", this.gameObject));
			}
		}

		else if (m_EffectType==e_EffectType.ImageFill) 
		{
			iTween.ValueTo (this.gameObject, iTween.Hash ("from", 0, "to", mf_FillVal, "looptype", loopType, "Time", mf_Time, "easeType", effectType, "delay", mf_Delay, "onupdate", "ChangeVal"));
		}

		if(mb_WithAlphaFade)
		{
			iTween.ValueTo (this.gameObject, iTween.Hash ("from", this.gameObject.GetComponent<Image> ().color.a, "to", mf_AlphaVal, "Time", (mf_Time - 0.1f), "delay", mf_Delay, "easetype", effectType, "onupdate", "SpriteFade", "oncomplete", "OnSpriteFadeComplete", "oncompletetarget", this.gameObject));
		}
		if (mb_isBG)
			this.GetComponent<Image> ().enabled = true;
//		else
//			this.GetComponent<Image> ().enabled = true;
	}

	void MoveToObj(Vector2 pos)
	{
		m_rect.anchoredPosition = pos;
	}

#endregion

#region UpadeteFunctions
	void ChangeVal(float newVal)
	{
		this.gameObject.GetComponent<Image>().fillAmount = newVal;
	}

	public void TextFade (float value)
	{
		this.gameObject.GetComponent<Text> ().color = new Color (this.gameObject.GetComponent<Text> ().color.r, this.gameObject.GetComponent<Text> ().color.g, this.gameObject.GetComponent<Text> ().color.b, value);
	}

	public void OnTextFadeComplete ()
	{
		this.gameObject.GetComponent<Text> ().color = new Color (this.gameObject.GetComponent<Text> ().color.r, this.gameObject.GetComponent<Text> ().color.g, this.gameObject.GetComponent<Text> ().color.b, mf_AlphaVal);
	}

	public void SpriteFade (float value)
	{
		this.gameObject.GetComponent<Image> ().color = new Color (this.gameObject.GetComponent<Image> ().color.r, this.gameObject.GetComponent<Image> ().color.g, this.gameObject.GetComponent<Image> ().color.b, value);
	}

	public void OnSpriteFadeComplete ()
	{
		this.gameObject.GetComponent<Image> ().color = new Color (this.gameObject.GetComponent<Image> ().color.r, this.gameObject.GetComponent<Image> ().color.g, this.gameObject.GetComponent<Image> ().color.b, mf_AlphaVal);
	}

	public void OnSpriteFadeReverseComplete ()
	{
		this.gameObject.GetComponent<Image> ().color = new Color (this.gameObject.GetComponent<Image> ().color.r, this.gameObject.GetComponent<Image> ().color.g, this.gameObject.GetComponent<Image> ().color.b, 0);
	}

	public void OnTextFadeReverseComplete ()
	{
		this.gameObject.GetComponent<Text> ().color = new Color (this.gameObject.GetComponent<Text> ().color.r, this.gameObject.GetComponent<Text> ().color.g, this.gameObject.GetComponent<Text> ().color.b, 0);
	}
#endregion

#region Reverse_Anim
	public	void Reverse_Anim ()
	{

		if(m_EffectType==e_EffectType.Slide)
		{
			switch(m_SlideFrom)
			{
			case e_SlideFrom.Left:
				iTween.ValueTo (this.gameObject, iTween.Hash ("from",m_rect.anchoredPosition,"to",new Vector2(-5000,m_rect.anchoredPosition.y),"islocal",true, "looptype", loopType,
					"onupdate","MoveToObj", "Time", mf_Time, "easeType", effectType,"delay", 0,"onupdatetarget",this.gameObject));
				break;
			case e_SlideFrom.Right:
				iTween.ValueTo (this.gameObject, iTween.Hash ("from",m_rect.anchoredPosition,"to",new Vector2(1500,m_rect.anchoredPosition.y),"islocal",true, "looptype", loopType,
					"onupdate","MoveToObj", "Time", mf_Time, "easeType", effectType,"delay", 0,"onupdatetarget",this.gameObject));
				break;

			case e_SlideFrom.Up:
				iTween.ValueTo (this.gameObject, iTween.Hash ("from",m_rect.anchoredPosition,"to",new Vector2(m_rect.anchoredPosition.x,1500),"islocal",true, "looptype", loopType,
					"onupdate","MoveToObj", "Time", mf_Time, "easeType", effectType,"delay", 0,"onupdatetarget",this.gameObject));
				break;
			case e_SlideFrom.Down:
				iTween.ValueTo (this.gameObject, iTween.Hash ("from",m_rect.anchoredPosition,"to",new Vector2(m_rect.anchoredPosition.x,-1500),"islocal",true, "looptype", loopType,
					"onupdate","MoveToObj", "Time", mf_Time, "easeType", effectType,"delay", 0,"onupdatetarget",this.gameObject));
				break;
			}
		}

		else if(m_EffectType==e_EffectType.Scale)
		{
			Debug.Log ("---- Out SCALE");
			iTween.ScaleTo (this.gameObject, iTween.Hash ("x", 0, "y", 0, "Time",0.1f, "delay", 0, "looptype", loopType,"islocal",true, "easetype", iTween.EaseType.linear));
//			iTween.ScaleTo (this.gameObject, iTween.Hash ("y", 0,"Time",0.05f, "delay", 0, "looptype", loopType,"islocal",true, "easetype", iTween.EaseType.linear));
		}
		else if(m_EffectType==e_EffectType.Scale_X)
		{
			iTween.ScaleTo (this.gameObject, iTween.Hash ("x", 0f, "Time", 0, "delay", 0, "looptype", loopType,"islocal",true, "easetype", iTween.EaseType.linear));
		}
		else if(m_EffectType==e_EffectType.Scale_Y)
		{
			iTween.ScaleTo (this.gameObject, iTween.Hash ("y", 0f, "Time", 0, "delay", 0, "looptype", loopType,"islocal",true, "easetype", iTween.EaseType.linear));
		}
		else if(m_EffectType==e_EffectType.AlphaFade)
		{
			if (mb_IsText) 
			{
				iTween.ValueTo (this.gameObject, iTween.Hash ("from", this.gameObject.GetComponent<Text> ().color.a, "to", 0, "Time", mf_Time, "delay", mf_RevDelay, "easetype", effectType, "onupdate", "TextFade", "oncomplete", "OnTextFadeReverseComplete", "oncompletetarget", gameObject));
			} 
			else
				iTween.ValueTo (this.gameObject, iTween.Hash ("from", this.gameObject.GetComponent<Image> ().color.a, "to", mc_Originalcolor.a, "Time", mf_Time, "delay", mf_RevDelay, "easetype", effectType, "onupdate", "SpriteFade", "oncomplete", "OnSpriteFadeReverseComplete", "oncompletetarget", gameObject));
		}

		else if (m_EffectType==e_EffectType.ImageFill) 
		{
			iTween.ValueTo (this.gameObject, iTween.Hash ("from", mf_FillVal, "to", 0, "looptype", loopType, "Time", mf_Time, "easeType", effectType, "delay", 0, "onupdate", "ChangeVal"));
		}

		if(mb_WithAlphaFade)
		{
			iTween.ValueTo (this.gameObject, iTween.Hash ("from", this.gameObject.GetComponent<Image> ().color.a, "to", mc_Originalcolor.a, "Time", mf_Time, "delay", 0, "easetype", effectType, "onupdate", "SpriteFade", "oncomplete", "OnSpriteFadeReverseComplete", "oncompletetarget", gameObject));
		}
		if (mb_isBG)
			this.GetComponent<Image> ().enabled = false;
//		else
//			this.GetComponent<Image> ().enabled = true;
	}	
#endregion
}
