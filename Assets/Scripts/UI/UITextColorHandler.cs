﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class UITextColorHandler : MonoBehaviour {

	public List<GameObject> _ListOfText;
	public static UITextColorHandler _this;
	public void Awake()
	{
		_this = this;
	}

	public void UIText(string UITextName)
	{

	
		for(int i=0;i<_ListOfText.Count;i++)
		{
			if (UITextName!="HomeOffTxt")
			{


				if (_ListOfText [i].gameObject.name == UITextName)
				{
					//Debug.Log ("here text in if name" + _ListOfText [i].gameObject.name);
					_ListOfText [i].GetComponent<Text> ().color = new Color (_ListOfText [i].GetComponent<Text> ().color.r, _ListOfText [i].GetComponent<Text> ().color.g, _ListOfText [i].GetComponent<Text> ().color.b, 0.2f);
				}
				else 
				{
		
					_ListOfText [i].GetComponent<Text> ().color = new Color (_ListOfText [i].GetComponent<Text> ().color.r, _ListOfText [i].GetComponent<Text> ().color.g, _ListOfText [i].GetComponent<Text> ().color.b, 1f);
			
				}
			}

		 else

			{

				_ListOfText [i].GetComponent<Text> ().color = new Color (_ListOfText [i].GetComponent<Text> ().color.r, _ListOfText [i].GetComponent<Text> ().color.g, _ListOfText [i].GetComponent<Text> ().color.b, 0.2f);
			}
		}
	
		
}

}
