﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterWingsRotation : MonoBehaviour {

	// Use this for initialization
	public GameObject HelicopterWings;
	public float WingsSpeed;
	public  GameObject BackSideWings;
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		HelicopterWings.transform.Rotate (Vector3.up * WingsSpeed * Time.deltaTime);
		BackSideWings.transform.Rotate (Vector3.left * WingsSpeed * Time.deltaTime);
	}
}
