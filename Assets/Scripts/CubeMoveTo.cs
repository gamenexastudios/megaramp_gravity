﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMoveTo : MonoBehaviour
{
    public Vector3 TargetPos;
    public iTween.EaseType AnimationType;
    public void Move()
    {
        this.gameObject.SetActive(true);
        iTween.MoveTo(this.gameObject, iTween.Hash("X",TargetPos.x,"Y", TargetPos.y,"Z",TargetPos.z,"islocal",true, "time", 0.3f, "delay", 0, "easetype", AnimationType));
    }
}
