﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundTankRotator : MonoBehaviour {

	public GameObject[] NoOfRotateObj;
	public float SpeedOfObject;
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		for(int i=0;i<NoOfRotateObj.Length;i++)
		{
			NoOfRotateObj [i].transform.Rotate (Vector3.up * Time.deltaTime *SpeedOfObject);
		}
	}
}
