﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GUI_ScrollChildCenter : MonoBehaviour ,IEndDragHandler ,IBeginDragHandler,IDragHandler
{
    Transform[] childs;

    public RectTransform content;
    public float speed = 20;

    private bool isDraging = false;
    private Vector2 pointerPosition;
    private float currentMoveSpeed;
    private Vector3 targetPosition;

	// Use this for initialization
	void Start () 
    {
        childs = new Transform[content.childCount];
        for (int i = 0; i < childs.Length; i++)
        {
            childs[i] = content.GetChild(i);
        }
		isDraging = true;
//        SetCenter(UI_GameLevelListUI.unlockGroupID);
	}

    private void LateUpdate()
    {
		
        if(!isDraging && targetPosition != content.localPosition)
        {
            content.localPosition += new Vector3((targetPosition.x - content.localPosition.x), (0),(targetPosition.z - content.localPosition.z)) * Time.deltaTime * 5; 
        }
    }

    Vector3 GetTargetPosition (int index)
    {
        GridLayoutGroup gridLayout = content.GetComponent<GridLayoutGroup>();

        float x = gridLayout.cellSize.x ;

        if (index < childs.Length && index >= 0)
		{
            return new Vector3(-x * index - x * 0.5f, 0, 0);
		}

        if(index >= childs.Length && childs.Length > 0)
        {
			return new Vector3(-x * (childs.Length - 1) - x, 0, 0);
        }

	
        return new Vector3(-x * 0.5f,0,0);

    }

    public void SetCenter (int index)
    {
        if(index < childs.Length)
        {
         //   Vector3 pos = GetTargetPosition(index);
        //    content.localPosition = pos;
         //   targetPosition = pos;
        }
    }

    public virtual void OnBeginDrag (PointerEventData eventData)
    {
        isDraging = true;
        pointerPosition = eventData.position;

    }

    public virtual void OnDrag (PointerEventData eventData)
    {
        if(isDraging)
        {
            float x = eventData.position.x - pointerPosition.x;
            Vector3 pos = content.localPosition + new Vector3(x*10, 0, 0);
            content.localPosition += (pos - content.localPosition) * Time.deltaTime * 4;
            currentMoveSpeed = x;
            pointerPosition = eventData.position;		
        }
    }

    public virtual void OnEndDrag (PointerEventData eventData)
    {
        targetPosition = Vector3.zero;

        if (Mathf.Abs(currentMoveSpeed) < 10)
            currentMoveSpeed = -currentMoveSpeed;
        
        for (int i = 0; i < childs.Length; i++)
        {
            Vector3 targetPos = GetTargetPosition(i);

            if (currentMoveSpeed < 0)
            {
                if (content.localPosition.x >= targetPos.x)
                {
                    targetPosition = targetPos;
                    break;
                }
                else if(i == childs.Length - 1)
                {
                    targetPosition = targetPos;
                }
            }
            else
            {
                if(content.localPosition.x <= targetPos.x)
                {
                    targetPosition = targetPos;
                }
                else if(i == 0)
                {
                    targetPosition = targetPos;
                }
            }
        }

        isDraging = false;

    }
}
