﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideRampEnter : MonoBehaviour
{
   
    Transform player;
    Rigidbody parent;

    private void Start()
    {
        parent = GetComponentInParent<Rigidbody>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            player = GameManager.Instance.CurrentVehile.transform;
            player.SetParent(parent.transform);
            this.GetComponentInParent<MovementAnimation>().enabled = true;

        }
    }

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.tag == "Player")
    //    {
    //        player = other.gameObject.GetComponentInParent<Rigidbody>();
    //        player.transform.SetParent(null);
    //        this.GetComponentInParent<MovementAnimation>().enabled = false;
   
    //    }
    //}
}
