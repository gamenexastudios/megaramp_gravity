﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarclipCulling : MonoBehaviour
{

    public int[] layers;
    public int[] layerDistance;
    void Start()
    {
        Camera camera = GetComponent<Camera>();
        float[] distances = new float[32];

        for(int i = 0; i < layers.Length; i++)
        {
            distances[layers[i]] = layerDistance[i];
            camera.layerCullDistances = distances;
        }
    
  
     
    }

}
