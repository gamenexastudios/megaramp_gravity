﻿using System.Collections;
using UnityEngine;
public class CameraMovement : MonoBehaviour
{


    private Touch touch;
    private float rotationAroundXAxis, rotationAroundYAxis, deltaTime, xRot, yRot, xSign, ySign;
    private void LateUpdate()
    {
        if (Input.touchCount == 1)
        {
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
               // UpgradeVechicle.Instance.carUpgrade.permissionFromCarRotaionScript = true;
                deltaTime = Time.deltaTime;
                rotationAroundYAxis = (touch.deltaPosition.x) * deltaTime;
                rotationAroundXAxis = -(touch.deltaPosition.y) * deltaTime;
                if (rotationAroundXAxis > 0)
                {
                    xSign = 1;
                }
                else
                {
                    xSign = -1;
                }
                if (rotationAroundYAxis > 0)
                {
                    ySign = 1;
                }
                else
                {
                    ySign = -1;
                }
                xRot = Mathf.Lerp(0, 22f, (rotationAroundXAxis * xSign) / 10f) * xSign;
                yRot = Mathf.Lerp(0, 22f, (rotationAroundYAxis * ySign) / 10f) * ySign;

                if (xRot + transform.eulerAngles.x >= 0f && xRot + transform.eulerAngles.x <= 40f)
                {
                    transform.Rotate(new Vector3(1, 0, 0), xRot);
                }
                transform.Rotate(new Vector3(0, 1, 0), yRot, Space.World);
            }
        }
       
    }


    IEnumerator UpgradeCarPermission()
    {
        yield return new WaitForSeconds(2f);
        UpgradeVechicle.Instance.carUpgrade.permissionFromCarRotaionScript = false;
    }
}