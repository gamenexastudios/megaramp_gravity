﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindTurbine : MonoBehaviour
{

    public float windForce = 100f;
    Rigidbody rb;





    RaycastHit hit;
    private void Start()
    {
        hit = new RaycastHit();
    }

    private void FixedUpdate()
    {
        if(Physics.Raycast(transform.position,transform.forward,out hit,8f))
        {
            if (hit.collider.tag == "Player")
            {
                rb = hit.transform.GetComponentInParent<Rigidbody>();
                rb.AddForceAtPosition(transform.forward*windForce*100f, hit.point);
            }
        }
        Debug.DrawRay(transform.position, transform.forward * 8f);
    }

}
