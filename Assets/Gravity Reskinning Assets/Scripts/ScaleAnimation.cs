﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleAnimation : MonoBehaviour
{
    public Vector3 startposition, endPosition;
    public float delay, speed;
    private float time, ll;

    private Coroutine dummyCoroutine;

    private void OnEnable()
    {
        dummyCoroutine = StartCoroutine(Scaling());
    }

    private void OnDisable()
    {
        StopCoroutine(dummyCoroutine);
    }

    IEnumerator Scaling()
    {
        bool reachedFinish = false;
        while (true)
        {
            time = 0;
            while (time < speed)
            {
                time += Time.deltaTime ;
                if (!reachedFinish)
                {
                    ll = (time / speed);

                    transform.localScale = Vector3.Lerp(startposition, endPosition, ll );
                    if (transform.localScale.x >= endPosition.x)
                    {
                        reachedFinish = true;
                        ll = 0;
                    }
                }
                else
                {
                    ll = (time / speed);

                    transform.localScale = Vector3.Lerp(endPosition, startposition, ll );
                    if (transform.localScale.x <= startposition.x)
                    {
                        reachedFinish = false;
                    }
                }

                yield return null;
            }
            if (transform.localScale.x != endPosition.x)
            {
                yield return new WaitForSeconds(delay);
            }
        }
    }

}
