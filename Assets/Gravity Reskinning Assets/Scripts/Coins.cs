﻿using UnityEngine;

public class Coins : MonoBehaviour
{

    public int CoinsValue=1;

    [HideInInspector]
    public Transform ownTransform;
    private void Start()
    {
       
        ownTransform = transform;
        CoinsManger.manager.TotalCoinsobjects.Add(ownTransform);
        ownTransform.rotation =Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {  
            CoinsManger.manager.UpdateCoinsText(CoinsValue,ownTransform.position);
            CoinsManger.manager.TotalCoinsobjects.Remove(ownTransform);
            Destroy(this.gameObject, 0.2f);
        }
    }
}
