﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollerDrumsTrigger : MonoBehaviour
{
    public Rigidbody[] drums;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            foreach(Rigidbody rig in drums)
            {
                rig.isKinematic = false;
            }
        }
    }
}
