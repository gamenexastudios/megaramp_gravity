﻿using UnityEngine;

public class GroundDetector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name != "LF_Trigger")
        {

            GameManager.Instance._IsGrounded = true;

        }
        else
        {
            GameManager.Instance._IsGrounded = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GameManager.Instance._IsGrounded = false;
    }
}
