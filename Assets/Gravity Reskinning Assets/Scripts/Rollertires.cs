﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rollertires : MonoBehaviour
{

    public Rigidbody[] tires;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            foreach(Rigidbody rb in tires)
            {
                if (rb != null)
                {
                    rb.isKinematic = false;
                    Destroy(rb?.gameObject, 10f);
                }
            }
        }
    }
}
