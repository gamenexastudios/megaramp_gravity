﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacleGates : MonoBehaviour
{

    public Transform inGate;
    public Transform outGate;

    public float GateMovingSpeed;
    private float time;
    private float ll;

    public Transform inGateUp;
    public Transform inGateDown;
    public Transform outGATEuP;
    public Transform OUTgATEDOWN;

    Coroutine gate;
    public Rigidbody rb;
 
    private void OnTriggerEnter(Collider other)
    {
        if (rb.velocity.magnitude <= 0)
        {
            if (other.gameObject.tag == "InGate")
            {
                if (gate != null)
                {
                    StopCoroutine(gate);
                }
                gate = StartCoroutine(GateMover( inGateDown.position, outGATEuP.position));
            }
            else if (other.gameObject.tag == "OutGate")
            {
                if (gate != null)
                {
                    StopCoroutine(gate);
                }
                gate = StartCoroutine(GateMover( inGateUp.position, OUTgATEDOWN.position));

            }
        }
    }


    IEnumerator GateMover(Vector3 inGatedestination,Vector3 outGaateDestination)
    {
        time = 0;
        while (time < (GateMovingSpeed))
        {
            time += Time.deltaTime;
            ll = (time / (GateMovingSpeed));
            inGate.position = Vector3.Lerp(inGate.position, new Vector3(inGate.position.x,inGatedestination.y, inGate.position.z), ll);
            outGate.position = Vector3.Lerp(outGate.position, new Vector3(outGate.position.x, outGaateDestination.y, outGate.position.z), ll);

            yield return new WaitForEndOfFrame();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (rb.velocity.magnitude <= 0)
        {
            if (other.gameObject.tag == "InGate")
            {
                if (gate != null)
                {
                    StopCoroutine(gate);
                }
                gate = StartCoroutine(GateMover( inGateUp.position, outGATEuP.position));

            }
            else if (other.gameObject.tag == "OutGate")
            {
                if (gate != null)
                {
                    StopCoroutine(gate);
                }
               gate = StartCoroutine(GateMover( inGateDown.position, outGATEuP.position));


            }
        }
    }
}
