﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineAnimation : MonoBehaviour
{
    public RectTransform startposition, endPosition;
    public float delay, speed;
    private float time, ll;
    public float startDelay = 0;

    private Image image;
    private float targetAlpha;
    private Coroutine dummyCoroutine;
    public int animType = 0;
    public float maxAlphaValue = 0.75f;

    // Start is called before the first frame update
    void Start()
    {

    }
    private void OnEnable()
    {
        image = GetComponent<Image>();
        dummyCoroutine = StartCoroutine(Movement(startDelay));
    }

    private void OnDisable()
    {
        StopCoroutine(dummyCoroutine);
    }

    IEnumerator Movement(float startDelayValue)
    {
        yield return new WaitForSeconds(startDelayValue);
        Color curColor = image.color;
        while (true)
        { 
        time = 0;
        while (time < speed)
        {
            time += Time.deltaTime; 
            ll = (time / speed);
             transform.position = Vector3.Lerp(startposition.position, endPosition.position, ll);

                if (animType == 0)
                {
                    if (ll <= 0.25f)
                    {
                        curColor.a = (float)Mathf.Lerp(0f, maxAlphaValue, ll * 4f);
                        image.color = curColor;
                    }
                    else if (ll > 0.25f && ll <= 0.75f)
                    {
                        curColor.a = (float)Mathf.Lerp(maxAlphaValue, maxAlphaValue, ll * 4f);
                    }
                    else
                    {
                        curColor.a = (float)Mathf.Lerp(maxAlphaValue, 0f, ll * 4f - 3f);
                        image.color = curColor;
                    }
                }
                else
                {
                    if (ll <= 0.5f)
                    {
                        curColor.a = (float)Mathf.Lerp(1f, 1f, ll * 2f);
                        image.color = curColor;
                    }
                    else
                    {
                        curColor.a = (float)Mathf.Lerp(1f, 0f, ll * 2f - 1f);
                        image.color = curColor;
                    }
                }
                
                yield return null;
        }
        yield return new WaitForSeconds(delay);
        }
    }

}
