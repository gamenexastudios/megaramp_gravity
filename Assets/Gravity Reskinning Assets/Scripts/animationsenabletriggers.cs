﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationsenabletriggers : MonoBehaviour
{


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {


            this.GetComponentInChildren<MovementAnimation>().enabled = true;


        }
    }

   /* private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            this.GetComponentInChildren<MovementAnimation>().enabled = false;
        }
    }
*/
}
