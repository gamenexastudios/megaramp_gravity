﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;

public class CoinsManger : MonoBehaviour
{
   
    public static CoinsManger manager;

    //public TextMeshProUGUI ToatalcoinsText;
    public ParticleSystem coinsPartcle;
    public AudioClip coinsSound;
    public float CoinRotationSpeed = 80f;

    [HideInInspector]
    public AudioSource coinsCollisionAudio;
    [HideInInspector]
    public int collectedCoins;
    [HideInInspector]
    public int TotalCoins;

    ParticleSystem particle;
    float deltatime;

    [HideInInspector]
    public List<Transform> TotalCoinsobjects;

    Transform tt;
    void Start()
    {
        if (manager == null)
        {
            manager = this;
        }

        particle=Instantiate(coinsPartcle, Vector3.zero, Quaternion.identity);
        TotalCoins = StaticVariables._iTotalCoins;
        InGameUIHandler.Instance.ToatalcoinsText.text = TotalCoins.ToString();
        coinsCollisionAudio = GetComponent<AudioSource>();
       
    }

    private int interval = 3;
    private void Update()
    {
        
        if (Time.frameCount % interval == 0)
        {
            deltatime = Time.deltaTime;
            foreach (Transform coin in TotalCoinsobjects)
            {
                if (coin != null)
                {
                    coin.Rotate(coin.up * deltatime * CoinRotationSpeed*2f);
                }
                else { return; }
            }
        }

        
    }

    public void UpdateCoinsText(int CoinsValue,Vector3 position)
    {
        particle.gameObject.transform.position = position;
        particle.Play();
        TotalCoins += CoinsValue;
        collectedCoins += CoinsValue;
        InGameUIHandler.Instance.ToatalcoinsText.text = TotalCoins.ToString();

        coinsCollisionAudio.PlayOneShot(coinsSound);

    }

}
