﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Side
{
    left,
    right
}
public class CameraOnjump : MonoBehaviour
{

    public float CameraDistance = 15f;
    public float CameraHeight = 10f;


    private RCC_Camera rcccCam;
    private RCC_ProtectCameraFromWallClip wallprotectCAM;
    Coroutine startJump;
    Coroutine endJump;

    public Side rotationSide;
    public float rotationangles = 45f;

    public bool obstacle;
    // Start is called before the first frame update
    void Start()
    {


        rcccCam = RCC_Camera.Instance;
        wallprotectCAM = rcccCam.GetComponent<RCC_ProtectCameraFromWallClip>();
        switch (rotationSide)
        {
            case (Side.left):

                rotationangle = rotationangles;
                break;
            case (Side.right):
                rotationangle = -rotationangles;
                break;

        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.transform.CompareTag("Player"))
        {
            rcccCam.Onjump = true;
            wallprotectCAM.enabled = false;
           // startJump = StartCoroutine(SmoothFlow());
            time = 0f;
            ll = 0f;
            Smoothflow = true;
#if UNITY_EDITOR
            return;
#else
            RCC_MobileButtons.Instance.enabled = false;
#endif
        }
    }

    bool Smoothflow;
    bool SmoothFlowBacks;
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("CAMERA ON JUMP TRIGGER EXIT ");
        if (other.transform.CompareTag("Player"))
        {
            time = 0;
            Smoothflow = false;
         
            ll = 0;
            yRot = 0;
            SmoothFlowBacks = true;
            //if (startJump != null)
            //{
            //    StopCoroutine(startJump);
            //}
          //  endJump = StartCoroutine(SmoothFlowBack());
#if UNITY_EDITOR
            return;
#else
            RCC_MobileButtons.Instance.enabled = true;
#endif
        }
    }




    float time, ll;
    public float speed, deltaTime;
    IEnumerator SmoothFlow()
    {
        Debug.Log("WE ARE IN SMOOTH FLOW CO_ROUTINE");
        time = 0;
        while (time < (speed))
        {
            /*           if (obstacle)
                       {
                          deltaTime = Time.fixedDeltaTime;
                       }
                       else
                       {*/
            deltaTime = Time.deltaTime;
            // }
            time += deltaTime;
            ll = (time / (speed));
            rcccCam.TPSDistance = Mathf.Lerp(4f, CameraDistance, ll);
            rcccCam.TPSHeight = Mathf.Lerp(2.5f, CameraHeight, ll);
            yRot = Mathf.Lerp(0f, rotationangle, ll);
            yrotFinal = yRot;
            height = rcccCam.TPSHeight;

            distance = rcccCam.TPSDistance;
            rcccCam.transform.Rotate(new Vector3(0, 1, 0), yRot * deltaTime * 10f, Space.World);
            Debug.Log("SMOOTH FLOW 02");
            if (obstacle)
            {
                yield return new WaitForEndOfFrame();
            }
            else
            {
                Debug.Log("SMOOTH FLOW 03");
                yield return null;
                // yield return new WaitForEndOfFrame();
            }

        }
        yield return null;

    }
    float height, distance;

    private float yRot, yrotFinal;
    private float rotationangle;

    IEnumerator SmoothFlowBack()
    {
        time = 0;

        while (time < (speed / 4f))
        {
            /*  if (obstacle)
              {
                  deltaTime = Time.fixedDeltaTime;
              }
              else
              {*/
            deltaTime = Time.deltaTime;
            //}
            time += deltaTime;
            ll = (time / (speed / 4f));
            rcccCam.TPSDistance = Mathf.Lerp(distance, 4f, ll);
            rcccCam.TPSHeight = Mathf.Lerp(height, 2.5f, ll);
            yRot = Mathf.Lerp(yrotFinal, 0f, ll);
            rcccCam.transform.Rotate(new Vector3(0, 1, 0), yRot * deltaTime * 10f, Space.World);
            if (obstacle)
            {
                yield return new WaitForEndOfFrame();
            }
            else
            {
                yield return null;
                //yield return new WaitForEndOfFrame();
            }
        }
        rcccCam.Onjump = false;
        wallprotectCAM.enabled = true;
        yield return null;
    }

    bool switchs;
    private void Update()
    {
        if (Smoothflow == true&& time < (speed))
        {
            deltaTime = Time.deltaTime;
            // }
            time += deltaTime;
            ll = (time / (speed));
            rcccCam.TPSDistance = Mathf.Lerp(4f, CameraDistance, ll);
            rcccCam.TPSHeight = Mathf.Lerp(2.5f, CameraHeight, ll);
            yRot = Mathf.Lerp(0f, rotationangle, ll);
            yrotFinal = yRot;
            height = rcccCam.TPSHeight;

            distance = rcccCam.TPSDistance;
            rcccCam.transform.Rotate(new Vector3(0, 1, 0), yRot * deltaTime * 10f, Space.World);
            Debug.Log("SMOOTH FLOW 02");
        }

        if (SmoothFlowBacks&& time < (speed / 4f))
        {
            deltaTime = Time.deltaTime;
            //}
            time += deltaTime;
            ll = (time / (speed / 4f));
            rcccCam.TPSDistance = Mathf.Lerp(distance, 4f, ll);
            rcccCam.TPSHeight = Mathf.Lerp(height, 2.5f, ll);
            yRot = Mathf.Lerp(yrotFinal, 0f, ll);
            rcccCam.transform.Rotate(new Vector3(0, 1, 0), yRot * deltaTime * 10f, Space.World);
            switchs = false;
        }
        else if(switchs==false)
        {
            
            SmoothFlowBacks = false;
            wallprotectCAM.enabled = true;
            switchs = true;
        }
    }


}
