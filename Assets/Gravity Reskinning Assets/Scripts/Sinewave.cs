﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sinewave : MonoBehaviour
{
    Transform ownTransform;

    private void Start()
    {
       
        ownTransform = transform;
        scale = ownTransform.localScale;
    }
    Vector3 scale;
    private void LateUpdate()
    {
        ownTransform.localScale = scale *(1+ ((Mathf.Sin(Time.time*8f )+1)/6f));
    }
}
