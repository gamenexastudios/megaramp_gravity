﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollision : MonoBehaviour
{
    public Vector3 _prevPosition,vel;
    public float velMag;
    public Rigidbody rb;
    public float forceToapplyWhenCollision;


    private void Start()
    {
        StartCoroutine(hh());
    }
    private void Update()
    {

    }

    IEnumerator hh()
    {
        while (true)
        {
            vel = (transform.position - _prevPosition) / Time.deltaTime;
            velMag = vel.magnitude;
            _prevPosition = transform.position;
            yield return null;
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {

            rb = collision.transform.GetComponent<Rigidbody>();
            Vector3 rot = -(transform.position - rb.transform.position).normalized;
            rb.AddForceAtPosition(rot * velMag*forceToapplyWhenCollision*10000f, rb.transform.position);
        }
    }
}
