using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rampsrestart : MonoBehaviour
{
    public MovementAnimation[] moves;

    public void Restart()
    {
        if (moves != null)
        {
            foreach (MovementAnimation m in moves)
            {
                m.Restart();
            }
        }
        else
        {
            return;
        }

    }

}
