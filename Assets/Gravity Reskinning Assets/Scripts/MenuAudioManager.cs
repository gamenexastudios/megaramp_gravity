﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MenuAudioManager : MonoBehaviour
{
    public AudioMixer TotalGameAudioController;
    public Sprite onSprite;
    public Sprite offSprite;
    public Sprite commonOn;
    public Sprite commonOff;

    public Image SoundOnImage;
    public Image soundOffImage;


    public Image musicOnImage;
    public Image musicoffImage;


    // Start is called before the first frame update
    void Start()
    {
        if (StaticVariables._Soundss == "SoundOn")
        {
            TotalGameAudioController.SetFloat("Sounds", 0f);
            TotalGameAudioController.SetFloat("CarSounds", 0f);
            SoundOnImage.sprite = onSprite;
            soundOffImage.sprite = commonOff;
        }
        else
        {
            TotalGameAudioController.SetFloat("Sounds", -80f);
            TotalGameAudioController.SetFloat("CarSounds", -80f);
            SoundOnImage.sprite = commonOn;
            soundOffImage.sprite = offSprite;
        }

        if (StaticVariables._Musicc == "MusicOn")
        {

            TotalGameAudioController.SetFloat("Music", 0f);
            musicOnImage.sprite = onSprite;
            musicoffImage.sprite = commonOff;
        }
        else
        {

            TotalGameAudioController.SetFloat("Music", -80f);
            musicOnImage.sprite = commonOn;
            musicoffImage.sprite = offSprite;
        }

    }

    public void SoundsOff()
    {
        TotalGameAudioController.SetFloat("Sounds", -80f);
        TotalGameAudioController.SetFloat("CarSounds", -80f);
        SoundOnImage.sprite = commonOn;
        soundOffImage.sprite = offSprite;
        StaticVariables._Soundss = "SoundsOff";

    }

    public void SoundsOn()
    {
        TotalGameAudioController.SetFloat("Sounds", 0f);
        TotalGameAudioController.SetFloat("CarSounds", 0f);
        SoundOnImage.sprite = onSprite;
        soundOffImage.sprite = commonOff;
        StaticVariables._Soundss = "SoundOn";
    }
    public void MusicOff()
    {
        TotalGameAudioController.SetFloat("Music", -80f);
        musicOnImage.sprite = commonOn;
        musicoffImage.sprite = offSprite;
        StaticVariables._Musicc = "MusicOff";
    }

    public void MusicOn()
    {
        TotalGameAudioController.SetFloat("Music", 0f);
        musicOnImage.sprite = onSprite;
        musicoffImage.sprite = commonOff;
        StaticVariables._Musicc = "MusicOn";
    }
}
