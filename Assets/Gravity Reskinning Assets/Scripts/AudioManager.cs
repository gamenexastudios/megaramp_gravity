﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;
    public int WorldNumber;


    [HideInInspector]
    public AudioSource GameSounds;




    public AudioClip LevelFailAudio;
    public AudioClip LevelFinishedSucessAudio;


    AudioClip themeAudio;


    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        if (WorldNumber == 1)
        {
            themeAudio = Resources.Load("In Game BG") as AudioClip;
        }
        else
        {
            themeAudio = Resources.Load("In Game BG 02") as AudioClip;
        }

        GameSounds = GetComponent<AudioSource>();
        GameSounds.clip = themeAudio;
        GameSounds.Play();
        
    }

    public void LevelFailed()
    {
        GameSounds.Stop();
        GameSounds.loop = false;
        GameSounds.clip = LevelFailAudio;
        GameSounds.Play();
    }

    public void LevelFInishedSucess()
    {
        GameSounds.Stop();
        GameSounds.loop = false;
        GameSounds.clip = LevelFinishedSucessAudio;
        GameSounds.Play();
    }






}
