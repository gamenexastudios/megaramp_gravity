﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSmoothMovement : MonoBehaviour
{
    Transform Camera;
    RCC_Camera rcccCam;
    private RCC_ProtectCameraFromWallClip wallprotectCAM;

    public Transform target;
    Coroutine main;
    // Start is called before the first frame update
    void Start()
    {
        Camera = GameObject.FindGameObjectWithTag("Camera").transform;
        rcccCam = Camera.GetComponentInParent<RCC_Camera>();
        wallprotectCAM = Camera.GetComponentInParent<RCC_ProtectCameraFromWallClip>();
    }

    Vector3 camPosition;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
          
            rcccCam.enabled = false;wallprotectCAM.enabled = false;
            camPosition = Camera.position;
            main = StartCoroutine(CameraRotation());
        }
    }
    float time, ll;
    public float speed;
    IEnumerator CameraRotation()
    {
        time = 0;
        while (time < speed)
        {

            time +=  Time.deltaTime;

            ll = (time / speed);

            Camera.position = Vector3.Lerp(camPosition, target.position, ll);
            Camera.transform.LookAt(GameManager.Instance.CurrentVehile.transform);
            yield return null;
        }
            
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
          
            rcccCam.enabled = true; 
            StopCoroutine(main);
        }
    }
}
