﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheerGirlController : MonoBehaviour
{

     Animator anim;

    public int random;

    private void Start()
    {
        anim = GetComponent<Animator>();
        anim.enabled = false;
        CinematicCameraMovement.instance.cheerGrils += PlayCheer;
    }


    void PlayCheer()
    {
        anim.enabled = true;
        anim.SetInteger("DanceState", random);
    
    }

}
