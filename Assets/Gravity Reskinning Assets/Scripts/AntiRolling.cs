﻿using UnityEngine;

public class AntiRolling : MonoBehaviour
{
    Rigidbody rb;
    private float mass;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        mass = rb.mass;
    
    }
    float  angle;
    void Update()
    {
        if (GameManager.Instance._IsGrounded == false&&GameManager.Instance.underObstacle==false)
        {
  
             angle = rb.transform.localEulerAngles.x;
             angle = (angle > 180) ? angle - 360 : angle;

            if (angle < -12f)
            {

                rb.AddTorque(rb.transform.right * mass * 75f * Time.deltaTime);

                rb.angularDrag = 10f;
            }
            else if (angle > 12f)
            {

                rb.AddTorque(-rb.transform.right * mass * 75f * Time.deltaTime);

                rb.angularDrag = 10f;
            }
            
        }
        else
        {
            if (rb.angularDrag > 0.05f)
            {
                rb.angularDrag= 0.05f;
                
            }
        }
    }
}
