﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RampEnter : MonoBehaviour
{
   
    Rigidbody player;
    


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {


                player = other.gameObject.GetComponentInParent<Rigidbody>();
                player.transform.SetParent(this.transform);
                this.GetComponentInParent<MovementAnimation>().enabled = true;
              
            

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            player = other.gameObject.GetComponentInParent<Rigidbody>();
            player.transform.SetParent(null);
            this.GetComponentInParent<MovementAnimation>().enabled = false;
   
        }
    }
}
