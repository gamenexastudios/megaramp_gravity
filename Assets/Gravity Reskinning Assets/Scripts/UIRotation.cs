﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRotation : MonoBehaviour
{
    public Transform StartRotation;
    public Transform EndRotation;
    public float Speed;

    Quaternion startRotValue;
    Quaternion endRotValue;

    // Start is called before the first frame update
    void Start()
    {
        startRotValue = StartRotation.rotation;
        endRotValue = EndRotation.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        startRotValue = StartRotation.rotation;
        endRotValue = EndRotation.rotation;
        transform.rotation = Quaternion.Lerp(startRotValue, endRotValue, Time.deltaTime * Speed);
    }
}
