﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;



public class CinematicCameraMovement : MonoBehaviour
{
    
    public Transform center;
    public static CinematicCameraMovement instance;
    public Transform Camera;




    public float speed;

    public bool Loop;

    [HideInInspector]
    public bool On;


    RCC_Camera rcccCam;
    RCC_ProtectCameraFromWallClip wallprotectCAM;

    public ParticleSystem particleFinal;
    

   //Defining Delegate
    public delegate void CheerGirl();
    public CheerGirl cheerGrils;

    Camera cam;

    private void OnEnable()
    {
        if (instance != this)
        {
            instance = this;
        }

        if (instance == null)
        {
            instance = this;
        }

     
        rcccCam= RCC_Camera.Instance;
        wallprotectCAM= rcccCam.GetComponent<RCC_ProtectCameraFromWallClip>();
        cam = rcccCam.GetComponentInChildren<Camera>();

        cheerGrils += CarmovementAtFinish;
    }
    bool kk,oo;


 
    private void Update()
    {
        if (On)
        {
            rcccCam.transform.Rotate(new Vector3(0, 1, 0), 80f * Time.deltaTime, Space.World);

            if (!kk)
            {
                cam.farClipPlane = 250f;
                rcccCam.Onjump = true;
                particleFinal.Play();
                wallprotectCAM.enabled = false;
                kk = true;
            }
     
            /*if (kk == false)
            {
                rcccCam.enabled = false;
                wallprotectCAM.enabled = false;
        
   
                kk = true;
            }
            Vector3 cam = Camera.position;
          
            if (Camera.position != pathCreator.path.GetPoint(0) && !oo)
            {
                if (rb.velocity.magnitude > 1f)
                {
                    velocityy = rb.velocity.magnitude;
                    distTraveld += ((velocityy / 1000f) * Time.deltaTime) + 0.5f;
                }
                else
                {
                    distTraveld += 22 * Time.deltaTime;
                }
                Camera.position = Vector3.Lerp(cam, pathCreator.path.GetPoint(0), distTraveld);          
                Camera.transform.LookAt(GameManager.Instance.CurrentVehile.transform);
            }
            else
            {
                oo = true;
  
              
                if (rb.velocity.magnitude > 1f)
                {
                    velocityy = rb.velocity.magnitude;
                    distTraveld += ((velocityy /1000f) * Time.deltaTime) +0.5f;
                }
                else
                {
                    distTraveld += 22 * Time.deltaTime;
                }
                
                Camera.position = pathCreator.path.GetPointAtDistance(distTraveld);
                
                Camera.transform.LookAt(GameManager.Instance.CurrentVehile.transform);

            }
            */

        }
        
    }

    float time, ll;
    public float speeds=1f;
    IEnumerator SmoothFlow()
    {
        time = 0;
        while (time < (speeds*40f))
        {
            time += Time.deltaTime;
            ll = (time / (speeds*40f));
            rcccCam.TPSDistance = Mathf.Lerp(4f, 5.5f, ll);
            rcccCam.TPSHeight = Mathf.Lerp(2.5f, 3.5f, ll);

            yield return null;
        }

    }





    RCC_CarControllerV3 rcc;
    Rigidbody rb;
    void CarmovementAtFinish()
    {

        rb = GameManager.Instance.CurrentVehile.GetComponent<Rigidbody>();
        
        rcc = rb.GetComponent<RCC_CarControllerV3>();

        
        
    
    }
    bool stop;
    public float steer;
    private void FixedUpdate()
    {
        if (On && !oo)
        {
            stop = true;
            cheerGrils();
            StartCoroutine(SmoothFlow());

            oo = true;
        }

        if (stop)
        {

            //steer = AngleDir(rb.transform.forward, center.transform.position, rb.transform.up);
            // rcc.steerInput = -1;        

            // if (distance <= 2f&&GameManager.Instance._IsGrounded==true)
            // {
            //    rb.isKinematic = true;
            //    rcc.enabled = false;
            //     stop = false;
            //  }
            if (rb.velocity.magnitude > 3f)
            {
                carReached = true;

                rcc.steerInput = 0;
                float distance = Vector3.Distance(center.transform.position, rb.transform.position);
                rb.angularVelocity = Vector3.Lerp(rb.angularVelocity, Vector3.zero, distance / 120f);
                rb.velocity = Vector3.Lerp(rb.velocity, Vector3.zero, distance / 120f);
                // rcc.brakeTorque = 500000f;
                //rcc.brakeInput = 1;              
                // rb.AddForce(-rb.transform.forward * 1000f * rb.velocity.magnitude);
            }

            if (rb.velocity.magnitude < 3f && !carReached)
            {
                rb.velocity = GameManager.Instance.CurrentVehile.transform.forward * 8f;
            }
        }
    }

    bool carReached = false;

    public float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0.0f)
        {
            return 1.0f;
        }
        else if (dir < 0.0f)
        {
            return -1.0f;
        }
        else
        {
            return 0.0f;
        }
    }

}
