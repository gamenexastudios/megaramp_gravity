﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarScroller : MonoBehaviour
{
    public Transform cameraMain;
    public GameObject[] cars;

    public float timeTaken,lenghtOfMovement,CarSelfRotationSpeed;

    float deltatime, ll,time;

    public bool mm,start;

   public int currentcar;
    private void Start()
    {
        int lenght = cars.Length+1;
        startPos= new Vector3[lenght];
        endPos = new Vector3[lenght];

        for (int i = 0; i < cars.Length; i++)
        {
    
            cars[i].transform.rotation = Quaternion.Euler(0f, 90f, 0f);
            startPos[i] = cars[i].transform.position;
            endPos[i] = new Vector3(cars[i].transform.position.x + lenghtOfMovement, cars[i].transform.position.y, cars[i].transform.position.z);


        }
    }
    private void Update()
    {
        if (mm == true&&start==false&&currentcar>=0&&currentcar<=cars.Length-1)
        {
            if (lenghtOfMovement < 0)
            {
                SS = -1;
            }
            else
            {
                SS = 1;
            }
            StartCoroutine(Move(lenghtOfMovement));

            start = true;
            mm = false;
        }
    }

    public void MoveCar(int value)
    {
        if (!start && currentcar >= 0 && currentcar <= cars.Length - 1)
        {
            if (value < 0)
            {
                SS = -1;
            }
            else
            {
                SS = 1;
            }
            permissionFromCarRotaionScript = false;
            StartCoroutine(Move(value));

            start= true;
        }
    }

    bool reset;

    public bool permissionFromCarRotaionScript;
    private void LateUpdate()
    {
        if (!reset&&!permissionFromCarRotaionScript)
        {
            cars[currentcar].transform.Rotate(Vector3.up * CarSelfRotationSpeed);
        }
        
    }

    Vector3[] startPos;
    Vector3[] endPos;

    int SS;
    IEnumerator Move(float value)
    {
        if (currentcar + SS <= cars.Length-1&& currentcar + SS >= 0)
        {
          
            cameraMain.transform.rotation = Quaternion.Euler(0, 180f, 0f);
            for (int i = 0; i < cars.Length; i++)
            {
                reset = true;
                cars[i].transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                startPos[i] = cars[i].transform.position;
                endPos[i] = new Vector3(cars[i].transform.position.x + value, cars[i].transform.position.y, cars[i].transform.position.z);
                reset = false;

            }
            deltatime = Time.deltaTime;
            time = 0;
            ll = 0;
            while (time < timeTaken)
            {
                time += deltatime;
                ll = (time / timeTaken);
                for (int i = 0; i < cars.Length; i++)
                {
                    cars[i].transform.position = Vector3.Lerp(startPos[i], endPos[i], ll);
                    cars[i].gameObject.SetActive(false);
                }
                if (currentcar + SS <= cars.Length-1&&currentcar+SS>=0)
                {
                    cars[currentcar].SetActive(true);
                    cars[currentcar + SS].SetActive(true);

                }

                yield return null;
            }
            start = false;
            if (currentcar + SS <= cars.Length-1 && currentcar + SS >= 0)
            {
                currentcar += SS;
                cars[currentcar - SS].SetActive(false);
            }
            

        }
        
    
    }
}
