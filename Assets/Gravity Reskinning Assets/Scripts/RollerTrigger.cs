﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollerTrigger : MonoBehaviour
{
   public Rigidbody rb;
    bool on;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            rb = other.GetComponentInParent<Rigidbody>();
            on = true;
            StartCoroutine(StartRoll());
        }
    }

    IEnumerator StartRoll()
    {
        while (on)
        {
            rb.AddForce(rb.transform.forward * 10000f);
            Debug.Log("iamworking");
            yield return null;
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            on = false;
            StopCoroutine(StartRoll());
        }
    }
}
