﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class InGameAudioManager : MonoBehaviour
{
    public static InGameAudioManager instance;
    public AudioMixer TotalGameAudioController;
    public Sprite MusicOnIcon;
    public Sprite MusicOffIcon;
    public Sprite SoundsOnIcon;
    public Sprite SoundsOffIcon;

    public Image SoundImage;
    public Image musicImage;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        if (StaticVariables._Soundss == "SoundOn")
        {
            TotalGameAudioController.SetFloat("Sounds", 0f);
            TotalGameAudioController.SetFloat("CarSounds", 0f);
            SoundImage.sprite = SoundsOnIcon;
        }
        else
        {
            TotalGameAudioController.SetFloat("Sounds", -80f);
            TotalGameAudioController.SetFloat("CarSounds", -80f);
            SoundImage.sprite = SoundsOffIcon;
        }

        if (StaticVariables._Musicc == "MusicOn")
        {
            TotalGameAudioController.SetFloat("Music", 0f);
            musicImage.sprite = MusicOnIcon;
        }
        else
        {
            TotalGameAudioController.SetFloat("Music", -80f);
            musicImage.sprite = MusicOffIcon;
        }

    }

    public void SoundsBtnClick()
    {
        Debug.Log(" SOUNDS ::::::::: " + StaticVariables._Soundss);
        Debug.Log(" SOUNDS ::::::::: " + StaticVariables._Sounds);

        if (StaticVariables._Soundss == "SoundOn")
        {
            TotalGameAudioController.SetFloat("Sounds", -80f);
            TotalGameAudioController.SetFloat("CarSounds", -80f);
            SoundImage.sprite = SoundsOffIcon;
            StaticVariables._Soundss = "SoundOff";
        }
        else
        {
            TotalGameAudioController.SetFloat("Sounds", 0f);
            TotalGameAudioController.SetFloat("CarSounds", 0f);
            SoundImage.sprite = SoundsOnIcon;
            StaticVariables._Soundss = "SoundOn";
        }
    }

    public void MusicBtnClick()
    {
        Debug.Log(" MUSIC ::::::::: " + StaticVariables._Musicc);
        Debug.Log(" MUSIC ::::::::: " + StaticVariables._Music);


        if (StaticVariables._Musicc == "MusicOn")
        {
            TotalGameAudioController.SetFloat("Music", -80f);
            musicImage.sprite = MusicOffIcon;
            StaticVariables._Musicc = "MusicOff";
        }
        else
        {
            TotalGameAudioController.SetFloat("Music", 0f);
            musicImage.sprite = MusicOnIcon;
            StaticVariables._Musicc = "MusicOn";
        }
    }

    public void CarSounds()
    {
        TotalGameAudioController.SetFloat("CarSounds", -80f); 
    }
}


