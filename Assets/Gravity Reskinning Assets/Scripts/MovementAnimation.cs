using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Axis
{
    Xaxis,
    Yaxis,
    Zaxis
}

public enum TypeOfMotion
{
    Linear,
    Rotation,
    LinearAndRotation

}



public enum RotationBehaviour
{
    ContinuosRotation,
    PendulamMotion
}

public enum LinearMotion
{
    positive,
    negative
}


public class MovementAnimation : MonoBehaviour
{
    public TypeOfMotion movementType;
    public bool Loop = true;
    private bool isLooping = true;
    public float delay;
    public float SpeedOfRotandLinear = 2f;

    public bool Obstacle15 = false;



    [Header("Change the curve for your desired Smooth movement.................................")]
    [Space(40)]
    [SerializeField]
    private AnimationCurve SmoothMovementCurve;




    [Header("Change this Values if Linear selected")]
    [Space(40)]
    public Axis MovementAxis;
    public LinearMotion LinearmotionType;
    public float LenghtOfMovement = 3f;




    [Header("Change this Values if rotation selected")]
    [Space(40)]
    public RotationBehaviour rotationMotionType;
    public float rotationSpeed = 2f;
    public Transform endingsRotation;



    Vector3 startposition;
    Vector3 endPosition;
    float time = 0;
    float speed = 1f;



    Coroutine main;
    bool amready;

    Transform startingRotation;


    Quaternion startRot;
    Quaternion endRot;
    Rigidbody rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        startposition = transform.position;
        startingRotation = GetComponent<Transform>();
        speed = 1f / SpeedOfRotandLinear;


        switch (MovementAxis)
        {
            case (Axis.Xaxis):

                switch (LinearmotionType)
                {
                    case (LinearMotion.positive):

                        endPosition = new Vector3(startposition.x + LenghtOfMovement, startposition.y, startposition.z);
                        break;
                    case (LinearMotion.negative):
                        endPosition = new Vector3(startposition.x - LenghtOfMovement, startposition.y, startposition.z);
                        break;
                }

                break;
            case (Axis.Yaxis):
                switch (LinearmotionType)
                {
                    case (LinearMotion.positive):
                        endPosition = new Vector3(startposition.x, startposition.y + LenghtOfMovement, startposition.z);
                        break;
                    case (LinearMotion.negative):
                        endPosition = new Vector3(startposition.x, startposition.y - LenghtOfMovement, startposition.z);
                        break;
                }

                break;
            case (Axis.Zaxis):
                switch (LinearmotionType)
                {
                    case (LinearMotion.positive):

                        endPosition = new Vector3(startposition.x, startposition.y, startposition.z + LenghtOfMovement);

                        break;
                    case (LinearMotion.negative):
                        endPosition = new Vector3(startposition.x, startposition.y, startposition.z - LenghtOfMovement);
                        break;
                }


                break;

        }
        switch (movementType)
        {
            case (TypeOfMotion.Rotation):
                startRot = startingRotation.rotation;
                endRot = endingsRotation.rotation;
                break;
            case (TypeOfMotion.LinearAndRotation):
                startRot = startingRotation.rotation;
                endRot = endingsRotation.rotation;
                break;
        }
    }
    void OnEnable()
    {

        if (isLooping == true)
        {
            main = StartCoroutine(Movements());
        }

    }


    float ll;


    IEnumerator Movements()
    {
        amready = true;
        while (isLooping == true)
        {
            //deltatime = Time.deltaTime;
            time = 0;
            while (time < speed)
            {

                time += 0.1f + (SmoothMovementCurve.Evaluate(ll) * Time.deltaTime * 100f);

                ll = (time / speed);

                switch (movementType)
                {

                    case (TypeOfMotion.Linear):
                        if (Loop == false)
                        {
                            rb.transform.position = Vector3.Lerp(transform.position, endPosition, ll);
                        }
                        else
                        {
                            rb.transform.position = Vector3.Lerp(startposition, endPosition, ll);
                        }

                        break;
                    case (TypeOfMotion.Rotation):
                        switch (rotationMotionType)
                        {
                            case (RotationBehaviour.ContinuosRotation):
                                startRot = startingRotation.rotation;
                                endRot = endingsRotation.rotation;
                                if (Time.deltaTime > 0)
                                {
                                    rb.transform.rotation = Quaternion.Lerp(startRot, endRot, Time.deltaTime * rotationSpeed);
                                }
                                break;
                            case (RotationBehaviour.PendulamMotion):
                                rb.transform.rotation = Quaternion.Lerp(startRot, endRot, ll);
                                break;
                        }
                        break;
                    case (TypeOfMotion.LinearAndRotation):
                        rb.transform.position = Vector3.Lerp(transform.position, endPosition, ll);
                        switch (rotationMotionType)
                        {
                            case (RotationBehaviour.ContinuosRotation):
                                startRot = startingRotation.rotation;
                                endRot = endingsRotation.rotation;
                                if (Time.deltaTime > 0)
                                {
                                    rb.transform.rotation = Quaternion.Lerp(startRot, endRot, Time.deltaTime * rotationSpeed);
                                }
                                break;
                            case (RotationBehaviour.PendulamMotion):
                                rb.transform.rotation = Quaternion.Lerp(startRot, endRot, ll);
                                break;
                        }
                        break;
                }

                    yield return new WaitForEndOfFrame();





            }

            yield return new WaitForSeconds(delay);
            if (Loop == false)
            {
                isLooping = false;
                yield return null;
            }
            else
            {
                time = 0;
                while (time < speed)
                {
                    time += 0.1f + (SmoothMovementCurve.Evaluate(ll) * Time.deltaTime * 100f);
                    ll = (time / speed);
                    switch (movementType)
                    {
                        case (TypeOfMotion.Linear):
                            rb.transform.position = Vector3.Lerp(endPosition, startposition, ll);
                            break;
                        case (TypeOfMotion.Rotation):
                            switch (rotationMotionType)
                            {
                                case (RotationBehaviour.ContinuosRotation):
                                    startRot = startingRotation.rotation;
                                    endRot = endingsRotation.rotation;
                                    if (Time.deltaTime > 0)
                                    {
                                        rb.transform.rotation = Quaternion.Lerp(startRot, endRot, Time.deltaTime * rotationSpeed);
                                    }

                                    break;
                                case (RotationBehaviour.PendulamMotion):
                                    rb.transform.rotation = Quaternion.Lerp(endRot, startRot, ll);
                                    break;
                            }

                            break;


                        case (TypeOfMotion.LinearAndRotation):
                            rb.transform.position = Vector3.Lerp(endPosition, startposition, ll);
                            switch (rotationMotionType)
                            {
                                case (RotationBehaviour.ContinuosRotation):
                                    startRot = startingRotation.rotation;
                                    endRot = endingsRotation.rotation;
                                    if (Time.deltaTime > 0)
                                    {
                                        rb.transform.rotation = Quaternion.Lerp(startRot, endRot, Time.deltaTime * rotationSpeed);
                                    }

                                    break;
                                case (RotationBehaviour.PendulamMotion):
                                    rb.transform.rotation = Quaternion.Lerp(endRot, startRot, ll);
                                    break;
                            }

                            break;
                    }

                        yield return new WaitForEndOfFrame();


                }
            }

            yield return new WaitForSeconds(delay);
        }

        yield return null;
    }

    private void OnDisable()
    {
        StopCoroutine(main);
    }


    public void Restart()
    {
        if (amready == true)
        {
            StopCoroutine(main);
            this.gameObject.transform.position = startposition;
            isLooping = true;
            amready = false;
        }

    }

    private void OnDestroy()
    {
        if (main != null)
        {
            StopCoroutine(main);
        }
        else
        {
            return;
        }
    }





}
