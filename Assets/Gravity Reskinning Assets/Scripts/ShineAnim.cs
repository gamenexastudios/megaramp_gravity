﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShineAnim : MonoBehaviour
{
    public GameObject ShineLine;
    public Transform ShineLineStartPos;


    // Start is called before the first frame update
    void Start()
    {
        ShineLineStartPos = ShineLine.transform;
        Animate();

    }

    void Update()
{
    if(Input.GetKeyDown("space"))
    {
    }
}
    public void Animate()
    {
        //iTween.MoveTo(ShineLine, iTween.Hash("X", 150, "time", 2.5f, "easetype", iTween.EaseType.easeOutCirc));
        iTween.MoveTo(ShineLine, iTween.Hash("X", 1600, "time", 2.5f, "easetype", iTween.EaseType.easeOutCirc, "oncomplete", "OncompleteHandler",
            "oncompletetarget", gameObject));
    }
    void OncompleteHandler()
    {
        ShineLine.transform.position = new Vector3(ShineLineStartPos.transform.position.x -800, ShineLineStartPos.transform.position.y, ShineLineStartPos.transform.position.z);
        Animate();
    }
}
