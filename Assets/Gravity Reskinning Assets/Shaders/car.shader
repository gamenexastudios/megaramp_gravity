﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Rajeshnalla/Car"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	 _BaseColor("BaseColor",COLOR) = (1,1,1,1)


		_GlowTex("GlowTexture", 2D) = "white" {}
		_Glow("GlowAmount",Float) = 2
	   _Color("GlowColor",COLOR) = (1,1,1,1)

	  _Cube("Reflection Map", CUBE) = "" {}
	  _reflectionAmount("Reflection amount", Range(0.0,1.0)) = 0.3

		   _Reflected("reflectColor",COLOR) = (1,1,1,1)

	}
		SubShader
	  {
		  Tags { "RenderType" = "Opaque"  "LightMode" = "ForwardBase" }
		  LOD 100

		  Pass
		  {
			  CGPROGRAM
			  #pragma vertex vert
			  #pragma fragment frag
			  // make fog work
			  #pragma multi_compile_fog

			  #include "UnityCG.cginc"
			  #include "Lighting.cginc"

			  // compile shader into multiple variants, with and without shadows
			  // (we don't care about any lightmaps yet, so skip these variants)
			  #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			  // shadow helper functions and macros
			  #include "AutoLight.cginc"

			  struct appdata
			  {
				  fixed4 vertex : POSITION;
				  fixed2 uv : TEXCOORD0;
				  fixed3 normal : NORMAL;

			  };

			  struct v2f
			  {
				  UNITY_FOG_COORDS(1)
				  fixed2 uv : TEXCOORD0;
				  fixed4 vertex : SV_POSITION;
				  fixed3 normal : TEXCOORD2;
				  fixed3 normalDir : TEXCOORD3;
				  fixed3 viewDir : TEXCOORD4;
				  fixed3 reflectedDir : TEXCOORD5;
				  SHADOW_COORDS(6) // put shadows data into TEXCOORD1

			  };

			  sampler2D _MainTex;
			  sampler2D _GlowTex;

			  fixed4 _MainTex_ST;


			  fixed _Glow;
			  fixed4 _Color;
			  fixed4 _BaseColor;

			  samplerCUBE _Cube;
			  uniform fixed _reflectionAmount;
			  fixed4 _Reflected;

			  v2f vert(appdata v)
			  {
				  v2f o;
				  o.uv = v.uv;
				  fixed4x4 modelMatrix = unity_ObjectToWorld;
				  fixed4x4 modelMatrixInverse = unity_WorldToObject;
				  // multiplication with unity_Scale.w is unnecessary
				  // because we normalize transformed vectors
				  o.viewDir = mul(modelMatrix, v.vertex).xyz
					  - _WorldSpaceCameraPos;
				  o.normalDir = normalize(
					  mul(fixed4(v.normal, 0.0), modelMatrixInverse).xyz);
				  o.reflectedDir =
					  reflect(o.viewDir, normalize(o.normalDir));

				  o.vertex = UnityObjectToClipPos(v.vertex);
				  o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				  UNITY_TRANSFER_FOG(o, o.vertex);
				  o.normal = v.normal;
				  // compute shadows data
				  TRANSFER_SHADOW(o)


				  return o;
			  }

			  fixed4 frag(v2f i) : SV_Target
			  {

				 fixed shadow = SHADOW_ATTENUATION(i);
			  // sample the texture
			  fixed4 col = tex2D(_MainTex, i.uv);
			  fixed4 GlowCol = tex2D(_GlowTex, i.uv);
			  fixed4 finalCol = ((col + _BaseColor) + ((GlowCol)*_Glow * _Color));
			  UNITY_APPLY_FOG(i.fogCoord, finalCol);
			  fixed4 hh = lerp(finalCol, texCUBE(_Cube,i.reflectedDir), _reflectionAmount);

			  return (hh * (_Reflected * 2)) * shadow;
		  }
		  ENDCG
	  }

		  // pull in shadow caster from VertexLit built-in shader
		  UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	  }
}
