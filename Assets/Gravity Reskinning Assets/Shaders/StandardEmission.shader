﻿Shader "Rajeshnalla/StandardEmission"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
       _Emission("EmissionTexture", 2D) = "white" {}
        _EmissionColor("Emission Color", Color) = (1, 1, 1, .5)
    }
    SubShader
    {
            Tags { "RenderType" = "Opaque" }
            LOD 100


        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile_fog

             #include "UnityCG.cginc"

            struct appdata
            {
                 fixed4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
            };

            struct v2f
            {
                fixed2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                fixed4 vertex : SV_POSITION;
            };

            sampler2D_half _MainTex;
            fixed4 _MainTex_ST;
            sampler2D_half  _Emission;
            fixed4  _Emission_ST;
            fixed4   _EmissionColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 colEmission = tex2D(_Emission, i.uv);
                fixed4 kk =  col* 5 * _EmissionColor* colEmission;
                UNITY_APPLY_FOG(i.fogCoord, kk);
                return kk;
            }
            ENDCG
        }
          
    } FallBack "Diffuse"
}
