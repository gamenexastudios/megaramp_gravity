﻿
Shader "Rajeshnalla/VerticalFog"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	   _IntersectionThresholdMax("Intersection Threshold Max", float) = 1
	}
		SubShader
	{
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent"  }
		LOD 100
		Pass
		{
		   Blend SrcAlpha OneMinusSrcAlpha
		   ZWrite Off
		   CGPROGRAM
		   #pragma vertex vert
		   #pragma fragment frag
		   #pragma multi_compile_fog
		   #include "UnityCG.cginc"

		   struct appdata
		   {
			  float2 uv : TEXCOORD4;
			  float4 vertex : POSITION;
		   };

		   struct v2f
		   {
			   float2 uv : TEXCOORD4;
			   float4 scrPos : TEXCOORD0;
			   UNITY_FOG_COORDS(1)
				   float4 vertex : SV_POSITION;
		   };

		   sampler2D _CameraDepthTexture;

		   float4 _IntersectionColor;
		   float _IntersectionThresholdMax;
		   sampler2D _MainTex;
		   float4 _MainTex_ST;


		   v2f vert(appdata v)
		   {
			   v2f o;
			   o.vertex = UnityObjectToClipPos(v.vertex);
			   o.scrPos = ComputeScreenPos(o.vertex);
			   o.uv = TRANSFORM_TEX(v.uv, _MainTex);
			   UNITY_TRANSFER_FOG(o,o.vertex);
			   return o;
		   }


		   float4 frag(v2f i) : SV_TARGET
			{ 
				
				float4 col = tex2D(_MainTex, i.uv);
				float depth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)));
			
				float diff = saturate(_IntersectionThresholdMax * (depth- i.scrPos.w));

			    col = lerp(float4(col.rgb, 0.0), col, diff * diff * diff * (diff * (6 * diff - 15) + 10));

			   UNITY_APPLY_FOG(i.fogCoord, col);
			   return col;
			}

			ENDCG
		}
	}FallBack "Diffuse"
}