﻿Shader "Rajeshnalla/Blink"
{
    Properties
    {
		
	   _Color("GlowColor",COLOR) = (1,1,1,1)
		_Glow("GlowAmount",Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			struct appdata
			{
				fixed4 vertex : POSITION;
		

			
			};

			struct v2f
			{
	
			

				fixed4 vertex : SV_POSITION;
				fixed sinvalue : TEXCOORD1;
			};

			fixed4 _Color;
			fixed _Glow;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.sinvalue = (_Glow + (sin(_Time.y * 6) + 1));
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{

				fixed4 col = _Color + i.sinvalue;

                return col;
            }
            ENDCG
        }
    }
}
