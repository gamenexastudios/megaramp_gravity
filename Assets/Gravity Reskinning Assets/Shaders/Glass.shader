﻿Shader "Rajeshnalla/Glass"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
	   _Cutoff("Cutoff",FLOAT)=2

	   _Color("GlowColor",COLOR) = (1,1,1,1)
		_Glow("GlowAmount",Float) = 2
    }
    SubShader
    {
		Lighting Off
		 ZWrite Off
		 Cull Back
		 Blend SrcAlpha OneMinusSrcAlpha
		Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
    
		Cull off
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                fixed4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
            };

            struct v2f
            {
                fixed2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                fixed4 vertex : SV_POSITION;
                fixed4 colorGlow : TEXCOORD2;
            };

            sampler2D_half _MainTex;
            fixed4 _MainTex_ST;
			fixed _Cutoff;

			fixed4 _Color;
			fixed _Glow;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.colorGlow = _Color * _Glow;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
				col.a = _Cutoff;
                return col*i.colorGlow;
            }
            ENDCG
        }
    }
}
