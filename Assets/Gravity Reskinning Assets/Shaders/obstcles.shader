﻿Shader "Rajeshnalla/obstacles"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	   _Gradient("Texture", 2D) = "white" {}
	   _GradAmount("GradientAmount",FLOAT) = 2
	   _Glow("Glow",FLOAT) = 1
	}
		SubShader
	   {
		   Tags { "RenderType" = "Opaque" }
		   LOD 100



		   Pass
		   {
			   CGPROGRAM
			   #pragma vertex vert
			   #pragma fragment frag
			   // make fog work
			   #pragma multi_compile_fog

			   #include "UnityCG.cginc"

			   struct appdata
			   {
				   fixed4 vertex : POSITION;
				   fixed2 uv : TEXCOORD0;
				   fixed3 normal : NORMAL;
			   };

			   struct v2f
			   {
				   fixed2 uv : TEXCOORD0;
				   UNITY_FOG_COORDS(1)
				   fixed4 vertex : SV_POSITION;
				   fixed3 normal : TEXCOORD2;
				   fixed lightFalloff : TEXCOORD3;
			   };

			   sampler2D _MainTex;
			   sampler2D _Gradient;
			   fixed4 _MainTex_ST;
			   fixed4 _Gradient_ST;
			   fixed _Glow;
			   fixed _GradAmount;

			   v2f vert(appdata v)
			   {
				   v2f o;
				   o.vertex = UnityObjectToClipPos(v.vertex);
				   o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				   UNITY_TRANSFER_FOG(o, o.vertex);

				   o.normal = v.normal;

				   fixed3 lightDir = normalize(fixed3(0, 1, 0));
					o.lightFalloff = max(0, dot(lightDir, o.normal));
				   return o;
			   }

			   fixed4 frag(v2f i) : SV_Target
			   {

				   fixed4 grad = tex2D(_Gradient, i.uv);
				   fixed4 col = (tex2D(_MainTex, i.uv) * i.lightFalloff * _Glow) + (grad / _GradAmount);

				   UNITY_APPLY_FOG(i.fogCoord, col);


				   return col;
			   }
			   ENDCG
		   }

		   // pull in shadow caster from VertexLit built-in shader
		   UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"


	   }


}
