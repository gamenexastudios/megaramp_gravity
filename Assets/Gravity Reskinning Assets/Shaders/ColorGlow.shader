﻿Shader "Rajeshnalla/ColorGlow"
{
    Properties
    {
		   _Color("GlowColor",COLOR) = (1,1,1,1)
		_Glow("GlowAmount",Float) = 2
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode" = "ForwardBase"  }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag



			#include "UnityCG.cginc"


            struct appdata
            {
                fixed4 vertex : POSITION;
                fixed sine : TEXCOORD1;
                fixed4 colors : TEXCOORD2;
       
            };

            struct v2f
            {
			
                fixed4 vertex : SV_POSITION;
                fixed sine : TEXCOORD1;
                fixed4 colors : TEXCOORD2;
            };

			fixed4 _Color;
			fixed _Glow;
        

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                v.sine = (1.5 + sin(_Time.x * 100)) * _Glow;
                o.colors = _Color * v.sine;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return i.colors;
            }
            ENDCG

        }

    }
}
