﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Rajeshnalla/Road"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
	    _MainColor("BaseColor",COLOR) = (1,1,1,1)
		 _Exposure("Exposure",Float) = 1

     }
    SubShader
    {
        Tags { "RenderType"="Opaque"  "LightMode" = "ForwardBase" }
        LOD 100
	
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
			#include "Lighting.cginc"

			// compile shader into multiple variants, with and without shadows
			// (we don't care about any lightmaps yet, so skip these variants)
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			// shadow helper functions and macros
			#include "AutoLight.cginc"
            #include "UnityLightingCommon.cginc" // for _LightColor0
            struct appdata
            {
                fixed4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
                fixed3 normal : NORMAL;

            };

            struct v2f
            {
                fixed4 diff : COLOR0;
                LIGHTING_COORDS(0, 1)
				UNITY_FOG_COORDS(1)
                fixed2 uv : TEXCOORD0;
                fixed4 vertex : SV_POSITION;
				//SHADOW_COORDS(2) // put shadows data into TEXCOORD1
	
            };

            sampler2D _MainTex;
		

	
        
			fixed _Exposure;
	
			fixed4 _Color;
			fixed4 _MainColor;



            v2f vert (appdata v)
            {
                v2f o;
			    o.uv = v.uv;
                o.vertex = UnityObjectToClipPos(v.vertex);  

                // get vertex normal in world space
                half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                // dot product between normal and light direction for
                // standard diffuse (Lambert) lighting
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                // factor in the light color
                o.diff = nl * _LightColor0;
				UNITY_TRANSFER_FOG(o, o.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
			    fixed shadow = LIGHT_ATTENUATION(i);
                fixed4 col = tex2D(_MainTex, i.uv);	
				UNITY_APPLY_FOG(i.fogCoord, col);    
				return (col* _MainColor* _Exposure* i.diff *clamp(shadow,0.6,1));
            }
            ENDCG
        }
       
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }  FallBack "Diffuse"
           
}
