﻿
Shader "Rajeshnalla/LightmapSupport"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
       _Color("Color",COLOR) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        Pass
        {   Lighting Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                fixed4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
                fixed2 uv1 : TEXCOORD2;
            };

            struct v2f
            {
                fixed2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                fixed4 vertex : SV_POSITION;       
                fixed2 texcoord1 : TEXCOORD2;
            };

        

            sampler2D_half _MainTex;
            fixed4 _MainTex_ST;

            fixed4 _Color;

            v2f vert (appdata i)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(i.vertex);
                o.uv = TRANSFORM_TEX(i.uv, _MainTex);
                o.texcoord1 = i.uv1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                UNITY_TRANSFER_FOG(o, o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
 
                fixed4 main_color = tex2D(_MainTex, i.uv);
                main_color.rgb *= DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.texcoord1));
                main_color *= _Color;
                UNITY_APPLY_FOG(i.fogCoord, main_color);
                return main_color; 
            }
            ENDCG
        }
    }FallBack "Diffuse"
}
