﻿using Unity;
using System;


[Serializable]
public class RemotePropertyData
{
    public string WinCounter;
    public string LoseCounter;
    public string BackButtonAd;
    public string ExtraTwoX;
}


[Serializable]
public class SpecialIAPVariables
{
    public bool Active;
    public string PackType;
    public string ProductID;
    public string PackName;
}
