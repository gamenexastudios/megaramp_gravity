﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NativeShareHandler : MonoBehaviour
{
    public static NativeShareHandler Instance;
    private void Awake()
    {
        Instance = this;
    }
    private string appUrl = "";
    public void NativeShareLink()
    {
        string subjectString = "All New Exciting Game";
#if UNITY_ANDROID
        appUrl = "https://play.google.com/store/apps/details?id=" + Application.identifier;
#elif UNITY_IOS
                        appUrl = "itms-apps://itunes.apple.com/app/id" + "1473255832" + "?mt=8";
#endif


            new NativeShare().SetTitle(Application.productName).SetText(subjectString + "\n" + appUrl).Share();
        if (FirebaseController.Instance != null)
            FirebaseController.Instance.LogFirebaseEvent("NativeShare", "Success", "");

    }
}
